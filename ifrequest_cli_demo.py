import modserv.modclient2 as modclient
from modclient2.interface.ifrequest_client import IfRequestClientInterface
from modclient2.interface.primitive_cli import PrimitiveCliInterface
from modserv.contextStore import ContextStore

client = modclient.getModclient(debug = True)

class IfrequestRequestContext:
	def __init__(self,iftype):
		self.arguments = []
		self.iftype = iftype

class Invokecall:
	def __init__(self,function,*arguments):
		self.arguments = arguments
		self.function = function
	
	def __call__(self,*x):
		self.function(*self.arguments)

class ListReturn:
	def __init__(self,callback):
		self.callback = callback
		
	def __call__(self,callcontext,types):
		out = ""
		for t in types:
			out = out+t+"\n"
		self.callback(out[:-1])

class RequestOnSuccess:
	def __init__(self,callback):
		self.callback = callback
	
	def __call__(self,callcontext,moduleId,interfaceId):
		self.callback("Your requested interface is at module "+str(moduleId)+" interface "+str(interfaceId))
		
class RequestOnInvalidArgument:
	def __init__(self,callback):
		self.callback = callback
	
	def __call__(self,callcontext,key,value):
		self.callback("The following argument was not accepted "+str(key)+"="+str(value))
		
#maps a connectionId to a CommandContext if it is currently waiting for context commands
connectionCommands = {}

@client.start
def start():
	
	def onMessage(context,message):
		connectionId = context.connectionId
		if connectionId in connectionCommands:
			cc = connectionCommands[connectionId]
			if type(cc) is IfrequestRequestContext:
				if message == "":
					del connectionCommands[connectionId]
					ifrequestInterface.request(cc.iftype,cc.arguments,
						onSuccess=RequestOnSuccess(context.call),
						onTypeNotSupported=Invokecall(context.call,"Interface type "+str(cc.iftype)+" is not supported"),
						onInvalidArgument=RequestOnInvalidArgument(context.call),
						onCannotSupply=Invokecall(context.call,"Cannot supply this interface for an unknown reason"),
						onNotConnected=Invokecall(context.call,"Not connected!"))
				elif message == "@cancel":
					del connectionCommands[connectionId]
					context.call("Canceled.")
				else:
					kvp = message.split("=")
					if len(kvp) == 1:
						kvp.append("")
					connectionCommands[connectionId].arguments.append(kvp[:2])
		else:
			i = message.split(" ",2)
			command = "help"
			arg = ""
			if len(i) == 2:
				command = i[0]
				arg = i[1]
				if not command in ["request"]:
					command = "help"
			elif len(i) == 1:
				command = i[0]
				if not command in ["list"]:
					command = "help"
			if command == "request":
				connectionCommands[connectionId] = IfrequestRequestContext(arg)
				context.call("Please enter the arguments in the following format: <key>[=<value>]\nType in an empty line to send request\nType @cancel to cancel")
			elif command == "list":
				ifrequestInterface.list(
					onSuccess=ListReturn(context.call),
					onNotConnected=Invokecall(context.call,"Not connected!"))
			else:
				context.call("Usage:\nrequest <type>\nlist")
				
	def onCliDisconect(connectionId):
		if connectionId in connectionCommands:
			del connectionCommands[connectionId]
	
	cliInterface = PrimitiveCliInterface(onMessage=onMessage,onConnectionClosed=onCliDisconect).open(client,3)
	ifrequestInterface = IfRequestClientInterface().open(client,12)
