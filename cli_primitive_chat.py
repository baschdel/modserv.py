import modserv.modclient2 as modclient
from modclient2.interface.primitive_cli import PrimitiveCliInterface

#reqest modclient instance
client = modclient.getModclient()

#keep a list of all open connections
openCliConnections = []

#add the connections when something connects
def onConnect(connectionId):
	openCliConnections.append(connectionId)
	
#remove the connections when something disconnects
def onDisconnect(connectionId):
	openCliConnections.remove(connectionId)
	
def onMessage(context,message):
	#copy the list of receivers and remove the connection the message came from
	receivers = list(openCliConnections)
	receivers.remove(context.connectionId)
	#send the message
	cliInterface.send(receivers,message)
		
@client.start
def start():
	global cliInterface
	#mae the interface and register the callback functions
	cliInterface = PrimitiveCliInterface(
		onMessage=onMessage,
		onConnectionClosed=onDisconnect,
		onConnectionOpened=onConnect)
	#open the interface as interface #3 without a connection limit
	cliInterface.open(client,3,maxconnections=-1)
