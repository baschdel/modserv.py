#wathever -> binary
from modserv.exceptions import FormatError
import struct

def bin(bin):
	return bin

def utf8(text):
	try:
		return text.encode('utf-8')
	except:
		raise FormatError("utf8 encoder")
		
def u64(int):
	try:
		return struct.pack(">Q",int)
	except:
		raise FormatError("u64 encoder")
		
def u32(int):
	try:
		return struct.pack(">I",int)
	except:
		raise FormatError("u32 encoder")
		
def u16(int):
	try:
		return struct.pack(">H",int)
	except:
		raise FormatError("u16 encoder")
		
def u8(int):
	try:
		return struct.pack(">B",int)
	except:
		raise FormatError("u8 encoder")
		
def s64(int):
	try:
		return struct.pack(">q",int)
	except:
		raise FormatError("s64 encoder")
		
def s32(int):
	try:
		return struct.pack(">i",int)
	except:
		raise FormatError("s32 encoder")
		
def s16(int):
	try:
		return struct.pack(">h",int)
	except:
		raise FormatError("s16 encoder")
		
def s8(int):
	try:
		return struct.pack(">b",int)
	except:
		raise FormatError("s8 encoder")

def bool(boolean):
	if boolean:
		return b'\x01'
	else:
		return b'\x00'
		
#dynamic sizes list
class dslist:
	def __init__(self,encoder):
		self.encoder = encoder
	def __call__(self,list):
		try:
			binary = b''
			i=0
			while i<len(list):
				entry = None
				entry = self.encoder(list[i])
				binary = binary+struct.pack(">I",len(entry))+entry
				i=i+1
			return binary
		except FormatError:
			raise
		except:
			raise FormatError("dslist encoder ["+str(self.encoder)+"]")
		
class dsentry:
	def __init__(self,keyencoder,valueencoder):
		self.keyencoder = keyencoder
		self.valueencoder = valueencoder
	def __call__(self,tuple):
		try:
			key = self.keyencoder(tuple[0])
			value = self.valueencoder(tuple[1])
			keylen = struct.pack(">I",len(key))
			return keylen+key+value
		except:
			raise FormatError("dsentry encoder ["+str(self.keyencoder)+","+str(self.valueencoder)+"]")
		
class dsmap:
	def __init__(self,keyencoder,valueencoder):
		self.keyencoder = keyencoder
		self.valueencoder = valueencoder
	def __call__(self,map):
		binary = b''
		try:
			for key,value in map.items():
				key = self.keyencoder(key)
				value = self.valueencoder(value)
				keylen = struct.pack(">I",len(key))
				entry = keylen+key+value
				entrylen = len = struct.pack(">I",len(entry))
				binary = binary+entrylen+entry
		except:
			raise FormatError("dsmap encoder ["+str(self.keyencoder)+","+str(self.valueencoder)+"]")
		
#fixed value length list
class flvlist:
	def __init__(self,encoder,length):
		self.length = length
		self.encoder = encoder
		
	def __call__(self,list):
		binary = b''
		i=0
		while i<len(list):
			entry = encoder(list[i])
			if len(entry)>self.length:
				raise FormatError("flvlist encoder length not matching on entry "+str(i)+" ("+str(self.length)+")["+str(self.encoder)+"]")
			binary = binary+entry
			i=i+1
		return binary
		
