class Eventbus:
	def __init__(self):
		self.eventListeners = {}
		self.fire = self._fireEvent
		
	def _fireEvent(self,event,*args,**context):
		if not (event in self.eventListeners):
			return
		for callbackHandler in self.eventListeners[event]:
			callbackHandler(*args,**context)
			
	def on(self,event,callback):
		if not callable(callback):
			return
		if not event in self.eventListeners:
			self.eventListeners[event] = []
		self.eventListeners[event].append(callback)
		return True
		
	def ignore(self,event,callback):
		if not event in self.eventListeners:
			return
		if not callback in self.eventListeners[event]:
			return
		self.eventListeners[event].remove(callback)
		if len(self.eventListeners[event]) == 0:
			del self.eventListeners[event]
	
	#bulk hook (useful for listener classes)
	#(takes a dict that maps events to callback functions or arrays of callback functions)
	def don(self,emap):
		for event,callback in emap:
			if type(callback) is list:
				for cb in callback:
					self.on(event,cb)
			else:
				self.on(event,callback)
	
	#bulk ignore (useful for listener classes)
	#(takes a dict that maps events to callback functions or arrays of callback functions)
	def dignore(self,emap):
		for event,callback in emap:
			if type(callback) is list:
				for cb in callback:
					self.ignore(event,cb)
			else:
				self.ignore(event,callback)
				
class EventCallback:
	def __init__(self,eventbus,event,**context):
		self.eventbus = eventbus
		self.event = event
		self.context = context
		
	def __call__(self,*arguments,**context):
		self.eventbus._fireEvent(self.event,*arguments,{**context,**self.context})
