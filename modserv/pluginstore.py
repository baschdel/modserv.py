import importlib

class Pluginstore:
	def __init__(self,base):
		self.base = base
		self.cache = {}
		
	def require(self,plugin):
		if plugin in self.cache:
			return self.cache[plugin]
		else:
			loaded_plugin = importlib.import_module(self.base+plugin)
			self.cache[plugin] = loaded_plugin
			return loaded_plugin
