class CrashAndBurnException(Exception):
	pass #If this is raised, something went very wrong
	#possible causes:
	#		The server didn't respect the protocol
	#		You did something funny with the internal state of the some classes
	
class FormatError(Exception):
	pass
	
class AlreadyAttachedExeption(Exception):
	pass
