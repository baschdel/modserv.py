#trys to open an interface in the specified range of ids
#returns the id on success, None on Failure

def open(client,interface,start,end,**options):
	idcounter = start
	while idcounter < end:
		if client.openInterface(idcounter,interface,**options):
			return idcounter
		else:
			idcounter += 1
	return None
