import struct
from modserv.exceptions import FormatError

def haskey(thing,key):
	if type(thing) is dict:
		return key in thing
	elif ((type(thing) is list) or (type(thing) is tuple)) and (type(key) is int):
		return (key < len(thing)) and (-key < len(thing))
	else:
		return False
		
# functions [argumentId]
class Transcoder:
	def __init__(self,functions):
		self.functions = functions
		
	def __call__(self,arguments):
		i = 0
		maxi = len(arguments)
		if maxi > len(self.functions):
			maxi = len(self.functions)
		out = []
		try:
			while i < maxi:
				out.append(self.functions[i](arguments[i]))
				i=i+1
		except FormatError:
			pass
		return out

class Encoder(Transcoder):
	pass
	
class Decoder(Transcoder):
	pass

# functions [argumentId]{[mode]}
class ModeDecoder:
	def __init__(self,functions):
		self.functions = functions
		
	def __call__(self,arguments):
		i = 0
		maxi = len(arguments)
		if maxi > len(self.functions):
			maxi = len(self.functions)
		out = []
		mode = 0
		try:
			while i < maxi:
				if callable(self.functions[i]):
					#decode
					out.append(self.functions[i](arguments[i]))
					if i == 0:
						mode = out[0]
				elif haskey(self.functions[i],mode):
					if callable(self.functions[i][mode]):
					#use alternative decoder
						out.append(self.functions[i][mode](arguments[i]))
				i=i+1
		except FormatError:
			pass
		return out
		
# functions [argumentId]{[mode]}
class ModeEncoder:
	def __init__(self,functions):
		self.functions = functions
		
	def __call__(self,arguments):
		i = 0
		maxi = len(arguments)
		if maxi > len(self.functions):
			maxi = len(self.functions)
		out = []
		mode = 0
		try:
			while i < maxi:
				if callable(self.functions[i]):
					#decode
					out.append(self.functions[i](arguments[i]))
					if i == 0:
						mode = arguments[0]
				elif haskey(self.functions[i],mode):
					if callable(self.functions[i][mode]):
					#use alternative decoder
						out.append(self.functions[i][mode](arguments[i]))
				i=i+1
		except FormatError:
			pass
		return out
		
#useful for functions that don't return anything (not very useful)
class BlankCoder():
	def __call__(self,arguments):
		return []
