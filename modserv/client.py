from modserv.packetsocket import PacketSocket,DummyPacketHandler
from modserv.netadapterClient import ClientPacketHandler,ClientCommandPacker
from modserv.typeenc import DSList,DValue
import socket
import struct
import sys
import time

class ClientHandshakeHandler:
	def __init__(self,callbackHandler,packetHandler,clientCommandPacker):
		self.callbackHandler = callbackHandler
		self.packetHandler = packetHandler
		self.clientCommandPacker = clientCommandPacker
		
	def onPacket(self,binary,packetsocket):
		#print("ClientHandshakeHandler:onPacket",binary,self.packetHandler)
		if binary == b"OK":
			packetsocket.setPackethandler(self.packetHandler)
			self.callbackHandler.initalize()
			self.clientCommandPacker.statusInitalized()
		else:
			packetsocket.close()

	def onClose(self,packetsocket):
		self.callbackHandler.authFailed()

def connect(address,port,moduleId,moduleKey,callbackHandler,debug=False):
	s = socket.socket()
	s.connect((address,port))
	packetsocket = PacketSocket(s, DummyPacketHandler())
	ccp = ClientCommandPacker(packetsocket)
	if type(callbackHandler) is type:
		callbackHandler = callbackHandler(ccp)
	cph = ClientPacketHandler(callbackHandler,debug=debug)
	cphh = ClientHandshakeHandler(callbackHandler,cph,ccp)
	packetsocket.setPackethandler(cphh)
	packetsocket.start()
	bmid = struct.pack(">Q",moduleId)
	bkey = moduleKey.encode('utf-8')
	packetsocket.send(DSList().setList([bmid,bkey]).toBinary())
	return ccp
	
#easy wrapper that does all the server connecting stuffs for you
	
def getCredsFromArgs(args=sys.argv):
	# sys.aegv[0] is the filepath from the pwd to the script
	if len(args) <= 2:
		print("Usage: <moduleId> <moduleKey> [<port>] [<serverAddress>]")
		print("Default Port: 8383")
		print("Default Server Address: 127.0.0.1 (aka. localhost)")
		sys.exit(1)
	
	moduleId = 0
	moduleKey = ""
	port = 8383
	server = "127.0.0.1"
	
	try:
		moduleId = int(args[1])
		if moduleId < 0:
			print("Usage: <moduleId> <moduleKey> [<port>] [<serverAddress>]")
			print("moduleId has to be a positive Integer!")
			sys.exit(1)
	except Error:
		print("Usage: <moduleId> <moduleKey> [<port>] [<serverAddress>]")
		print("moduleId has to be a positive Integer!")
		sys.exit(1)
		
	moduleKey = args[2]
	
	if len(args) > 3:
		try:
			port = int(args[3])
			if port < 0:
				print("Usage: <moduleId> <moduleKey> [<port>] [<serverAddress>]")
				print("port has to be a positive Integer!")
				sys.exit(1)
		except Error:
			print("Usage: <moduleId> <moduleKey> [<port>] [<serverAddress>]")
			print("port has to be a positive Integer!")
			sys.exit(1)
			
	if len(args) > 4:
		server = args[4]
	
	return moduleId,moduleKey,port,server
	
clientCommandPacker = None
	
def start(callbackHandlerClass):
	
	moduleId,moduleKey,port,server = getCredsFromArgs()
	
	ccp = connect(server,port,moduleId,moduleKey,callbackHandlerClass)
	global clientCommandPacker
	clientCommandPacker = ccp
	return ccp
	
def running():
	if clientCommandPacker.packetsocket.hasClosed:
		return False
	return True
	
def waitForEnd():
	while clientCommandPacker is None:
		time.sleep(0.5)
	clientCommandPacker.packetsocket.join()
	
#for a dummy callback Handler use the one in module.py
