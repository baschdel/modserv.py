#binary -> wathever
from modserv.exceptions import FormatError
import struct

def bin(bin):
	return bin

def utf8(bin):
	try:
		return bin.decode('utf-8')
	except:
		raise FormatError("utf8 decoder")
		
def u64(bin):
	try:
		return struct.unpack(">Q",bin)[0]
	except:
		raise FormatError("u64 decoder")
		
def u32(bin):
	try:
		return struct.unpack(">I",bin)[0]
	except:
		raise FormatError("u32 decoder")
		
def u16(bin):
	try:
		return struct.unpack(">H",bin)[0]
	except:
		raise FormatError("u16 decoder")
		
def u8(bin):
	try:
		return struct.unpack(">B",bin)[0]
	except:
		raise FormatError("u8 decoder")
		
def s64(bin):
	try:
		return struct.unpack(">q",bin)[0]
	except:
		raise FormatError("s64 decoder")
		
def s32(bin):
	try:
		return struct.unpack(">i",bin)[0]
	except:
		raise FormatError("s32 decoder")
		
def s16(bin):
	try:
		return struct.unpack(">h",bin)[0]
	except:
		raise FormatError("s16 decoder")
		
def s8(bin):
	try:
		return struct.unpack(">b",bin)[0]
	except:
		raise FormatError("s8 decoder")
		
def bool(bin):
	if bin == b'\0'*len(bin):
		return False
	else:
		return True
		
#dynamic sizes list
class dslist:
	def __init__(self,decoder):
		self.decoder = decoder
	def __call__(self,binary):
		list = []
		while len(binary) >= 4:
			entrylen = struct.unpack(">I",binary[:4])[0]
			entry = binary[4:4+entrylen]
			list.append(self.decoder(entry))
			binary = binary[4+entrylen:]
		return list
		
#dynamic size key value pair
class dsentry:
	def __init__(self,keydecoder,valuedecoder):
		self.keydecoder = keydecoder
		self.valuedecoder = valuedecoder
	def __call__(self,binary):
		keylen = struct.unpack(">I",binary[:4])[0]
		key = self.keydecoder(binary[4:4+keylen])
		value = self.valuedecoder(binary[4+keylen:])
		return (key,value)
		

#dynamic size map (dsentrys in a dslist)
class dsmap:
	def __init__(self,keydecoder,valuedecoder):
		self.keydecoder = keydecoder
		self.valuedecoder = valuedecoder
	def __call__(self,binary):
		map = {}
		while len(binary) >= 4:
			entrylen = struct.unpack(">I",binary[:4])[0]
			entry = binary[4:4+entrylen]
			keylen = struct.unpack(">I",entry[:4])[0]
			key = self.keydecoder(entry[4:4+keylen])
			value = self.valuedecoder(entry[4+keylen:])
			map[key] = value
			binary = binary[4+entrylen:]
		return map
		
#fixed value length list
class flvlist:
	def __init__(decoder,length):
		self.length = length
		self.decoder = decoder
		
	def __call__(binary):
		list = []
		while len(binary) >= self.length:
			entry = binary[:self.length]
			list.append(decoder(entry))
			binary = binary[self.length:]
		return list
