class Context:
	def __init__(self,callback,**context):
		self.context = context
		self.callback = callback
	
	def __call__(self,*args,**kargs):
		self.callback(*args,**{**kargs,**self.context})
