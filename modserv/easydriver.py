import threading

#TODO: put this in another file
#calls loopfuntion in an endless loop in a seperate thread until it returns false, or the module shuts down
class EasyDriverLooper(threading.Thread):
	def __init__(self,loopfunction,easyclient,autostart=True,daemon=False):
		threading.Thread.__init__(self,daemon=daemon)
		self.loopfunction = loopfunction
		self.easyclient = easyclient
		self.daemon = daemon
		if autostart:
			self.start()
		
	#To make regitring it as a callback easier
	#def __call__(self):
		#self.stop()
		
	def start(self):
		#register selfShutdown listener
		#self.easyclient.on("selfShutdown",self)
		#call Thread.start
		threading.Thread.start(self)
		
	def stop(self):
		#remove selfShutdown listener
		#self.easyclient.ignore("selfShutdown",self)
		#call Thread.stop
		#threading.Thread.stop(self) #FIXME
		pass
		
	def run(self):
		try:
			running = True
			while self.easyclient.running() and running:
				running = self.loopfunction()
			self.easyclient.ignore("selfShutdown",self)
		except:
			self.easyclient.ignore("selfShutdown",self)
			raise
		
