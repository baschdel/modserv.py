import sys
import uuid
import modserv.client as client
from modserv.portsetup import Portsetup
from modserv.exceptions import CrashAndBurnException
from modserv.exceptions import FormatError
from modserv.eventbus import Eventbus
from modserv.pluginstore import Pluginstore

def getModclient(debug=False):
	moduleId,moduleKey,port,server = client.getCredsFromArgs()
	#print([moduleId,moduleKey,port,server])
	return Modclient(moduleId,moduleKey,port,server,debug=debug)

#gets passed to every call receiver as the first argument		
class CallContext:
	def __init__(self,client,interfaceId,connectionId,portId,functionId,cookie=None,mode=None):
		self.client = client
		self.interfaceId = interfaceId
		self.connectionId = connectionId
		self.portId = portId
		self.functionId = functionId
		self.cookie = cookie
		self.mode = mode
		
	#callback with cookie on the same interface,port,connection,function the call came in on
	def callback(self,*args,**options):
		#print("[DEBUG-TEMP]CallContext.callback",args,options,self.mode,self.cookie)
		if (self.cookie is None):
			raise CrashAndBurnException("CallContext callback without a cookie")
		else:
			self.client.call(self.interfaceId,self.connectionId,self.portId,self.functionId,*args,cookie=self.cookie,**options)
			
	#just call the same interface,port,connection,function without a cookie
	#but you may also specify a custom connectionId (intented for broadcasting)
	#or a custom functionId to just call a different function on the same port
	def call(self,*args,functionId=None,connectionId=None,**options):
		if functionId is None:
			functionId = self.functionId
		if connectionId is None:
			connectionId = self.connectionId
		self.client.call(self.interfaceId,connectionId,self.portId,functionId,*args,**options)

#This class is inteded to be a helper class for ort drivers to expand a callcontext object with moded callbacks
class ModeCallback:
	def __init__(self,callcontext,mode):
		self.callcontext = callcontext
		self.mode = mode
	
	def __call__(self,*args,**options):
		self.callcontext.callback(self.mode,*args,**options)

#initalizes an object of class clazz with the arguments give additional to clazz
#and calls the object, as if it was a function
#the object itself is callable
class ChainStarter:
	def __init__(self,clazz,*args,**options):
		self.clazz = clazz
		self.args = args
		self.options = options
		
	def __call__(self,*args,**options):
		instance = clazz(*self.args,**self.options)
		instance(*args,**options)

class Context:
	def __init__(self,callback,**context):
		self.context = context
		self.callback = callback
	
	def __call__(self,*args,**kargs):
		self.callback(*args,**{**kargs,**self.context})

###############################################################################
###############################################################################
###############################################################################
		
class Modclient:
	def __init__(self,moduleId,moduleKey,port,server,debug=False):
		self.debug = debug
		self.commandPacker = client.connect(server,port,moduleId,moduleKey,self,debug=debug)
		self.moduleId = moduleId
		self.eventbus = Eventbus()
		self.openingConnectionData = {} #A table that maps cookies to the data of the connection, to store it until the callback arrives
		self.connected = False
		self.isShutdown = False
		self.cookiehooks = {} #callbacks for everything that uses cookies
		self.cookiecontext = {}
		self.interfaces = {} #maps the interface ids tothe interfaces
		self.openInterfaces = [] #contains the ids of all open interfaces
		self.pluginstore = Pluginstore("modclient2.plugins.")
		self.callbacks = {} #maps interfaceIds to a dict, that maps connnectionIds to a dict, that maps the callbackId to ...
		                    #... a function
		                    #... a list of functions
		                    #... a dict, that maps modes to one of the above
		                    #the nested dicts (interfaceId and connectionId) are there to make the cleanup easier,
		                    #when the connection or interface closes
		self.connections = {} #maps interfaceIds to lists of connectionids
		
	################################
	#### API ######################
	##############################
	
	################
	# EVENTS ######
	
	#register a callback for an event
	def on(self,event,callback):
		self.eventbus.on(event,callback)
		
	#unregister a5205cb4fa70d4c968b715347a2a4a7eb callback for an event
	def ignore(self,event,callback):
		self.eventbus.ignore(event,callback)
		
	#bulk hook (useful for listener classes)
	#(takes a dict that maps events to callback functions or arrays of callback functions)
	def don(self,emap):
		self.eventbus.don(emap)
	
	#bulk ignore (useful for listener classes)
	#(takes a dict that maps events to callback functions or arrays of callback functions)
	def dignore(self,emap):
		self.eventbus.dignore(emap)
	
	############################
	# SERVER INTERACTION ######
	
	def shutdown(self,moduleId=None,onSuccess=None,onFailure=None,onTimeout=None,**context):
		if moduleId is None:
			moduleId=self.moduleId
		cookie = self._generateCookie()
		if not (onSuccess is None and onFailure is None and onTimeout is None):
			self._setCookieCallbackHandlers(cookie,{'retNoErr':onSuccess,'errNoWriteAccs':onFailure,'timeout':onTimeout},**context)
		self.commandPacker.cmdShutdown(cookie,moduleId)
	
	def shutdownTree(self,moduleId=None,onSuccess=None,onFailure=None,onTimeout=None,**context):
		if moduleId is None:
			moduleId=self.moduleId
		cookie = self._generateCookie()
		if not (onSuccess is None and onFailure is None and onTimeout is None):
			self._setCookieCallbackHandlers(cookie,{'retNoErr':onSuccess,'errNoWriteAccs':onFailure,'timeout':onTimeout},**context)
		self.commandPacker.cmdShutdownTree(cookie,moduleId)
		
	def makeCild(self,onSuccess=None,onTimeout=None,**context):
		cookie = self._generateCookie()
		if not (onSuccess is None and onTimeout is None):
			self._setCookieCallbackHandlers(cookie,{'childModCreated':onSuccess,'timeout':onTimeout},**context)
		self.commandPacker.cmdMakeChild(cookie)
		
	def openInterface(self,interfaceId,interfaceObject,maxconnections=None,onAlreadyOpened=None):
		if self.isInterfaceOpen(interfaceId):
			if callable(onAlreadyOpened):
				onAlreadyOpened()
			return False
		ports = interfaceObject._getPorts()
		portsetups = {} #stores portsetups for communication with the netadapter
		for portId,portObject in ports.items():
			#limit connections to what the maximum is, the port can handle
			if (maxconnections is None) or (maxconnections == -1):
				maxconnections = portObject.maxconnections
			elif portObject.maxconnections == -1:
				pass
			elif maxconnections > portObject.maxconnections:
				maxconnections = portObject.maxconnections
			#if the interface specifies a limit on maximum connections, enforce it
			try:
				if interfaceObject.maxconnections < maxconnections and interfaceObject.maxconnections >= 0:
					maxconnections = interfaceObject.maxconnections
			except AttributeError:
				pass
			#load the required encoder and decoder plugins
			inSide = "\0"
			outSide = "\0"
			inVersion = 0
			outVersion = 0
			if not (portObject.inSide is None):
				decoderPlugin = self.pluginstore.require(portObject.protocol+"_"+portObject.inSide+"_decoder")
				inSide = portObject.inSide
				inVersion = decoderPlugin.version
			if not (portObject.outSide is None):
				encoderPlugin = self.pluginstore.require(portObject.protocol+"_"+portObject.outSide+"_encoder")
				outSide = portObject.outSide
				outVersion = encoderPlugin.version
			#create the good old portsetup
			portsetups[portId] = Portsetup(portObject.protocol,inSide,inVersion,outSide,outVersion,isOptional=portObject.isOptional)
		if maxconnections is None:
			if self.debug:
				print("[INFO] An interface was opened without specifing a connection limit (defaulting to no limit)")
			maxconnections = -1
		interfacemetadata = InterfaceMetadata(maxconnections,portsetups)
		self.interfaces[interfaceId] = interfaceObject
		self.commandPacker.cmdOpenInterface(interfaceId,interfacemetadata)
		return True
		
		
	def closeInterface(self,interfaceId):
		if not (interfaceId in self.interfaces):
			return False #don't close interfaces that are already closed
		self.commandPacker.cmdCloseInterface(interfaceId)
		return True #does not mean it was successful but that the interface if not already, will close
	
	def connect(self,moduleIdA,interfaceIdA,moduleIdB,interfaceIdB,onSuccess=None,onFailure=None,onTimeout=None,**context):
		cookie = self._generateCookie()
		self.openingConnectionData[cookie] = {'ma':moduleIdA,'mb':moduleIdB,'ia':interfaceIdA,'ib':interfaceIdB}
		if not (onSuccess is None and onFailure is None and onTimeout is None):
			self._setCookieCallbackHandlers(cookie,{'connOpened':onSuccess,'connOpenErr':onFailure,'timeout':onTimeout},**context)
		self.commandPacker.cmdOpenConnection(cookie,moduleIdA,interfaceIdA,moduleIdB,interfaceIdB)
	
	def call(self,interfaceId,connectionId,portId,functionId,*arguments,callback=None,cookie=None):
		if self.debug:
			print("[DEBUG] modclient.call("+str(interfaceId)+","+str(connectionId)+","+str(portId)+","+str(functionId)+","+str(arguments)+")")
		if interfaceId in self.interfaces and self.isInterfaceOpen(interfaceId):
			interface = self.interfaces[interfaceId]
			#print("[CALL-TEMP]",interface.isOpen,interface.encoders,portId,functionId,arguments)
			ports = interface._getPorts()
			if portId in ports:
				portObject = ports[portId]
				#get "plugin" based on type and side <protocol>_<side>_encoder
				if portObject.outSide is None:
					if self.debug:
						print("[DEBUG][ERROR]You were trying to call out on a port, that does not suppport calling out ("+str(interfaceId)+"."+str(portId)+")")
					return
				encoder_plugin = self.pluginstore.require(portObject.protocol+"_"+portObject.outSide+"_encoder")
				if encoder_plugin is None:
					raise CrashAndBurnException("Missing plugin: "+portObject.protocol+"_"+portObject.outSide+"_encoder")
				if not(cookie is None):
					if encoder_plugin.hasMode(functionId):
						arguments = arguments[:1]+(cookie,)+arguments[1:]
					else:
						arguments = (cookie,)+arguments
					if self.debug:
						print("[DEBUG] arguments after inserting cookie: "+str(arguments))
				if functionId in encoder_plugin.encoders:
					#print("[CALL-TEMP] arguments:",arguments)
					binarguments = encoder_plugin.encoders[functionId](arguments)
					#print("[CALL-TEMP] binarguments:",binarguments)
					#get a list of all connections that should be called
					connections = None
					if connectionId is None: #broadcast to all open connections
						connections = self.connections[interfaceId]
					elif type(connectionId) is list or type(connectionId) is tuple: #broadcast to only a few specified connections
						connections = connectionId
					else: #normal call to one connection
						connections = [connectionId]
					#get cookie if callback should be registred
					cookie = None
					if (not (callback is None)):
						cookie = binarguments[0]
						if encoder_plugin.hasMode(functionId):
							cookie = binarguments[1]
					for connectionId in connections:
						#register callback
						if not (cookie is None):
							if not (connectionId in self.callbacks[interfaceId]):
								if self.debug:
									print("[DEBUG][ERROR]You were trying to call a non open connection ("+str(interfaceId)+"-"+str(connectionId)+")")
							else:
								self.callbacks[interfaceId][connectionId][str(cookie)+"__"+str(portId)+"."+str(functionId)] = callback
						#call (does not relly matter if the connection is open, the server will recogize, that the connection is not open
						#      and responds with an error message, that will be ignored)
						#     (also you shouldn't call non open connections anyway)
						self.commandPacker.cmdCall(interfaceId,connectionId,portId,functionId,binarguments)
				else:
					if self.debug:	
						print("[DEBUG][ERROR]You were trying to call a function, that cannot be encoded ("+str(interfaceId)+"."+str(portId)+"."+str(functionId)+")\nMaybe your encoder needs an update")
			else:
				if self.debug:
					print("[DEBUG][ERROR]You were trying to call a port, that is not open ("+str(interfaceId)+"."+str(portId)+")")
		else:
			if self.debug:
				print("[DEBUG][ERROR]You were trying to call an interface, that is not open ("+str(interfaceId)+")")
	
	def broadcast(self,interfaceId,portId,functionId,*args,**options):
		self.call(interfaceId,None,portId,functionId,*args,**options)
		
	#arguments is a list of binarys (bytes objects)
	def rawCall(self,interfaceId,connectionId,portId,functionId,arguments):
		self.commandPacker.cmdCall(interfaceId,connectionId,portId,functionId,arguments)
	
	def closeConnection(self,moduleId,interfaceId,connectionId,onSuccess=None,onFailure=None,onTimeout=None,**context):
		cookie = self._generateCookie()
		if not (onSuccess is None and onFailure is None and onTimeout is None):
			self._setCookieCallbackHandlers(cookie,{'retNoErr':onSuccess,'errNoWriteAccs':onFailure,'timeout':onTimeout},**context)
		self.commandPacker.cmdCloseConnection(cookie,moduleId,interfaceId,connectionId)
		
	def requestDirectCildren(self,moduleId,onSuccess=None,onFailure=None,onTimeout=None,**context):
		cookie = self._generateCookie()
		if not (onSuccess is None and onFailure is None and onTimeout is None):
			self._setCookieCallbackHandlers(cookie,{'retDirectChildren':onSuccess,'errNoReadAccs':onFailure,'timeout':onTimeout},**context)
		self.commandPacker.cmdGetDirectChildren(cookie,moduleId)
		
	def requestModuleInfo(self,moduleId,onSuccess=None,onFailure=None,onTimeout=None,**context):
		cookie = self._generateCookie()
		if not (onSuccess is None and onFailure is None and onTimeout is None):
			self._setCookieCallbackHandlers(cookie,{'retModInfo':onSuccess,'errNoReadAccs':onFailure,'timeout':onTimeout},**context)
		self.commandPacker.cmdGetModuleInfo(cookie,moduleId)
		
	def requestInterfaceInfo(self,moduleId,interfaceId,onSuccess=None,onFailure=None,onTimeout=None,**context):
		cookie = self._generateCookie()
		if not (onSuccess is None and onFailure is None and onTimeout is None):
			self._setCookieCallbackHandlers(cookie,{'retIntfInfo':onSuccess,'errNoReadAccs':onFailure,'timeout':onTimeout},**context)
		self.commandPacker.cmdGetInterfaceInfo(cookie,moduleId,interfaceId)
		
	def requestConnectionInfo(self,moduleId,interfaceId,connectionId,onSuccess=None,onFailure=None,onTimeout=None,**context):
		cookie = self._generateCookie()
		if not (onSuccess is None and onFailure is None and onTimeout is None):
			self._setCookieCallbackHandlers(cookie,{'retConnInfo':onSuccess,'errNoReadAccs':onFailure,'timeout':onTimeout},**context)
		self.commandPacker.cmdGetConnectionInfo(cookie,moduleId,interfaceId,connectionId)
	
	##################
	# STATE #########
	
	def running(self):
		return not self.isShutdown	
	
	def me(self):
		return self.moduleId
		
	def isInterfaceOpen(self,interfaceId):
		return interfaceId in self.openInterfaces
	
	##################
	# DECORATORS ####
	
	def start(self,function):
		self.on("start",function)
		
	################################
	#### INTERNAL #################
	##############################
	
	###################
	# COOKIES ########
	
	def _generateCookie(self):
		return uuid.uuid4().hex
	
	#handlers is a dict that maps the callback name to the callback function
	def _setCookieCallbackHandlers(self,cookie,handlers,**context):
		self.cookiehooks[cookie] = handlers
		self.cookiecontext[cookie] = context
		
	def _fireCookie(self,cookie,fn,*args,**context):
		if cookie in self.cookiehooks:
			context = {**context,**self.cookiecontext[cookie]}
			if fn in self.cookiehooks[cookie]:
				if callable(self.cookiehooks[cookie][fn]):
					self.cookiehooks[cookie][fn](*args,**context)
				elif type(self.cookiehooks[cookie][fn]) is list:
					for cb in self.cookiehooks[cookie][fn]:
						if callable(cb):
							cb(*args,**context)
			del self.cookiehooks[cookie] #only one callback per cookie
		if cookie in self.cookiecontext:
			del self.cookiecontext[cookie]
	
	################
	# EVENTS ######
	
	def _fireEvent(self,event,*args,**options):
		self.eventbus.fire(event,*args,**options)
		
	def _onShutdown(self):
		if not self.isShutdown:
			self.isShutdown = True
			self._fireEvent("shutdown")
			sys.exit()
		
	################################
	#### CALLBACKS ################
	##############################
	
	def initalize(self):
		self.connected = True
		self._fireEvent("start")
	
	def authFailed(self):
		print("authentication failed!\nexiting ...")
		sys.exit(2)
	
	def cbReturnNoError(self,cookie):
		self._fireCookie(cookie,"retNoErr")
	
	def cbErrorNoWriteAccess(self,cookie):
		self._fireCookie(cookie,"errNoWriteAccs")
	
	def cbErrorNoReadAccess(self,cookie):
		self._fireCookie(cookie,"errNoReadAccs")
	
	def cbChildModuleCreated(self,cookie,id,key):
		self._fireCookie(cookie,"childModCreated",id,key)
		
	def cbErrorInterfaceWasAlreadyOpen(self,interfaceId):
		raise CrashAndBurnException("The server says that the interface #"+str(interfaceId)+" was already open.\nThis should not happen, beacause modclient does these checks before it calls the Server!")
		
	def cbInterfaceOpened(self,interfaceId):
		if not (interfaceId in self.interfaces):
			raise CrashAndBurnException("An interface, that is not in the interface table has opened!")
		self.interfaces[interfaceId]._opened_(self,interfaceId)
		for portId,portObject in self.interfaces[interfaceId]._getPorts().items():
			portObject._opened_(self,interfaceId,portId)
		#self.interfaces[interfaceId]._onOpen()
		self.openInterfaces.append(interfaceId)
		self._fireEvent("intfOpened",self.moduleId,interfaceId)
		#print("cbInterfaceOpened",interfaceId,self.interfaces)
		self.callbacks[interfaceId] = {}
		self.connections[interfaceId] = []
			
	def cbErrorInterfaceWasAlreadyClosed(self,interfaceId):
		if self.debug:
			print("[WARNING]The server says that the interface #"+str(interfaceId)+" was already closed.\nThis can happen, when the close function gets called again for an interface, that is already closing.")
		else:
			#raise CrashAndBurnException("The server says that the interface #"+str(interfaceId)+" was already closed.\nThis should not happen, beacause modclient does these checks before it calls the Server!")
			pass
		
	def cbInterfaceClosed(self,interfaceId):
		if not (interfaceId in self.interfaces):
			raise CrashAndBurnException("An interface, that is not in the interface table has closed!")
		self.interfaces[interfaceId]._closed_()
		for portId,portObject in self.interfaces[interfaceId]._getPorts().items():
			portObject._closed_()
		#self.interfaces[interfaceId]._onClose()
		self.openInterfaces.remove(interfaceId)
		self.interfaces.pop(interfaceId)
		#print("Closed interface "+str(interfaceId))
		self._fireEvent("intfClosed",self.moduleId,interfaceId)
		self._fireEvent("interface."+str(interfaceId)+".closed")
		self.callbacks.pop(interfaceId)
		self.connections.pop(interfaceId)
				
	def cbErrorCouldNotOpenConnection(self,cookie,reason):
		#errors:
		#  noModuleA #no access to module A
		#  noModuleB #no access to module B
		#  noInterfaceA #interface A does not exist
		#  noInterfaceB #interface B does not exist
		#  incompatible #the two interfaces are not compatible
		#  connlimitA #connection limit reached on interface A
		#  connlimitB #connection limit reached on interface B
		if not reason in ["noModuleA","noModuleB","noInterfaceA","noInterfaceB","incompatible","connlimitA","connlimitB"]:
			raise CrashAndBurnException("Server returned an unknown reason for a Connection Open Error. ("+str(reason)+")")
		if cookie in self.openingConnectionData:
			info = self.openingConnectionData.pop(cookie)
			self._fireCookie(cookie,"connOpenErr",info['ma'],info['ia'],info['mb'],info['ib'],reason)
		else:
			self._fireCookie(cookie,"-error-")#just to clear the cookie if it existed
			raise CrashAndBurnException("Received a Couldn't open Connection, with a non registred cookie! (might be a big bug)")
			
	def cbReturnConnectionOpen(self,cookie,connectionIdA,connectionIdB):
		if cookie in self.openingConnectionData:
			info = self.openingConnectionData.pop(cookie)
			self._fireCookie(cookie,"connOpened",info['ma'],info['ia'],connectionIdA,info['mb'],info['ib'],connectionIdB)
		else:
			raise CrashAndBurnException("Received a Connection Open, with a non registred cookie! (might be a big bug)")
				
	def cbConnectionOpened(self,interfaceId,connectionId):
		#print("cbConnectionOpened",interfaceId,connectionId,self.interfaces)
		if not interfaceId in self.interfaces:
			raise CrashAndBurnException("A connection Opened for a non registered interface (This might be a big bug) ("+str(interfaceId)+"."+str(connectionId)+")")
		self.interfaces[interfaceId]._onConnectionOpened(connectionId)
		self.callbacks[interfaceId][connectionId] = {}
		self.connections[interfaceId].append(connectionId)
		for portId,portObject in self.interfaces[interfaceId]._getPorts().items():
			portObject._onConnectionOpened(connectionId)
		self._fireEvent("connOpened",self.moduleId,interfaceId,connectionId)
		self._fireEvent("interface."+str(interfaceId)+".newConnection",connectionId,interfaceId=interfaceId)
		
	def cbConnectionClosed(self,interfaceId,connectionId):
		#if interfaceId in self.interfaces:
		if not interfaceId in self.interfaces:
			raise CrashAndBurnException("A connection Closed for a non registered interface (This might be a big bug) ("+str(interfaceId)+"."+str(connectionId)+")")
		self.callbacks[interfaceId].pop(connectionId)
		self.connections[interfaceId].remove(connectionId)
		self.interfaces[interfaceId]._onConnectionClosed(connectionId)
		for portId,portObject in self.interfaces[interfaceId]._getPorts().items():
			portObject._onConnectionClosed(connectionId)
		self._fireEvent("connClosed",self.moduleId,interfaceId,connectionId)
		self._fireEvent("interface."+str(interfaceId)+".connectionClosed",connectionId,interfaceId=interfaceId)
		self._fireEvent("connection."+str(interfaceId)+"."+str(connectionId)+".closed")
		
	#DEPRECATED
	def cbErrorCallError(self,interfaceId,connectionId,portId,functionId,reason):
		#errors:
		# noInterface #if the interface does not exist
		# noConnection #if the connection does not exist (or is not yet initalized)
		# noPort #if the port isn't open
		# noFunction #if the function does not exist (a module may also close the connection instead)
		# badArgument #if the arguments are not compliant wih the protocol (a module may also close the connection insteafunctionIdd)
		if not reason in ["noInterface","noConnection","noPort","noFunction","badArgument"]:
			raise CrashAndBurnException("Server returned an unknown reason for a Call Error. ("+str(reason)+")")
		#if interfaceId in self.interfaces:
		#		self.interfaces[interfaceId]._onCallError(connectionId,portId,functionId,reason)
		
	def cbConnectionAlreadyClosed(self,cookie):
		pass #ignore this, as this can happen, when both ends close the connection at the same time
		#a conection closed should be fired anyway
				
	def cbParentChanged(self,newParentId,oldParentId):
		self._fireEvent("moduleParentChanged",self.moduleId,newParentId,oldParentId)
				
	#management
	
	def cbOtherModuleCreated(self,moduleId):
		self._fireEvent("newModule",moduleId)
	
	def cbOtherModuleShutDown(self,moduleId):
		self._fireEvent("moduleSutdown",moduleId)
				
	def cbOtherModuleInitalized(self,moduleId):
		self._fireEvent("moduleInitalized",moduleId)
		
	def cbOtherInterfaceOpened(self,moduleId,interfaceId):
		self._fireEvent("intfOpened",moduleId,interfaceId)
		
	def cbOtherInterfaceClosed(self,moduleId,interfaceId):
		self._fireEvent("intfClosed",moduleId,interfaceId)
		
	def cbOtherConnectionOpened(self,moduleId,interfaceId,connectionId):
		self._fireEvent("connOpened",moduleId,interfaceId,connectionId)
		
	def cbOtherConnectionClosed(self,moduleId,interfaceId,connectionId):
		self._fireEvent("connClosed",moduleId,interfaceId,connectionId)
		
	def cbOtherParentChanged(self,moduleId,newParentId,oldParentId):
		self._fireEvent("moduleParentChanged",moduleId,newParentId,oldParentId)
	
	#selfmanagement
		
	def cbSelfShutdown(self):
		self._onShutdown()
		
	#calls
	
	def cbCalled(self,interfaceId,connectionId,portId,functionId,arguments):
		#context = {'interfaceId':interfaceId,'connectionId':connectionId,'portId':portId,'functionId':functionId}
		#self._fireEvent(self.getCallEventName(interfaceId,portId,functionId)+".raw",*arguments,**context)
		if interfaceId in self.interfaces:
			interface = self.interfaces[interfaceId]
			ports = interface._getPorts()
			if portId in ports:
				portObject = ports[portId]
				#get the decoder plugin <protocol>_<side>_decoder
				decoderPlugin = self.pluginstore.require(portObject.protocol+"_"+portObject.inSide+"_decoder")
				#decode arguments
				if functionId in decoderPlugin.decoders:
					try:
						darguments = decoderPlugin.decoders[functionId](arguments)
					except FormatError as error:
						print("[ERROR] Could not decode an incoming packet ("+
							str(interfaceId)+"-"+str(connectionId)+"."+str(portId)+"."+str(functionId)+")\n"+str(error),
							file=sys.stderr)
					#print debugging info
					if self.debug:
						print("[CALL-IN] "+str(interfaceId)+"-"+str(connectionId)+"."+str(portId)+"."+str(functionId)+
							" "+str(darguments))
					#get Port callback and fire
					callcontext = CallContext(self,interfaceId,connectionId,portId,functionId)
					#get cookie
					cookie = None
					bincookie = None
					mode = None
					if decoderPlugin.hasMode(functionId) and len(darguments)>=2:
						cookie = darguments[1]
						bincookie = arguments[1]
						mode = darguments[0]
						darguments = darguments[1:]
					elif (not decoderPlugin.hasMode(functionId)) and len(darguments)>=1:
						cookie = darguments[0]
						bincookie = arguments[0]
					#print("[TEMP]",mode,cookie,darguments)
					callcontext.cookie = cookie
					callcontext.mode = mode
					try:
						if (type(portObject.calls) is dict) and (functionId in portObject.calls):
							portObject.calls[functionId](callcontext,*darguments)
						elif ((type(portObject.calls) is list) or (type(portObject.calls) is tuple)) and (len(portObject.calls) > functionId) and (functionId >= 0):
							portObject.calls[functionId](callcontext,*darguments)
					except TypeError as e:
						if self.debug:
							print("Just got a type error from a call (Probably, beacause someone didn't respect the interface protocol):\n")
							raise
					#print("[TEMP]still going",interfaceId,connectionId,self.callbacks)
					#callback stuffs
					if interfaceId in self.callbacks:
						if connectionId in self.callbacks[interfaceId]:
							darguments = darguments[1:]
							callbackId = str(bincookie)+"__"+str(portId)+"."+str(functionId)
							#fire callback listenersopenInterface
							#print("[TEMP]callbackId:",callbackId,self.callbacks[interfaceId][connectionId])
							if callbackId in self.callbacks[interfaceId][connectionId]:
								callback = self.callbacks[interfaceId][connectionId].pop(callbackId)
								if self.debug:
									print("[CALL-IN][CALLBACK]Calling back:",interfaceId,callbackId,mode,callback)
								if type(callback) is dict:
									if mode in callback:
										callback = callback[mode]
								if callable(callback):
									callback(callcontext,*darguments)
								elif type(callback) is list or type(callback) is tuple:
									for cb in callback:
										cb(callcontext,*darguments)
					
	#getter returns
	
	def cbRetDirectCildren(self,cookie,moduleId,listOfDirectChildren):
		self._fireCookie(cookie,"retDirectChildren",moduleId,listOfDirectChildren)
		self._fireEvent("updateModDirectChildList",moduleId,listOfDirectChildren)
		
	def cbRetModuleInfo(self,cookie,moduleId,parentId,listOfOpenInterfaces):
		self._fireCookie(cookie,"retModInfo",moduleId,parentId,listOfOpenInterfaces)
		self._fireEvent("updateModInfo",moduleId,parentId,listOfOpenInterfaces)
		
	def cbRetInterfaceInfo(self,cookie,moduleId,interfaceId,maxconnections,listOfOpenConnections,mapOfPortsetups):
		self._fireCookie(cookie,"retIntfInfo",moduleId,interfaceId,maxconnections,listOfOpenConnections,mapOfPortsetups)
		self._fireEvent("updateIntfInfo",moduleId,interfaceId,maxconnections,listOfOpenConnections,mapOfPortsetups)
		
	def cbRetConnectionInfo(self,cookie,moduleId,interfaceId,connectionId,peerModuleId,peerInterfaceId,peerConnectionId):
		self._fireCookie(cookie,"retConnInfo",moduleId,interfaceId,connectionId,peerModuleId,peerInterfaceId,peerConnectionId)
		self._fireEvent("updateConnInfo",moduleId,interfaceId,connectionId,peerModuleId,peerInterfaceId,peerConnectionId)
	
#For internal use only
#to tell the client command packer, what the interface should look like
class InterfaceMetadata:
	def __init__(self,maxconnections,portsetups):
		self.maxconnections = maxconnections
		self.portsetups = portsetups
			
###############################################################################
###############################################################################
###############################################################################

class Port:
	#gets called when the object is created, before the port is opened
	#can be overwritten
	def __init__(self):
		self.calls = [] #Maps the functionId to the callbackfunction (currently has to be an array but can contain None entrys)
		self.maxconnections = -1 #the amount of connections this port object can handle
		self.protocol = "none" #the port protocol
		self.inSide = "/" #the side of the protocol this object acts as a receiver for
		self.outSide = "/" #the side of the protocol this object uses to call out
		
	#wrapper around the modclients call function to make life easier
	def _call(self,connectionId,functionId,*args,**options):
		try:
			self.client.call(self.interfaceId,connectionId,self.portId,functionId,*args,**options)
		except AttributeError:
			pass
	
	#wrapper around the modclients broadcast function to make life easier
	def _broadcast(self,functionId,*args,**options):
		try:
			self.client.broadcast(self.interfaceId,self.portId,functionId,*args,**options)
		except AttributeError:
			pass
		
	#gets called when the interface opens
	#gets called by the client object
	#this should not be overwritten
	def _opened_(self,client,interfaceId,portId):
		self.client = client
		self.interfaceId = interfaceId
		self.portId = portId
		self._onOpen(client,interfaceId,portId)
		
	#gets called when the interface opens
	#gets called via the _opened_ method
	#can be overwritten
	def _onOpen(self,client,interfaceId,portId):
		pass
		
	#gets called when the interface, this port is part of closes
	#gets called by the client object
	#this should not be overwritten
	def _closed_(self):
		self._onClose
		self.client = None
		self.interfaceId = None
		self.portId = None
		
	#gets called when the interface, this port is part of closes
	#can be overwritten
	def _onClose(self):
		pass
	
	#gets called when a connection opens	
	#can be overwritten
	def _onConnectionOpened(self,connectionId):
		pass
	
	#gets called when a connection closes
	#can be overwritten	
	def _onConnectionClosed(self,connectionId):
		pass
	
class InterfaceCannotBeModifiedAfterOpenError(Exception):
	pass
	
class Interface:
	#gets called when the interface is created, before the port is opened
	#can be overwritten
	def __init__(self):
		self.opened = False
		
	def setPort(self,portId,portObject,optional=False):
		try:
			if self.client.isInterfaceOpen(self.interfaceId):
				raise InterfaceCannotBeModifiedAfterOpenError
		except AttributeError: #not opened
			pass
		try:
			tmp = self.ports
		except AttributeError:
			self.ports = {}
		portObject.isOptional = optional
		self.ports[portId] = portObject
		return self
		
	#call this function to open the interface	
	def open(self,client,interfaceId,**options):
		client.openInterface(interfaceId,self,**options)
		return self
		
	#call this function to close the interface
	def close(self):
		try:
			self.client.closeInterface(interfaceId)
		except AttributeError: #interface was never opened (and client is not a valid Attribute)
			pass
	
	def _opened_(self,client,interfaceId):
		self.client = client
		self.interfaceId = interfaceId
		self._onOpen()
			
	#gets called when the interface opened
	#gets called by the _opened_ method
	#can be overwritten
	def _onOpen(self):
		pass
	
	def _closed_(self):
		self._onClose()
		self.client = None
		self.interfaceId = None
		
	#gets called when the interface closed
	#gets called by the _closed_ method
	#can be overwritten
	def _onClose(self):
		pass
		
	def _getPorts(self):
		try:
			return self.ports
		except AttributeError:
			self.ports = {}
			return self.ports
			
	#gets called by the client object, when something connects to the interface
	#can be overwritten
	def _onConnectionOpened(self,connectionId):
		pass
		
	#gets called by the client object, when something disconnects from the interface
	#can be overwritten
	def _onConnectionClosed(self,connectionId):
		pass
