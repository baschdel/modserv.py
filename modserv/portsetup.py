class Portsetup:
	#major Versions should be part of the protocol name, since its a different protocol if its not compatible
	def __init__(self,protocol,inSide,inMinVersion,outSide,outMinVersion,isOptional=False):
		self.protocol = protocol #the protocol used
		self.inSide = inSide #the part of the protocol that defines this ports inputs (one ascii char) (this side)
		self.inMinVersion = inMinVersion #the minor version of the input protocol part
		self.outSide = outSide #the part of the protocol that defines this ports outputs (one ascii char) (the other side)
		self.outMinVersion = outMinVersion #the minor version of the output protocol part (has to be smaller or equal to the minor input side version)
		self.isOptional = isOptional
		
	def isCompatibleWith(self,other):
		if (other is None) and (not self.isOptional):
			return False;
		return (
				self.protocol == other.protocol and
				self.inSide == other.outSide and
				self.outSide == other.inSide and
				self.inMinVersion >= other.outMinVersion and
				self.outMinVersion <= other.inMinVersion
			)
			
