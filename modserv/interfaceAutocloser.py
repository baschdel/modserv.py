from modserv.exceptions import AlreadyAttachedExeption
#This class can be attached to an interface and sutomatically closes it when the last conection closes

class CloseOnLastDisconnect:
	def __init__(self,modclient,interfaceId):
		self.modclient = modclient
		self.interfaceId = interfaceId
		self.connectionCounter = 0
		self.modclient.on("interface."+str(interfaceId)+".connectionClosed",self._onConnectionClosed)
		self.modclient.on("interface."+str(interfaceId)+".newConnection",self._onConnectionOpened)
		
	def _onConnectionOpened(self,connectionId,**context):
		self.connectionCounter += 1
		
	def _onConnectionClosed(self,connectionId,**context):
		self.connectionCounter -= 1
		if self.connectionCounter <= 0:
			self._triggered()
		
	def _triggered(self):
		self.modclient.ignore("interface."+str(self.interfaceId)+".connectionClosed",self._onConnectionClosed)
		self.modclient.ignore("interface."+str(self.interfaceId)+".newConnection",self._onConnectionOpened)
		self.modclient.closeInterface(self.interfaceId)
