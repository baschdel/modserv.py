import threading
from modserv.cronjob import Cronjob
import modserv.client as client
import uuid #for cookie generation
import sys
from modserv.exceptions import CrashAndBurnException

#this file is intended to be used by modules in order to avoid having to mess around with calls, callbacks and so on

def getEasyClient():
	moduleId,moduleKey,port,server = client.getCredsFromArgs()
	#print([moduleId,moduleKey,port,server])
	return EasyClient(moduleId,moduleKey,port,server)

#class CrashAndBurnException(Exception):
	#pass #If this is raised, something went very wrong
	#possible causes:
	#		The server didn't respect the protocol
	#		You did something funny with the internal state of the easyclient classes
	#		something in the easyclient went wrong and a packet from the server got ignored	
	
class EasyCallback:
	def __init__(self,callbackfunction,*extra):
		self.callbackfunction = callbackfunction
		self.extra = extra
	
	def __call__(self,*args,**kargs):
		cbf = self.callbackfunction
		cbf(*(self.extra),*args,**kargs)
	
class AsyncCallback:
	def __init__(self,callbackfunction):
		self.callbackfunction = callbackfunction
	
	def __call__(self,*args,**kargs):
		self.thread = threading.Thread(target=self.callbackfunction, args=args, kwargs=kargs)
		self.thread.start()
		
class CallbackLock:
	def __init__(self,counter=1,timeout=None):
		if counter <= 0:
			counter = 1
		self.timeout = timeout
		self.counter = counter
		self.semaphore = threading.Semaphore(value=0)
		self.lock = threading.Lock()
		self.args = ()
		self.kargs = {}
	
	def __call__(self,*args,**kargs):
		self.lock.acquire()
		counter = counter-1
		if counter == 0:
			self.semaphore.release()
			self.args = args
			self.kargs = kargs
		self.lock.release()
		
	def acquire(self,timeout=None):
		if timeout is None:
			timeout = self.timeout
		return self.semaphore.acquire(timeout=timeout)
		
	def release(self,*args,**kargs):
		self(*args,*kargs)
		
class AbstractConection:
	#gets called when a connection was opened
	def __init__(self,interface,connectionId,*connArgs,**connKArgs):
		self.interface = interface
		self.id = connectionId
		self.onInit(*connArgs,**connKArgs)
		
	#abstract function, that gets called before the __init__ function exits
	def onInit(self,*connArgs,**connKArgs):
		pass
		
	#gets called when the connection closes (the object will be deleted after this function returns)
	def closed(self):
		pass
	
	#gets called when something calls this connection
	def called(self,portId,functionId,arguments):
		pass
		
	#gets called when a call went wrong
	def callerror(self,portId,functionId,reason):
		pass
		
	#call this to call the peer of this connection
	#still takes arguments as binarys
	def call(self,portId,functionId,arguments):
		self.interface.easyclient.call(self.interface.id,self.id,portId,functionId,arguments)
		
	#call this to close the connection
	def close(self,onSuccess=None,onFailure=None,onTimeout=None):
		self.interface.easyclient.closeConnection(self.interface.easyclient.moduleId,self.interface.id,self.id, onSuccess=onSuccess,onFailure=onFailure,onTimeout=onTimeout)
		
	def error(self,portId,functionId):
		self.close()
	
	def badArgument(self,portId,functionId):
		#TODO: implement a command on the server to submit a call error
		self.close()
		
class EasyInterface:
	def __init__(self,portsetups,maxconnections,callbacks,connectionClass,*connArgs,**connKArgs):
			self.maxconnections = maxconnections #has to be called maxconnections and be an integer number
			self.portsetups = portsetups #has to be called portsetups and be a dict of Portsetup objects
			self.id = None
			self.easyclient = None
			self.callbacks = callbacks
			self.connectionClass = connectionClass
			self.connArgs = connArgs
			self.connKArgs = connKArgs
			self.connections = {} #maps connectionIds to the connection objects
			#print("[DBG]Initalized easyclient")
				
	# Called when The interface is Bound to the easyclient with the InterfaceOpen function
	def _bind(self,easyclient,id):
		self.id = id
		self.easyclient = easyclient
		
	def _callback(self,command,*args):
		if self.callbacks is None:
			return
		if command in self.callbacks:
			self.callbacks[command](*args)
		
	#called when the server says, that the interface has opened	
	def _onOpen(self):
		self._callback("intfOpened")
		
	#called when the server says, that the interface has closed
	def _onClose(self):
		self._callback("intfClosed")
	
	def _onConnectionOpened(self,connectionId):
		if connectionId in self.connections:
			raise CrashAndBurnException("The A connection that was already open opened again! cid="+str(connectionId))
		self.connections[connectionId] = self.connectionClass(self,connectionId,*self.connArgs,**self.connKArgs)
		self._callback("connOpened",connectionId)
		
	def _onConnectionClosed(self,connectionId):
		if connectionId in self.connections:
			self.connections[connectionId].closed()
			del self.connections[connectionId]
	
	def _onCalled(self,connectionId,portId,functionId,arguments):
		if connectionId in self.connections:
			self.connections[connectionId].called(portId,functionId,arguments)
			
	def _onCallError(self,connectionId,portId,functionId,reason):
		if connectionId in self.connections:
			self.connections[connectionId].callerror(portId,functionId,reason)
			
	def close(self):
		if not easyclient is None:
			self.easyclient.closeInterface(self.id)
			
	def connect(self,peerModuleId,peerInterfaceId,onSuccess=None,onFailure=None,onTimeout=None):
		self.easyclient.connect(self.easyclient.moduleId,self.id,peerModuleId,peerInterfaceId, onSuccess=onSuccess,onFailure=onFailure,onTimeout=onTimeout)
		
class EasyClient:
	def __init__(self,moduleId,moduleKey,port,server,debug=False):
		self.interfaces = {} #interface objects, that handle the callbacks for interface stuff
		self.cookiehooks = {} #callbacks for everything that uses cookies
		self.eventhooks = {} #lists of callbacks for "global" events
		self.mainloops = []
		self.connected = False
		self.isShutdown = False
		self.moduleId = moduleId
		self.commandPacker = client.connect(server,port,moduleId,moduleKey,self,debug=debug)
		self.openingConnectionData = {} #A table that maps cookies to the data of the connection, to store it until the callback arrives
		self.cookiedata = {}
		
	#####################################################
	#####################################################
	# API
	
	#call this when you are done with your initalization (nothing after this function gets executed)
	def initalized(self):
		self.connected = True
		self._fireEvent("moduleInitalized",self.moduleId)
		self.commandPacker.packetsocket.join()
		#end 
	
	def on(self,event,callback):
		if not event in self.eventhooks:
			self.eventhooks[event] = []
		if not callable(callback):
			raise CrashAndBurnException("easyclient.on A non callable object was registred as callback for event '"+str(event)+"' ("+str(callback)+")")
		self.eventhooks[event].append(callback)
		return True
		
	def ignore(self,event,callback):
		if not event in self.eventhooks:
			return
		if not callback in self.eventhooks[event]:
			return
		self.eventhooks[event].remove(callback)
		if len(self.eventhooks[event]) == 0:
			del self.eventhooks[event]
		
	def shutdown(self,moduleId=None,onSuccess=None,onFailure=None,onTimeout=None,data=None):
		if moduleId is None:
			moduleId=self.moduleId
		cookie = self._generateCookie()
		if not (onSuccess is None and onFailure is None and onTimeout is None):
			self._setCookieCallbackHandlers(cookie,{'retNoErr':onSuccess,'errNoWriteAccs':onFailure,'timeout':onTimeout},data=data)
		self.commandPacker.cmdShutdown(cookie,moduleId)
	
	def shutdownTree(self,moduleId=None,onSuccess=None,onFailure=None,onTimeout=None,data=None):
		if moduleId is None:
			moduleId=self.moduleId
		cookie = self._generateCookie()
		if not (onSuccess is None and onFailure is None and onTimeout is None):
			self._setCookieCallbackHandlers(cookie,{'retNoErr':onSuccess,'errNoWriteAccs':onFailure,'timeout':onTimeout},data=data)
		self.commandPacker.cmdShutdownTree(cookie,moduleId)
		
	def makeCild(self,onSuccess=None,onTimeout=None,data=None):
		cookie = self._generateCookie()
		if not (onSuccess is None and onTimeout is None):
			self._setCookieCallbackHandlers(cookie,{'childModCreated':onSuccess,'timeout':onTimeout},data=data)
		self.commandPacker.cmdMakeChild(cookie)
			
	def openInterface(self,interfaceId,interfaceObject):
		if interfaceId in self.interfaces:
			return False #don't open interfaces that are already open
		self.interfaces[interfaceId] = interfaceObject
		self.commandPacker.cmdOpenInterface(interfaceId,interfaceObject)
		interfaceObject._bind(self,interfaceId)
		#print("opening interface:",interfaceId,interfaceObject,self.interfaces)
		return True
		
	def closeInterface(self,interfaceId):
		if not interfaceId in self.interfaces:
			return False #don't close interfaces that are already closed
		self.commandPacker.cmdCloseInterface(interfaceId)
		return True
	
	def connect(self,moduleIdA,interfaceIdA,moduleIdB,interfaceIdB,onSuccess=None,onFailure=None,onTimeout=None,data=None):
		cookie = self._generateCookie()
		self.openingConnectionData[cookie] = {'ma':moduleIdA,'mb':moduleIdB,'ia':interfaceIdA,'ib':interfaceIdB}
		if not (onSuccess is None and onFailure is None and onTimeout is None):
			self._setCookieCallbackHandlers(cookie,{'connOpened':onSuccess,'connOpenErr':onFailure,'timeout':onTimeout},data=data)
		self.commandPacker.cmdOpenConnection(cookie,moduleIdA,interfaceIdA,moduleIdB,interfaceIdB)
	
	def closeConnection(self,moduleId,interfaceId,connectionId,onSuccess=None,onFailure=None,onTimeout=None,data=None):
		cookie = self._generateCookie()
		if not (onSuccess is None and onFailure is None and onTimeout is None):
			self._setCookieCallbackHandlers(cookie,{'retNoErr':onSuccess,'errNoWriteAccs':onFailure,'timeout':onTimeout},data=data)
		self.commandPacker.cmdCloseConnection(cookie,moduleId,interfaceId,connectionId)
		
	#arguments is a list of binarys (bytes objects)
	def call(self,interfaceId,connectionId,portId,functionId,arguments):
		self.commandPacker.cmdCall(interfaceId,connectionId,portId,functionId,arguments)
		
	def requestDirectCildren(self,moduleId,onSuccess=None,onFailure=None,onTimeout=None,data=None):
		cookie = self._generateCookie()
		if not (onSuccess is None and onFailure is None and onTimeout is None):
			self._setCookieCallbackHandlers(cookie,{'retDirectChildren':onSuccess,'errNoReadAccs':onFailure,'timeout':onTimeout},data=data)
		self.commandPacker.cmdGetDirectChildren(cookie,moduleId)
		
	def requestModuleInfo(self,moduleId,onSuccess=None,onFailure=None,onTimeout=None,data=None):
		cookie = self._generateCookie()
		if not (onSuccess is None and onFailure is None and onTimeout is None):
			self._setCookieCallbackHandlers(cookie,{'retModInfo':onSuccess,'errNoReadAccs':onFailure,'timeout':onTimeout},data=data)
		self.commandPacker.cmdGetModuleInfo(cookie,moduleId)
		
	def requestInterfaceInfo(self,moduleId,interfaceId,onSuccess=None,onFailure=None,onTimeout=None,data=None):
		cookie = self._generateCookie()
		if not (onSuccess is None and onFailure is None and onTimeout is None):
			self._setCookieCallbackHandlers(cookie,{'retIntfInfo':onSuccess,'errNoReadAccs':onFailure,'timeout':onTimeout},data=data)
		self.commandPacker.cmdGetInterfaceInfo(cookie,moduleId,interfaceId)
		
	def requestConnectionInfo(self,moduleId,interfaceId,connectionId,onSuccess=None,onFailure=None,onTimeout=None,data=None):
		cookie = self._generateCookie()
		if not (onSuccess is None and onFailure is None and onTimeout is None):
			self._setCookieCallbackHandlers(cookie,{'retConnInfo':onSuccess,'errNoReadAccs':onFailure,'timeout':onTimeout},data=data)
		self.commandPacker.cmdGetConnectionInfo(cookie,moduleId,interfaceId,connectionId)
		
	def running(self):
		return not self.isShutdown
	
	#####################################################
	#####################################################
	# internal
	
	def _generateCookie(self):
		return uuid.uuid4().hex
	
	#handlers is a dict that maps the callback name to the callback function
	def _setCookieCallbackHandlers(self,cookie,handlers,data = None):
		self.cookiehooks[cookie] = handlers
		if not data is None:
			self.cookiedata[cookie] = data
		
	def _fireCookie(self,cookie,fn,*args):
		data = None
		if cookie in self.cookiedata:
			data = self.cookiedata[cookie]
		if cookie in self.cookiehooks:
			if fn in self.cookiehooks[cookie]:
				if callable(self.cookiehooks[cookie][fn]):
					if data is None:
						self.cookiehooks[cookie][fn](*args)
					else:
						self.cookiehooks[cookie][fn](*args,data=data)
				elif type(self.cookiehooks[cookie][fn]) is list:
					for cb in self.cookiehooks[cookie][fn]:
						if callable(cb):
							if data is None:
								cb(*args)
							else:
								cb(*args,data=data)
			del self.cookiehooks[cookie] #only one callback per cookie
		if cookie in self.cookiedata:
			del self.cookiedata[cookie]
		
	def _fireEvent(self,event,*args):
		if not event in self.eventhooks:
			return
		for callbackHandler in self.eventhooks[event]:
			callbackHandler(*args)
			
	def _onShutdown(self):
		if not self.isShutdown:
			self.isShutdown = True
			#print("_onShutdown called")
			self._fireEvent("selfShutdown")
			self._fireEvent("moduleSutdown",self.moduleId)
			self.commandPacker.packetsocket.close()
		
	#####################################################
	#####################################################
	# Callbacks
		
	def initalize(self):
		self.connected = True
		#no need for an initalizer function for now, the script contiues after it gets it's easyclient instance
			
	def authFailed(self):
		print("authentication failed!\nexiting ...")
		sys.exit(2)
	
	def cbReturnNoError(self,cookie):
		self._fireCookie(cookie,"retNoErr")
	
	def cbErrorNoWriteAccess(self,cookie):
		self._fireCookie(cookie,"errNoWriteAccs")
	
	def cbErrorNoReadAccess(self,cookie):
		self._fireCookie(cookie,"errNoReadAccs")
	
	def cbChildModuleCreated(self,cookie,id,key):
		self._fireCookie(cookie,"childModCreated",id,key)
		
	def cbErrorInterfaceWasAlreadyOpen(self,id):
		raise CrashAndBurnException("The server says that the interface #"+str(id)+" was already open.\nThis should not happen, beacause easyclient does these checks before it calls the Server!")
		
	def cbInterfaceOpened(self,id):
		if not id in self.interfaces:
			raise CrashAndBurnException("An interface, that is not in the easyclient interface table has opened!")
		self.interfaces[id]._onOpen()
		self._fireEvent("intfOpened",self.moduleId,id)
		#print("cbInterfaceOpened",id,self.interfaces)
			
	def cbErrorInterfaceWasAlreadyClosed(self,id):
		raise CrashAndBurnException("The server says that the interface #"+str(id)+" was already closed.\nThis should not happen, beacause easyclient does these checks before it calls the Server!")
		
	def cbInterfaceClosed(self,id):
		if not id in self.interfaces:
			raise CrashAndBurnException("An interface, that is not in the easyclient interface table has closed!")
		self.interfaces[id]._onClose()
		self.interfaces.pop(id)
		#print("Closed interface "+str(id))
		self._fireEvent("intfClosed",self.moduleId,id)
				
	def cbErrorCouldNotOpenConnection(self,cookie,reason):
		#errors:
		#  noModuleA #no access to module A
		#  noModuleB #no access to module B
		#  noInterfaceA #interface A does not exist
		#  noInterfaceB #interface B does not exist
		#  incompatible #the two interfaces are not compatible
		#  connlimitA #connection limit reached on interface A
		#  connlimitB #connection limit reached on interface B
		if not reason in ["noModuleA","noModuleB","noInterfaceA","noInterfaceB","incompatible","connlimitA","connlimitB"]:
			raise CrashAndBurnException("Server returned an unknown reason for a Connection Open Error. ("+str(reason)+")")
		if cookie in self.openingConnectionData:
			info = self.openingConnectionData.pop(cookie)
			self._fireCookie(cookie,"connOpenErr",info['ma'],info['ia'],info['mb'],info['ib'],reason)
		else:
			self._fireCookie(cookie,"-error-")#just to clear the cookie if it existed
			raise CrashAndBurnException("Received a Couldn't open Connection, with a non registred cookie! (might be a big bug)")
			
	def cbReturnConnectionOpen(self,cookie,connectionIdA,connectionIdB):
		if cookie in self.openingConnectionData:
			info = self.openingConnectionData.pop(cookie)
			self._fireCookie(cookie,"connOpened",info['ma'],info['ia'],connectionIdA,info['mb'],info['ib'],connectionIdB)
		else:
			raise CrashAndBurnException("Received a Connection Open, with a non registred cookie! (might be a big bug)")
				
	def cbConnectionOpened(self,interfaceId,connectionId):
		#print("cbConnectionOpened",interfaceId,connectionId,self.interfaces)
		if not interfaceId in self.interfaces:
			raise CrashAndBurnException("A connection Opened for a non registered interface (This might be a big bug)")
		self.interfaces[interfaceId]._onConnectionOpened(connectionId)
		self._fireEvent("connOpened",self.moduleId,interfaceId,connectionId)
		
	def cbConnectionClosed(self,interfaceId,connectionId):
		if interfaceId in self.interfaces:
			self.interfaces[interfaceId]._onConnectionClosed(connectionId)
		self._fireEvent("connClosed",self.moduleId,interfaceId,connectionId)
		
	def cbErrorCallError(self,interfaceId,connectionId,portId,functionId,reason):
		#errors:
		# noInterface #if the interface does not exist
		# noConnection #if the connection does not exist (or is not yet initalized)
		# noPort #if the port isn't open
		# noFunction #if the function does not exist (a module may also close the connection instead)
		# badArgument #if the arguments are not compliant wih the protocol (a module may also close the connection instead)
		if not reason in ["noInterface","noConnection","noPort","noFunction","badArgument"]:
			raise CrashAndBurnException("Server returned an unknown reason for a Call Error. ("+str(reason)+")")
		if interfaceId in self.interfaces:
				self.interfaces[interfaceId]._onCallError(connectionId,portId,functionId,reason)
		
	def cbConnectionAlreadyClosed(self,cookie):
		pass #ignore this, as this can happen, when both ends close the connection at the same time
				
	def cbParentChanged(self,newParentId,oldParentId):
		self._fireEvent("moduleParentChanged",self.moduleId,newParentId,oldParentId)
				
	#management
	
	def cbOtherModuleCreated(self,moduleId):
		self._fireEvent("newModule",moduleId)
	
	def cbOtherModuleShutDown(self,moduleId):
		self._fireEvent("moduleSutdown",moduleId)
				
	def cbOtherModuleInitalized(self,moduleId):
		self._fireEvent("moduleInitalized",moduleId)
		
	def cbOtherInterfaceOpened(self,moduleId,interfaceId):
		self._fireEvent("intfOpened",moduleId,interfaceId)
		
	def cbOtherInterfaceClosed(self,moduleId,interfaceId):
		self._fireEvent("intfClosed",moduleId,interfaceId)
		
	def cbOtherConnectionOpened(self,moduleId,interfaceId,connectionId):
		self._fireEvent("connOpened",moduleId,interfaceId,connectionId)
		
	def cbOtherConnectionClosed(self,moduleId,interfaceId,connectionId):
		self._fireEvent("connClosed",moduleId,interfaceId,connectionId)
		
	def cbOtherParentChanged(self,moduleId,newParentId,oldParentId):
		self._fireEvent("moduleParentChanged",moduleId,newParentId,oldParentId)
	
	#selfmanagement
		
	def cbSelfShutdown(self):
		self._onShutdown()
		
	#calls
	
	def cbCalled(self,interfaceId,connectionId,portId,functionId,arguments):
		if interfaceId in self.interfaces:
			self.interfaces[interfaceId]._onCalled(connectionId,portId,functionId,arguments)
			
		#print("cbCalled "+interfaceId+"."+connectionId+"."+portId+"."+functionId+" ("+arguments+")")
		
	#getter returns
	
	def cbRetDirectCildren(self,cookie,moduleId,listOfDirectChildren):
		self._fireCookie(cookie,"retDirectChildren",moduleId,listOfDirectChildren)
		self._fireEvent("updateModDirectChildList",moduleId,listOfDirectChildren)
		
	def cbRetModuleInfo(self,cookie,moduleId,parentId,listOfOpenInterfaces):
		self._fireCookie(cookie,"retModInfo",moduleId,parentId,listOfOpenInterfaces)
		self._fireEvent("updateModInfo",moduleId,parentId,listOfOpenInterfaces)
		
	def cbRetInterfaceInfo(self,cookie,moduleId,interfaceId,maxconnections,listOfOpenConnections,mapOfPortsetups):
		self._fireCookie(cookie,"retIntfInfo",moduleId,interfaceId,maxconnections,listOfOpenConnections,mapOfPortsetups)
		self._fireEvent("updateIntfInfo",moduleId,interfaceId,maxconnections,listOfOpenConnections,mapOfPortsetups)
		
	def cbRetConnectionInfo(self,cookie,moduleId,interfaceId,connectionId,peerModuleId,peerInterfaceId,peerConnectionId):
		self._fireCookie(cookie,"retConnInfo",moduleId,interfaceId,connectionId,peerModuleId,peerInterfaceId,peerConnectionId)
		self._fireEvent("updateConnInfo",moduleId,interfaceId,connectionId,peerModuleId,peerInterfaceId,peerConnectionId)
