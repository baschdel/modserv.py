import socket
import threading
import struct

class DummyPacketHandler:
	def onPacket(self,binary,packetsocket):
		pass

	def onClose(self,packetsocket):
		pass

class PacketSocket(threading.Thread):
	def __init__(self,socket,callbackHandler):
		threading.Thread.__init__(self)
		self.socket = socket
		self.callbackHandler = callbackHandler
		self.hasClosed = False
		
	def setPackethandler(self,packetHandler):
		self.callbackHandler = packetHandler
		
	def close(self):
		self.socket.close()
		self.hasClosed = True
		
	def send(self,binary):
		length = len(binary)
		binary = struct.pack(">I",length)+binary
		try:
			self.socket.send(binary)
		except:
			pass #this usually happens when a message get send from the shutdown handler and the module close the socket
		#print("Packetsocket sent : ",binary)
		
	def run(self):
		buff = b''
		newpack = True #if false the socket is waiting for a additional packet data to arrive
		packlen = 0 #the lenght of the current packet
		while True:
			if (not newpack) or (len(buff) < 4):
				#get more data
				i=b''
				try:
					i = self.socket.recv(256)
				except:
					pass
				if len(i) == 0:
					self.hasClosed = True
					#print("[PACKETSOCKET]received packet of len 0: exiting listener thread ...")
					self.callbackHandler.onClose(self)
					return
				buff = buff+i
			if newpack and (len(buff)>=4):
				#get packet lenght
				packlen = struct.unpack(">I",buff[:4])[0]
				buff = buff[4:]
				newpack = False
			if (not newpack) and (len(buff) >= packlen):
				#callback
				#print("PacketSocket received: ",buff[:packlen])
				self.callbackHandler.onPacket(buff[:packlen],self)
				buff = buff[packlen:]
				newpack = True
