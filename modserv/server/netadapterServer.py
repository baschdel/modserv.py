from modserv.typeenc import DValue,DSList
import struct
import modserv.portsetupBin as psenc
from modserv.server.interface import Interface

class ServerPacketHandler:
	def __init__(self,module,packetsocket,debug=False):
		self.module = module
		self.packetsocket = packetsocket
		self.debug = debug
		if self.debug:
			print("Debugging mode enabled, will peint incoming packets\nWARNING:They may contain contant, that should stay hidden!")
		
	def onPacket(self,binary,packetsocket):
		inputs = DSList().fromBinary(binary).getList()
		if len(inputs) == 0:
			return
		command = inputs[0].decode('utf8')
		
		if self.debug:
			print("onPacket",command,inputs)
		
		#cmdShutdown(self,cookie,moduleId)
		if (command == "/shutdown" and len(inputs) == 3 and len(inputs[2])==8):
			cookie = inputs[1]
			mid = struct.unpack(">Q",inputs[2])[0]
			self.module.cmdShutdown(cookie,mid)
			return
		#cmdShutdownTree(self,cookie,moduleId)
		if (command == "/shutdownTree" and len(inputs) == 3 and len(inputs[2])==8):
			cookie = inputs[1]
			mid = struct.unpack(">Q",inputs[2])[0]
			self.module.cmdShutdownTree(cookie,mid)
			return
		#cmdMakeChild(self,cookie)
		if (command == "/makeChild" and len(inputs) == 2):
			cookie = inputs[1]
			self.module.cmdMakeChild(cookie)
			return
		#cmdOpenInterface(self,id,Interface)
		if (command == "/openIntf" and len(inputs) == 4 and len(inputs[1])==4 and len(inputs[2])==4):
			ifid = struct.unpack(">I",inputs[1])[0]
			maxconnections = struct.unpack(">i",inputs[2])[0]
			bportsetups = DSList().fromBinary(inputs[3]).getList()
			portsetups = {}
			for binaryPSME in bportsetups: #Binary PortSetup Map Entity
				if len(binaryPSME) > 4:
					ps = psenc.PSfromBinary(binaryPSME[4:])
					num = struct.unpack(">I",binaryPSME[:4])[0]
					if ps is None:
						print("received bad /openIntf from "+self.module.id+" (Couldn't decode Port setup #"+str(num)+")")
					else:
						portsetups[num] = ps
				else:
					print("received bad /openIntf from "+self.module.id+" (Too short BinaryPortSetupMapEntry)")
					#TODO: senderror
					return
			interface = Interface(portsetups,maxconnections)
			self.module.cmdOpenInterface(ifid,interface)
		#cmdCloseInterface(self,id)
		if (command == "/closeIntf" and len(inputs) == 2 and len(inputs[1])==4):
			ifid = struct.unpack(">I",inputs[1])[0]
			self.module.cmdCloseInterface(ifid)
			return
		#cmdOpenConnection(self,cookie,moduleIdA,interfaceIdA,moduleIdB,interfaceIdB)
		if (command == "/openConn" and len(inputs) == 6 and len(inputs[2])==8 and len(inputs[3])==4 and len(inputs[4])==8 and len(inputs[5])==4):
			cookie = inputs[1]
			print(inputs[2])
			midA = struct.unpack(">Q",inputs[2])[0]
			ifidA = struct.unpack(">I",inputs[3])[0]
			midB = struct.unpack(">Q",inputs[4])[0]
			ifidB = struct.unpack(">I",inputs[5])[0]
			self.module.cmdOpenConnection(cookie,midA,ifidA,midB,ifidB)		
			return
		#cmdCall(self,interfaceId,connectionId,portId,functionId,arguments)
		if command=="/call" and len(inputs)==6 and len(inputs[1])==4 and len(inputs[2])==8 and len(inputs[3])==4 and len(inputs[4])==2:
			ifid = struct.unpack(">I",inputs[1])[0]
			cid = struct.unpack(">Q",inputs[2])[0]
			pid = struct.unpack(">I",inputs[3])[0]
			fid = struct.unpack(">H",inputs[4])[0]
			argumentlist = DSList().fromBinary(inputs[5]).getList() #could pass the raw binary here, but this is the easiest way to ensure the binary is a DSList (also makes it easier to add other protocols)
			self.module.cmdCall(ifid,cid,pid,fid,argumentlist)
			return
		#cmdCloseConnection(self,cookie,moduleId,interfaceId,connectionId)
		if command=="/closeConn" and len(inputs)==5 and len(inputs[2])==8 and len(inputs[3])==4 and len(inputs[4])==8:
			cookie = inputs[1]
			mid = struct.unpack(">Q",inputs[2])[0]
			ifid = struct.unpack(">I",inputs[3])[0]
			cid = struct.unpack(">Q",inputs[4])[0]
			self.module.cmdCloseConnection(cookie,mid,ifid,cid)
		#onInitalized
		if (command == "/initalized" and len(inputs) == 1):
			self.module.onModuleInitalized(self.module.id)
			return
		#cmdGetDirectChildren(self,cookie,moduleId)
		if (command == "/getDirChldrn" and len(inputs) == 3 and len(inputs[2])==8):
			cookie = inputs[1]
			mid = struct.unpack(">Q",inputs[2])[0]
			self.module.cmdGetDirectChildren(cookie,mid)
			return
		#cmdGetModuleInfo(self,cookie,moduleId)
		if (command == "/getModInfo" and len(inputs) == 3 and len(inputs[2])==8):
			cookie = inputs[1]
			mid = struct.unpack(">Q",inputs[2])[0]
			self.module.cmdGetModuleInfo(cookie,mid)
			return
		#cmdGetInterfaceInfo(self,cookie,moduleId,interfaceId)
		if (command == "/getIntfInfo" and len(inputs) == 4 and len(inputs[2])==8 and len(inputs[3])==4):
			cookie = inputs[1]
			mid = struct.unpack(">Q",inputs[2])[0]
			ifid = struct.unpack(">I",inputs[3])[0]
			self.module.cmdGetInterfaceInfo(cookie,mid,ifid)
			return 
		#cmdGetConnectionInfo(self,cookie,moduleId,interfaceId,connectionId)
		if (command == "/getConnInfo" and len(inputs) == 5 and len(inputs[2])==8 and len(inputs[3])==4 and len(inputs[4])==8):
			cookie = inputs[1]
			mid = struct.unpack(">Q",inputs[2])[0]
			ifid = struct.unpack(">I",inputs[3])[0]
			cid = struct.unpack(">Q",inputs[4])[0]
			self.module.cmdGetConnectionInfo(cookie,mid,ifid,cid)
			return 
		
	def onClose(self,packetsocket):
		self.module.shutdownTree()



class ServerModuleCallbackHandler:
	
	def __init__(self,packetsocket):
		self.packetsocket = packetsocket
	
	def cbReturnNoError(self,cookie):
		self.packetsocket.send(DSList().setList([b".retNoErr",cookie]).toBinary())
		#self.log("cbReturnSuccess > "+cookie,"CB")
	
	def cbErrorNoWriteAccess(self,cookie):
		self.packetsocket.send(DSList().setList([b".errNoWriteAccs",cookie]).toBinary())
		#self.log("cbErrorNoWriteAccess > "+cookie,"CB")
		
	def cbErrorNoReadAccess(self,cookie):
		self.packetsocket.send(DSList().setList([b".errNoReadAccs",cookie]).toBinary())
		
	def cbChildModuleCreated(self,cookie,id,key):
		mid = struct.pack(">Q",id)
		bkey = key.encode('utf-8')
		print("cbChildModuleCreated",id,bkey)
		self.packetsocket.send(DSList().setList([b".childModCreated",cookie,mid,bkey]).toBinary())
		#self.log("cbChildModuleCreated ("+id+") > "+cookie,"CB")
		
	def cbErrorInterfaceWasAlreadyOpen(self,id):
		ifid = struct.pack(">I",id)
		self.packetsocket.send(DSList().setList([b".errIntfAlrdyOpen",ifid]).toBinary())
		#self.log("cbErrorInterfaceWasAlreadyOpen ("+id+")","CB")
		
	def cbInterfaceOpened(self,id):
		ifid = struct.pack(">I",id)
		self.packetsocket.send(DSList().setList([b".intfOpened",ifid]).toBinary())
		#self.log("cbInterfaceOpened ("+id+")","CB")
		
	def cbErrorInterfaceWasAlreadyClosed(self,id):
		ifid = struct.pack(">I",id)
		self.packetsocket.send(DSList().setList([b".errIntfAlrdyClosed",ifid]).toBinary())
		#self.log("cbErrorInterfaceWasAlreadyClosed ("+id+")","CB")
		
	def cbInterfaceClosed(self,id):
		ifid = struct.pack(">I",id)
		self.packetsocket.send(DSList().setList([b".intfClosed",ifid]).toBinary())
		#self.log("cbInterfaceClosed ("+id+")","CB")
		
	def cbErrorCouldNotOpenConnection(self,cookie,reason):
		#errors:
		#  noModuleA #no access to module A
		#  noModuleB #no access to module B
		#  noInterfaceA #interface A does not exist
		#  noInterfaceB #interface B does not exist
		#  incompatible #the two interfaces are not compatible
		#  connlimitA #connection limit reached on interface A
		#  connlimitB #connection limit reached on interface B
		self.packetsocket.send(DSList().setList([b".errNotOpenConn",cookie,reason.encode('utf-8')]).toBinary())
		#self.log("cbErrorCouldNotOpenConnection ("+reason+") > "+cookie,"CB")
		
	def cbReturnConnectionOpen(self,cookie,connectionIdA,connectionIdB):
		cidA = struct.pack(">Q",connectionIdA)
		cidB = struct.pack(">Q",connectionIdB)
		self.packetsocket.send(DSList().setList([b".retConnOpen",cookie,cidA,cidB]).toBinary())
		#self.log("cbReturnConnectionOpen ("+connectionIdA+"+"+connectionIdB+")")
		
	def cbConnectionOpened(self,interfaceId,connectionId):
		ifid = struct.pack(">I",interfaceId)
		cid = struct.pack(">Q",connectionId)
		self.packetsocket.send(DSList().setList([b".connOpened",ifid,cid]).toBinary())
		#self.log("cbConnectionOpened "+interfaceId+"."+connectionId,"CB")
		
	def cbConnectionClosed(self,interfaceId,connectionId):
		ifid = struct.pack(">I",interfaceId)
		cid = struct.pack(">Q",connectionId)
		self.packetsocket.send(DSList().setList([b".connClosed",ifid,cid]).toBinary())
		#self.log("cbConnectionClosed "+interfaceId+"."+connectionId,"CB")
		
	def cbErrorCallError(self,interfaceId,connectionId,portId,functionId,reason):
		#errors:
		# noInterface #if the interface does not exist
		# noConnection #if the connection does not exist (or is not yet initalized)
		# noPort #if the port isn't open
		# noFunction #if the function does not exist (a module may also close the connection instead)
		# badArgument #if the arguments are not compliant wih the protocol (a module may also close the connection instead)
		ifid = struct.pack(">I",interfaceId)
		cid = struct.pack(">Q",connectionId)
		pid = struct.pack(">I",portId)
		fid = struct.pack(">H",functionId)
		reason = reason.encode('utf-8')
		self.packetsocket.send(DSList().setList([b".errCallErr",ifid,cid,pid,fid,reason]).toBinary())
		#self.log("cbErrorCallError "+interfaceId+"."+connectionId+"."+portId+"."+functionId+" ("+reason+")","CB")
		
	def cbConnectionAlreadyClosed(self,cookie):
		self.packetsocket.send(DSList().setList([b".errconnAlrdyClosed",cookie]).toBinary())
		#self.log("cbConnectionAlreadyClosed > "+cookie,"CB")
		
	def cbParentChanged(self,newParentId,oldParentId):
		npmid = struct.pack(">Q",newParentId)
		opmid = struct.pack(">Q",oldParentId)
		self.packetsocket.send(DSList().setList([b".newParent",npmid,opmid]).toBinary())
		#self.log("cbParentChanged [ "+newParentId+" > "+oldParentId+" ]","CB")
		
	#management
	
	def cbOtherModuleCreated(self,moduleId):
		mid = struct.pack(">Q",moduleId)
		self.packetsocket.send(DSList().setList([b".otrModCreated",mid]).toBinary())
		#self.log("cbOtherModuleCreated "+moduleId,"CB")
	
	def cbOtherModuleShutDown(self,moduleId):
		mid = struct.pack(">Q",moduleId)
		self.packetsocket.send(DSList().setList([b".otrModShutDown",mid]).toBinary())
		#self.log("cbOtherModuleShutDown "+moduleId,"CB")
		
	def cbOtherModuleInitalized(self,moduleId):
		mid = struct.pack(">Q",moduleId)
		self.packetsocket.send(DSList().setList([b".otrModInitalized",mid]).toBinary())
		#self.log("cbOtherModuleInitalized "+moduleId,"CB")
		
	def cbOtherInterfaceOpened(self,moduleId,interfaceId):
		mid = struct.pack(">Q",moduleId)
		ifid = struct.pack(">I",interfaceId)
		self.packetsocket.send(DSList().setList([b".otrIntfOpened",mid,ifid]).toBinary())
		#self.log("cbOtherInterfaceOpened "+moduleId+"."+interfaceId,"CB")
		
	def cbOtherInterfaceClosed(self,moduleId,interfaceId):
		mid = struct.pack(">Q",moduleId)
		ifid = struct.pack(">I",interfaceId)
		self.packetsocket.send(DSList().setList([b".otrIntfClosed",mid,ifid]).toBinary())
		#self.log("cbOtherInterfaceClosed "+moduleId+"."+interfaceId,"CB")
		
	def cbOtherConnectionOpened(self,moduleId,interfaceId,connectionId):
		mid = struct.pack(">Q",moduleId)
		ifid = struct.pack(">I",interfaceId)
		cid = struct.pack(">Q",connectionId)
		self.packetsocket.send(DSList().setList([b".otrConnOpened",mid,ifid,cid]).toBinary())
		#self.log("cbOtherConnectionOpened "+moduleId+"."+interfaceId+"."+connectionId,"CB")
		
	def cbOtherConnectionClosed(self,moduleId,interfaceId,connectionId):
		mid = struct.pack(">Q",moduleId)
		ifid = struct.pack(">I",interfaceId)
		cid = struct.pack(">Q",connectionId)
		self.packetsocket.send(DSList().setList([b".otrConnClosed",mid,ifid,cid]).toBinary())
		#self.log("cbOtherConnectionClosed "+moduleId+"."+interfaceId+"."+connectionId,"CB")
		
	def cbOtherParentChanged(self,moduleId,newParentId,oldParentId):
		mid = struct.pack(">Q",moduleId)
		npmid = struct.pack(">Q",newParentId)
		opmid = struct.pack(">Q",oldParentId)
		self.packetsocket.send(DSList().setList([b".otrNewParent",mid,npmid,opmid]).toBinary())
		#self.log("cbParentChanged "+moduleId+" [ "+newParentId+" > "+oldParentId+" ]","CB")
	
	#selfmanagement
		
	def cbSelfShutdown(self):
		self.packetsocket.send(DSList().setList([b".shutdown"]).toBinary())
		self.packetsocket.close()
		#self.log("cbSelfShutdown","CB")
		
	#calls
	
	def cbCalled(self,interfaceId,connectionId,portId,functionId,arguments):
		ifid = struct.pack(">I",interfaceId)
		cid = struct.pack(">Q",connectionId)
		pid = struct.pack(">I",portId)
		fid = struct.pack(">H",functionId)
		arguments = DSList().setList(arguments).toBinary()
		self.packetsocket.send(DSList().setList([b".call",ifid,cid,pid,fid,arguments]).toBinary())
		#self.log("cbCalled "+interfaceId+"."+connectionId+"."+portId+"."+functionId+" ("+arguments+")","CB")
		
	def cbRetDirectCildren(self,cookie,moduleId,listOfDirectChildren):
		mid = struct.pack(">Q",moduleId)
		binaryChildrenList = b''
		for id in listOfDirectChildren:
			binaryChildrenList=binaryChildrenList+(struct.pack(">Q",id))
		self.packetsocket.send(DSList().setList([b".retDirChldrn",cookie,mid,binaryChildrenList]).toBinary())
		#self.log("cbRetDirectCildren "+str(moduleId)+":"+str(listOfDirectChildren)+" > "+cookie)
		
	def cbRetModuleInfo(self,cookie,moduleId,parentId,listOfOpenInterfaces):
		mid = struct.pack(">Q",moduleId)
		pmid = struct.pack(">Q",parentId)
		binaryListOfOpenInterfaces = b''
		for id in listOfOpenInterfaces:
			binaryListOfOpenInterfaces=binaryListOfOpenInterfaces+(struct.pack(">I",id))
		self.packetsocket.send(DSList().setList([b".retModInfo",cookie,mid,pmid,binaryListOfOpenInterfaces]).toBinary())
		#self.log("cbRetModuleInfo "+str(moduleId)+":"+str(listOfOpenInterfaces)+" parent:"+str(parentId)+" > "+cookie)
		
	def cbRetInterfaceInfo(self,cookie,moduleId,interfaceId,maxconnections,listOfOpenConnections,mapOfPortsetups):
		mid = struct.pack(">Q",moduleId)
		ifid = struct.pack(">I",interfaceId)
		maxconn = struct.pack(">i",maxconnections)
		binaryListOfOpenConnections = b''
		for id in listOfOpenConnections:
			binaryListOfOpenConnections=binaryListOfOpenConnections+struct.pack(">Q",id)
		bportsetups = []
		for num,ps in mapOfPortsetups.items():
			num = struct.pack(">I",num)
			bps = psenc.PStoBinary(ps)
			bportsetups.append(num+bps)
		self.packetsocket.send(DSList().setList([b".retIntfInfo",cookie,mid,ifid,maxconn,binaryListOfOpenConnections,DSList().setList(bportsetups).toBinary()]).toBinary())
		#self.log("cbRetInterfaceInfo "+str(moduleId)+"."+str(interfaceId)+":"+str(mapOfPortsetups)+" #Connections:"+str(listOfOpenConnections)+" maxconnections:"+str(maxconnections)+" > "+cookie)
		
	def cbRetConnectionInfo(self,cookie,moduleId,interfaceId,connectionId,peerModuleId,peerInterfaceId,peerConnectionId):
		mid = struct.pack(">Q",moduleId)
		ifid = struct.pack(">I",interfaceId)
		cid = struct.pack(">Q",connectionId)
		pmid = struct.pack(">Q",peerModuleId)
		pifid = struct.pack(">I",peerInterfaceId)
		pcid = struct.pack(">Q",peerConnectionId)
		self.packetsocket.send(DSList().setList([b".retConnInfo",cookie,mid,ifid,cid,pmid,pifid,pcid]).toBinary())
		#self.log("cbRetInterfaceInfo "+str(moduleId)+"."+str(interfaceId)+"."+str(connectionId)+":"+str(peerModuleId)+"."+str(peerInterfaceId)+"."+str(peerConnectionId)+" > "+cookie)
		
