#DEPRECATED do not use (this connect function not the file)
'''
def connect(moduleA,interfaceIdA,moduleB,interfaceIdB):

	interfaceA = moduleA.interfaces[interfaceIdA]
	interfaceB = moduleB.interfaces[interfaceIdB]
	#check compatibility
	if not interfaceA.isCompatibleWith(interfaceB):
		return False
	#reserve connections
	if interfaceA.reserveConnection():
		if interfaceB.reserveConnection():
			#reverved connections successfully
			#get connection ids
			connection = Connection(moduleA,interfaceIdA,moduleB,interfaceIdB)
			cidA = moduleA.putConnection()
			cidB = moduleB.putConnection()
			connection.setConnectionIds(cidA,cidB)
			moduleA.onConnectionOpened(moduleA.id,interfaceIdA,cidA)
			moduleB.onConnectionOpened(moduleB.id,interfaceIdB,cidB)
			return True
		else:
			interfaceA.freeConnection()
			return False
	else:
		return False
'''
class Connection:
	def __init__(self,moduleA,interfaceIdA,moduleB,interfaceIdB):
		self.moduleA = moduleA
		self.interfaceIdA = interfaceIdA
		self.connectionIdA = None
		self.moduleB = moduleB
		self.interfaceIdB = interfaceIdB
		self.connectionIdB = None
		
	def setConnectionIds(self,connectionIdA,connectionIdB):
		self.connectionIdA = connectionIdA
		self.connectionIdB = connectionIdB
		
	def call(self,caller,portId,functionId,arguments):
		if self.interfaceIdA is None or self.interfaceIdB is None:
			return False
		if caller == self.moduleA:
			self.moduleB.cbCalled(self.interfaceIdB,self.connectionIdB,portId,functionId,arguments)
		if caller == self.moduleB:
			self.moduleA.cbCalled(self.interfaceIdA,self.connectionIdA,portId,functionId,arguments)
		return True
	
	def getInterfaceId(self,caller):
		if caller == self.moduleA:
			return self.interfaceIdA
		if caller == self.moduleB:
			return self.interfaceIdB

	def getPeerInterface(self,caller):
		if caller == self.moduleA:
			return self.moduleB.getInterface(self.interfaceIdB)
		if caller == self.moduleB:
			return self.moduleA.getInterface(self.interfaceIdA)
			
	def close(self):
		interfaceA = self.moduleA.interfaces[self.interfaceIdA]
		interfaceB = self.moduleB.interfaces[self.interfaceIdB]
		self.moduleA.removeConnection(self.connectionIdA,self.interfaceIdA)
		self.moduleB.removeConnection(self.connectionIdB,self.interfaceIdB)
		self.connectionIdA = None
		self.connectionIdB = None
		interfaceA.freeConnection()
		interfaceB.freeConnection()
