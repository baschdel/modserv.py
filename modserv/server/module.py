import modserv.typeenc as typeenc
import modserv.portsetup as portsetup
from modserv.server.connection import Connection
import uuid
from modserv.server.interface import Interface

def generateKey():
	return uuid.uuid4().hex
	
class ModuleRegistry:
	def __init__(self):
		self.modules={}
		
	def putmodule(self,module):
		i=1 #module Id 0 is reserved for non existing modules
		while True :
			if not (i in self.modules):
				self.modules[i] = module
				return i
			i=i+1
			
	def getAllModules(self):
		return self.modules
		
	def removeModule(self,id):
		if id in self.modules:
			del self.modules[id]
			
	def getModule(self,id):
		if id == 0:
			return None
		if id in self.modules:
			return self.modules[id]
		else:
		 return None
			
#pkgcallback(command,arglist)
class Module:
	def __init__(self,module_registry,parent=None):
		self.handler = None
		self.running = False
		self.connected = False #To prevent multiple logins
		self.interfaces = {}
		self.connections = {}
		self.children = []
		self.registry = module_registry
		self.parent = parent
		self.key = generateKey()
		self.id = module_registry.putmodule(self)
		self.onModuleCreated(self.id)
		self.log("created!")
		
	#####################
	## Internals calls ##
	#####################
	
	#also returns true for grand- ,gandgrandparents and so on
	def isParentOf(self,module):
		if module is None:
			return False
		while True:
			if module.parent is None:
				return False
			if module.parent == self:
				return True
			module = module.parent
			
	def getDirectChildren(self):
		return self.children
		
	def getAllChildren(self):
		allchildren = getDirectChildren()
		for child in getDirectChildren():
			allchildren = allchildren+child.getAllChildren()
		return allchildren
		
	def getChildById(self,id):
		module = module_registry.getModule(id)
		if self.isParentOf(module):
			return module
		else:
			return None
	
	def getDirectChild(self,id):
		for child in self.children:
			if child.id == id:
				return child
		return None
		
	def shutdown(self):
		self.registry.removeModule(self.id)
		for connection in list(self.connections.values()):
			connection.close()
		parentId = 0
		if not self.parent is None:
			parentId = self.parent.id
		self.onModuleShutdown(self.id)
	
	#shutdown the module and all of its children
	def shutdownTree(self):
		for child in self.getDirectChildren():
			#print(child)
			child.shutdownTree()
		self.shutdown()
	
	#also inludes full read Access
	def hasWriteAccessToModule(self,moduleId):
		return self.id == moduleId or self.isParentOf(self.registry.getModule(moduleId))
	
	def hasReadAccessToModule(self,moduleId):
		return self.id == moduleId or self.isParentOf(self.registry.getModule(moduleId))
	
	def putConnection(self,connection,interfaceId):
		i=0
		while True :
			if not (i in self.connections):
				self.connections[i] = connection
				return i
			i=i+1
	
	def removeConnection(self,connectionId,interfaceId):
		if connectionId in self.connections:
			del self.connections[connectionId]
			self.onConnectionClosed(self.id,interfaceId,connectionId)
	
	def getInterface(self,interfaceId):
		if interfaceId in self.interfaces:
			return self.interfaces[interfaceId]
		else:
			return None
	
	def log(self,message,channel="LOG"):
		print("[MODULE]["+str(self.id)+"]:"+message)
	
	def setModuleCallbackHandler(self,handler):
		self.handler = handler
	
	####################
	## Event Handlers ##
	####################
	
	def onModuleCreated(self,moduleId):
		if type(self.parent) is Module:
			self.parent.onModuleCreated(moduleId)
		if moduleId != self.id:
			self.cbOtherModuleCreated(moduleId)
			
	#gets called by network
	def onModuleConnect(self):
		self.connected=True
		self.log("CONNECTED!")
		
	#gets called by network
	def onModuleInitalized(self,moduleId):
		if type(self.parent) is Module:
			self.parent.onModuleInitalized(moduleId)
		if moduleId != self.id:
			self.cbOtherModuleInitalized(moduleId)
		else:
			self.log("initalized!")
			#raise Exception("Brakpoint")
			self.running = True
	
	def onModuleShutdown(self,moduleId):
		parentId = 0
		if type(self.parent) is Module:
			self.parent.onModuleShutdown(moduleId)
			parentId = self.parent.id
		if moduleId == self.id:
			for child in self.children:
				child.parent = self.parent
				child.onParentChanged(child.id,self.id,parentId)
			self.cbSelfShutdown()
			self.log("shutdown!")
		else:
			self.cbOtherModuleShutDown(moduleId)
			module = self.getDirectChild(moduleId)
			if not (module is None):
				self.children.remove(module)
	
	#old function in case the above one gets messed up	
	#def onModuleShutdown(self,moduleId):
	#	if type(self.parent) is Module:
	#		self.parent.onModuleShutdown(moduleId)
	#	parentId = 0
	#	if not self.parent is None:
	#		parentId = self.parent.id
	#	if moduleId == self.id:
	#		for child in self.children:
	#			child.parent = self.parent
	#			child.onParentChanged(child.id,self.id,parentId)
	#		self.cbSelfShutdown()
	#		self.log("shutdown!")
	#	else:
	#		self.cbOtherModuleShutDown(moduleId)
	#		module = self.getDirectChild(moduleId)
	#		if module is Module:
	#			self.children.remove(module)
		
	def onInterfaceOpened(self,moduleId,interfaceId):
		if type(self.parent) is Module:
			self.parent.onInterfaceOpened(moduleId,interfaceId)
		if moduleId == self.id:
			self.cbInterfaceOpened(interfaceId)
		else:
			self.cbOtherInterfaceOpened(moduleId,interfaceId)
			
	def onInterfaceClosed(self,moduleId,interfaceId):
		if type(self.parent) is Module:
			self.parent.onInterfaceClosed(moduleId,interfaceId)
		if moduleId == self.id:
			self.cbInterfaceClosed(interfaceId)
		else:
			self.cbOtherInterfaceClosed(moduleId,interfaceId)
		
	def onConnectionOpened(self,moduleId,interfaceId,connectionId):
		if self.parent is Module:
			self.parent.onConnectionOpened(moduleId,interfaceId,connectionId)
		if moduleId == self.id:
			self.cbConnectionOpened(interfaceId,connectionId)
		else:
			self.cbOtherConnectionOpened(moduleId,interfaceId,connectionId)
			
	def onConnectionClosed(self,moduleId,interfaceId,connectionId):
		if self.parent is Module:
			self.parent.cbOtherConnectionClosed(moduleId,interfaceId,connectionId)
		if moduleId == self.id:
			self.cbConnectionClosed(interfaceId,connectionId)
		else:
			self.cbOtherConnectionClosed(moduleId,interfaceId,connectionId)
		
	def onParentChanged(self,moduleId,newParentId,oldParentId):
		if type(self.parent) is Module:	
			self.parent.onParentChanged(moduleId,newParentId,oldParentId)
		if moduleId == self.id:
			self.cbParentChanged(newParentId,oldParentId)
		else:
			self.cbOtherParentChanged(moduleId,newParentId,oldParentId)
		
	##################
	## Remote Calls ##
	##################
	
	def cmdShutdown(self,cookie,moduleId):
		if self.hasWriteAccessToModule(moduleId):
			self.registry.getModule(moduleId).shutdown()
			self.cbReturnNoError(cookie)
		else:
			self.cbErrorNoWriteAccess(cookie)
			
	def cmdShutdownTree(self,cookie,moduleId):
		if self.hasWriteAccessToModule(moduleId):
			self.registry.getModule(moduleId).shutdownTree()
			self.cbReturnNoError(cookie)
		else:
			self.cbErrorNoWriteAccess(cookie)
			
	def cmdMakeChild(self,cookie):
		child = Module(self.registry,parent=self)
		print("Created Module "+str(child.id)+" / "+str(child.key))
		self.children.append(child)
		self.cbChildModuleCreated(cookie,child.id,child.key)
			
	def cmdOpenInterface(self,ifid,interface):
		if ifid in self.interfaces:
			self.cbErrorInterfaceWasAlreadyOpen(ifid)
			return
		self.log("Opening interface "+str(ifid)+" "+str(self.interfaces))
		self.interfaces[ifid] = interface
		self.onInterfaceOpened(self.id,ifid)
	
	def cmdCloseInterface(self,ifid):
		if not (ifid in self.interfaces):
			self.cbErrorInterfaceWasAlreadyClosed(ifid)
			return
		self.log("Closing interface "+str(ifid)+" "+str(self.interfaces))
		#close connections
		for cid,connection in self.connections.items():
			if connection.getInterfaceId(self) == ifid:
				connection.close()
		del self.interfaces[ifid]
		self.onInterfaceClosed(self.id,ifid)
		
	def cmdOpenConnection(self,cookie,moduleIdA,interfaceIdA,moduleIdB,interfaceIdB):
		moduleA = self.registry.getModule(moduleIdA)
		moduleB = self.registry.getModule(moduleIdB)
		if (moduleA is None) or (not self.hasWriteAccessToModule(moduleIdA)):
			self.cbErrorCouldNotOpenConnection(cookie,"noModuleA")
			return
		if (moduleB is None) or (not self.hasWriteAccessToModule(moduleIdB)):
			self.cbErrorCouldNotOpenConnection(cookie,"noModuleB")
			return
		if not interfaceIdA in moduleA.interfaces:
			self.cbErrorCouldNotOpenConnection(cookie,"noInterfaceA")
			return
		if not interfaceIdB in moduleB.interfaces:
			self.cbErrorCouldNotOpenConnection(cookie,"noInterfaceB")
			return
		interfaceA = moduleA.interfaces[interfaceIdA]
		interfaceB = moduleB.interfaces[interfaceIdB]
		#check compatibility
		if not interfaceA.isCompatibleWith(interfaceB):
			self.cbErrorCouldNotOpenConnection(cookie,"incompatible")
			return
		#reserve connections
		if interfaceA.reserveConnection():
			if interfaceB.reserveConnection():
				#reverved connections successfully
				#get connection ids
				connection = Connection(moduleA,interfaceIdA,moduleB,interfaceIdB)
				cidA = moduleA.putConnection(connection,interfaceIdA)
				cidB = moduleB.putConnection(connection,interfaceIdB)
				connection.setConnectionIds(cidA,cidB)
				moduleA.onConnectionOpened(moduleA.id,interfaceIdA,cidA)
				moduleB.onConnectionOpened(moduleB.id,interfaceIdB,cidB)
				self.cbReturnConnectionOpen(cookie,cidA,cidB)
				return True
			else:
				interfaceA.freeConnection()
				self.cbErrorCouldNotOpenConnection(cookie,"connlimitB")
				return
		else:
			self.cbErrorCouldNotOpenConnection(cookie,"connlimitA")
			return
		
	def cmdCall(self,interfaceId,connectionId,portId,functionId,arguments):
		if not interfaceId in self.interfaces:
			self.cbErrorCallError(interfaceId,connectionId,portId,functionId,"noInterface")
			return
		if not connectionId in self.connections:
			self.cbErrorCallError(interfaceId,connectionId,portId,functionId,"noConnection")
			return
		connection = self.connections[connectionId]
		interface = self.interfaces[interfaceId]
		if connection.getInterfaceId(self) != interfaceId:
			self.cbErrorCallError(interfaceId,connectionId,portId,functionId,"noConnection")
			return
		peerInterface = connection.getPeerInterface(self)
		if not (interface.isPortOpen(portId) and (not peerInterface is None) and peerInterface.isPortOpen(portId)):
			self.cbErrorCallError(interfaceId,connectionId,portId,functionId,"noPort")
			return
		#functionId and arguments are checked client side
		connection.call(self,portId,functionId,arguments)
		
	def cmdCloseConnection(self,cookie,moduleId,interfaceId,connectionId):
		if not self.hasWriteAccessToModule(moduleId):
			self.cbErrorNoWriteAccess(cookie)
			return
		module = self.registry.getModule(moduleId)
		if not interfaceId in module.interfaces:
			self.cbConnectionAlreadyClosed(cookie)
			return
		if not connectionId in module.connections:
			self.cbConnectionAlreadyClosed(cookie)
			return
		connection = module.connections[connectionId]
		interface = module.interfaces[interfaceId]
		if connection.getInterfaceId(module) != interfaceId:
			self.cbConnectionAlreadyClosed(cookie)
			return
		connection.close()
		self.cbReturnNoError(cookie)
		
	def cmdGetDirectChildren(self,cookie,moduleId):
		if not self.hasReadAccessToModule(moduleId):
			self.cbErrorNoReadAccess(cookie)
			return
		ret = []
		for child in self.registry.getModule(moduleId).getDirectChildren():
			ret.append(child.id)
		self.cbRetDirectCildren(cookie,moduleId,ret)
		
	def cmdGetModuleInfo(self,cookie,moduleId):
		if not self.hasReadAccessToModule(moduleId):
			self.cbErrorNoReadAccess(cookie)
			return
		module = self.registry.getModule(moduleId)
		listOfOpenInterfaces = []
		for id,interface in module.interfaces.items():
			listOfOpenInterfaces.append(id)
		parentId = 0
		if not module.parent is None:
			parentId = module.parent.id
		self.cbRetModuleInfo(cookie,moduleId,parentId,listOfOpenInterfaces)
		
	def cmdGetInterfaceInfo(self,cookie,moduleId,interfaceId):
		if not self.hasReadAccessToModule(moduleId):
			self.cbErrorNoReadAccess(cookie)
			return
		module = self.registry.getModule(moduleId)
		if not interfaceId in module.interfaces:
			self.cbErrorNoReadAccess(cookie)
			return
		interface = module.interfaces[interfaceId]
		maxconnections = interface.maxconnections
		openConnections = []
		for id,connection in module.connections.items():
			if connection.getInterfaceId(module) == interfaceId:
				openConnections.append(id)
		mapOfPortsetups = interface.portsetups
		self.cbRetInterfaceInfo(cookie,moduleId,interfaceId,maxconnections,openConnections,mapOfPortsetups)
		
	def cmdGetConnectionInfo(self,cookie,moduleId,interfaceId,connectionId):
		if not self.hasReadAccessToModule(moduleId):
			self.cbErrorNoReadAccess(cookie)
			return
		module = self.registry.getModule(moduleId)
		if not interfaceId in module.interfaces:
			self.cbErrorNoReadAccess(cookie)
			return
		interface = module.interfaces[interfaceId]
		if not connectionId in module.connections:
			self.cbErrorNoReadAccess(cookie)
			return
		connection = module.connections[connectionId]
		if connection.moduleA == module and connection.interfaceIdA == interfaceId and connection.connectionIdA == connectionId:
			peerModuleId = connection.moduleB.id
			peerInterfaceId = connection.interfaceIdB
			peerConnectionId = connection.connectionIdB
		elif connection.moduleB == module and connection.interfaceIdB == interfaceId and connection.connectionIdB == connectionId:
			peerModuleId = connection.moduleA.id
			peerInterfaceId = connection.interfaceIdA
			peerConnectionId = connection.connectionIdA
		else: #invalid connection was requested
			self.cbErrorNoReadAccess(cookie)
			return
		self.cbRetConnectionInfo(cookie,moduleId,interfaceId,connectionId,peerModuleId,peerInterfaceId,peerConnectionId)
	######################
	## Remote Callbacks ##
	######################
	
	#return
	
	def cbReturnNoError(self,cookie):
		if not (self.handler is None):
			self.handler.cbReturnNoError(cookie)
		#self.log("cbReturnSuccess > "+cookie,"CB")
	
	def cbErrorNoWriteAccess(self,cookie):
		if not (self.handler is None):
			self.handler.cbErrorNoWriteAccess(cookie)
		#self.log("cbErrorNoWriteAccess > "+cookie,"CB")
	
	def cbErrorNoReadAccess(self,cookie):
		if not (self.handler is None):
			self.handler.cbErrorNoReadAccess(cookie)
		#self.log("cbErrorNoWriteAccess > "+cookie,"CB")
	
	def cbChildModuleCreated(self,cookie,id,key):
		if not (self.handler is None):
			self.handler.cbChildModuleCreated(cookie,id,key)
		#self.log("cbChildModuleCreated ("+id+") > "+cookie,"CB")
		
	def cbErrorInterfaceWasAlreadyOpen(self,id):
		if not (self.handler is None):
			self.handler.cbErrorInterfaceWasAlreadyOpen(id)
		#self.log("cbErrorInterfaceWasAlreadyOpen ("+id+")","CB")
		
	def cbInterfaceOpened(self,id):
		if not (self.handler is None):
			self.handler.cbInterfaceOpened(id)
		#self.log("cbInterfaceOpened ("+id+")","CB")
		
	def cbErrorInterfaceWasAlreadyClosed(self,id):
		if not (self.handler is None):
			self.handler.cbErrorInterfaceWasAlreadyClosed(id)
		#self.log("cbErrorInterfaceWasAlreadyClosed ("+id+")","CB")
		
	def cbInterfaceClosed(self,id):
		if not (self.handler is None):
			self.handler.cbInterfaceClosed(id)
		#self.log("cbInterfaceClosed ("+id+")","CB")
		
	def cbErrorCouldNotOpenConnection(self,cookie,reason):
		#errors:
		#  noModuleA #no access to module A
		#  noModuleB #no access to module B
		#  noInterfaceA #interface A does not exist
		#  noInterfaceB #interface B does not exist
		#  incompatible #the two interfaces are not compatible
		#  connlimitA #connection limit reached on interface A
		#  connlimitB #connection limit reached on interface B
		if not (self.handler is None):
			self.handler.cbErrorCouldNotOpenConnection(cookie,reason)
		#self.log("cbErrorCouldNotOpenConnection ("+reason+") > "+cookie,"CB")
		
	def cbReturnConnectionOpen(self,cookie,connectionIdA,connectionIdB):
		if not (self.handler is None):
			self.handler.cbReturnConnectionOpen(cookie,connectionIdA,connectionIdB)
		#self.log("cbReturnConnectionOpen ("+connectionIdA+"+"+connectionIdB+")")
		
	def cbConnectionOpened(self,interfaceId,connectionId):
		if not (self.handler is None):
			self.handler.cbConnectionOpened(interfaceId,connectionId)
		#self.log("cbConnectionOpened "+interfaceId+"."+connectionId,"CB")
		
	def cbConnectionClosed(self,interfaceId,connectionId):
		if not (self.handler is None):
			self.handler.cbConnectionClosed(interfaceId,connectionId)
		#self.log("cbConnectionClosed "+interfaceId+"."+connectionId,"CB")
		
	def cbErrorCallError(self,interfaceId,connectionId,portId,functionId,reason):
		#errors:
		# noInterface #if the interface does not exist
		# noConnection #if the connection does not exist (or is not yet initalized)
		# noPort #if the port isn't open
		# noFunction #if the function does not exist (a module may also close the connection instead)
		# badArgument #if the arguments are not compliant wih the protocol (a module may also close the connection instead)
		if not (self.handler is None):
			self.handler.cbErrorCallError(interfaceId,connectionId,portId,functionId,reason)
		#self.log("cbErrorCallError "+interfaceId+"."+connectionId+"."+portId+"."+functionId+" ("+reason+")","CB")
		
	def cbConnectionAlreadyClosed(self,cookie):
		if not (self.handler is None):
			self.handler.cbConnectionAlreadyClosed(cookie)
		#self.log("cbConnectionAlreadyClosed > "+cookie,"CB")
		
	def cbParentChanged(self,newParentId,oldParentId):
		if not (self.handler is None):
			self.handler.cbParentChanged(newParentId,oldParentId)
		#self.log("cbParentChanged [ "+newParentId+" > "+oldParentId+" ]","CB")
		
	#management
	
	def cbOtherModuleCreated(self,moduleId):
		if not (self.handler is None):
			self.handler.cbOtherModuleCreated(moduleId)
		#self.log("cbOtherModuleCreated "+moduleId,"CB")
	
	def cbOtherModuleShutDown(self,moduleId):
		if not (self.handler is None):
			self.handler.cbOtherModuleShutDown(moduleId)
		#self.log("cbOtherModuleShutDown "+moduleId,"CB")
		
	def cbOtherModuleInitalized(self,moduleId):
		if not (self.handler is None):
			self.handler.cbOtherModuleInitalized(moduleId)
		#self.log("cbOtherModuleInitalized "+moduleId,"CB")
		
	def cbOtherInterfaceOpened(self,moduleId,interfaceId):
		if not (self.handler is None):
			self.handler.cbOtherInterfaceOpened(moduleId,interfaceId)
		#self.log("cbOtherInterfaceOpened "+moduleId+"."+interfaceId,"CB")
		
	def cbOtherInterfaceClosed(self,moduleId,interfaceId):
		if not (self.handler is None):
			self.handler.cbOtherInterfaceClosed(moduleId,interfaceId)
		#self.log("cbOtherInterfaceClosed "+moduleId+"."+interfaceId,"CB")
		
	def cbOtherConnectionOpened(self,moduleId,interfaceId,connectionId):
		if not (self.handler is None):
			self.handler.cbOtherConnectionOpened(moduleId,interfaceId,connectionId)
		#self.log("cbOtherConnectionOpened "+moduleId+"."+interfaceId+"."+connectionId,"CB")
		
	def cbOtherConnectionClosed(self,moduleId,interfaceId,connectionId):
		if not (self.handler is None):
			self.handler.cbOtherConnectionClosed(moduleId,interfaceId,connectionId)
		#self.log("cbOtherConnectionClosed "+moduleId+"."+interfaceId+"."+connectionId,"CB")
		
	def cbOtherParentChanged(self,moduleId,newParentId,oldParentId):
		if not (self.handler is None):
			self.handler.cbOtherParentChanged(moduleId,newParentId,oldParentId)
		#self.log("cbParentChanged "+moduleId+" [ "+newParentId+" > "+oldParentId+" ]","CB")
	
	#selfmanagement
		
	def cbSelfShutdown(self):
		if not (self.handler is None):
			self.handler.cbSelfShutdown()
		#self.log("cbSelfShutdown","CB")
		
	#calls
	
	def cbCalled(self,interfaceId,connectionId,portId,functionId,arguments):
		if not (self.handler is None):
			self.handler.cbCalled(interfaceId,connectionId,portId,functionId,arguments)
		#self.log("cbCalled "+interfaceId+"."+connectionId+"."+portId+"."+functionId+" ("+arguments+")","CB")

	#getter returns
	
	def cbRetDirectCildren(self,cookie,moduleId,listOfDirectChildren):
		if not (self.handler is None):
			self.handler.cbRetDirectCildren(cookie,moduleId,listOfDirectChildren)
	
	def cbRetModuleInfo(self,cookie,moduleId,parentId,listOfOpenInterfaces):
		if not (self.handler is None):
			self.handler.cbRetModuleInfo(cookie,moduleId,parentId,listOfOpenInterfaces)
			
	def cbRetInterfaceInfo(self,cookie,moduleId,interfaceId,maxconnections,listOfOpenConnections,mapOfPortsetups):
		if not (self.handler is None):
				self.handler.cbRetInterfaceInfo(cookie,moduleId,interfaceId,maxconnections,listOfOpenConnections,mapOfPortsetups)
			
	def cbRetConnectionInfo(self,cookie,moduleId,interfaceId,connectionId,peerModuleId,peerInterfaceId,peerConnectionId):
		if not (self.handler is None):
				self.handler.cbRetConnectionInfo(cookie,moduleId,interfaceId,connectionId,peerModuleId,peerInterfaceId,peerConnectionId)
##################################################################################
##################################################################################


class DummyModuleCallbackHandler:
	
	def cbReturnNoError(self,cookie):
		self.log("cbReturnSuccess > "+cookie,"CB")
	
	def cbErrorNoWriteAccess(self,cookie):
		self.log("cbErrorNoWriteAccess > "+cookie,"CB")
		
	def cbErrorNoReadAccess(self,cookie):
		self.log("cbErrorNoReadAccess > "+cookie,"CB")
		
	def cbChildModuleCreated(self,cookie,id):
		self.log("cbChildModuleCreated ("+id+") > "+cookie,"CB")
		
	def cbErrorInterfaceWasAlreadyOpen(self,id):
		self.log("cbErrorInterfaceWasAlreadyOpen ("+id+")","CB")
		
	def cbInterfaceOpened(self,id):
		self.log("cbInterfaceOpened ("+id+")","CB")
		
	def cbErrorInterfaceWasAlreadyClosed(self,id):
		self.log("cbErrorInterfaceWasAlreadyClosed ("+id+")","CB")
		
	def cbInterfaceClosed(self,id):
		self.log("cbInterfaceClosed ("+id+")","CB")
		
	def cbErrorCouldNotOpenConnection(self,cookie,reason):
		#errors:
		#  noModuleA #no access to module A
		#  noModuleB #no access to module B
		#  noInterfaceA #interface A does not exist
		#  noInterfaceB #interface B does not exist
		#  incompatible #the two interfaces are not compatible
		#  connlimitA #connection limit reached on interface A
		#  connlimitB #connection limit reached on interface B
		self.log("cbErrorCouldNotOpenConnection ("+reason+") > "+cookie,"CB")
		
	def cbReturnConnectionOpen(self,cookie,connectionIdA,connectionIdB):
		self.log("cbReturnConnectionOpen ("+connectionIdA+"+"+connectionIdB+")")
		
	def cbConnectionOpened(self,interfaceId,connectionId):
		self.log("cbConnectionOpened "+interfaceId+"."+connectionId,"CB")
		
	def cbConnectionClosed(self,interfaceId,connectionId):
		self.log("cbConnectionClosed "+interfaceId+"."+connectionId,"CB")
		
	def cbErrorCallError(self,interfaceId,connectionId,portId,functionId,reason):
		#errors:
		# noInterface #if the interface does not exist
		# noConnection #if the connection does not exist (or is not yet initalized)
		# noPort #if the port isn't open
		# noFunction #if the function does not exist (a module may also close the connection instead)
		# badArgument #if the arguments are not compliant wih the protocol (a module may also close the connection instead)
		self.log("cbErrorCallError "+interfaceId+"."+connectionId+"."+portId+"."+functionId+" ("+reason+")","CB")
		
	def cbConnectionAlreadyClosed(self,cookie):
		self.log("cbConnectionAlreadyClosed > "+cookie,"CB")
		
	def cbParentChanged(self,moduleId,newParentId,oldParentId):
		self.log("cbParentChanged "+moduleId+" [ "+newParentId+" > "+oldParentId+" ]","CB")
		
	#management
	
	def cbOtherModuleCreated(self,moduleId):
		self.log("cbOtherModuleCreated "+moduleId,"CB")
	
	def cbOtherModuleShutDown(self,moduleId):
		self.log("cbOtherModuleShutDown "+moduleId,"CB")
		
	def cbOtherModuleInitalized(self,moduleId):
		self.log("cbOtherModuleInitalized "+moduleId,"CB")
		
	def cbOtherInterfaceOpened(self,moduleId,interfaceId):
		self.log("cbOtherInterfaceOpened "+moduleId+"."+interfaceId,"CB")
		
	def cbOtherInterfaceClosed(self,moduleId,interfaceId):
		self.log("cbOtherInterfaceClosed "+moduleId+"."+interfaceId,"CB")
		
	def cbOtherConnectionOpened(self,moduleId,interfaceId,connectionId):
		self.log("cbOtherConnectionOpened "+moduleId+"."+interfaceId+"."+connectionId,"CB")
		
	def cbOtherConnectionClosed(self,moduleId,interfaceId,connectionId):
		self.log("cbOtherConnectionClosed "+moduleId+"."+interfaceId+"."+connectionId,"CB")
		
	def cbOtherParentChanged(self,moduleId,newParentId,oldParentId):
		self.log("cbParentChanged "+moduleId+" [ "+newParentId+" > "+oldParentId+" ]","CB")
	
	#selfmanagement
		
	def cbSelfShutdown(self):
		self.log("cbSelfShutdown","CB")
		
	#calls
	
	def cbCalled(self,interfaceId,connectionId,portId,functionId,arguments):
		self.log("cbCalled "+interfaceId+"."+connectionId+"."+portId+"."+functionId+" ("+arguments+")","CB")
		
	#getter returns
	
	def cbRetDirectCildren(self,cookie,moduleId,listOfDirectChildren):
		self.log("cbRetDirectCildren "+str(moduleId)+":"+str(listOfDirectChildren)+" > "+cookie)
		
	def cbRetModuleInfo(self,cookie,moduleId,parentId,listOfOpenInterfaces):
		self.log("cbRetModuleInfo "+str(moduleId)+":"+str(listOfOpenInterfaces)+" parent:"+str(parentId)+" > "+cookie)
		
	def cbRetInterfaceInfo(self,cookie,moduleId,interfaceId,maxconnections,listOfOpenConnections,mapOfPortsetups):
		self.log("cbRetInterfaceInfo "+str(moduleId)+"."+str(interfaceId)+":"+str(mapOfPortsetups)+" #Connections:"+str(listOfOpenConnections)+" maxconnections:"+str(maxconnections)+" > "+cookie)
		
	def cbRetConnectionInfo(self,cookie,moduleId,interfaceId,connectionId,peerModuleId,peerInterfaceId,peerConnectionId):
		self.log("cbRetInterfaceInfo "+str(moduleId)+"."+str(interfaceId)+"."+str(connectionId)+":"+str(peerModuleId)+"."+str(peerInterfaceId)+"."+str(peerConnectionId)+" > "+cookie)
		
