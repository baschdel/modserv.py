import modserv.portsetup as portsetup

class Interface:
	#portsetups is a dict of Portsetup objects
	def __init__(self,portsetups,maxconnections=-1):
		self.portsetups = portsetups #dict that maps port numbers to Portsetup objects
		self.maxconnections = maxconnections
		#print("INTFINIT:",self.maxconnections)
		self.connections = 0
		
	def reserveConnection(self):
		self.connections = self.connections+1
		if self.connections>self.maxconnections and self.maxconnections > 0:
			self.connections = self.connections-1
			return False
		return True
		
	def freeConnection(self):
		self.connections = self.connections-1
	
	def isPortOpen(self,port):
		return port in self.portsetups
	
	def isCompatibleWith(self,other):
		if other is None:
			return False;
		for portId,setup in self.portsetups.items():
			if portId in other.portsetups:
				if not setup.isCompatibleWith(other.portsetups[portId]):
					return False
			else:
				if not setup.isOptional:
					return False
		for portId,setup in other.portsetups.items(): #other can have ports that check compatibility different or ports that this interface does not have
			if portId in self.portsetups:
				if not setup.isCompatibleWith(self.portsetups[portId]):
					return False
			else:
				if not setup.isOptional:
					return False
		return True
