import uuid

def generateCookie():
	return uuid.uuid4().hex.encode('utf-8')
