import threading
import time

class Cronjob(threading.Thread):
	def __init__(self,loopfunction,interval,autostart=True,daemon=False):
		threading.Thread.__init__(self,daemon=daemon)
		self.loopfunction = loopfunction
		self.interval = interval
		self.running = True
		if autostart:
			self.start()
		
	def run(self):
			while self.running:
				self.loopfunction()
				time.sleep(self.interval)
