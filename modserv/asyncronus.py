import threading
from modserv.eventbus import Eventbus

class AsyncCallback:
	def __init__(self,callbackfunction):
		self.callbackfunction = callbackfunction
	
	def __call__(self,*args,**kargs):
		self.thread = threading.Thread(target=self.callbackfunction, args=args, kwargs=kargs, deamon=True)
		self.thread.start()
		
class AcquireLock:
	def __init__(self,lock,callback):
		self.lock = lock
		self.callback = callback
		
	def __call__(self,*args,**kargs):
		self.lock.acquire()
		self.callback(*args,**kargs)
				
class TimeoutOrEarlier:
	def __init__(self,callback,timeout):
		self.callback = callback
		self.semaphore = threading.Semaphore(value=0)
		self.timeout = timeout
		self.thread = threading.Thread(target=self._thread)
		
	def start(self):
		self.thread.start()
		return self
		
	def early(self):
		self.semaphore.release()
		
	def _thread(self):
		self.semaphore.acquire(timeout=self.timeout)
		self.callback()

class _Collector:
	def __init__(self,callbackcollector,argumentMap,fail=False):
		self.fail=fail
		self.callbackcollector = callbackcollector
		self.argumentMap = argumentMap
	
	def __call__(self,*args,**options):
		i = 0
		while i<len(args):
			if i in self.argumentMap:
				self.callbackcollector.put(self.argumentMap[i],args[i],fail=self.fail)
			i=i+1
		for k,v in options.items():
			if k in self.argumentMap:
				self.callbackcollector.put(self.argumentMap[k],v,fail=self.fail)
				
class _Putter:
	def __init__(self,callbackcollector,values,fail=False):
		self.fail=fail
		self.callbackcollector = callbackcollector
		self.values = values

	def __call__(self,*args,*options):
		for key,value in self.values.items():
			self.callbackcollector.put(key,value,fail=self.fail)
			
class Callbackcollector:
	def __init__(self,*keys):
		self.eventbus = Eventbus
		self.on = self.eventbus.on
		self.ignore = self.eventbus.ignore
		self.don = self.eventbus.don
		self.dignore = self.eventbus.dignore
		self.values = {}
		self.keysToCollect = keys
		self.failed = False
		self.success = False
		self.hasTimeout = False
		for key in keys:
			self.values[key] = None
		
	def timeout(self,timeout):
		if self.hasTimeout:
			return self
		self.hasTimeout = True
		self.timeoutOrEarlier = TimeoutOrEarlier(self._done,timeout).start()
		return self
			
	def put(self,key,value,failed=False):
		self.values[key] = value
		self.failed = self.failed or failed
		if key in self.keysToCollect:
			self.keysToCollect.remove(key)
			if len(self.keysToCollect) == 0:
				self.success = not self.failed
				self.onDone()
		if fail:
			self.eventbus.fire(key+"-failure")
		else:
			self.eventbus.fire(key+"-success")
		
	def get(self,key):
		try:
			return self.values[key]
		except:
			return None
			
	def getAll(self):
		return self.values
	
	def onDone(self):	
		if self.hasTimeout:
			self.timeoutOrEarlier.early()
		else:
			self._done()
		
	def _done(self):
		if self.success:
			self.eventbus.fire("success")
		elif self.failed:
			self.eventbus.fire("failure")
		else:
			self.eventbus.fire("timeout")
			
	def collector(self,argumentMap,fail=False):
		return _Collector(self,argumentMap,fail=fail)
	
	def putOnCall(self,values,fail=False):
		return _Collector(self,values,fail=fail)
