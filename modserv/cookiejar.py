import uuid

def haskey(thing,key):
	if type(thing) is dict:
		return key in dict
	elif ((type(thing) is list) or (type(thing) is tuple)) and (type(key) is int):
		return (key < len(thing)) and (-key < len(thing))
	else:
		return False

class CookieJar:
	def __init__(self):
		self.hooks = {}
		self.context = {}
		
	def getCookie(self):
		return uuid.uuid4().hex.encode('utf-8')
		
	def hook(self,_cookie_,_callbacks_,**context):
		#print("Hooked up cookie "+str(_cookie_))
		self.hooks[_cookie_] = _callbacks_
		self.context[_cookie_] = context
		return _cookie_
		
	def fire(self,_cookie_,*arguments,**context):
		#print("firing cookie "+str(_cookie_),arguments,self.hooks,context)
		if _cookie_ in self.hooks:
			hook = self.hooks.pop(_cookie_)
			context = {**context,**self.context.pop(_cookie_)}
			if callable(hook):
				hook(*arguments,**context)
			elif (len(arguments) > 0) and haskey(hook,arguments[0]) and callable(hook[arguments[0]]):
				hook[arguments[0]](*arguments[1:],**context)
