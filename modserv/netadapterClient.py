import struct
from modserv.typeenc import DValue,DSList
import modserv.portsetupBin as psenc

class ClientCommandPacker:
	def __init__(self,packetsocket):
		self.packetsocket = packetsocket
		
	def statusInitalized(self):
		self.packetsocket.send(DSList().setList([b"/initalized"]).toBinary())
		
	def cmdShutdown(self,cookie,mid):
		mid = struct.pack(">Q",mid)
		cookie = DValue().setValue(cookie).toBinary()
		self.packetsocket.send(DSList().setList([b"/shutdown",cookie,mid]).toBinary())
		
	def cmdShutdownTree(self,cookie,mid):
		mid = struct.pack(">Q",mid)
		cookie = DValue().setValue(cookie).toBinary()
		self.packetsocket.send(DSList().setList([b"/shutdownTree",cookie,mid]).toBinary())
		
	def cmdMakeChild(self,cookie):
		cookie = DValue().setValue(cookie).toBinary()
		self.packetsocket.send(DSList().setList([b"/makeChild",cookie]).toBinary())
		
	def cmdOpenInterface(self,id,interface):
		ifid = struct.pack(">I",id)
		maxconnections = struct.pack(">i",interface.maxconnections)
		bportsetups = []
		for num,ps in interface.portsetups.items():
			num = struct.pack(">I",num)
			bps = psenc.PStoBinary(ps)
			bportsetups.append(num+bps)
		self.packetsocket.send(DSList().setList([b"/openIntf",ifid,maxconnections,DSList().setList(bportsetups).toBinary()]).toBinary())
		
	def cmdCloseInterface(self,id):
		ifid = struct.pack(">I",id)
		self.packetsocket.send(DSList().setList([b"/closeIntf",ifid]).toBinary())
		
	def cmdOpenConnection(self,cookie,moduleIdA,interfaceIdA,moduleIdB,interfaceIdB):
		cookie = DValue().setValue(cookie).toBinary()
		midA = struct.pack(">Q",moduleIdA)
		ifidA = struct.pack(">I",interfaceIdA)
		midB = struct.pack(">Q",moduleIdB)
		ifidB = struct.pack(">I",interfaceIdB)
		self.packetsocket.send(DSList().setList([b"/openConn",cookie,midA,ifidA,midB,ifidB]).toBinary())
		
	def cmdCall(self,interfaceId,connectionId,portId,functionId,arguments):
		ifid = struct.pack(">I",interfaceId)
		cid = struct.pack(">Q",connectionId)
		pid = struct.pack(">I",portId)
		fid = struct.pack(">H",functionId)
		argumentlist = DSList().setList(arguments).toBinary()
		self.packetsocket.send(DSList().setList([b"/call",ifid,cid,pid,fid,argumentlist]).toBinary())
				
	def cmdCloseConnection(self,cookie,moduleId,interfaceId,connectionId):
		cookie = DValue().setValue(cookie).toBinary()
		mid = struct.pack(">Q",moduleId)
		ifid = struct.pack(">I",interfaceId)
		cid = struct.pack(">Q",connectionId)
		self.packetsocket.send(DSList().setList([b"/closeConn",cookie,mid,ifid,cid]).toBinary())
		
	def cmdGetDirectChildren(self,cookie,moduleId):
		cookie = DValue().setValue(cookie).toBinary()
		mid = struct.pack(">Q",moduleId)
		self.packetsocket.send(DSList().setList([b"/getDirChldrn",cookie,mid]).toBinary())
		
	def cmdGetModuleInfo(self,cookie,moduleId):
		cookie = DValue().setValue(cookie).toBinary()
		mid = struct.pack(">Q",moduleId)
		self.packetsocket.send(DSList().setList([b"/getModInfo",cookie,mid]).toBinary())
		
	def cmdGetInterfaceInfo(self,cookie,moduleId,interfaceId):
		cookie = DValue().setValue(cookie).toBinary()
		mid = struct.pack(">Q",moduleId)
		ifid = struct.pack(">I",interfaceId)
		self.packetsocket.send(DSList().setList([b"/getIntfInfo",cookie,mid,ifid]).toBinary())
		
	def cmdGetConnectionInfo(self,cookie,moduleId,interfaceId,connectionId):
		cookie = DValue().setValue(cookie).toBinary()
		mid = struct.pack(">Q",moduleId)
		ifid = struct.pack(">I",interfaceId)
		cid = struct.pack(">Q",connectionId)
		self.packetsocket.send(DSList().setList([b"/getConnInfo",cookie,mid,ifid,cid]).toBinary())
		
class ClientPacketHandler:
	def __init__(self,callbackhandler,debug=False):
		self.callbackhandler = callbackhandler
		self.wasShutdownCalled = False
		if debug:	
			print("Debugging mode enabled! will print incoming packets!")
		self.debug = debug
		
	def onPacket(self,binary,packetsocket):
		inputs = DSList().fromBinary(binary).getList()
		if len(inputs) == 0:
			return
		command = inputs[0].decode('utf8') #not really a command but a callbackname
		if self.debug:
			print("[PACKET-IN]",command,inputs)
		# cbCalled(self,interfaceId,connectionId,portId,functionId,arguments)
		if command==".call" and len(inputs)==6 and len(inputs[1])==4 and len(inputs[2])==8 and len(inputs[3])==4 and len(inputs[4])==2:
			ifid = struct.unpack(">I",inputs[1])[0]
			cid = struct.unpack(">Q",inputs[2])[0]
			pid = struct.unpack(">I",inputs[3])[0]
			fid = struct.unpack(">H",inputs[4])[0]
			argumentlist = DSList().fromBinary(inputs[5]).getList()
			self.callbackhandler.cbCalled(ifid,cid,pid,fid,argumentlist)
		# cbReturnNoError(self,cookie)
		if command==".retNoErr" and len(inputs)==2 and len(inputs[1])>5:
			cookie = DValue().fromBinary(inputs[1]).getValue()
			self.callbackhandler.cbReturnNoError(cookie)
		# cbErrorNoWriteAccess(self,cookie)
		if command==".errNoWriteAccs" and len(inputs)==2 and len(inputs[1])>5:
			cookie = DValue().fromBinary(inputs[1]).getValue()
			self.callbackhandler.cbErrorNoWriteAccess(cookie)
		# cbErrorNoReadAccess(self,cookie)
		if command==".errNoReadAccs" and len(inputs)==2 and len(inputs[1])>5:
			cookie = DValue().fromBinary(inputs[1]).getValue()
			self.callbackhandler.cbErrorNoReadAccess(cookie)
		# cbChildModuleCreated(self,cookie,id)
		if command==".childModCreated" and len(inputs)==4 and len(inputs[1])>5 and len(inputs[2]) == 8:
			cookie = DValue().fromBinary(inputs[1]).getValue()
			mid = struct.unpack(">Q",inputs[2])[0]
			key = inputs[3].decode('utf-8')
			self.callbackhandler.cbChildModuleCreated(cookie,mid,key)
		# cbErrorInterfaceWasAlreadyOpen(self,id)
		if command==".errIntfAlrdyOpen" and len(inputs)==2 and len(inputs[1]) == 4:
			ifid = struct.unpack(">I",inputs[1])[0]
			self.callbackhandler.cbErrorInterfaceWasAlreadyOpen(ifid)
		# cbInterfaceOpened(self,id)
		if command==".intfOpened" and len(inputs)==2 and len(inputs[1]) == 4:
			ifid = struct.unpack(">I",inputs[1])[0]
			self.callbackhandler.cbInterfaceOpened(ifid)
		# cbErrorInterfaceWasAlreadyClosed(self,id)
		if command==".errIntfAlrdyClosed" and len(inputs)==2 and len(inputs[1]) == 4:
			ifid = struct.unpack(">I",inputs[1])[0]
			self.callbackhandler.cbErrorInterfaceWasAlreadyClosed(ifid)
		# cbInterfaceClosed(self,id)
		if command==".intfClosed" and len(inputs)==2 and len(inputs[1]) == 4:
			ifid = struct.unpack(">I",inputs[1])[0]
			self.callbackhandler.cbInterfaceClosed(ifid)
		# cbErrorCouldNotOpenConnection(self,cookie,reason)
		if command==".errNotOpenConn" and len(inputs)==3 and len(inputs[1])>5 and len(inputs[2])>0:
			cookie = DValue().fromBinary(inputs[1]).getValue()
			reason = inputs[2].decode('utf-8')
			self.callbackhandler.cbErrorCouldNotOpenConnection(cookie,reason)
		# cbReturnConnectionOpen(self,cookie,connectionIdA,connectionIdB)
		if command==".retConnOpen" and len(inputs)==3 and len(inputs[1])>5 and len(inputs[2])==8 and len(inputs[3])==8:
			cookie = DValue().fromBinary(inputs[1]).getValue()
			cidA = struct.unpack(">Q",inputs[2])[0]
			cidB = struct.unpack(">Q",inputs[3])[0]
			self.callbackhandler.cbConnectionOpened(cookie,cidA,cidB)		
		# cbConnectionOpened(self,interfaceId,connectionId)
		if command==".connOpened" and len(inputs)==3 and len(inputs[1])==4 and len(inputs[2])==8:
			ifid = struct.unpack(">I",inputs[1])[0]
			cid = struct.unpack(">Q",inputs[2])[0]
			self.callbackhandler.cbConnectionOpened(ifid,cid)
		# cbConnectionClosed(self,interfaceId,connectionId)
		if command==".connClosed" and len(inputs)==3 and len(inputs[1])==4 and len(inputs[2])==8:
			ifid = struct.unpack(">I",inputs[1])[0]
			cid = struct.unpack(">Q",inputs[2])[0]
			self.callbackhandler.cbConnectionClosed(ifid,cid)
		# cbErrorCallError(self,interfaceId,connectionId,portId,functionId,reason)
		#if command==".errCallErr" and len(inputs)==6 and len(inputs[1])==4 and len(inputs[2])==8 and len(inputs[3])==4 and len(inputs[4])==2 and len(inputs[5])>0:
		#	ifid = struct.unpack(">I",inputs[1])[0]
		#	cid = struct.unpack(">Q",inputs[2])[0]
		#	pid = struct.unpack(">I",inputs[3])[0]
		#	fid = struct.unpack(">H",inputs[4])[0]
		#	reason = inputs[5].decode('utf-8')
		#	self.callbackhandler.cbErrorCallError(ifid,cid,pid,fid,reason)
		# cbConnectionAlreadyClosed(self,cookie)
		if command==".errconnAlrdyClosed" and len(inputs[1])>5:
			cookie = DValue().fromBinary(inputs[1]).getValue()
			self.callbackhandler.cbReturnNoError(cookie)
		# cbParentChanged(self,newParentId,oldParentId)
		if command==".newParent" and len(inputs)==3 and len(inputs[1])==8 and len(inputs[2])==8:
			npmid = struct.unpack(">Q",inputs[1])[0]
			opmid = struct.unpack(">Q",inputs[2])[0]
			self.callbackhandler.cbParentChanged(npmid,opmid)
		# cbOtherModuleCreated(self,moduleId)
		if command==".otrModCreated" and len(inputs)==2 and len(inputs[1])==8:
			mid = struct.unpack(">Q",inputs[1])[0]
			self.callbackhandler.cbOtherModuleCreated(mid)
		# cbOtherModuleShutDown(self,moduleId)
		if command==".otrModShutDown" and len(inputs)==2 and len(inputs[1])==8:
			mid = struct.unpack(">Q",inputs[1])[0]
			self.callbackhandler.cbOtherModuleShutDown(mid)
		# cbOtherModuleInitalized(self,moduleId)
		if command==".otrModInitalized" and len(inputs)==2 and len(inputs[1])==8:
			mid = struct.unpack(">Q",inputs[1])[0]
			self.callbackhandler.cbOtherModuleInitalized(mid)
		# cbOtherInterfaceOpened(self,moduleId,interfaceId)
		if command==".otrIntfOpened" and len(inputs)==3 and len(inputs[1])==8 and len(inputs[2])==4:
			mid = struct.unpack(">Q",inputs[1])[0]
			ifid = struct.unpack(">I",inputs[2])[0]
			self.callbackhandler.cbOtherInterfaceOpened(mid,ifid)
		# cbOtherInterfaceClosed(self,moduleId,interfaceId)
		if command==".otrIntfClosed" and len(inputs)==3 and len(inputs[1])==8 and len(inputs[2])==4:
			mid = struct.unpack(">Q",inputs[1])[0]
			ifid = struct.unpack(">I",inputs[2])[0]
			self.callbackhandler.cbOtherInterfaceOpened(mid,ifid)
		# cbOtherConnectionOpened(self,moduleId,interfaceId,connectionId)
		if command==".otrConnOpened" and len(inputs)==4 and len(inputs[1])==8 and len(inputs[2])==4 and len(inputs[3])==8:
			mid = struct.unpack(">Q",inputs[1])[0]
			ifid = struct.unpack(">I",inputs[2])[0]
			cid = struct.unpack(">Q",inputs[3])[0]
			self.callbackhandler.cbOtherConnectionOpened(mid,ifid,cid)
		# cbOtherConnectionClosed(self,moduleId,interfaceId,connectionId)
		if command==".otrConnClosed" and len(inputs)==4 and len(inputs[1])==8 and len(inputs[2])==4 and len(inputs[3])==8:
			mid = struct.unpack(">Q",inputs[1])[0]
			ifid = struct.unpack(">I",inputs[2])[0]
			cid = struct.unpack(">Q",inputs[3])[0]
			self.callbackhandler.cbOtherConnectionOpened(mid,ifid,cid)
		# cbOtherParentChanged(self,moduleId,newParentId,oldParentId)
		if command==".otrNewParent" and len(inputs)==4 and len(inputs[1])==8 and len(inputs[2])==8 and len(inputs[3])==8:
			mid = struct.unpack(">Q",inputs[1])[0]
			npmid = struct.unpack(">Q",inputs[2])[0]
			opmid = struct.unpack(">Q",inputs[3])[0]
			self.callbackhandler.cbOtherParentChanged(mid,npmid,opmid)
		# cbSelfShutdown(self)
		if command==".shutdown" and len(inputs)==1:
			self.selfShutdown()
		#cbRetDirectCildren(self,cookie,moduleId,listOfDirectChildren)
		if command==".retDirChldrn" and len(inputs)==4 and len(inputs[1])>5 and len(inputs[2])==8:
			cookie = DValue().fromBinary(inputs[1]).getValue()
			mid = struct.unpack(">Q",inputs[2])[0]
			i = 0
			bDCList = inputs[3]
			DCList = [] #DirectChildrenList
			while i+8 <= len(bDCList):
				DCList.append(struct.unpack(">Q",bDCList[i:i+8])[0])
				i=i+8
			self.callbackhandler.cbRetDirectCildren(cookie,mid,DCList)
		#cbRetModuleInfo(self,cookie,moduleId,parentId,listOfOpenInterfaces)
		if command==".retModInfo" and len(inputs)==5 and len(inputs[1])>5 and len(inputs[2])==8 and len(inputs[3])==8:
			cookie = DValue().fromBinary(inputs[1]).getValue()
			mid = struct.unpack(">Q",inputs[2])[0]
			pmid = struct.unpack(">Q",inputs[3])[0]
			i = 0
			bOIList = inputs[4]
			OIList = [] #openInterfaceList
			while i+4 <= len(bOIList):
				OIList.append(struct.unpack(">I",bOIList[i:i+4])[0])
				i=i+4
			self.callbackhandler.cbRetModuleInfo(cookie,mid,pmid,OIList)
		#cbRetInterfaceInfo(self,cookie,moduleId,interfaceId,maxconnections,listOfOpenConnections,mapOfPortsetups)
		if command==".retIntfInfo" and len(inputs)==7 and len(inputs[1])>5 and len(inputs[2])==8 and len(inputs[3])==4 and len(inputs[4])==4:
			cookie = DValue().fromBinary(inputs[1]).getValue()
			mid = struct.unpack(">Q",inputs[2])[0]
			ifid = struct.unpack(">I",inputs[3])[0]
			maxconnections = struct.unpack(">i",inputs[4])[0]
			i = 0
			bOCList = inputs[5]
			OCList = [] #openConnectionList
			while i+8 <= len(bOCList):
				OCList.append(struct.unpack(">Q",bOCList[i:i+8])[0])
				i=i+8
			bportsetups = DSList().fromBinary(inputs[6]).getList()
			portsetups = {}
			for binaryPSME in bportsetups: #Binary PortSetup Map Entity
				if len(binaryPSME) > 4:
					ps = psenc.PSfromBinary(binaryPSME[4:])
					num = struct.unpack(">I",binaryPSME[:4])[0]
					if ps is None:
						#print("received bad .retIntfInfo from server (Couldn't decode Port setup #"+str(num)+")")
						pass
					else:
						portsetups[num] = ps
				else:
					if self.debug:
						print("received bad .retIntfInfo from server (Too short BinaryPortSetupMapEntry)")
					#TODO: senderror
					return
			self.callbackhandler.cbRetInterfaceInfo(cookie,mid,ifid,maxconnections,OCList,portsetups)
		if command==".retConnInfo" and len(inputs)==8 and len(inputs[1])>5 and len(inputs[2])==8 and len(inputs[3])==4 and len(inputs[4])==8 and len(inputs[5])==8 and len(inputs[6])==4 and len(inputs[7])==8:
			cookie = DValue().fromBinary(inputs[1]).getValue()
			mid = struct.unpack(">Q",inputs[2])[0]
			ifid = struct.unpack(">I",inputs[3])[0]
			cid = struct.unpack(">Q",inputs[4])[0]
			pmid = struct.unpack(">Q",inputs[5])[0]
			pifid = struct.unpack(">I",inputs[6])[0]
			pcid = struct.unpack(">Q",inputs[7])[0]
			self.callbackhandler.cbRetConnectionInfo(cookie,mid,ifid,cid,pmid,pifid,pcid)
			
	def onClose(self,packetsocket):
		if self.debug:
			print("Connection closed! -> Sutting down")
		self.selfShutdown()
		
	def selfShutdown(self):
		if not self.wasShutdownCalled:
			self.wasShutdownCalled = True
			self.callbackhandler.cbSelfShutdown()
			
class DummyClientCallbackHandler:
	
	def log(self,message,channel="LOG"):
		print("["+channel+"][DUMMY CALLBACK HANDLER]:"+message)
	
	def __init__(self,packetsocket):
		self.packetsocket = packetsocket
	
	
	def cbReturnNoError(self,cookie):
		self.log("cbReturnSuccess > "+cookie,"CB")
	
	def cbErrorNoWriteAccess(self,cookie):
		self.log("cbErrorNoWriteAccess > "+cookie,"CB")
		
	def cbChildModuleCreated(self,cookie,id,key):
		self.log("cbChildModuleCreated ("+id+","+key+") > "+cookie,"CB")
		
	def cbErrorInterfaceWasAlreadyOpen(self,id):
		self.log("cbErrorInterfaceWasAlreadyOpen ("+id+")","CB")
		
	def cbInterfaceOpened(self,id):
		self.log("cbInterfaceOpened ("+id+")","CB")
		
	def cbErrorInterfaceWasAlreadyClosed(self,id):
		self.log("cbErrorInterfaceWasAlreadyClosed ("+id+")","CB")
		
	def cbInterfaceClosed(self,id):
		self.log("cbInterfaceClosed ("+id+")","CB")
		
	def cbErrorCouldNotOpenConnection(self,cookie,reason):
		#errors:
		#  noModuleA #no access to module A
		#  noModuleB #no access to module B
		#  noInterfaceA #interface A does not exist
		#  noInterfaceB #interface B does not exist
		#  incompatible #the two interfaces are not compatible
		#  connlimitA #connection limit reached on interface A
		#  connlimitB #connection limit reached on interface B
		self.log("cbErrorCouldNotOpenConnection ("+reason+") > "+cookie,"CB")
		
	def cbReturnConnectionOpen(self,cookie,connectionIdA,connectionIdB):
		self.log("cbReturnConnectionOpen ("+connectionIdA+"+"+connectionIdB+")")
		
	def cbConnectionOpened(self,interfaceId,connectionId):
		self.log("cbConnectionOpened "+interfaceId+"."+connectionId,"CB")
		
	def cbConnectionClosed(self,interfaceId,connectionId):
		self.log("cbConnectionClosed "+interfaceId+"."+connectionId,"CB")
		
	def cbErrorCallError(self,interfaceId,connectionId,portId,functionId,reason):
		#errors:
		# noInterface #if the interface does not exist
		# noConnection #if the connection does not exist (or is not yet initalized)
		# noPort #if the port isn't open
		# noFunction #if the function does not exist (a module may also close the connection instead)
		# badArgument #if the arguments are not compliant wih the protocol (a module may also close the connection instead)
		self.log("cbErrorCallError "+interfaceId+"."+connectionId+"."+portId+"."+functionId+" ("+reason+")","CB")
		
	def cbConnectionAlreadyClosed(self,cookie):
		self.log("cbConnectionAlreadyClosed > "+cookie,"CB")
		
	def cbParentChanged(self,newParentId,oldParentId):
		self.log("cbParentChanged [ "+newParentId+" > "+oldParentId+" ]","CB")
		
	#management
	
	def cbOtherModuleCreated(self,moduleId):
		self.log("cbOtherModuleCreated "+moduleId,"CB")
	
	def cbOtherModuleShutDown(self,moduleId):
		self.log("cbOtherModuleShutDown "+moduleId,"CB")
		
	def cbOtherModuleInitalized(self,moduleId):
		self.log("cbOtherModuleInitalized "+moduleId,"CB")
		
	def cbOtherInterfaceOpened(self,moduleId,interfaceId):
		self.log("cbOtherInterfaceOpened "+moduleId+"."+interfaceId,"CB")
		
	def cbOtherInterfaceClosed(self,moduleId,interfaceId):
		self.log("cbOtherInterfaceClosed "+moduleId+"."+interfaceId,"CB")
		
	def cbOtherConnectionOpened(self,moduleId,interfaceId,connectionId):
		self.log("cbOtherConnectionOpened "+moduleId+"."+interfaceId+"."+connectionId,"CB")
		
	def cbOtherConnectionClosed(self,moduleId,interfaceId,connectionId):
		self.log("cbOtherConnectionClosed "+moduleId+"."+interfaceId+"."+connectionId,"CB")
		
	def cbOtherParentChanged(self,moduleId,newParentId,oldParentId):
		self.log("cbParentChanged "+moduleId+" [ "+newParentId+" > "+oldParentId+" ]","CB")
	
	#selfmanagement
		
	def cbSelfShutdown(self):
		self.log("cbSelfShutdown","CB")
		
	#calls
	
	def cbCalled(self,interfaceId,connectionId,portId,functionId,arguments):
		self.log("cbCalled "+interfaceId+"."+connectionId+"."+portId+"."+functionId+" ("+arguments+")","CB")
		
	#getter returns
	
	def cbRetDirectCildren(self,cookie,moduleId,listOfDirectChildren):
		self.log("cbRetDirectCildren "+str(moduleId)+":"+str(listOfDirectChildren)+" > "+cookie)
		
	def cbRetModuleInfo(self,cookie,moduleId,parentId,listOfOpenInterfaces):
		self.log("cbRetModuleInfo "+str(moduleId)+":"+str(listOfOpenInterfaces)+" parent:"+str(parentId)+" > "+cookie)
		
	def cbRetInterfaceInfo(self,cookie,moduleId,interfaceId,maxconnections,listOfOpenConnections,mapOfPortsetups):
		self.log("cbRetInterfaceInfo "+str(moduleId)+"."+str(interfaceId)+":"+str(mapOfPortsetups)+" #Connections:"+str(listOfOpenConnections)+" maxconnections:"+str(maxconnections)+" > "+cookie)
		
	def cbRetConnectionInfo(self,cookie,moduleId,interfaceId,connectionId,peerModuleId,peerInterfaceId,peerConnectionId):
		self.log("cbRetInterfaceInfo "+str(moduleId)+"."+str(interfaceId)+"."+str(connectionId)+":"+str(peerModuleId)+"."+str(peerInterfaceId)+"."+str(peerConnectionId)+" > "+cookie)
