from modserv.portsetup import Portsetup
import struct

def PSfromBinary(binary):
	if len(binary) > 8 and binary[0]==1:
		protocol = binary[8:].decode('utf8')
		data = struct.unpack(">BBHBH?",binary[:8])
		inSide = chr(data[1])
		inMinVersion = data[2]
		outSide = chr(data[3])
		outMinVersion = data[4]
		isOptional = data[5]
		return Portsetup(protocol,inSide,inMinVersion,outSide,outMinVersion,isOptional)
	return None
	
def PStoBinary(portsetup):
	return struct.pack(">BBHBH?", 1, ord(portsetup.inSide), portsetup.inMinVersion, ord(portsetup.outSide), portsetup.outMinVersion, portsetup.isOptional) + ( portsetup.protocol.encode('utf8') )
