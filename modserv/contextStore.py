from modserv.exceptions import AlreadyAttachedExeption

#This class stores context objects for connections of an interface, and creates them from a constructor if necessary
class ContextStore:
	def __init__(self,template,*args,**options):
		self.store = {} #maps connectionIds to stored objects
		self.template = template
		self.modclient = None
		self.interfaceId = None
		self.args = args
		self.options = options
		
	def attach(self,modclient,interfaceId):
		if self.modclient is None:
			self.modclient = modclient
			self.interfaceId = interfaceId
			modclient.on("interface."+str(interfaceId)+".closed",self.detach)
			modclient.on("interface."+str(interfaceId)+".connectionClosed",self._connClosed)
		else:
			raise AlreadyAttachedExeption()
				
	def detach(self):
		self.store = {}
		self.modclient.ignore("interface."+str(self.interfaceId)+".closed",self._detach)
		self.modclient.ignore("interface."+str(self.interfaceId)+".connectionClosed",self._connClosed)
		self.interfaceId = None
		self.modclient = None
		
	def _connClosed(connectionId,interfaceId=None):
		if interfaceId == self.interfaceId:
			del self.store[connections]
		
	def get(self,connectionId):
		if connectionId in self.store:
			return self.store[connectionId]
		else:
			thing = self.template(*self.args,**self.options)
			self.store[connectionId] = thing
			return thing
			
	def put(self,connectionId,thing):
		self.store[connectionId] = thing
