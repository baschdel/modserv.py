import struct
import json

#assuming:
#	big Endian Encoding
#	unicode for text
#	utf-8 to encode unicode

# DValue				\/ Please don't go above that, just to set a standard somewhere
#    0-255    0-16,777,216
# 0         1             5                         9+typelen         9+typelen+datalen
# +---------+-------------+-------------------------+-----------------+
# | typelen |   datalen   | type                    | data            |
# +---------+-------------+-------------------------+-----------------+

def getInttype(bits):
	if bits<=8:
		return 1
	if bits<=16:
		return 2
	if bits<=32:
		return 3
	if bits<=64:
		return 4
	return None

class DValue:
	def __init__ (self,binary=None,dtype="" ):
		self.binary=binary
		self.dtype=dtype
		
	def setValue(self,value,dtype=None):
		print(type(value))
		if dtype == "JSON":
			self.binary=json.dumps(value).encode('utf-8')
			self.dtype="JSON"
			return self
		if type(value) is list:
			self.binary=json.dumps(value).encode('utf-8')
			self.dtype="JSON"
			return self
		if type(value) is dict:
			self.binary=json.dumps(value).encode('utf-8')
			self.dtype="JSON"
			return self
		if type(value) is bool:
			b = None
			if value:
				b = b'\x01'
			else:
				b = b'\x00'
			self.binary = b
			self.dtype="BOOL"
			return self
		if value is None:
			self.binary=b''
			self.type="NULL"
			return self
		if type(value) is str:
			self.binary=value.encode('utf-8')
			self.dtype="TEXT"
			return self
		if type(value) is bytes:
			self.binary=value
			if dtype is None:
				self.dtype="BLOB"
			else:
				self.dtype = dtype
			return self
		if type(value) is int:
			blen = value.bit_length()
			if(value<0 or dtype == "SINT"):
				strctfmt= ">"+("bhiq"[getInttype(blen+1)-1])
				print(strctfmt)
				print(struct.pack(strctfmt,value))
				self.binary=struct.pack(strctfmt,value)
				self.dtype="SINT"
				return self
			else:
				strctfmt= ">"+("BHIQ"[getInttype(blen)-1])
				print(strctfmt)
				print(struct.pack(strctfmt,value))				
				self.binary=struct.pack(strctfmt,value)
				self.dtype="UINT"
				return self
		if type(value) is float:
			if dtype is None:
				dtype = "FLOAT64"
			if dtype == "FLOAT64":
				self.dtype = dtype
				self.binary = struct.pack(">d",value)
				return self
			if dtype == "FLOAT32":
				self.dtype = dtype
				self.binary = struct.pack(">f",value)
				return self
		print("Couldn't encode value")
		return self
		
	def getValue(self):
		try:
			if self.dtype == "NULL":
				return None
			if self.dtype == "TEXT":
				return self.binary.decode('utf-8')
			if self.dtype == "BLOB":
				return self.binary
			if self.dtype == "SINT":
				bylen=len(self.binary)
				if(bylen>8):
					return None
				strctfmt= ">"+("bhiq"[getInttype(bylen*8)-1])
				return struct.unpack(strctfmt,self.binary)[0] #TODO add padding if value is too short
			if self.dtype == "UINT":
				bylen=len(self.binary)
				if(bylen>8):
					return None			
				strctfmt= ">"+("BHIQ"[getInttype(bylen*8)-1])
				return struct.unpack(strctfmt,self.binary)[0]
			if self.dtype == "FLOAT64":
				return struct.unpack(">d",self.binary)[0]
			if self.dtype == "FLOAT32":
				return struct.unpack(">f",self.binary)[0]
			if self.dtype == "JSON":
				return json.loads(self.binary.decode('utf-8'))
			if self.dtype == "BOOL":
				return self.binary[0] != 0
				#return struct.unpack(">?",self.binary)[0]
			return None
		except Exception:
			print("MOD ERROR: exception in getValue():",e)
			return None
			
	def getType(self):
		return self.dtype
		
	def toBinary(self):
		enctype = self.dtype.encode('utf-8')[:255]
		enctypelen = len(enctype)
		binlen = len(self.binary)
		return struct.pack(">BI",enctypelen,binlen)+enctype+self.binary
		
	def fromBinary(self,binary):
		dat = struct.unpack(">BI",binary[:5])
		typelen = dat[0]
		binlen = dat[1]
		self.dtype = binary[5:5+typelen].decode('utf-8')
		self.binary = binary[5+typelen:5+typelen+binlen]
		return self

class DSList:
	def	__init__(self):
		self.list = []
	
	#takes a list of binarys as input
	def setList(self,newlist):
		self.list = newlist
		return self

	def getList(self):
		return self.list
		
	def toBinary(self):
		binary = b''
		i=0
		while i<len(self.list):
			entry = self.list[i]
			binary = binary+struct.pack(">I",len(entry))+entry
			i=i+1
		return binary
		
	def fromBinary(self,binary):
		self.list = []
		while len(binary) >= 4:
			entrylen = struct.unpack(">I",binary[:4])[0]
			entry = binary[4:4+entrylen]
			self.list.append(entry)
			binary = binary[4+entrylen:]
		return self
		

