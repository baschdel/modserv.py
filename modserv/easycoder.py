from modserv.exceptions import FormatError

def haskey(thing,key):
	if type(thing) is dict:
		return key in dict
	elif ((type(thing) is list) or (type(thing) is tuple)) and (type(key) is int):
		return (key < len(thing)) and (-key < len(thing))
	else:
		return False
# functions [functionId][argumentId]{[mode]}
# callbacks [functionId]{[mode]}

class EasyDecoder:
	def __init__(self,functions,callbacks,onFError=None,onTError=None,obj=None):
		self.functions = functions
		self.callbacks = callbacks
		self.onFError = onFError # gets called when something could not be decoded beacause it had the wrong Format/Type (functionId,argId,mode)
		self.onTError = onTError #gets called when a callback raises a type error (functionId,argId,mode)
		self.obj = obj
		
	def __call__(self,functionId,*arguments,obj=None):
		if obj is None:
			obj = self.obj
		if not haskey(self.functions,functionId):
			return
		if not haskey(self.callbacks,functionId):
			return
		output = [None]*len(arguments)
		mode = 0
		argId = 0
		try:
			while argId<len(arguments):
				if callable(self.functions[functionId][argId]):
					#decode
					output[argId] = self.functions[functionId][argId](arguments[argId])
					if argId == 0:
						mode = output[0]
				else:
					if callable(self.functions[functionId][argId][mode]):
					#use alternative decoder
						output[argId] = self.functions[functionId][argId][mode](arguments[argId])
				argId=argId+1
		except FormatError:
			if callable(self.onFError):
				self.onFError(functionId,argId,mode)
			
		try:
			if callable(self.callbacks[functionId]):
				if obj is None:
					self.callbacks[functionId](*output)
				else:
					self.callbacks[functionId](obj,*output)
			elif haskey(self.callbacks[functionId],mode):
				if obj is None:
					self.callbacks[functionId][mode](*(output[1:]))
				else:
					self.callbacks[functionId][mode](obj,*(output[1:]))
		except TypeError as e:
			if callable(self.onTError):
			 self.onTError(functionId,argId,mode)
			
# callback([obj],functionId,...)
# functions[functionId][argId]{[mode]}
class EasyEncoder:
	def __init__(self,functions,callback,onFError=None,obj=None):
		self.functions = functions
		self.callback = callback
		self.onFError = onFError # gets called when something could not be encoded beacause it had the wrong Format/Type (functionId,argId,mode)
		self.obj = obj
		
	def __call__(self,functionId,*arguments,obj=None):
		if obj is None:
			obj = self.obj
		#print("EasyEncoder() #--",self.functions)
		if not haskey(self.functions,functionId):
			return
		#print("EasyEncoder() #00")
		output = [None]*len(arguments)
		mode = 0
		argId = 0
		try:
			while argId<len(arguments):
				if argId == 0:
						mode = arguments[0]
				if callable(self.functions[functionId][argId]):
					#encode
					output[argId] = self.functions[functionId][argId](arguments[argId])
				else:
					if callable(self.functions[functionId][argId][mode]):
					#use alternative encoder
						output[argId] = self.functions[functionId][argId][mode](arguments[argId])
				#print("EasyEncoder() #loop")
				argId=argId+1
		except FormatError:
			if callable(self.onFError):
				self.onFError(functionId,argId,mode)
		#print("EasyEncoder() #99")
		if obj is None:
			self.callback(functionId,*output)
		else:
			self.callback(obj,functionId,*output)
			
