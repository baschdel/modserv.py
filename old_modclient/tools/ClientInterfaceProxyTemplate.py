#Placeholders:
# $protocol$ - replace with the protocol id/name

class VsfsClientInterfaceProxy:
	def __init__(self,modclient,interfaceId=DEFAULT_INTERFACE,onInterfaceAlreadyOpen=None):
		self.modclient = modclient
		self.interfaceId = interfaceId
		self.proxy = None
		self.eventbus = Eventbus()
		self.modclient.on("interface."+str(interfaceId)+".newConnection",self._onNewConnection)
		modclient.on("interface."+str(interfaceId)+".connectionClosed",self._onConnectionClosed)
		if not self.modclient.openInterface(interfaceId,"$protocol$-client",1):
			if callable(onInterfaceAlreadyOpen):
				onInterfaceAlreadyOpen()
				
	def _onNewConnection(self,connectionId,**context):
		if self.proxy is None:
			self.proxy = VsfsClientProxy(self.modclient,self.interfaceId,connectionId,DEFAULT_PORT)
			self.eventbus._fireEvent("connected")
		else:
			raise CrashAndBurnException("More than one connection open on a single connnection $protocol$-client interface")
			
	def _onConnectionClosed(self,connectionId,**context):
		self.proxy = None
		self.eventbus._fireEvent("disconnected")
		
	def on(self,event,callback):
		self.eventbus.on(event,callback)
	
	def ignore(self,event,callback):
		self.eventbus.ignore(event,callback)
		
	def isConnected(self):
		return not (self.proxy is None)
