import modserv.modclient as modclient
import portlib.config_provider_client as config_provider_client

moduleId = input("moduleId:")
moduleKey = input("moduleKey:")
client = modclient.Modclient(*modclient.client.getCredsFromArgs(args=[None,moduleId,moduleKey]),debug=True)

config_provider_client.register(client)

CONFIG_PROVIDER_INTERFACE = config_provider_client.DEFAULT_INTERFACE
CONFIG_PROVIDER_PORT = config_provider_client.DEFAULT_PORT

proxy = None

def onConnect(connectionId,**context):
	print("--connected--")
	global proxy
	proxy = config_provider_client.ConfigProviderClientProxy(client,CONFIG_PROVIDER_INTERFACE,connectionId,CONFIG_PROVIDER_PORT)
	
def onDisconnect(connectionId,**context):
	print("--disconnected--")
	global proxy
	proxy = None
	
def onStart():
	client.on("interface."+str(CONFIG_PROVIDER_INTERFACE)+".newConnection",onConnect)
	client.on("interface."+str(CONFIG_PROVIDER_INTERFACE)+".connectionClosed",onDisconnect)
	
	client.openInterface(CONFIG_PROVIDER_INTERFACE,"config-provider-client",1)
	
client.on("start",onStart)
