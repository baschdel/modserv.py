protocol = "ifrequest"

return {
	protocol = protocol,
	port = 9,
	interface = 9,
	in_calls = protocol..".to-requester-calls",
	out_calls = protocol..".to-provider-calls",
	transcoders = protocol..".transcoders",
	portname = protocol.."-client",
	interfacename = protocol.."-client",
	portSetup = true,
	interfaceSetup = true,
	portProxy = true,
	cookieJar = true,
}
