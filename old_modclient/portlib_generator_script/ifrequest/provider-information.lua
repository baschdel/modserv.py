protocol = "ifrequest"

return {
	protocol = protocol,
	port = 9,
	interface = 9,
	in_calls = protocol..".to-provider-calls",
	out_calls = protocol..".to-requester-calls",
	transcoders = protocol..".transcoders",
	portname = protocol,
	interfacename = protocol,
	portSetup = true,
	interfaceSetup = true,
	portProxy = true,
	cookieJar = true,
}
