description_file = ...
description_file = description_file or "information"

information = require(description_file)

sections = {}

encoders = {
	bin="enc.bin",
	utf8="enc.utf8",
	["utf-8"]="enc.utf8",
	u8="enc.u8",
	u16="enc.u16",
	u32="enc.u32",
	u64="enc.u64",
	s8="enc.s8",
	s16="enc.s16",
	s32="enc.s32",
	s64="enc.s64",
	bool="enc.bool",
	dslist="enc.dslist",
	dsmap="enc.dsmap",
	dsentry="enc.dsentry",
	flvlist="enc.flvlist"
}

decoders = {
	bin="dec.bin",
	utf8="dec.utf8",
	["utf-8"]="dec.utf8",
	u8="dec.u8",
	u16="dec.u16",
	u32="dec.u32",
	u64="dec.u64",
	s8="dec.s8",
	s16="dec.s16",
	s32="dec.s32",
	s64="dec.s64",
	bool="dec.bool",
	dslist="dec.dslist",
	dsmap="dec.dsmap",
	dsentry="dec.dsentry",
	flvlist="dec.flvlist"
}

function encodeArgument(argdesc,fnlookup)
--print("encodeArgument",fnlookup)
	if argdesc == nil then return "None" end
	if type(argdesc) == "string" then
		local fnname = fnlookup[argdesc]
		if fnname == nil then
			fnname = argdesc
		end
		return fnname
	elseif type(argdesc) == "table" then
		local fnname = fnlookup[argdesc["type"]]
		if fnname == nil then
			fnname = argdesc["function"]
		end
		if fnname == nil then
			error("No function name specified")
		end
		return fnname..argsToString(encodeArguments(argdesc,fnlookup))
	elseif type(argdesc) == "number" then
		return tostring(argdesc)
	elseif type(argdesc) == "bool" then
		if argdesc then
			return "True"
		else
			return "False"
		end
	end
end

function encodeArguments(arguments,fnlookup)
--print("encodeArguments",fnlookup)
	local args = {}
	local i=1
	while true do
		argdesc = arguments[i]
		if argdesc == nil then break end
		args[i] = encodeArgument(argdesc,fnlookup)
		i=i+1
	end
	return args
end

function argsToString(args)
	local out = ""
	local firsttime = true
	local i=1
	while true do
		if not args[i] then break end
		if not firsttime then out=out.."," end
		firsttime = false
		out=out..tostring(args[i])
		i=i+1
	end
	return "("..out..")"
end

function argsToList(args)
	local out = ""
	local firsttime = true
	local i=1
	while true do
		if not args[i] then break end
		if not firsttime then out=out.."," end
		firsttime = false
		out=out..tostring(args[i])
		i=i+1
	end
	return "["..out.."]"
end

--WARNING this function is not safe
--please don't use for full automationencoder
function dictToString(args)
	local out = ""
	local firsttime = true
	for k,v in pairs(args) do
		if not firsttime then out=out.."," end
		firsttime = false
		if type(k) == "string" then
			out=out.."'"..k.."':"..tostring(v)
		elseif type(k) == "number" then
			out=out..k..":"..tostring(v)
		end
	end
	return "{"..out.."}"
end

function getEncoder(arguments)
	local encoder = "Encoder"
	if arguments == nil then return encoder.."()" end
	local args = {}
	local i=1
	while true do
		argdesc = arguments[i]
		if type(argdesc) == "string" then
			args[i] = encodeArgument(argdesc,encoders)
		elseif type(argdesc) == "table" then
			--either a mode table or a complex encoder
			if argdesc.type then
				--complex encoder
				args[i] = encodeArgument(argdesc,encoders)
			else
				--modetable
				encoder = "ModeEncoder"
				local j=1
				local entries = {}
				local listCompatible = true
				for k,v in pairs(argdesc) do
					entries[k] = encodeArgument(v,encoders)
					if not (type(k) == "number") then
						listCompatible = false
					elseif (k > #argdesc) or (k <= 0) then
						listCompatible = false
					end
				end
				if listCompatible then
					args[i] = argsToList(entries)
				else
					args[i] = dictToString(entries)
				end
			end
		else
			break
		end
		i=i+1
	end
	return encoder.."("..argsToList(args)..")"
end

function getDecoder(arguments)
	local decoder = "Decoder"
	if arguments == nil then return decoder.."()" end
	local args = {}
	local i=1
	while true do
		argdesc = arguments[i]
		if type(argdesc) == "string" then
			args[i] = encodeArgument(argdesc,decoders)
		elseif type(argdesc) == "table" then
			--either a mode table or a complex decoder
			if argdesc.type then
				--complex decoder
				args[i] = encodeArgument(argdesc,decoders)
			else
				--modetable
				decoder = "ModeDecoder"
				local j=1
				local entries = {}
				local listCompatible = true
				for k,v in pairs(argdesc) do
					entries[k] = encodeArgument(v,decoders)
					if not (type(k) == "number") then
						listCompatible = false
					elseif (k > #argdesc) or (k <= 0) then
						listCompatible = false
					end
				end
				if listCompatible then
					args[i] = argsToList(entries)
				else
					args[i] = dictToString(entries)
				end
			end
		else
			break
		end
		i=i+1
	end
	return decoder.."("..argsToList(args)..")"
end

function getProprty(key)
	value = information[key]
	if value then return value end
	print(tostring(key).."?")
	value = io.read()
	information[key] = value
	return value
end

inCalls = require(getProprty("in_calls"))
outCalls = require(getProprty("out_calls"))
transcoders = require(getProprty("transcoders"))

function add(section,stuff)
	sections[section]=(sections[section] or "")..stuff.."\n"
end

--add imports
add("import",[[
from modserv.portsetup import Portsetup
from modserv.transcoders import Encoder,Decoder,ModeEncoder,ModeDecoder
from modserv.modclient import Context
import modserv.encoders as enc
import modserv.decoders as dec
]])

if information.cookieJar then
add("import","from modserv.cookiejar import CookieJar")
end

--define constants
add("constants","PROTOCOL = "..getProprty("protocol"))
add("constants","ENCVERSION = "..outCalls.version)
add("constants","DECVERSION = "..inCalls.version)
add("constants","DEFAULT_PORT = "..getProprty("port"))
add("constants","DEFAULT_INTERFACE = "..getProprty("interface"))
add("constants","THIS_SIDE = \""..inCalls.side.."\"")
add("constants","OTHER_SIDE = \""..outCalls.side.."\"")

--define CALL_NAMES
add("constants","")
add("constants","#call names")
for k,v in pairs(outCalls) do
	if type(k) == "number" then
		if v.constant then
			add("constants","CALL_"..v.constant.." = "..k)
			--encoderconstants[k] = "CALL_"..v.constant
		end
	end
end

--define CALLBACK_NAMES
add("constants","")
add("constants","#in call names")
for k,v in pairs(inCalls) do
	if type(k) == "number" then
		if v.constant then
			add("constants","INCALL_"..v.constant.." = "..k)
			--decoderconstants[k] = "INCALL_"..v.constant
		end
	end
end

--generate protocol object class
encodertable = {}
decodertable = {}
encoderdict = {}
decoderdict = {}

--generate encoder list
for k,v in pairs(outCalls) do
	if type(k) == "number" then
		constant = v.constant
		if not constant then
			constant = tostring(k)
		end
		encoder = nil
		if v.transcoder then
			encoder = v.transcoder.."_ENCODER"
			if not encodertable[encoder] then
				if transcoders[v.transcoder] then
					encodertable[encoder] = getEncoder(transcoders[v.transcoder])
				else
					error("Unknown transcoder: "..tostring(v.transcoder))
				end
			end
		end
		if v.arguments then
			encoder = getEncoder(v.arguments)
		end
		encoderdict[constant] = encoder
	end
end

--generate decoder list
for k,v in pairs(inCalls) do
	if type(k) == "number" then
		constant = v.constant
		if not constant then
			constant = tostring(k)
		end
		decoder = nil
		if v.transcoder then
			decoder = v.transcoder.."_DECODER"
			if not decodertable[decoder] then
				if transcoders[v.transcoder] then
					decodertable[decoder] = getDecoder(transcoders[v.transcoder])
				else
					error("Unknown transcoder: "..tostring(v.transcoder))
				end
			end
		end
		if v.arguments then
			decoder = getDecoder(v.arguments)
		end
		decoderdict[constant] = decoder
	end
end

add("protocol-decoder","class ProtocolObject:")
add("protocol-decoder","")
for k,v in pairs(decodertable) do
	add("protocol-decoder","	"..k.." = "..v)
end
add("protocol-decoder","")
add("protocol-decoder","	def __init__(self):")
add("protocol-decoder","		self.decVersion = DECVERSION")
add("protocol-decoder","		self.decoders = {")
for k,v in pairs(decoderdict) do
	add("protocol-decoder","			INCALL_"..k..": self."..v..",")
end
add("protocol-decoder","		}")


add("protocol-encoder","class ProtocolObject:")
for k,v in pairs(encodertable) do
	add("protocol-encoder","	"..k.." = "..v)
end
add("protocol-encoder","")
add("protocol-encoder","	def __init__(self):")
add("protocol-encoder","		self.encVersion = ENCVERSION")
add("protocol-encoder","		self.encoders = {")
for k,v in pairs(encoderdict) do
	add("protocol-encoder","			CALL_"..k..": self."..v..",")
end
add("protocol-encoder","		}")


add("register","PORT_ENCODER_"..outCalls.side:upper().." = Protocol"..outCalls.side:upper().."EncoderObject()")
add("register","PORT_DECODER_"..inCalls.side:upper().." = Protocol"..inCalls.side:upper().."DecoderObject()")
if information.portSetup or information.interfaceSetup then
	add("register","PORTSETUP_"..inCalls.side:upper()..outCalls.side:upper().." = Portsetup(PROTOCOL,THIS_SIDE,DECVERSION,OTHER_SIDE,ENCVERSION)")
end
add("register","")

portnamearg = ""
interfacenamearg = ""

if information.portSetup then
	portnamearg = ",portname=\""..information.portname.."\""
end
if information.interfaceSetup then
	interfacenamearg = ",interfacename=\""..information.interfacename.."\""
end

add("register","def register(modclient"..portnamearg..interfacenamearg..")")
add("register","	modclient.registerPortProtocolDecoder(PROTOCOL,THIS_SIDE,PORT_DECODER_"..inCalls.side:upper()..")")
add("register","	modclient.registerPortProtocolEncoder(PROTOCOL,OTHER_SIDE,PORT_ENCODER_"..outCalls.side:upper()..")")
if information.portSetup then
	add("register","	modclient.registerPortSetup(portname,PORTSETUP_"..inCalls.side:upper()..outCalls.side:upper()..")")
end
if information.interfaceSetup then
	add("register","	modclient.registerInterfaceSetup(interfacename,{DEFAULT_PORT:PORTSETUP_"..inCalls.side:upper()..outCalls.side:upper().."})")
end

add("proxy","class Proxy:")
add("proxy",[[	def __init__(self,modclient,interfaceId,connectionId,portId):
		self.interfaceId = interfaceId
		self.portId = portId
		self.connectionId = connectionId
		self.modclient = modclient]])
if information.cookieJar then
	add("proxy","		self.cookieJar = CookieJar()")
end
add("proxy","		self.hooks = {}")
add("proxy","		self.hooks[\"connection.\"+str(interfaceId)+\".\"+str(connectionId)+\".closed\"] = self._hookDisconnect")
add("proxy","		self.hooks[\"interface.\"+str(interfaceId)+\".closed\"] = self._hookDisconnect")
for k,v in pairs(decoderdict) do
	add("proxy","		self.hooks[modclient.getCallEventName(interfaceId,portId,INCALL_"..k..")] = self.")
end
add("proxy","		self.modclient.don(hooks)")
add("proxy",[[

	def _call(self,functionId,*arguments):
		self.modclient.call(self.interfaceId,self.connectionId,self.portId,functionId,*arguments)
		
	def _hookDisconnect(self,**context):
		self.modclient.dignore(self.hooks)
]])

if information.cookieJar then
	add("proxy",[[	def _fireNormalCookie(self,cookie,*arguments,**context):
		self.cookieJar.fire(cookie,*arguments,**context)
		
	def _fireModeCookie(self,mode,cookie,*arguments,**context):
		self.cookieJar.fire(cookie,mode,*arguments,**context)
]])
end

add("proxy","	#your functions go here ...")

--print results
order = {"import","constants","protocol-decoder","protocol-encoder","register","proxy"}
for _,key in pairs(order) do
	if key == "proxy" then
		if information.portProxy then
			print(sections[key])
		end
	else
		print(sections[key])
	end
end
