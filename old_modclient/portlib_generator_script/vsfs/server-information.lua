protocol = "vsfs"

return {
	protocol = protocol,
	port = 9,
	interface = 9,
	in_calls = "vsfs.to-client-calls",
	out_calls = "vsfs.to-server-calls",
	transcoders = "vsfs.transcoders",
	portname = protocol,
	interfacename = protocol,
	portSetup = true,
	interfaceSetup = true,
	portProxy = true,
	cookieJar = true,
}
