protocol = "vsfs"

return {
	protocol = protocol,
	port = 9,
	interface = 9,
	in_calls = "vsfs.to-server-calls",
	out_calls = "vsfs.to-client-calls",
	transcoders = "vsfs.transcoders",
	portname = protocol.."-client",
	interfacename = protocol.."-client",
	portSetup = true,
	interfaceSetup = true,
	portProxy = true,
	cookieJar = true,
}
