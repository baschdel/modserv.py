This is a lua script that automates part of the repetitive task of creating a base for a port adapter for the prtlib

-------------------------------------------------
How to use?
-------------------------------------------------

crate an information file (example: vsfs-information.lua)
crate a file that specifies a to b calls and another one for calls in the other direction (examples: vsfs-to-server-calls.lua and vsfs-to-client-calls.lua)
crate a transcoder file (example: vsfs-transcoders.lua)

launch the generator script and give it the information file as the first argument the sourcecode will be written to stdout

NOTE: the file vsfs.txt has the specification all in one file but there currently is no way to automatically parse it
