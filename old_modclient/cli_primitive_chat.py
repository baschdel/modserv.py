import modserv.modclient as modclient
import portlib.cli as cliport

#define some constants
CLI_INTERFACE = cliport.DEFAULT_INTERFACE #3

#reqest modclient instance
client = modclient.getModclient(debug=True)

#register everything the cli adapter has to offer
cliport.register(client)

#keep a list of all open connections
openCliConnections = []

#define a function that listens on the interface opening
def onCliConnectionOpened(connectionId,**context):
	openCliConnections.append(connectionId)
	
def onCliConnectionClosed(connectionId,**context):
	openCliConnections.remove(connectionId)
	
def onCliConnectionMessage(message,connectionId=None,**context):
	print(message,openCliConnections)
	for otherConnectionId in openCliConnections:
		print(otherConnectionId,connectionId == otherConnectionId)
		if not (connectionId == otherConnectionId):
			client.call(CLI_INTERFACE,otherConnectionId,cliport.DEFAULT_PORT,cliport.CALL_SEND,message)

client.on("interface."+str(CLI_INTERFACE)+".newConnection",onCliConnectionOpened)
client.on("interface."+str(CLI_INTERFACE)+".connectionClosed",onCliConnectionClosed)
client.on(client.getCallEventName(CLI_INTERFACE,cliport.DEFAULT_PORT,cliport.CALL_SEND),onCliConnectionMessage)

#open the interface
client.openInterface(CLI_INTERFACE,"primitive-cli",-1)
