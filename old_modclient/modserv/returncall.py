class Returncall(Exception):
	def __init__(self,*arguments):
		self.arguments = arguments
		
class Returncaller:
	def __init__(self,modclient,function,**context):
		self.modclient = modclient
		self.function = function
		self.context = context
		
		def __call__(self,*arguments,**context):
			context = {**context,**self.context}
			if ("interfaceId" in context) and ("connectionId" in context) and ("portId" in context) and ("functionId" in context):
				try:
					ret = self.function(*arguments,**context)
					if ret is Returncall:
						raise ret
					if (ret is tuple) or (ret is list):
						raise Returncall(ret)
				except Returncall as call:
					modclient.call(context["interfaceId"],context["connectionId"],context["portId"],context["functionId"],*call.arguments)
