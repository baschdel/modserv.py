import modserv.client as client
import uuid
from modserv.portsetup import Portsetup
import sys
from modserv.exceptions import CrashAndBurnException
from modserv.context import Context

def getModclient(debug=False):
	moduleId,moduleKey,port,server = client.getCredsFromArgs()
	#print([moduleId,moduleKey,port,server])
	return Modclient(moduleId,moduleKey,port,server,debug=debug)

#class Context:
#	def __init__(self,callback,**context):
#		self.context = context
#		self.callback = callback
#	
#	def __call__(self,*args,**kargs):
#		self.callback(*args,**{**kargs,**self.context})

#class CrashAndBurnException(Exception):
#	pass #If this is raised, something went very wrong
	#possible causes:
	#		The server didn't respect the protocol
	#		You did something funny with the internal state of the easyclient classes
	#		something in the easyclient went wrong and a packet from the server got ignored	
	
class Modclient:
	def __init__(self,moduleId,moduleKey,port,server,debug=False):
		self.debug = debug
		self.commandPacker = client.connect(server,port,moduleId,moduleKey,self,debug=debug)
		self.portSetupRegistry = {} # maps names to portsetups
		self.portProtocolEncoderRegistry = {} # maps port types to another dict, that maps the side to the en/decoder objects
		self.portProtocolDecoderRegistry = {} # same as above
		self.interfaceSetupRegistry = {} #maps 
		self.interfaces = {} #maps the interface ids tothe interfaces
		self.eventListeners = {} #{'event':[callback]}
		self.cookiehooks = {} #callbacks for everything that uses cookies
		self.openingConnectionData = {} #A table that maps cookies to the data of the connection, to store it until the callback arrives
		self.connected = False
		self.isShutdown = False
		self.cookiecontext = {}
		self.moduleId = moduleId
		
	################################
	#### API ######################
	##############################
	
	def getCallEventName(self,interfaceIdOrClass,portId,functionId):
		return "call."+str(interfaceIdOrClass)+"."+str(portId)+"."+str(functionId)
	
	################
	# EVENTS ######
	
	def on(self,event,callback):
		if not event in self.eventListeners:
			self.eventListeners[event] = []
		if not callable(callback):
			raise CrashAndBurnException("modclient.on A non callable object was registred as callback for event '"+str(event)+"' ("+str(callback)+")")
		self.eventListeners[event].append(callback)
		return True
		
	def ignore(self,event,callback):
		if not event in self.eventListeners:
			return
		if not callback in self.eventListeners[event]:
			return
		self.eventListeners[event].remove(callback)
		if len(self.eventListeners[event]) == 0:
			del self.eventListeners[event]
	
	#bulk hook (useful for listener classes)
	#(takes a dict that maps events to callback functions or arrays of callback functions)
	def don(self,emap):
		for event,callback in emap.items():
			if type(callback) is list:
				for cb in callback:
					self.on(event,cb)
			else:
				self.on(event,callback)
	
	#bulk ignore (useful for listener classes)
	#(takes a dict that maps events to callback functions or arrays of callback functions)
	def dignore(self,emap):
		for event,callback in emap.items():
			if type(callback) is list:
				for cb in callback:
					self.ignore(event,cb)
			else:
				self.ignore(event,callback)
	
	####################
	# PORT REGISTRY ###
	
	def registerPortSetup(self,name,setup):
		self.portSetupRegistry[name] = setup
		
	def registerPortProtocolEncoder(self,portType,portSide,protocolObject):
		if not portType in self.portProtocolEncoderRegistry:
			self.portProtocolEncoderRegistry[portType] = {}
		self.portProtocolEncoderRegistry[portType][portSide]=protocolObject
	
	def registerPortProtocolDecoder(self,portType,portSide,protocolObject):
		if not portType in self.portProtocolDecoderRegistry:
			self.portProtocolDecoderRegistry[portType] = {}
		self.portProtocolDecoderRegistry[portType][portSide]=protocolObject
	
	def registerInterfaceSetup(self,name,setup):
		self.interfaceSetupRegistry[name] = setup
		
	############################
	# SERVER INTERACTION ######
	
	def shutdown(self,moduleId=None,onSuccess=None,onFailure=None,onTimeout=None,**context):
		if moduleId is None:
			moduleId=self.moduleId
		cookie = self._generateCookie()
		if not (onSuccess is None and onFailure is None and onTimeout is None):
			self._setCookieCallbackHandlers(cookie,{'retNoErr':onSuccess,'errNoWriteAccs':onFailure,'timeout':onTimeout},**context)
		self.commandPacker.cmdShutdown(cookie,moduleId)
	
	def shutdownTree(self,moduleId=None,onSuccess=None,onFailure=None,onTimeout=None,**context):
		if moduleId is None:
			moduleId=self.moduleId
		cookie = self._generateCookie()
		if not (onSuccess is None and onFailure is None and onTimeout is None):
			self._setCookieCallbackHandlers(cookie,{'retNoErr':onSuccess,'errNoWriteAccs':onFailure,'timeout':onTimeout},**context)
		self.commandPacker.cmdShutdownTree(cookie,moduleId)
		
	def makeCild(self,onSuccess=None,onTimeout=None,**context):
		cookie = self._generateCookie()
		if not (onSuccess is None and onTimeout is None):
			self._setCookieCallbackHandlers(cookie,{'childModCreated':onSuccess,'timeout':onTimeout},**context)
		self.commandPacker.cmdMakeChild(cookie)
	
	def openInterface(self,interfaceId,ports,maxconnections,interfaceClass=None):
		if interfaceId in self.interfaces:
			return False #don't open interfaces that are already open
		encoders = {} #{<portId>:{<functionId>:encoderFunction}}
		decoders = {} #{<portId>:{<functionId>:decoderFunction}}
		portsetups = {}
		if ports in self.interfaceSetupRegistry:
			ports = self.interfaceSetupRegistry[ports]
		if type(ports) is str:
			raise CrashAndBurnException("Interface Setup '"+ports+"' is unknown")
		for pid,psetup in ports.items():
		#NOTE: This currently does not crash on invalid entrys but just sorts them out
		#      This coud be close to the source of an interface incompatibility issue where there shouldn't be one
		#      (check protocol handlers / portsetups / interface setups)
			if psetup in self.portSetupRegistry:
				psetup = self.portSetupRegistry[psetup]
			if (psetup.protocol in self.portProtocolDecoderRegistry) and (psetup.protocol in self.portProtocolEncoderRegistry):
				protocolDecoderObjects = self.portProtocolDecoderRegistry[psetup.protocol]
				protocolEncoderObjects = self.portProtocolEncoderRegistry[psetup.protocol]
				thisSide  = psetup.inSide
				otherSide = psetup.outSide 
				#get decoder
				if (thisSide in protocolDecoderObjects): 
					decVersion = protocolDecoderObjects[thisSide].decVersion
					if decVersion < psetup.inMinVersion:
						raise CrashAndBurnException("protocol decoder Version is too low! version:"+str(decVersion)+" required:"+str(psetup.outMinVersion))
					decoders[pid] = protocolDecoderObjects[thisSide].decoders
				else:
					raise CrashAndBurnException("protocol decoder for side "+thisSide+" of protocol '"+psetup-protocol+"' is not resgistred")
				#get encoder
				if (thisSide in protocolEncoderObjects):
					encVersion = protocolEncoderObjects[otherSide].encVersion
					if encVersion < psetup.outMinVersion:
						raise CrashAndBurnException("protocol encoder Version is too low! version:"+str(encVersion)+" required:"+str(psetup.outMinVersion))
					encoders[pid] = protocolEncoderObjects[otherSide].encoders
				else:
					raise CrashAndBurnException("protocol encoder for side "+thisSide+" of protocol '"+psetup-protocol+"' is not resgistred")
				#Note: this code relys on the two above ifs to crash if anything is missing!
				portsetups[pid] = psetup
			elif not psetup.isOptional:
				raise CrashAndBurnException("Non optional protocol '"+psetup.protocol+"' is not registered")
		interface = Interface(interfaceId,portsetups,encoders,decoders,maxconnections,interfaceClass)
		self.interfaces[interfaceId] = interface
		self.commandPacker.cmdOpenInterface(interfaceId,interface)
			
		
	def closeInterface(self,interfaceId):
		if not interfaceId in self.interfaces:
			return False #don't close interfaces that are already closed
		self.commandPacker.cmdCloseInterface(interfaceId)
		del self.interfaces[interfaceId]
		return True
	
	def connect(self,moduleIdA,interfaceIdA,moduleIdB,interfaceIdB,onSuccess=None,onFailure=None,onTimeout=None,**context):
		cookie = self._generateCookie()
		self.openingConnectionData[cookie] = {'ma':moduleIdA,'mb':moduleIdB,'ia':interfaceIdA,'ib':interfaceIdB}
		if not (onSuccess is None and onFailure is None and onTimeout is None):
			self._setCookieCallbackHandlers(cookie,{'connOpened':onSuccess,'connOpenErr':onFailure,'timeout':onTimeout},**context)
		self.commandPacker.cmdOpenConnection(cookie,moduleIdA,interfaceIdA,moduleIdB,interfaceIdB)
	
	def call(self,interfaceId,connectionId,portId,functionId,*arguments):
		#print("modclient.call("+str(interfaceId)+","+str(connectionId)+","+str(portId)+","+str(functionId)+","+str(arguments)+")")
		if interfaceId in self.interfaces:
			interface = self.interfaces[interfaceId]
			#print("[CALL-TEMP]",interface.isOpen,interface.encoders,portId,functionId,arguments)
			if portId in interface.encoders and interface.isOpen:
				encoderFunctions = interface.encoders[portId]
				if functionId in encoderFunctions:
					#print("calling encoders")
					binarguments = encoderFunctions[functionId](arguments)
					#print("calling command packer")
					self.commandPacker.cmdCall(interfaceId,connectionId,portId,functionId,binarguments)
		else:
			if self.debug:
				print("[DEBUG][ERROR]You were trying to call an interface, that is not in the interface list")
		#print("modserv.call() end")
	
	def callback(self,*arguments,interfaceId=None,connectionId=None,portId=None,functionId=None,**context):
		if interfaceId is None:
			raise CrashAndBurnException("[callback] Missing interfaceId")
		if connectionId is None:
			raise CrashAndBurnException("[callback] Missing connectionId")
		if portId is None:
			raise CrashAndBurnException("[callback] Missing portId")
		if functionId is None:
			raise CrashAndBurnException("[callback] Missing functionId")
		self.call(interfaceId,connectionId,portId,functionId,*arguments)
	
	#arguments is a list of binarys (bytes objects)
	def rawCall(self,interfaceId,connectionId,portId,functionId,arguments):
		self.commandPacker.cmdCall(interfaceId,connectionId,portId,functionId,arguments)
	
	def closeConnection(self,moduleId,interfaceId,connectionId,onSuccess=None,onFailure=None,onTimeout=None,**context):
		cookie = self._generateCookie()
		if not (onSuccess is None and onFailure is None and onTimeout is None):
			self._setCookieCallbackHandlers(cookie,{'retNoErr':onSuccess,'errNoWriteAccs':onFailure,'timeout':onTimeout},**context)
		self.commandPacker.cmdCloseConnection(cookie,moduleId,interfaceId,connectionId)
		
	def requestDirectCildren(self,moduleId,onSuccess=None,onFailure=None,onTimeout=None,**context):
		cookie = self._generateCookie()
		if not (onSuccess is None and onFailure is None and onTimeout is None):
			self._setCookieCallbackHandlers(cookie,{'retDirectChildren':onSuccess,'errNoReadAccs':onFailure,'timeout':onTimeout},**context)
		self.commandPacker.cmdGetDirectChildren(cookie,moduleId)
		
	def requestModuleInfo(self,moduleId,onSuccess=None,onFailure=None,onTimeout=None,**context):
		cookie = self._generateCookie()
		if not (onSuccess is None and onFailure is None and onTimeout is None):
			self._setCookieCallbackHandlers(cookie,{'retModInfo':onSuccess,'errNoReadAccs':onFailure,'timeout':onTimeout},**context)
		self.commandPacker.cmdGetModuleInfo(cookie,moduleId)
		
	def requestInterfaceInfo(self,moduleId,interfaceId,onSuccess=None,onFailure=None,onTimeout=None,**context):
		cookie = self._generateCookie()
		if not (onSuccess is None and onFailure is None and onTimeout is None):
			self._setCookieCallbackHandlers(cookie,{'retIntfInfo':onSuccess,'errNoReadAccs':onFailure,'timeout':onTimeout},**context)
		self.commandPacker.cmdGetInterfaceInfo(cookie,moduleId,interfaceId)
		
	def requestConnectionInfo(self,moduleId,interfaceId,connectionId,onSuccess=None,onFailure=None,onTimeout=None,**context):
		cookie = self._generateCookie()
		if not (onSuccess is None and onFailure is None and onTimeout is None):
			self._setCookieCallbackHandlers(cookie,{'retConnInfo':onSuccess,'errNoReadAccs':onFailure,'timeout':onTimeout},**context)
		self.commandPacker.cmdGetConnectionInfo(cookie,moduleId,interfaceId,connectionId)
	
	##################
	# STATE #########
	
	def running(self):
		return not self.isShutdown	
	
	def me(self):
		return self.moduleId
	
	################################
	#### INTERNAL #################
	##############################
	
	###################
	# COOKIES ########
	
	def _generateCookie(self):
		return uuid.uuid4().hex
	
	#handlers is a dict that maps the callback name to the callback function
	def _setCookieCallbackHandlers(self,cookie,handlers,**context):
		self.cookiehooks[cookie] = handlers
		self.cookiecontext[cookie] = context
		
	def _fireCookie(self,cookie,fn,*args,**context):
		if cookie in self.cookiehooks:
			context = {**context,**self.cookiecontext[cookie]}
			if fn in self.cookiehooks[cookie]:
				if callable(self.cookiehooks[cookie][fn]):
					self.cookiehooks[cookie][fn](*args,**context)
				elif type(self.cookiehooks[cookie][fn]) is list:
					for cb in self.cookiehooks[cookie][fn]:
						if callable(cb):
							cb(*args,**context)
			del self.cookiehooks[cookie] #only one callback per cookie
		if cookie in self.cookiecontext:
			del self.cookiecontext[cookie]
	
	##################
	# EVENTS ########
	
	def _fireEvent(self,event,*args,**context):
		if self.debug:
			print("[EVENT]",event,args,context)
		if not event in self.eventListeners:
			return
		for callbackHandler in self.eventListeners[event]:
			callbackHandler(*args,**context)
	
	def _onShutdown(self):
		if not self.isShutdown:
			self.isShutdown = True
			#print("_onShutdown called")
			self._fireEvent("selfShutdown")
			self._fireEvent("moduleSutdown",self.moduleId)
			self.commandPacker.packetsocket.close()
	
	################################
	#### CALLBACKS ################
	##############################
	
	def initalize(self):
		self.connected = True
		self._fireEvent("start")
	
	def authFailed(self):
		print("authentication failed!\nexiting ...")
		sys.exit(2)
	
	def cbReturnNoError(self,cookie):
		self._fireCookie(cookie,"retNoErr")
	
	def cbErrorNoWriteAccess(self,cookie):
		self._fireCookie(cookie,"errNoWriteAccs")
	
	def cbErrorNoReadAccess(self,cookie):
		self._fireCookie(cookie,"errNoReadAccs")
	
	def cbChildModuleCreated(self,cookie,id,key):
		self._fireCookie(cookie,"childModCreated",id,key)
		
	def cbErrorInterfaceWasAlreadyOpen(self,id):
		raise CrashAndBurnException("The server says that the interface #"+str(id)+" was already open.\nThis should not happen, beacause easyclient does these checks before it calls the Server!")
		
	def cbInterfaceOpened(self,id):
		if not id in self.interfaces:
			raise CrashAndBurnException("An interface, that is not in the easyclient interface table has opened!")
		#self.interfaces[id]._onOpen()
		self.interfaces[id].isOpen = True
		self._fireEvent("intfOpened",self.moduleId,id)
		#print("cbInterfaceOpened",id,self.interfaces)
			
	def cbErrorInterfaceWasAlreadyClosed(self,id):
		raise CrashAndBurnException("The server says that the interface #"+str(id)+" was already closed.\nThis should not happen, beacause easyclient does these checks before it calls the Server!")
		
	def cbInterfaceClosed(self,id):
		if not id in self.interfaces:
			raise CrashAndBurnException("An interface, that is not in the easyclient interface table has closed!")
		#self.interfaces[id]._onClose()
		self.interfaces[id].isOpen = False
		self.interfaces.pop(id)
		#print("Closed interface "+str(id))
		self._fireEvent("intfClosed",self.moduleId,id)
		self._fireEvent("interface."+str(id)+".closed")
				
	def cbErrorCouldNotOpenConnection(self,cookie,reason):
		#errors:
		#  noModuleA #no access to module A
		#  noModuleB #no access to module B
		#  noInterfaceA #interface A does not exist
		#  noInterfaceB #interface B does not exist
		#  incompatible #the two interfaces are not compatible
		#  connlimitA #connection limit reached on interface A
		#  connlimitB #connection limit reached on interface B
		if not reason in ["noModuleA","noModuleB","noInterfaceA","noInterfaceB","incompatible","connlimitA","connlimitB"]:
			raise CrashAndBurnException("Server returned an unknown reason for a Connection Open Error. ("+str(reason)+")")
		if cookie in self.openingConnectionData:
			info = self.openingConnectionData.pop(cookie)
			self._fireCookie(cookie,"connOpenErr",info['ma'],info['ia'],info['mb'],info['ib'],reason)
		else:
			self._fireCookie(cookie,"-error-")#just to clear the cookie if it existed
			raise CrashAndBurnException("Received a Couldn't open Connection, with a non registred cookie! (might be a big bug)")
			
	def cbReturnConnectionOpen(self,cookie,connectionIdA,connectionIdB):
		if cookie in self.openingConnectionData:
			info = self.openingConnectionData.pop(cookie)
			self._fireCookie(cookie,"connOpened",info['ma'],info['ia'],connectionIdA,info['mb'],info['ib'],connectionIdB)
		else:
			raise CrashAndBurnException("Received a Connection Open, with a non registred cookie! (might be a big bug)")
				
	def cbConnectionOpened(self,interfaceId,connectionId):
		#print("cbConnectionOpened",interfaceId,connectionId,self.interfaces)
		#if not interfaceId in self.interfaces:
		#	raise CrashAndBurnException("A connection Opened for a non registered interface (This might be a big bug)")
		#self.interfaces[interfaceId]._onConnectionOpened(connectionId)
		self._fireEvent("connOpened",self.moduleId,interfaceId,connectionId)
		self._fireEvent("interface."+str(interfaceId)+".newConnection",connectionId,
			interfaceId=interfaceId)
		
	def cbConnectionClosed(self,interfaceId,connectionId):
		#if interfaceId in self.interfaces:
		#	self.interfaces[interfaceId]._onConnectionClosed(connectionId)
		self._fireEvent("connClosed",self.moduleId,interfaceId,connectionId)
		self._fireEvent("interface."+str(interfaceId)+".connectionClosed",connectionId,
			interfaceId=interfaceId)
		self._fireEvent("connection."+str(interfaceId)+"."+str(connectionId)+".closed")
		
	def cbErrorCallError(self,interfaceId,connectionId,portId,functionId,reason):
		#errors:
		# noInterface #if the interface does not exist
		# noConnection #if the connection does not exist (or is not yet initalized)
		# noPort #if the port isn't open
		# noFunction #if the function does not exist (a module may also close the connection instead)
		# badArgument #if the arguments are not compliant wih the protocol (a module may also close the connection instead)
		if not reason in ["noInterface","noConnection","noPort","noFunction","badArgument"]:
			raise CrashAndBurnException("Server returned an unknown reason for a Call Error. ("+str(reason)+")")
		#if interfaceId in self.interfaces:
		#		self.interfaces[interfaceId]._onCallError(connectionId,portId,functionId,reason)
		
	def cbConnectionAlreadyClosed(self,cookie):
		pass #ignore this, as this can happen, when both ends close the connection at the same time
		#a conection closed should be fired anyway
				
	def cbParentChanged(self,newParentId,oldParentId):
		self._fireEvent("moduleParentChanged",self.moduleId,newParentId,oldParentId)
				
	#management
	
	def cbOtherModuleCreated(self,moduleId):
		self._fireEvent("newModule",moduleId)
	
	def cbOtherModuleShutDown(self,moduleId):
		self._fireEvent("moduleSutdown",moduleId)
				
	def cbOtherModuleInitalized(self,moduleId):
		self._fireEvent("moduleInitalized",moduleId)
		
	def cbOtherInterfaceOpened(self,moduleId,interfaceId):
		self._fireEvent("intfOpened",moduleId,interfaceId)
		
	def cbOtherInterfaceClosed(self,moduleId,interfaceId):
		self._fireEvent("intfClosed",moduleId,interfaceId)
		
	def cbOtherConnectionOpened(self,moduleId,interfaceId,connectionId):
		self._fireEvent("connOpened",moduleId,interfaceId,connectionId)
		
	def cbOtherConnectionClosed(self,moduleId,interfaceId,connectionId):
		self._fireEvent("connClosed",moduleId,interfaceId,connectionId)
		
	def cbOtherParentChanged(self,moduleId,newParentId,oldParentId):
		self._fireEvent("moduleParentChanged",moduleId,newParentId,oldParentId)
	
	#selfmanagement
		
	def cbSelfShutdown(self):
		self._onShutdown()
		
	#calls
	
	def cbCalled(self,interfaceId,connectionId,portId,functionId,arguments):
		context = {'interfaceId':interfaceId,'connectionId':connectionId,'portId':portId,'functionId':functionId}
		self._fireEvent(self.getCallEventName(interfaceId,portId,functionId)+".raw",*arguments,**context)
		if interfaceId in self.interfaces:
			interface = self.interfaces[interfaceId]
			if not (interface.interfaceClass is None):
				self._fireEvent(self.getCallEventName(interface.interfaceClass,portId,functionId)+".raw",*arguments,**context)
			if portId in interface.decoders:
				decoders = interface.decoders[portId]
				if functionId in decoders:
					darguments = decoders[functionId](arguments)
					self._fireEvent(self.getCallEventName(interfaceId,portId,functionId),*darguments,**context)
					if not (interface.interfaceClass is None):
						self._fireEvent(self.getCallEventName(interface.interfaceClass,portId,functionId),*darguments,**context)
		
	#getter returns
	
	def cbRetDirectCildren(self,cookie,moduleId,listOfDirectChildren):
		self._fireCookie(cookie,"retDirectChildren",moduleId,listOfDirectChildren)
		self._fireEvent("updateModDirectChildList",moduleId,listOfDirectChildren)
		
	def cbRetModuleInfo(self,cookie,moduleId,parentId,listOfOpenInterfaces):
		self._fireCookie(cookie,"retModInfo",moduleId,parentId,listOfOpenInterfaces)
		self._fireEvent("updateModInfo",moduleId,parentId,listOfOpenInterfaces)
		
	def cbRetInterfaceInfo(self,cookie,moduleId,interfaceId,maxconnections,listOfOpenConnections,mapOfPortsetups):
		self._fireCookie(cookie,"retIntfInfo",moduleId,interfaceId,maxconnections,listOfOpenConnections,mapOfPortsetups)
		self._fireEvent("updateIntfInfo",moduleId,interfaceId,maxconnections,listOfOpenConnections,mapOfPortsetups)
		
	def cbRetConnectionInfo(self,cookie,moduleId,interfaceId,connectionId,peerModuleId,peerInterfaceId,peerConnectionId):
		self._fireCookie(cookie,"retConnInfo",moduleId,interfaceId,connectionId,peerModuleId,peerInterfaceId,peerConnectionId)
		self._fireEvent("updateConnInfo",moduleId,interfaceId,connectionId,peerModuleId,peerInterfaceId,peerConnectionId)
		
class Interface:
	def __init__(self,interfaceId,portsetups,encoders,decoders,maxconnections,interfaceClass=None):
		self.interfaceId = interfaceId
		self.encoders = encoders
		self.decoders = decoders
		self.isOpen = False
		self.interfaceClass = interfaceClass
		self.maxconnections = maxconnections
		self.portsetups = portsetups
		
#just ignore the lists of encoder functions (they are not supported right now)
#use wrapper objects
#class DummyPortProtocolSideObject:
#	def __init__(self):
#		self.version = 1
#		self.callnames = {'foo':1,'bar':2,'baz':3}
#		self.encoders = {1:self.encodeBlah,2:[self.bin,self.encodeUtf8],3:self.encodeBlah}
#		self.decoders = {1:[self.decodeUtf8,self.decodeUtf8],2:[self.bin,self.decodeUtf8],3:[self.decodeUtf8,self.decodeUtf8]}
#		
#	def bin(self,b):
#		return b
#	
#	def encodeUtf8(self,txt):
#		return txt.encode('utf-8')
#		
#	def decodeUtf8(self,b)
#		return b.decode('utf-8')
#		
#	def encodeBlah(self,x,y):
#		return [x.encode('utf-8'),y.encode('utf-8')]
