from modserv.portsetup import Portsetup
from modserv.transcoders import Encoder,Decoder
import modserv.encoders as enc
import modserv.decoders as dec
from modserv.cookiejar import CookieJar
from modserv.exceptions import CrashAndBurnException
from modserv.eventbus import Eventbus

PROTOCOL = "vsfs"
ENCVERSION = 1
DECVERSION = 1
DEFAULT_PORT = 9
DEFAULT_INTERFACE = 9
CALL_READ = 0
CALL_WRITE = 1
CALL_APPEND = 2
CALL_DELETE = 3
CALL_LIST = 4
CALL_GETSIZE = 5

class VsfsClientProtocolObject:
	COOKIE_TEXT_ENCODER = Encoder([enc.bin,enc.utf8])
	COOKIE_TEXT_DATA_ENCODER = Encoder([enc.bin,enc.utf8,enc.bin])
	
	MODE_COOKIE_DECODER = Decoder([dec.u8,dec.bin])
	MODE_COOKIE_DATA_DECODER = Decoder([dec.u8,dec.bin,dec.bin])
	COOKIE_TEXTDSLIST_DECODER = Decoder([dec.bin,dec.dslist(dec.utf8)])
	MODE_COOKIE_SIZE_DECODER = Decoder([dec.u8,dec.bin,dec.u32])
	
	def __init__(self):
		self.encVersion = ENCVERSION
		self.decVersion = DECVERSION
		self.encoders = {
			CALL_READ:    self.COOKIE_TEXT_ENCODER,
			CALL_WRITE:   self.COOKIE_TEXT_DATA_ENCODER,
			CALL_APPEND:  self.COOKIE_TEXT_DATA_ENCODER,
			CALL_DELETE:  self.COOKIE_TEXT_ENCODER,
			CALL_LIST:    self.COOKIE_TEXT_ENCODER,
			CALL_GETSIZE: self.COOKIE_TEXT_ENCODER
		}
		self.decoders = {
			CALL_READ:    self.MODE_COOKIE_DATA_DECODER,
			CALL_WRITE:   self.MODE_COOKIE_DECODER,
			CALL_APPEND:  self.MODE_COOKIE_DECODER,
			CALL_DELETE:  self.MODE_COOKIE_DECODER,
			CALL_LIST:    self.COOKIE_TEXTDSLIST_DECODER,
			CALL_GETSIZE: self.MODE_COOKIE_SIZE_DECODER
		}
		
PORT_OBJECT = VsfsClientProtocolObject()
PORTSETUP_C = Portsetup(PROTOCOL,"c",DECVERSION,"s",ENCVERSION)

def register(modclient,portname=PROTOCOL+"-client",interfacename=PROTOCOL+"-client"):
	modclient.registerPortSetup(portname,PORTSETUP_C)
	modclient.registerPortProtocolEncoder(PROTOCOL,"s",PORT_OBJECT)
	modclient.registerPortProtocolDecoder(PROTOCOL,"c",PORT_OBJECT)
	modclient.registerInterfaceSetup(interfacename,{DEFAULT_PORT:PORTSETUP_C})

class VsfsClientProxy:
	def __init__(self,modclient,interfaceId,connectionId,portId):
		self.interfaceId = interfaceId
		self.portId = portId
		self.connectionId = connectionId
		self.modclient = modclient
		self.cookieJar = CookieJar()
		self.hooks = {}
		self.hooks["connection."+str(interfaceId)+"."+str(connectionId)+".closed"] = self._hookDisconnect
		self.hooks["interface."+str(interfaceId)+".closed"] = self._hookDisconnect
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_READ)] = self._fireModeCookie
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_WRITE)] = self._fireModeCookie
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_APPEND)] = self._fireModeCookie
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_DELETE)] = self._fireModeCookie
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_LIST)] = self._fireNormalCookie
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_GETSIZE)] = self._fireModeCookie
		self.modclient.don(self.hooks)
		
	def _call(self,functionId,*arguments):
		self.modclient.call(self.interfaceId,self.connectionId,self.portId,functionId,*arguments)
		
	def read(self,name,onSuccess=None,onDoesNotExist=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onDoesNotExist],**context)
		self._call(CALL_READ,cookie,name)
		
	def write(self,name,data,onSuccess=None,onInvalidName=None,onReadOnly=None,onNotEnoughSpace=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onInvalidName,onReadOnly,onNotEnoughSpace],**context)
		self._call(CALL_WRITE,cookie,name,data)
		
	def append(self,name,data,onSuccess=None,onInvalidName=None,onReadOnly=None,onNotEnoughSpace=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onInvalidName,onReadOnly,onNotEnoughSpace],**context)
		self._call(CALL_APPEND,cookie,name,data)
		
	def delete(self,name,onSuccess=None,onDoesNotExist=None,onReadOnly=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onDoesNotExist,onReadOnly],**context)
		self._call(CALL_DELETE,cookie,name)
		
	def list(self,name,onSuccess=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),onSuccess,**context)
		self._call(CALL_LIST,cookie,name)
		
	def getSize(self,name,onSuccess=None,onDoesNotExist=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onDoesNotExist],**context)
		self._call(CALL_GETSIZE,cookie,name)
		
	def _fireNormalCookie(self,cookie,*arguments,**context):
		self.cookieJar.fire(cookie,*arguments,**context)
		
	def _fireModeCookie(self,mode,cookie,*arguments,**context):
		self.cookieJar.fire(cookie,mode,*arguments,**context)
		
	def _hookDisconnect(self,**context):
		self.modclient.dignore(self.hooks)
		
class VsfsClientInterfaceProxy:
	def __init__(self,modclient,interfaceId=DEFAULT_INTERFACE,onInterfaceAlreadyOpen=None):
		self.modclient = modclient
		self.interfaceId = interfaceId
		self.proxy = None
		self.eventbus = Eventbus()
		self.modclient.on("interface."+str(interfaceId)+".newConnection",self._onNewConnection)
		modclient.on("interface."+str(interfaceId)+".connectionClosed",self._onConnectionClosed)
		if not self.modclient.openInterface(interfaceId,{DEFAULT_PORT:PORTSETUP_C},1):
			if callable(onInterfaceAlreadyOpen):
				onInterfaceAlreadyOpen()
				
	def _onNewConnection(self,connectionId,**context):
		if self.proxy is None:
			self.proxy = VsfsClientProxy(self.modclient,self.interfaceId,connectionId,DEFAULT_PORT)
			self.eventbus._fireEvent("connected")
		else:
			raise CrashAndBurnException("More than one connection open on a single connnection vsfs-client interface")
			
	def _onConnectionClosed(self,connectionId,**context):
		self.proxy = None
		self.eventbus._fireEvent("disconnected")
		
	def on(self,event,callback):
		self.eventbus.on(event,callback)
	
	def ignore(self,event,callback):
		self.eventbus.ignore(event,callback)
		
	def isConnected(self):
		return not (self.proxy is None)
		
	#for information about callbacks see the proxy class above
	def read(self,name,onNotConnected=None,**context):
		if self.proxy is None:
			if callable(onNotConnected):
				onNotConnected(**context)
			return
		self.proxy.read(name,**context)
				
	def write(self,name,data,onNotConnected=None,**context):
		if self.proxy is None:
			if callable(onNotConnected):
				onNotConnected(**context)
			return
		self.proxy.write(name,data,**context)
		
	def append(self,name,data,onNotConnected=None,**context):
		if self.proxy is None:
			if callable(onNotConnected):
				onNotConnected(**context)
			return
		self.proxy.append(name,data,**context)
				
	def delete(self,name,onNotConnected=None,**context):
		if self.proxy is None:
			if callable(onNotConnected):
				onNotConnected(**context)
			return
		self.proxy.delete(name,context)
				
	def list(self,name,onNotConnected=None,**context):
		if self.proxy is None:
			if callable(onNotConnected):
				onNotConnected(**context)
			return
		self.proxy.list(name,**context)
		
	def getSize(self,name,onNotConnected=None,**context):
		if self.proxy is None:
			if callable(onNotConnected):
				onNotConnected(**context)
			return
		self.proxy.getSize(name,**context)
