from modserv.portsetup import Portsetup
from modserv.transcoders import Encoder,Decoder,ModeEncoder,ModeDecoder
from modserv.modclient import Context
import modserv.encoders as enc
import modserv.decoders as dec

from modserv.cookiejar import CookieJar

PROTOCOL = ifrequest
ENCVERSION = 1
DECVERSION = 1
DEFAULT_PORT = 9
DEFAULT_INTERFACE = 9
THIS_SIDE = "p"
OTHER_SIDE = "r"

#call names
CALL_LIST = 0
CALL_REQUEST = 1
CALL_NEW_TYPES = 2

#in call names
INCALL_REQUEST = 1
INCALL_LIST = 0

class ProtocolObject:

	COOKIE_DECODER = Decoder([dec.bin])
	COOKIE_TEXT_U16_TEXTKVLIST_DECODER = Decoder([dec.bin,dec.utf8,dec.dslist(dec.dsentry(dec.utf8,dec.utf8))])

	def __init__(self):
		self.decVersion = DECVERSION
		self.decoders = {
			INCALL_LIST: self.COOKIE_DECODER,
			INCALL_REQUEST: self.COOKIE_TEXT_U16_TEXTKVLIST_DECODER,
		}

class ProtocolObject:
	COOKIE_TEXTLIST_ENCODER = Encoder([cookie,enc.dslist(enc.utf8)])
	MODE_COOKIE_MID_IFID_ENCODER = ModeEncoder([enc.u8,enc.bin,[enc.u64],[enc.u32]])
	NO_ARGS_ENCODER = Encoder([])

	def __init__(self):
		self.encVersion = ENCVERSION
		self.encoders = {
			CALL_REQUEST: self.MODE_COOKIE_MID_IFID_ENCODER,
			CALL_NEW_TYPES: self.NO_ARGS_ENCODER,
			CALL_LIST: self.COOKIE_TEXTLIST_ENCODER,
		}

PORT_ENCODER_R = ProtocolREncoderObject()
PORT_DECODER_P = ProtocolPDecoderObject()
PORTSETUP_PR = Portsetup(PROTOCOL,THIS_SIDE,DECVERSION,OTHER_SIDE,ENCVERSION)

def register(modclient,portname="ifrequest",interfacename="ifrequest")
	modclient.registerPortProtocolDecoder(PROTOCOL,THIS_SIDE,PORT_DECODER_P)
	modclient.registerPortProtocolEncoder(PROTOCOL,OTHER_SIDE,PORT_ENCODER_R)
	modclient.registerPortSetup(portname,PORTSETUP_PR)
	modclient.registerInterfaceSetup(interfacename,{DEFAULT_PORT:PORTSETUP_PR})

class Proxy:
	def __init__(self,modclient,interfaceId,connectionId,portId,provider):
		self.interfaceId = interfaceId
		self.portId = portId
		self.connectionId = connectionId
		self.modclient = modclient
		self.cookieJar = CookieJar()
		self.hooks = {}
		self.hooks["connection."+str(interfaceId)+"."+str(connectionId)+".closed"] = self._hookDisconnect
		self.hooks["interface."+str(interfaceId)+".closed"] = self._hookDisconnect
		self.hooks[modclient.getCallEventName(interfaceId,portId,INCALL_LIST)] = Context(self.provider.list,proxy=self)
		self.hooks[modclient.getCallEventName(interfaceId,portId,INCALL_REQUEST)] = Context(self.provider.request,proxy=self)
		self.modclient.don(hooks)

	def _call(self,functionId,*arguments):
		self.modclient.call(self.interfaceId,self.connectionId,self.portId,functionId,*arguments)
		
	def _hookDisconnect(self,**context):
		self.modclient.dignore(self.hooks)

	def _fireNormalCookie(self,cookie,*arguments,**context):
		self.cookieJar.fire(cookie,*arguments,**context)
		
	def _fireModeCookie(self,mode,cookie,*arguments,**context):
		self.cookieJar.fire(cookie,mode,*arguments,**context)

	def newTypes(self):
		self._call(CALL_NEW_TYPES)
		
class InterfaceProxy:
	def __init__(self,modclient,interfaceId,onInterfaceAlreadyOpen=None,portId=DEFAULT_PORT)
		self.modclient = modclient
		self.interfaceId = interfaceId
		self.portId = portId
		self.eventbus = Eventbus()
		self.connectionIds = []
		self.handlers = {}
		if not self.modclient.openInterface(interfaceId,{DEFAULT_PORT:PORTSETUP_PR},-1):
			if callable(onInterfaceAlreadyOpen):
				onInterfaceAlreadyOpen()
	
	def _call(self,connectionId,functionId,*arguments):
		self.modclient.call(self.interfaceId,connectionId,self.portId,functionId,*arguments)
		
	def _onNewConnection(self,connectionId,**context):
		self.connectionIds.append(connectionId)
			
	def _onConnectionClosed(self,connectionId,**context):
		self.connectionIds.remove(connectionId)
			
	def _onNewTypes(self):
		for connectionId in self.connectionIds:	
			self._call(connectionId,CALL_NEW_TYPES)
	
	def setHandler(self,iftype,handler):
		if not callable(handler):
			raise TypeError("Handler has to be callable")
		self.handlers[iftype] = handler
		self._onNewTypes()
				
	def removeHandler(self,iftype):
		del self.handlers[iftype]
		self._onNewTypes()
		
	def _onRequestCall(self,cookie,iftype,arguments,**context):
		if iftype in self.handlers:
			context["ifr_cookie"] = cookie
			context["iftype"] = iftype
			context["proxy"] = self
			self.handlers(version,**context)
		else:
			self.modclient.callback(1,cookie,**context) #type not avaiable
			
	def _onListCall(self,cookie,**context):
		ret = []
		for iftype,handler in self.handlers:
			ret.append(iftype)
		self.modclient.callback(0,cookie,list,**context)
		
	def returnCreated(self,moduleId,interfaceId,ifr_cookie=None,**context):
		if ifr_cookie is None:
			raise TypeError("ifr_cookie must be present")
		self.modclient.callback(0,ifr_cookie,moduleId,interfaceId)
		
	def returnInvalidArgument(self,ifr_cookie=None,**context):
		if ifr_cookie is None:
			raise TypeError("ifr_cookie must be present")
		self.modclient.callback(2,ifr_cookie)
		
	def returnCannotSupplyInterface(self,ifr_cookie=None,**context):
		if ifr_cookie is None:
			raise TypeError("ifr_cookie must be present")
		self.modclient.callback(3,ifr_cookie)
