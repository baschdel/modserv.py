#This is inentionally uncommented to throw an error
WARNING: This implements an outdated experimantal version of the portlib protocol
#DO NOT USE!

from modserv.portsetup import Portsetup
from modserv.transcoders import Decoder,Encoder,ModeDecoder
import modserv.decoders as dec
import modserv.encoders as enc
from modserv.eventbus import Eventbus,EventCallback
from modserv.cookiejar import CookieJar

PROTOCOL = "config-provider"
DEFAULT_PORT = 340
DEFAULT_INTERFACE = 69
CALL_GET_PROVIDER_INFO = 0
CALL_LIST_SECTIONS = 1
CALL_GET_SECTION_INFO = 2
CALL_READ_SECTION_DATA = 3
CALL_WRITE_SECTION_DATA = 4
CALL_MAKE_SECTION = 5
CALL_DELETE_SECTION = 6
CALL_SET_SECTION_ENABLED = 7
CALL_LIST_INDICES = 8
CALL_GET_INDEX_INFO = 9
CALL_RESOLVE_SECTION_NAME = 10
CALL_LIST_INDEX_SUPERSECTIONS = 11
CALL_LIST_INDEX_SECTIONS = 12
CALL_MAKE_INDEX = 13
CALL_MAKE_INDEX_SUPERSECTION = 14
CALL_REMOVE_INDEX = 15
CALL_REMOVE_INDEX_SUPERSECTION = 16
CALL_MAP_INDEX_SECTION = 17
CALL_MAP_INDEX_EXTERNAL_SECTION = 18
CALL_UNMAP_INDEX_SECTION = 19
CALL_SET_INDEX_ENABLED = 20
CALL_RENAME_PROVIDER = 21
CALL_SET_SECTION_APPLICATION_WRITABLE = 22
CALL_BACKEND_CHANGED = 23
CALL_NAME_UPDATED = 24
CALL_SET_DEFAULT_SUPERSECTION = 25

class ConfigProviderClientProtocolObject:
	
	RETURN_PROVDER_INFO_DECODER = Decoder([dec.u8,dec.bin,dec.utf8,dec.utf8,dec.bool,dec.bool,dec.u64,dec.u64])
	MODE_COOKIE_TEXTLIST_DECODER = Decoder([dec.u8,dec.bin,dec.dslist(dec.utf8)])
	RETURN_SECTION_INFO_DECODER = Decoder([dec.u8,dec.bin,dec.utf8,dec.u32,dec.bool,dec.bool,dec.bool])
	MODE_COOKIE_DATA_DECODER = Decoder([dec.u8,dec.bin,dec.bin])
	MODE_COOKIE_DECODER = Decoder([dec.u8,dec.bin])
	MODE_COOKIE_TEXT_BOOL_BOOL_DECODER = Decoder([dec.u8,dec.bin,dec.utf8,dec.bool,dec.bool])
	RETURN_RESOLVED_SECTION_NAMES_DECODER = ModeDecoder([dec.u8,dec.bin,dec.utf8,[dec.bool,dec.utf8]])
	
	COOKIE_ENCODER = Encoder([enc.bin])
	COOKIE_TEXT_ENCODER = Encoder([enc.bin,enc.utf8])
	COOKIE_TEXT_DATA_ENCODER = Encoder([enc.bin,enc.utf8,enc.bin])
	COOKIE_TEXT_BOOL_ENCODER = Encoder([enc.bin,enc.utf8,enc.bool])
	COOKIE_TEXT_TEXT_ENCODER = Encoder([enc.bin,enc.utf8,enc.utf8])
	COOKIE_TEXTx4_BOOL_ENCODER = Encoder([enc.bin,enc.utf8,enc.utf8,enc.utf8,enc.utf8,enc.bool])
	COOKIE_TEXTx5_ENCODER = Encoder([enc.bin,enc.utf8,enc.utf8,enc.utf8,enc.utf8,enc.utf8])
	COOKIE_TEXTx3_ENCODER = Encoder([enc.bin,enc.utf8,enc.utf8,enc.utf8])
	COOKIE_TEXT_TEXT_BOOL_BOOL_ENCODER = Encoder([enc.bin,enc.utf8,enc.utf8,enc.bool,enc.bool])
	
	def __init__(self):
		self.encVersion = 1
		self.decVersion = 1
		self.decoders = {
			CALL_GET_PROVIDER_INFO:                self.RETURN_PROVDER_INFO_DECODER,
			CALL_LIST_SECTIONS:                    self.MODE_COOKIE_TEXTLIST_DECODER,
			CALL_GET_SECTION_INFO:                 self.RETURN_SECTION_INFO_DECODER,
			CALL_READ_SECTION_DATA:                self.MODE_COOKIE_DATA_DECODER,
			CALL_WRITE_SECTION_DATA:               self.MODE_COOKIE_DECODER,
			CALL_MAKE_SECTION:                     self.MODE_COOKIE_DECODER,
			CALL_DELETE_SECTION:                   self.MODE_COOKIE_DECODER,
			CALL_SET_SECTION_ENABLED:              self.MODE_COOKIE_DECODER,
			CALL_LIST_INDICES:                     self.MODE_COOKIE_TEXTLIST_DECODER,
			CALL_GET_INDEX_INFO:                   self.MODE_COOKIE_TEXT_BOOL_BOOL_DECODER,
			CALL_RESOLVE_SECTION_NAME:             self.RETURN_RESOLVED_SECTION_NAMES_DECODER,
			CALL_LIST_INDEX_SUPERSECTIONS:         self.MODE_COOKIE_TEXTLIST_DECODER,
			CALL_LIST_INDEX_SECTIONS:              self.MODE_COOKIE_TEXTLIST_DECODER,
			CALL_MAKE_INDEX:                       self.MODE_COOKIE_DECODER,
			CALL_MAKE_INDEX_SUPERSECTION:          self.MODE_COOKIE_DECODER,
			CALL_REMOVE_INDEX:                     self.MODE_COOKIE_DECODER,
			CALL_REMOVE_INDEX_SUPERSECTION:        self.MODE_COOKIE_DECODER,
			CALL_MAP_INDEX_SECTION:                self.MODE_COOKIE_DECODER,
			CALL_MAP_INDEX_EXTERNAL_SECTION:       self.MODE_COOKIE_DECODER,
			CALL_UNMAP_INDEX_SECTION:              self.MODE_COOKIE_DECODER,
			CALL_SET_INDEX_ENABLED:                self.MODE_COOKIE_DECODER,
			CALL_RENAME_PROVIDER:                  self.MODE_COOKIE_DECODER,
			CALL_SET_SECTION_APPLICATION_WRITABLE: self.MODE_COOKIE_DECODER,
			CALL_BACKEND_CHANGED:                  Encoder([]),
			CALL_NAME_UPDATED:                     Encoder([dec.utf8]),
			CALL_SET_DEFAULT_SUPERSECTION:         self.MODE_COOKIE_DECODER
		}
		self.encoders = {
			CALL_GET_PROVIDER_INFO:                self.COOKIE_ENCODER,
			CALL_LIST_SECTIONS:                    self.COOKIE_ENCODER,
			CALL_GET_SECTION_INFO:                 self.COOKIE_TEXT_ENCODER,
			CALL_READ_SECTION_DATA:                self.COOKIE_TEXT_ENCODER,
			CALL_WRITE_SECTION_DATA:               self.COOKIE_TEXT_DATA_ENCODER,
			CALL_MAKE_SECTION:                     self.COOKIE_TEXT_TEXT_BOOL_BOOL_ENCODER,
			CALL_DELETE_SECTION:                   self.COOKIE_TEXT_ENCODER,
			CALL_SET_SECTION_ENABLED:              self.COOKIE_TEXT_BOOL_ENCODER,
			CALL_LIST_INDICES:                     self.COOKIE_ENCODER,
			CALL_GET_INDEX_INFO:                   self.COOKIE_TEXT_ENCODER,
			CALL_RESOLVE_SECTION_NAME:             self.COOKIE_TEXTx3_ENCODER,
			CALL_LIST_INDEX_SUPERSECTIONS:         self.COOKIE_TEXT_ENCODER,
			CALL_LIST_INDEX_SECTIONS:              self.COOKIE_TEXT_TEXT_ENCODER,
			CALL_MAKE_INDEX:                       self.COOKIE_TEXT_ENCODER,
			CALL_MAKE_INDEX_SUPERSECTION:          self.COOKIE_TEXT_TEXT_ENCODER,
			CALL_REMOVE_INDEX:                     self.COOKIE_TEXT_ENCODER,
			CALL_REMOVE_INDEX_SUPERSECTION:        self.COOKIE_TEXT_TEXT_ENCODER,
			CALL_MAP_INDEX_SECTION:                self.COOKIE_TEXTx4_BOOL_ENCODER,
			CALL_MAP_INDEX_EXTERNAL_SECTION:       self.COOKIE_TEXTx5_ENCODER,
			CALL_UNMAP_INDEX_SECTION:              self.COOKIE_TEXTx3_ENCODER,
			CALL_SET_INDEX_ENABLED:                self.COOKIE_TEXT_BOOL_ENCODER,
			CALL_RENAME_PROVIDER:                  self.COOKIE_TEXT_ENCODER,
			CALL_SET_SECTION_APPLICATION_WRITABLE: self.COOKIE_TEXT_BOOL_ENCODER,
			#CALL_BACKEND_CHANGED:                  ,
			#CALL_NAME_UPDATED:                     ,
			CALL_SET_DEFAULT_SUPERSECTION:         self.COOKIE_TEXT_TEXT_ENCODER
		}
		
PORT_OBJECT = ConfigProviderClientProtocolObject()
PORTSETUP_R = Portsetup(PROTOCOL,"r",1,"p",1)

def register(modclient,portname=PROTOCOL+"-client",interfacename=PROTOCOL+"-client"):
	modclient.registerPortSetup(portname,PORTSETUP_R)
	modclient.registerPortProtocolEncoder(PROTOCOL,"p",PORT_OBJECT)
	modclient.registerPortProtocolDecoder(PROTOCOL,"r",PORT_OBJECT)
	modclient.registerInterfaceSetup(interfacename,{DEFAULT_PORT:PORTSETUP_R})
	
class ConfigProviderClientProxy:
	def __init__(self,modclient,interfaceId,connectionId,portId):
		self.interfaceId = interfaceId
		self.portId = portId
		self.connectionId = connectionId
		self.modclient = modclient
		self.cookieJar = CookieJar()
		self.eventbus = Eventbus()
		self.hooks = {}
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_GET_PROVIDER_INFO)]   = self._fireModeCookie
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_LIST_SECTIONS)]       = self._fireModeCookie
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_GET_SECTION_INFO)]    = self._fireModeCookie
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_READ_SECTION_DATA)]   = self._fireModeCookie
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_WRITE_SECTION_DATA)]  = self._fireModeCookie
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_MAKE_SECTION)]        = self._fireModeCookie
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_DELETE_SECTION)]      = self._fireModeCookie
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_SET_SECTION_ENABLED)] = self._fireModeCookie
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_LIST_INDICES)]        = self._fireModeCookie
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_GET_INDEX_INFO)]      = self._fireModeCookie
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_RESOLVE_SECTION_NAME)]     = self._fireModeCookie
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_LIST_INDEX_SUPERSECTIONS)] = self._fireModeCookie
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_LIST_INDEX_SECTIONS)]      = self._fireModeCookie
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_MAKE_INDEX)]               = self._fireModeCookie
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_MAKE_INDEX_SUPERSECTION)]  = self._fireModeCookie
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_REMOVE_INDEX)]             = self._fireModeCookie
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_REMOVE_INDEX_SUPERSECTION)]  = self._fireModeCookie
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_MAP_INDEX_SECTION)]          = self._fireModeCookie
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_MAP_INDEX_EXTERNAL_SECTION)] = self._fireModeCookie
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_SET_INDEX_ENABLED)]                = self._fireModeCookie
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_RENAME_PROVIDER)]                  = self._fireModeCookie
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_SET_SECTION_APPLICATION_WRITABLE)] = self._fireModeCookie
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_BACKEND_CHANGED)] = EventCallback(self.eventbus,"backendChanged")
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_NAME_UPDATED)]    = EventCallback(self.eventbus,"nameChanged")
		self.modclient.don(self.hooks)
		
	def getProviderInfo(self,
		onSuccess=None,onBackendNotConnected=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onBackendNotConnected],**context)
		self._call(CALL_GET_PROVIDER_INFO,cookie)
		
	def listSections(self,
		onSuccess=None,onBackendNotConnected=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onBackendNotConnected],**context)
		self._call(CALL_LIST_SECTIONS,cookie)
		
	def getSectionInfo(self,name,
		onSuccess=None,onNoSuchSection=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onNoSuchSection],**context)
		self._call(CALL_GET_ConfigProviderClientProxySECTION_INFO,cookie,name)
	
	def readSectionData(self,name,
		onSuccess=None,onNoSuchSection=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onNoSuchSection],**context)
		self._call(CALL_READ_SECTION_DATA,cookie,name)
		
	def writeSectionData(self,name,data,
		onSuccess=None,onNoSuchSection=None,onReadOnly=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onNoSuchSection,onReadOnly],**context)
		self._call(CALL_WRITE_SECTION_DATA,cookie,name,data)
		
	def makeSection(self,name,rawType,enabled,applicationWritable,
		onSuccess=None,onAlreadyExists=None,onReadOnly=None,onInvalidName=None,onInvalidType=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onAlreadyExists,onReadOnly,onInvalidName,onInvalidType],**context)
		self._call(CALL_MAKE_SECTION,cookie,name,rawType,enabled,applicationWritable)
		
	def deleteSection(self,name,
		onSuccess=None,onReadOnly=None,onNoSuchSection=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onReadOnly,onNoSuchSection],**context)
		self._call(CALL_DELETE_SECTION,cookie,name) 
		
	def setSectionEnabled(self,name,enabled,
		onSuccess=None,onNoSuchSection=None,onReadOnly=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onNoSuchSection,onReadOnly],**context)
		self._call(CALL_SET_SECTION_ENABLED,cookie,name,enabled)
		
	def listIndices(self,
		onSuccess=None,onBackendNotConnected=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onBackendNotConnected],**context)
		self._call(CALL_LIST_INDICES,cookie)
		
	def getIndexInfo(self,name,
		onSuccess=None,onNoSuchIndex=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onNoSuchIndex],**context)
		self._call(CALL_GET_INDEX_INFO,cookie,name)
		
	def resolveSectionName(self,indexName,superSectionName,sectionName,
		onSection=None,onExternalSection=None,onNoSuchIndex=None,onNoSuchSupersection=None,onNoSuchSection=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSection,onExternalSection,onNoSuchIndex,onNoSuchSupersection,onNoSuchSection],**context)
		self._call(CALL_RESOLVE_SECTION_NAME,cookie,indexName,superSectionName,sectionName)
		
	def listIndexSupersections(self,indexName,
		onSuccess=None,onNoSuchIndex=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onNoSuchIndex],**context)
		self._call(CALL_LIST_INDEX_SUPERSECTIONS,cookie,indexName)
		
	def listIndexSections(self,indexName,superSectionName,
		onSuccess=None,onNoSuchIndex=None,onNoSuchSupersection=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onNoSuchIndex,onNoSuchSupersection],**context)
		self._call(CALL_LIST_INDEX_SECTIONS,cookie,cookie,indexName,superSectionName)
		
	def makeIndex(self,name,
		onSuccess=None,onAlreadyExists=None,onReadOnly=None,onInvalidName=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onAlreadyExists,onReadOnly,onInvalidName],**context)
		self._call(CALL_MAKE_INDEX,cookie,name)
		
	def makeIndexSupersection(self,indexName,superSectionName,
		onSuccess=None,onAlreadyExists=None,onReadOnly=None,onNoSuchIndex=None,onInvalidSupersectionName=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onAlreadyExists,onReadOnly,onNoSuchIndex,onInvalidSupersectionName],**context)
		self._call(CALL_MAKE_INDEX_SUPERSECTION,cookie,indexName,superSectionName)
		
	def removeIndex(self,name,
		onSuccess=None,onDoesNotExist=None,onReadOnly=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onDoesNotExist,onReadOnly],**context)
		self._call(CALL_REMOVE_INDEX,cookie,name)
		
	def removeIndexSupersection(self,indexName,superSectionName,
		onSuccess=None,onDoesNotExist=None,onReadOnly=None,onNoSuchIndex=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onDoesNotExist,onReadOnly,onNoSuchIndex],**context)
		self._call(CALL_REMOVE_INDEX_SUPERSECTION,cookie,indexName,superSectionName)
		
	def mapIndexSection(self,indexName,superSectionName,sectionName,destinationSectionName,applicationWritable,
		onSuccess=None,onReadOnly=None,onNoSuchIndex=None,onNoSuchSupersection=None,
		onInvalidName=None,onInvalidDestinationName=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onReadOnly,onNoSuchIndex,onNoSuchSupersection,onInvalidName,onInvalidDestinationName],**context)
		self._call(CALL_MAP_INDEX_SECTION,cookie,indexName,superSectionName,sectionName,destinationSectionName,applicationWritable)
		
	def mapIndexExternalSection(self,indexName,superSectionName,sectionName,provider,resource,
		onSuccess=None,onReadOnly=None,onNoSuchIndex=None,onNoSuchSupersection=None, 
		onInvalidName=None,onInvalidProviderName=None,onInvalidResourceName=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onReadOnly,onNoSuchIndex,onNoSuchSupersection,onInvalidName,onInvalidProviderName,onInvalidResourceName],**context)
		self._call(CALL_MAP_INDEX_EXTERNAL_SECTION,cookie,indexName,superSectionName,sectionName,provider,resource)
		
	def unmapIndexSection(self,indexName,superSectionName,sectionName,
		onSuccess=None,onReadOnly=None,onNoSuchIndex=None,onNoSuchSupersection=None,onNoSuchSection=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onReadOnly,onNoSuchIndex,onNoSuchSupersection,onNoSuchSection],**context)
		self._call(CALL_UNMAP_INDEX_SECTION,cookie,indexName,superSectionName,sectionName)
		
	def setIndexEnabled(self,name,enabled,
		onSuccess=None,onNoSuchIndex=None,onReadOnly=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onNoSuchIndex,onReadOnly],**context)
		self._call(CALL_SET_INDEX_ENABLED,cookie,name,enabled)
		
	def renameProvider(self,newName,
		onSuccess=None,onReadOnly=None,onInvalidName=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onReadOnly,onInvalidName],**context)
		self._call(CALL_RENAME_PROVIDER,cookie,newName)
		
	def setSectionApplicationWritable(self,sectionName,applicationWritable,
		onSuccess=None,onReadOnly=None,onNoSuchSection=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onReadOnly,onNoSuchSection],**context)
		self._call(CALL_SET_SECTION_APPLICATION_WRITABLE,cookie,sectionName,applicationWritable)
	
	def setDefaultSupersection(self,indexName,supersectionName,
		onSuccess=None,onNoSuchIndex=None,onReadOnly=None,onInvalidSupersectionName=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onNoSuchIndex,onReadOnly,onInvalidSupersectionName],**context)
		self._call(CALL_SET_DEFAULT_SUPERSECTION,cookie,indexName,supersectionName)
		
	def onNameChaged(self,callback):
		self.eventbus.on("nameChanged",callback)
		
	def onBackendChaged(self,callback):
		self.eventbus.on("backendChanged",callback)
		
	def ignoreNameChaged(self,callback):
		self.eventbus.on("nameChanged",callback)
		
	def ignoreBackendChaged(self,callback):
		self.eventbus.on("backendChanged",callback)
		
	def _call(self,functionId,*arguments):
		self.modclient.call(self.interfaceId,self.connectionId,self.portId,functionId,*arguments)
		
	def _fireNormalCookie(self,cookie,*arguments,**context):
		self.cookieJar.fire(cookie,*arguments,**context)
		
	def _fireModeCookie(self,mode,cookie,*arguments,**context):
		self.cookieJar.fire(cookie,mode,*arguments,**context)
		
	def _hookDisconnect(self,**context):
		self.modclient.dignore(self.hooks)
