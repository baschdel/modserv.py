from modserv.portsetup import Portsetup
from modserv.transcoders import Encoder,Decoder,ModeEncoder,ModeDecoder
import modserv.encoders as enc
import modserv.decoders as dec
from modserv.cookiejar import CookieJar
from modserv.modclient import Context

PROTOCOL = "vsfs"
ENCVERSION = 1
DECVERSION = 1
DEFAULT_PORT = 9
DEFAULT_INTERFACE = 9
THIS_SIDE = "s"
OTHER_SIDE = "c"

#call names
CALL_READ = 0
CALL_WRITE = 1
CALL_APPEND = 2
CALL_DELETE = 3
CALL_LIST = 4
CALL_GET_SIZE = 5

#in call names
INCALL_READ = 0
INCALL_WRITE = 1
INCALL_APPEND = 2
INCALL_DELETE = 3
INCALL_LIST = 4
INCALL_GET_SIZE = 5

class ProtocolObject:
	MODE_COOKIE_ENCODER = Encoder([enc.u8(),bin()])
	MODE_COOKIE_DATA_ENCODER = ModeEncoder([enc.u8(),bin(),[bin()]])
	COOKIE_TEXTDSLIST_ENCODER = Encoder([bin(),enc.dslist(enc.utf8())])
	MODE_COOKIE_SIZE_ENCODER = Encoder([enc.u8(),bin(),enc.u32()])

	COOKIE_TEXT_DATA_DECODER = Decoder([bin(),dec.utf8(),bin()])
	COOKIE_TEXT_DECODER = Decoder([bin(),dec.utf8()])

	def __init__(self):
		self.encVersion = ENCVERSION
		self.decVersion = DECVERSION
		self.encoders = {
			CALL_LIST: self.COOKIE_TEXTDSLIST_ENCODER
			CALL_GET_SIZE: self.MODE_COOKIE_SIZE_ENCODER
			CALL_WRITE: self.MODE_COOKIE_ENCODER
			CALL_DELETE: self.MODE_COOKIE_ENCODER
			CALL_READ: self.MODE_COOKIE_DATA_ENCODER
			CALL_APPEND: self.MODE_COOKIE_ENCODER
		}
		self.decoders = {
			INCALL_LIST: self.COOKIE_TEXT_DECODER
			INCALL_GET_SIZE: self.COOKIE_TEXT_DECODER
			INCALL_WRITE: self.COOKIE_TEXT_DATA_DECODER
			INCALL_DELETE: self.COOKIE_TEXT_DECODER
			INCALL_READ: self.COOKIE_TEXT_DECODER
			INCALL_APPEND: self.COOKIE_TEXT_DATA_DECODER
		}

PORT_OBJECT = ProtocolObject()
PORTSETUP_S = Portsetup(PROTOCOL,"s",DECVERSION,"c",ENCVERSION)

def register(modclient,portname="vsfs",interfacename="vsfs")
	modclient.registerPortSetup(portname,PORTSETUP_S)
	modclient.registerPortProtocolEncoder(PROTOCOL,"c",PORT_OBJECT)
	modclient.registerPortProtocolDecoder(PROTOCOL,"s",PORT_OBJECT)
	modclient.registerInterfaceSetup(interfacename,{DEFAULT_PORT:PORTSETUP_S})

class Proxy:
	def __init__(self,modclient,interfaceId,connectionId,portId):
		self.interfaceId = interfaceId
		self.portId = portId
		self.connectionId = connectionId
		self.modclient = modclient
self.cookieJar = CookieJar()
		self.hooks = {}
		self.hooks["connection."+str(interfaceId)+"."+str(connectionId)+".closed"] = self._hookDisconnect
		self.hooks["interface."+str(interfaceId)+".closed"] = self._hookDisconnect
		self.hooks[modclient.getCallEventName(interfaceId,portId,INCALL_READ)]     = Context(self.read,proxy=None)
		self.hooks[modclient.getCallEventName(interfaceId,portId,INCALL_WRITE)]    = Context(self.write,proxy=None)
		self.hooks[modclient.getCallEventName(interfaceId,portId,INCALL_APPEND)]   = Context(self.append,proxy=None)
		self.hooks[modclient.getCallEventName(interfaceId,portId,INCALL_DELETE)]   = Context(self.delete,proxy=None)
		self.hooks[modclient.getCallEventName(interfaceId,portId,INCALL_LIST)]     = Context(self.list,proxy=None)
		self.hooks[modclient.getCallEventName(interfaceId,portId,INCALL_GET_SIZE)] = Context(self.getSize,proxy=None)
		self.modclient.don(hooks)
		
	def _call(self,functionId,*arguments):
		self.modclient.call(interfaceId,connectionId,portId,functionId,*arguments)
		
	def _hookDisconnect(self,**context):
		self.modclient.dignore(self.hooks)

	def _fireNormalCookie(self,cookie,*arguments,**context):
		self.cookieJar.fire(cookie,*arguments,**context)
		
	def _fireModeCookie(self,mode,cookie,*arguments,**context):
		self.cookieJar.fire(cookie,mode,*arguments,**context)

	def read(self,cookie,name,**context):
		pass
		
	def write(self,cookie,name,data,**context):
		pass
		
	def append(self,cookie,name,data,**context):
		pass
		
	def delete(self,cookie,name,**context):
		pass
		
	def list(self,cookie,name,**context):
		pass
		
	def getSize(self,cookie,name):
		pass
		
