from modserv.portsetup import Portsetup
from modserv.transcoders import Encoder,Decoder,ModeEncoder,ModeDecoder
from modserv.modclient import Context
from modserv.eventbus import Eventbus
import modserv.encoders as enc
import modserv.decoders as dec

from modserv.cookiejar import CookieJar

PROTOCOL = ifrequest
ENCVERSION = 1
DECVERSION = 1
DEFAULT_PORT = 9
DEFAULT_INTERFACE = 9
THIS_SIDE = "r"
OTHER_SIDE = "p"

#call names
CALL_REQUEST = 1
CALL_LIST = 0

#in call names
INCALL_LIST = 0
INCALL_REQUEST = 1
INCALL_NEW_TYPES = 2

class ProtocolObject:

	MODE_COOKIE_MID_IFID_DECODER = ModeDecoder([dec.u8,dec.bin,[dec.u64],[dec.u32]])
	COOKIE_TEXTLIST_DECODER = Decoder([cookie,dec.utf8,dec.u16])
	NO_ARGS_DECODER = Decoder([])

	def __init__(self):
		self.decVersion = DECVERSION
		self.decoders = {
			INCALL_NEW_TYPES: self.NO_ARGS_DECODER,
			INCALL_REQUEST: self.MODE_COOKIE_MID_IFID_DECODER,
			INCALL_LIST: self.COOKIE_TEXTLIST_DECODER,
		}

class ProtocolObject:
	COOKIE_TEXT_TEXTKVLIST_ENCODER = Encoder([enc.bin,enc.utf8,enc.dslist(enc.dsentry(enc.utf8,enc.utf8))])
	COOKIE_ENCODER = Encoder([enc.bin])

	def __init__(self):
		self.encVersion = ENCVERSION
		self.encoders = {
			CALL_LIST: self.COOKIE_ENCODER,
			CALL_REQUEST: self.COOKIE_TEXT_TEXTKVLIST_ENCODER,
		}

PORT_ENCODER_P = ProtocolPEncoderObject()
PORT_DECODER_R = ProtocolRDecoderObject()
PORTSETUP_RP = Portsetup(PROTOCOL,THIS_SIDE,DECVERSION,OTHER_SIDE,ENCVERSION)

def register(modclient,portname="ifrequest-client",interfacename="ifrequest-client")
	modclient.registerPortProtocolDecoder(PROTOCOL,THIS_SIDE,PORT_DECODER_R)
	modclient.registerPortProtocolEncoder(PROTOCOL,OTHER_SIDE,PORT_ENCODER_P)
	modclient.registerPortSetup(portname,PORTSETUP_RP)
	modclient.registerInterfaceSetup(interfacename,{DEFAULT_PORT:PORTSETUP_RP})

class Proxy:
	def __init__(self,modclient,interfaceId,connectionId,portId):
		self.interfaceId = interfaceId
		self.portId = portId
		self.connectionId = connectionId
		self.modclient = modclient
		self.cookieJar = CookieJar()
		self.newInterfaceTypeListeners = []
		self.hooks = {}
		self.hooks["connection."+str(interfaceId)+"."+str(connectionId)+".closed"] = self._hookDisconnect
		self.hooks["interface."+str(interfaceId)+".closed"] = self._hookDisconnect
		self.hooks[modclient.getCallEventName(interfaceId,portId,INCALL_NEW_TYPES)] = self._onNewTypes
		self.hooks[modclient.getCallEventName(interfaceId,portId,INCALL_REQUEST)] = self._fireModeCookie
		self.hooks[modclient.getCallEventName(interfaceId,portId,INCALL_LIST)] = self._fireNormalCookie
		self.modclient.don(hooks)

	def _call(self,functionId,*arguments):
		self.modclient.call(self.interfaceId,self.connectionId,self.portId,functionId,*arguments)
		
	def _hookDisconnect(self,**context):
		self.modclient.dignore(self.hooks)

	def _fireNormalCookie(self,cookie,*arguments,**context):
		self.cookieJar.fire(cookie,*arguments,**context)
		
	def _fireModeCookie(self,mode,cookie,*arguments,**context):
		self.cookieJar.fire(cookie,mode,*arguments,**context)
		
	def onNewTypes(self,callback):	
		self.newInterfaceTypeListeners.append(callback)
		
	def ignoreNewTypes(self,callback):
		self.newInterfaceTypeListeners.remove(callback)
		
	def _onNewTypes(self,**context):
		for call in self.newInterfaceTypeListeners:
			if callable(call):
				call()
	
	def list(self,onSuccess=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),onSuccess,**context)
		self._call(CALL_LIST,cookie)
		
	def reqest(self,interface,version,arguments,
		onSuccess=None,onInterfaceNotAvaiable=None,onInvalidArguments=None,onCannotSupply=None,**context):
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onInterfaceNotAvaiable,onInvalidArguments,onCannotSupply],**context)
		self._call(CALL_REQUEST,cookie,interface,version,arguments)

class VsfsClientInterfaceProxy:
	def __init__(self,modclient,interfaceId=DEFAULT_INTERFACE,onInterfaceAlreadyOpen=None):
		self.modclient = modclient
		self.interfaceId = interfaceId
		self.proxy = None
		self.eventbus = Eventbus()
		self.modclient.on("interface."+str(interfaceId)+".newConnection",self._onNewConnection)
		modclient.on("interface."+str(interfaceId)+".connectionClosed",self._onConnectionClosed)
		if not self.modclient.openInterface(interfaceId,"ifrequest-client",1):
			if callable(onInterfaceAlreadyOpen):
				onInterfaceAlreadyOpen()
				
	def _onNewConnection(self,connectionId,**context):
		if self.proxy is None:
			self.proxy = VsfsClientProxy(self.modclient,self.interfaceId,connectionId,DEFAULT_PORT)
			self.proxy.onNewTypes(self._onNewTypes)
			self.eventbus._fireEvent("connected")
		else:
			raise CrashAndBurnException("More than one connection open on a single connnection ifrequest-client interface")
			
	def _onConnectionClosed(self,connectionId,**context):
		self.proxy = None
		self.eventbus._fireEvent("disconnected")
		
	def _onNewTypes(self):
		self.eventbus._fireEvent("newTypes")
		
	def on(self,event,callback):
		self.eventbus.on(event,callback)
	
	def ignore(self,event,callback):
		self.eventbus.ignore(event,callback)
		
	def isConnected(self):
		return not (self.proxy is None)
		
	def list(self,onNotConnected=None,**context):
		if self.proxy is None:
			if callable(onNotConnected):
				onNotConnected(**context)
			return
		self.proxy.list(**context)
		
	def request(self,interface,version,arguments,onNotConnected=None,**context):
		if self.proxy is None:
			if callable(onNotConnected):
				onNotConnected(**context)
			return
		self.proxy.getSize(interface,version,arguments,**context)
