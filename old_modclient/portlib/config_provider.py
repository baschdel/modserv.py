#This is inentionally uncommented to throw an error
WARNING: This implements an outdated experimantal version of the portlib protocol
#DO NOT USE!

from modserv.portsetup import Portsetup
from modserv.transcoders import Encoder,Decoder,ModeEncoder
import modserv.encoders as enc
import modserv.decoders as dec
from modserv.modclient import Context

PROTOCOL = "config-provider"
DECVERSION = 1
ENCVERSION = 1
DEFAULT_PORT = 340
DEFAULT_INTERFACE = 70
CALL_GET_PROVIDER_INFO = 0
CALL_LIST_SECTIONS = 1
CALL_GET_SECTION_INFO = 2
CALL_READ_SECTION_DATA = 3
CALL_WRITE_SECTION_DATA = 4
CALL_MAKE_SECTION = 5
CALL_DELETE_SECTION = 6
CALL_SET_SECTION_ENABLED = 7
CALL_LIST_INDICES = 8
CALL_GET_INDEX_INFO = 9
CALL_RESOLVE_SECTION_NAME = 10
CALL_LIST_INDEX_SUPERSECTIONS = 11
CALL_LIST_INDEX_SECTIONS = 12
CALL_MAKE_INDEX = 13
CALL_MAKE_INDEX_SUPERSECTION = 14
CALL_REMOVE_INDEX = 15
CALL_REMOVE_INDEX_SUPERSECTION = 16
CALL_MAP_INDEX_SECTION = 17
CALL_MAP_INDEX_EXTERNAL_SECTION = 18
CALL_UNMAP_INDEX_SECTION = 19
CALL_SET_INDEX_ENABLED = 20
CALL_RENAME_PROVIDER = 21
CALL_SET_SECTION_APPLICATION_WRITABLE = 22
CALL_BACKEND_CHANGED = 23
CALL_NAME_UPDATED = 24
CALL_SET_DEFAULT_SUPERSECTION = 25

class ConfigProviderProtocolObject:
	
	RETURN_PROVDER_INFO_ENCODER = Encoder([enc.u8,enc.bin,enc.utf8,enc.utf8,enc.bool,enc.bool,enc.u64,enc.u64])
	MODE_COOKIE_TEXTLIST_ENCODER = Encoder([enc.u8,enc.bin,enc.dslist(enc.utf8)])
	RETURN_SECTION_INFO_ENCODER = Encoder([enc.u8,enc.bin,enc.utf8,enc.u32,enc.bool,enc.bool,enc.bool])
	MODE_COOKIE_DATA_ENCODER = Encoder([enc.u8,enc.bin,enc.bin])
	MODE_COOKIE_ENCODER = Encoder([enc.u8,enc.bin])
	MODE_COOKIE_TEXT_BOOL_BOOL_ENCODER = Encoder([enc.u8,enc.bin,enc.utf8,enc.bool,enc.bool])
	RETURN_RESOLVED_SECTION_NAMES_ENCODER = ModeEncoder([enc.u8,enc.bin,enc.utf8,[enc.bool,enc.utf8]])
	
	COOKIE_DECODER = Decoder([dec.bin])
	COOKIE_TEXT_DECODER = Decoder([dec.bin,dec.utf8])
	COOKIE_TEXT_DATA_DECODER = Decoder([dec.bin,dec.utf8,dec.bin])
	COOKIE_TEXT_BOOL_DECODER = Decoder([dec.bin,dec.utf8,dec.bool])
	COOKIE_TEXT_TEXT_DECODER = Decoder([dec.bin,dec.utf8,dec.utf8])
	COOKIE_TEXTx4_BOOL_DECODER = Decoder([dec.bin,dec.utf8,dec.utf8,dec.utf8,dec.utf8,dec.bool])
	COOKIE_TEXTx5_DECODER = Decoder([dec.bin,dec.utf8,dec.utf8,dec.utf8,dec.utf8,dec.utf8])
	COOKIE_TEXTx3_DECODER = Decoder([dec.bin,dec.utf8,dec.utf8,dec.utf8])
	COOKIE_TEXT_TEXT_BOOL_BOOL_DECODER = Decoder([dec.bin,dec.utf8,dec.utf8,dec.bool,dec.bool])
		
	def __init__(self):
		self.encVersion = ENCVERSION
		self.decVersion = DECVERSION
		self.encoders = {
			CALL_GET_PROVIDER_INFO:                self.RETURN_PROVDER_INFO_ENCODER,
			CALL_LIST_SECTIONS:                    self.MODE_COOKIE_TEXTLIST_ENCODER,
			CALL_GET_SECTION_INFO:                 self.RETURN_SECTION_INFO_ENCODER,
			CALL_READ_SECTION_DATA:                self.MODE_COOKIE_DATA_ENCODER,
			CALL_WRITE_SECTION_DATA:               self.MODE_COOKIE_ENCODER,
			CALL_MAKE_SECTION:                     self.MODE_COOKIE_ENCODER,
			CALL_DELETE_SECTION:                   self.MODE_COOKIE_ENCODER,
			CALL_SET_SECTION_ENABLED:              self.MODE_COOKIE_ENCODER,
			CALL_LIST_INDICES:                     self.MODE_COOKIE_TEXTLIST_ENCODER,
			CALL_GET_INDEX_INFO:                   self.MODE_COOKIE_TEXT_BOOL_BOOL_ENCODER,
			CALL_RESOLVE_SECTION_NAME:             self.RETURN_RESOLVED_SECTION_NAMES_ENCODER,
			CALL_LIST_INDEX_SUPERSECTIONS:         self.MODE_COOKIE_TEXTLIST_ENCODER,
			CALL_LIST_INDEX_SECTIONS:              self.MODE_COOKIE_TEXTLIST_ENCODER,
			CALL_MAKE_INDEX:                       self.MODE_COOKIE_ENCODER,
			CALL_MAKE_INDEX_SUPERSECTION:          self.MODE_COOKIE_ENCODER,
			CALL_REMOVE_INDEX:                     self.MODE_COOKIE_ENCODER,
			CALL_REMOVE_INDEX_SUPERSECTION:        self.MODE_COOKIE_ENCODER,
			CALL_MAP_INDEX_SECTION:                self.MODE_COOKIE_ENCODER,
			CALL_MAP_INDEX_EXTERNAL_SECTION:       self.MODE_COOKIE_ENCODER,
			CALL_UNMAP_INDEX_SECTION:              self.MODE_COOKIE_ENCODER,
			CALL_SET_INDEX_ENABLED:                self.MODE_COOKIE_ENCODER,
			CALL_RENAME_PROVIDER:                  self.MODE_COOKIE_ENCODER,
			CALL_SET_SECTION_APPLICATION_WRITABLE: self.MODE_COOKIE_ENCODER,
			CALL_BACKEND_CHANGED:                  Encoder([]),
			CALL_NAME_UPDATED:                     Decoder([dec.utf8]),
			CALL_SET_DEFAULT_SUPERSECTION:         self.MODE_COOKIE_ENCODER
		}
		self.decoders = {
			CALL_GET_PROVIDER_INFO:                self.COOKIE_DECODER,
			CALL_LIST_SECTIONS:                    self.COOKIE_DECODER,
			CALL_GET_SECTION_INFO:                 self.COOKIE_TEXT_DECODER,
			CALL_READ_SECTION_DATA:                self.COOKIE_TEXT_DECODER,
			CALL_WRITE_SECTION_DATA:               self.COOKIE_TEXT_DATA_DECODER,
			CALL_MAKE_SECTION:                     self.COOKIE_TEXT_TEXT_BOOL_BOOL_DECODER,
			CALL_DELETE_SECTION:                   self.COOKIE_TEXT_DECODER,
			CALL_SET_SECTION_ENABLED:              self.COOKIE_TEXT_BOOL_DECODER,
			CALL_LIST_INDICES:                     self.COOKIE_DECODER,
			CALL_GET_INDEX_INFO:                   self.COOKIE_TEXT_DECODER,
			CALL_RESOLVE_SECTION_NAME:             self.COOKIE_TEXTx3_DECODER,
			CALL_LIST_INDEX_SUPERSECTIONS:         self.COOKIE_TEXT_DECODER,
			CALL_LIST_INDEX_SECTIONS:              self.COOKIE_TEXT_TEXT_DECODER,
			CALL_MAKE_INDEX:                       self.COOKIE_TEXT_DECODER,
			CALL_MAKE_INDEX_SUPERSECTION:          self.COOKIE_TEXT_TEXT_DECODER,
			CALL_REMOVE_INDEX:                     self.COOKIE_TEXT_DECODER,
			CALL_REMOVE_INDEX_SUPERSECTION:        self.COOKIE_TEXT_TEXT_DECODER,
			CALL_MAP_INDEX_SECTION:                self.COOKIE_TEXTx4_BOOL_DECODER,
			CALL_MAP_INDEX_EXTERNAL_SECTION:       self.COOKIE_TEXTx5_DECODER,
			CALL_UNMAP_INDEX_SECTION:              self.COOKIE_TEXTx3_DECODER,
			CALL_SET_INDEX_ENABLED:                self.COOKIE_TEXT_BOOL_DECODER,
			CALL_RENAME_PROVIDER:                  self.COOKIE_TEXT_DECODER,
			CALL_SET_SECTION_APPLICATION_WRITABLE: self.COOKIE_TEXT_BOOL_DECODER,
			#CALL_BACKEND_CHANGED:                  ,
			#CALL_NAME_UPDATED:                     ,
			CALL_SET_DEFAULT_SUPERSECTION:         self.COOKIE_TEXT_TEXT_DECODER
		}
		
PORT_OBJECT = ConfigProviderProtocolObject()
PORTSETUP_P = Portsetup(PROTOCOL,"p",DECVERSION,"r",ENCVERSION)

def register(modclient,portname=PROTOCOL,interfacename=PROTOCOL):
	modclient.registerPortSetup(portname,PORTSETUP_P)
	modclient.registerPortProtocolEncoder(PROTOCOL,"r",PORT_OBJECT)
	modclient.registerPortProtocolDecoder(PROTOCOL,"p",PORT_OBJECT)
	modclient.registerInterfaceSetup(interfacename,{DEFAULT_PORT:PORTSETUP_P})
	
class ConfigProviderProxy:
	def __init__(self,modclient,interfaceId,connectionId,portId,provider,**context):
		self.interfaceId = interfaceId
		self.portId = portId
		self.connectionId = connectionId
		self.modclient = modclient
		self.provider = provider
		self.hooks = {}
		self.hooks["connection."+str(interfaceId)+"."+str(connectionId)+".closed"] = self._hookDisconnect
		self.hooks["interface."+str(interfaceId)+".closed"] = self._hookDisconnect
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_GET_PROVIDER_INFO)]   = Context(provider.getProviderInfo,proxy=self,**context)
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_LIST_SECTIONS)]       = Context(provider.listSections,proxy=self,**context)
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_GET_SECTION_INFO)]    = Context(provider.getSectionInfo,proxy=self,**context)
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_READ_SECTION_DATA)]   = Context(provider.readSectionData,proxy=self,**context)
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_WRITE_SECTION_DATA)]  = Context(provider.writeSectionData,proxy=self,**context)
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_MAKE_SECTION)]        = Context(provider.makeSection,proxy=self,**context)
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_DELETE_SECTION)]      = Context(provider.deleteSection,proxy=self,**context)
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_SET_SECTION_ENABLED)] = Context(provider.setSectionEnabled,proxy=self,**context)
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_LIST_INDICES)]        = Context(provider.listIndices,proxy=self,**context)
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_GET_INDEX_INFO)]      = Context(provider.getIndexInfo,proxy=self,**context)
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_RESOLVE_SECTION_NAME)]     = Context(provider.resolveSectionName,proxy=self,**context)
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_LIST_INDEX_SUPERSECTIONS)] = Context(provider.listIndexSupersections,proxy=self,**context)
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_LIST_INDEX_SECTIONS)]      = Context(provider.listIndexSections,proxy=self,**context)
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_MAKE_INDEX)]               = Context(provider.makeIndex,proxy=self,**context)
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_MAKE_INDEX_SUPERSECTION)]  = Context(provider.makeIndexSupersection,proxy=self,**context)
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_REMOVE_INDEX)]             = Context(provider.removeIndex,proxy=self,**context)
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_REMOVE_INDEX_SUPERSECTION)]  = Context(provider.removeIndexSupersection,proxy=self,**context)
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_MAP_INDEX_SECTION)]          = Context(provider.mapIndexSection,proxy=self,**context)
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_MAP_INDEX_EXTERNAL_SECTION)] = Context(provider.mapIndexExternalSection,proxy=self,**context)
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_UNMAP_INDEX_SECTION)]        = Context(provider.unmapIndexSection,proxy=self,**context)
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_SET_INDEX_ENABLED)]                = Context(provider.setIndexEnabled,proxy=self,**context)
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_RENAME_PROVIDER)]                  = Context(provider.renameProvider,proxy=self,**context)
		self.hooks[modclient.getCallEventName(interfaceId,portId,CALL_SET_SECTION_APPLICATION_WRITABLE)] = Context(provider.setSectionApplicationWritable,proxy=self,**context)
		modclient.don(self.hooks)
	
	def _hookDisconnect(self,**context):
		self.modclient.dignore(self.hooks)
	
	def backendChanged(self):
		self.modclient.call(self.interfaceId,self.connectionId,self.portId,CALL_BACKEND_CHANGED)
			
	def nameChanged(self,newName):
		self.modclient.call(self.interfaceId,self.connectionId,self.portId,CALL_NAME_UPDATED,newName)

class ConfigProviderInterfaceProxy():
	def __init__(self,modclient,provider,interfaceId=DEFAULT_INTERFACE,portId=DEFAULT_PORT,**context):
		self.modclient = modclient
		self.interfaceId = interfaceId
		self.portId = portId
		self.context = context
		self.provider = provider
		self.proxys = {} #maps connrction id to proxy objects
		modclient.on("interface."+str(interfaceId)+".newConnection",self._connectionOpened)
		modclient.on("interface."+str(interfaceId)+".connectionClosed",self._connectionClosed)
		modclient.on("interface."+str(interfaceId)+".closed",self._interfaceClosed)
	
	def _connectionOpened(self,connectionId,**context):
		self.proxys[connectionId] = ConfigProviderProxy(self.modclient,self.interfaceId,connectionId,self.portId,self.provider,**self.context)
		
	def _connectionClosed(self,connectionId,**context):
		del self.proxys[connectionId]
		
	def _interfaceClosed(self,**context):
		self.modclient.ignore("interface."+str(interfaceId)+".newConnection",self._connectionOpened)
		self.modclient.ignore("interface."+str(interfaceId)+".connectionClosed",self._connectionClosed)
		self.modclient.ignore("interface."+str(interfaceId)+".closed",self._interfaceClosed)
		
	def backendChanged(self):
		for connectionId,proxy in self.proxys.items():
			proxy.backendChanged()
			
	def nameChanged(self,newName):
		for connectionId,proxy in self.proxys.items():
			proxy.nameChanged(newName)
		
class DummyConfigProvider:
	def getProviderInfo(self,cookie,proxy=None,**context):
		return [1,cookie]
		
	def listSections(self,cookie,proxy=None,**context):
		return [1,cookie]

	def getSectionInfo(self,cookie,sectionName,proxy=None,**context):
		return [1,cookie]
	
	def readSectionData(self,cookie,sectionName,proxy=None,**context):
		return [1,cookie]
		
	def writeSectionData(self,cookie,sectionName,data,proxy=None,**context):
		return [1,cookie]
		
	def makeSection(self,cookie,sectionName,rawType,enabled,applicationWritable,proxy=None,**context):
		return [2,cookie]
		
	def deleteSection(self,cookie,sectionName,proxy=None,**context):
		return [2,cookie] 
		
	def setSectionEnabled(self,cookie,sectionName,enabled,proxy=None,**context):
		return[1,cookie]
		
	def listIndices(self,cookie,proxy=None,**context):
		return [1,cookie]
		
	def getIndexInfo(self,cookie,indexName,proxy=None,**context):
		return [1,cookie] 
		
	def resolveSectionName(self,cookie,indexName,superSectionName,sectionName,proxy=None,**context):
		return [4,cookie]
		
	def listIndexSupersections(self,cookie,indexName,proxy=None,**context):
		return [1,cookie]
		
	def listIndexSections(self,cookie,indexName,superSectionName,proxy=None,**context):
		return [1,cookie]
		
	def makeIndex(self,cookie,name,proxy=None,**context):
		return [2,cookie]
		
	def makeIndexSupersection(self,cookie,indexName,superSectionName,proxy=None,**context):
		return [3,cookie]
		
	def removeIndex(self,cookie,name,proxy=None,**context):
		return [2,cookie] 
		
	def removeIndexSupersection(self,cookie,indexName,superSectionName,proxy=None,**context):
		return [3,cookie]
		
	def mapIndexSection(self,cookie,indexName,superSectionName,sectionName,destinationSectionName,applicationWritable,proxy=None,**context):
		return [2,cookie]
		
	def mapIndexExternalSection(self,cookie,indexName,superSectionName,sectionName,provider,resource,proxy=None,**context):
		return [2,cookie]
		
	def unmapIndexSection(self,cookie,indexName,superSectionName,sectionName,proxy=None,**context):
		return [2,cookie]
		
	def setIndexEnabled(self,cookie,name,enabled,proxy=None,**context):
		return [2,cookie]
		
	def renameProvider(self,cookie,newName,proxy=None,**context):
		return [1,cookie]
		
	def setSectionApplicationWritable(self,cookie,sectionName,applicationWritable,proxy=None,**context):
		return [1,cookie]
		
	def setDefaultSupersection(self,cookie,indexName,supersectionName,proxy=None,**context):
		return [2,cookie]
