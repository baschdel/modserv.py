from modserv.portsetup import Portsetup

PROTOCOL = "primitive-cli"
VERSION = 1
DEFAULT_PORT = 0
DEFAULT_INTERFACE = 3
CALL_SEND = 0

class CliProtocolObject:
	def __init__(self):
		self.decVersion = VERSION
		self.encVersion = VERSION
		self.encoders = {CALL_SEND:self.encodeSend}
		self.decoders = {CALL_SEND:self.decodeSend}
		
	def encodeSend(self,arguments):
		if len(arguments)>=1:
			return [arguments[0].encode('utf-8')]
		return [b""]
		
	def decodeSend(self,arguments):
		if len(arguments)>=1:
			return [arguments[0].decode('utf-8')]
		return [""]
		
PORT_OBJECT_X = CliProtocolObject()
PORTSETUP = Portsetup(PROTOCOL,"x",VERSION,"x",VERSION)

def register(modclient,portname=PROTOCOL,interfacename=PROTOCOL):
	modclient.registerPortSetup(portname,PORTSETUP)
	modclient.registerPortProtocolEncoder(PROTOCOL,"x",PORT_OBJECT_X)
	modclient.registerPortProtocolDecoder(PROTOCOL,"x",PORT_OBJECT_X)
	modclient.registerInterfaceSetup(interfacename,{DEFAULT_PORT:PORTSETUP})

class CliProxy:
	def __init__(self,modclient,interfaceId,connectionId,portId):
		self.interfaceId = interfaceId
		self.portId = portId
		self.connectionId = connectionId
		self.modclient = modclient
		self.messagehooks = []
		modclient.on("interface."+str(interfaceId)+".closed",self._closehook)
		modclient.on("connection."+str(interfaceId)+"."+str(connectionId)+".closed",self._closehook)
		modclient.on(modclient.getCallEventName(interfaceId,portId,CALL_SEND),self._messagehook)
		
	def onMessage(self,function):
		self.messagehooks.append(function)
		
	def ignoreMessage(self,function):
		if function in self.messagehooks:
			self.messagehooks.remove(function)
			
	def send(self,message):
		self.modclient.call(self.interfaceId,self.connectionId,self.portId,CALL_SEND,message)
	
	def _messagehook(self,message,**context):
		for function in self.messagehooks:
			function(message,**context)
			
	def _closehook(self,**context):
		self.modclient.ignore("interface."+str(self.interfaceId)+".closed",self._closehook)
		self.modclient.ignore("connection."+str(self.interfaceId)+"."+str(self.connectionId)+".closed",self._closehook)
		self.modclient.ignore(self.modclient.getCallEventName(self.interfaceId,self.portId,CALL_SEND),self._messagehook)
