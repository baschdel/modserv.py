import modserv.modclient as modclient
import portlib.cli as cliport
from modserv.exceptions import CrashAndBurnException
import modserv.easydriver as easydriver

#define some constants
CLI_INTERFACE = cliport.DEFAULT_INTERFACE #3
PROMT = ""

#reqest modclient instance
client = modclient.getModclient()

#register everything the cli adapter has to offer
cliport.register(client)

#store the cli proxy somewhere
proxy = None

#define a function that listens on the interface opening
def onCliConnectionOpened(connectionId,**context):
	global proxy
	if proxy is None:
		proxy = cliport.CliProxy(client,CLI_INTERFACE,connectionId,cliport.DEFAULT_PORT)
		print("--CONNECTED--")
	
def onCliConnectionClosed(connectionId,**context):
	global proxy
	proxy = None
	out("--DISCONNECTED--")
		
def onCliConnectionMessage(message,connectionId=None,**context):	
	out(message)

def out(message):
	print(message)

def mainloop():
	global proxy
	i = input(PROMT)
	#print(onInput)
	if not (proxy is None):
		proxy.send(i)
	else:
		out("--DISCONNECTED--")
	return True

def start():
	client.on("interface."+str(CLI_INTERFACE)+".newConnection",onCliConnectionOpened)
	client.on("interface."+str(CLI_INTERFACE)+".connectionClosed",onCliConnectionClosed)
	client.on(client.getCallEventName(CLI_INTERFACE,cliport.DEFAULT_PORT,cliport.CALL_SEND),onCliConnectionMessage)
	#open the interface
	client.openInterface(CLI_INTERFACE,"primitive-cli",1)
	easydriver.EasyDriverLooper(mainloop,client,daemon=True)
	
client.on("start",start)
