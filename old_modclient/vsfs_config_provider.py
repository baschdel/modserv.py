#WARNING: This module was an experiment and does not have a complete implementation of the config provider protocol

import modserv.modclient as modclient
import portlib.config_provider as config_provider
import portlib.vsfs_client as vsfs_client
import uuid
from threading import Lock

#alias the context class
Context = modclient.Context

indexWriteLock = Lock() #acquire this lock to write to any index (acquire before reading)

vsfsInterfaceId = vsfs_client.DEFAULT_INTERFACE
configInterfaceId = config_provider.DEFAULT_INTERFACE

client = modclient.getModclient(debug=True)

vsfs_client.register(client)
config_provider.register(client)

providerConnecions = []
configProviderUuid = None
configProviderName = ""
configProviderDisabledByDefault = False
vsfsConnected = False

def parseIndex(index):
	defaultSupersection = ""
	flags=[]
	supersections={}
	currentSupersection = None
	linecounter = 0
	lines = index.split("\n")
	for line in lines:
		linecounter = linecounter+1
		#section start
		if line == "":
			pass
		elif (line[0] == "[") and (line[-1] == "]"):
			currentSupersection = line[1:-1]
			supersections[currentSupersection] = {}
		elif currentSupersection is None:
			if linecounter == 1:
				defaultSupersection = line
			if linecounter == 2:
				flags = line.split(" ")
		else :
			tokens = line.split("\t")
			if len(tokens) >= 3 and tokens[0]=="int":
				applicationWritable = False
				if len(tokens) >= 4 and tokens[3]=="[aw]":
					applicationWritable = True
				supersections[currentSupersection][tokens[1]] = [tokens[2],applicationWritable]
			if len(tokens) >= 4 and tokens[0]=="ext":
				supersections[currentSupersection][tokens[1]] = (tokens[2],tokens[3])
	return (defaultSupersection,flags,supersections)

def makeIndex(defaultSupersection,flags,supersections):
	out = defaultSupersection+"\n"
	firstflag = True
	for flag in flags:
		if firstflag:
			firstflag = False
			out=out+flag
		else:
			out = out+" "+flag
	out = out+"\n"
	for supersection,sections in supersections.items():
		out = out+"["+supersection+"]\n"
		for section,content in sections.items():
			if type(content) is tuple:
				out=out+"int\t"+section+"\t"+content[0]+"\t"+content[1]+"\n"
			if type(content) is list:
				out=out+"int\t"+section+"\t"+content[0]
				if content[1]:
					out=out+"\t[aw]"
				out=out+"\n"
	return out
	
def isNameOk(name):
	return not (("\n" in name) or ("\0" in name) or ("\t" in name))

def onVsfsConnect(**context):
	vsfs.read("config-uuid",onSuccess=uuidRead,onDoesNotExist=uuidNotPresent)
	vsfs.read("config-name",onSuccess=nameRead)
	configProviderDisabledByDefault = False
	vsfs.read("config-disabled",onSuccess=disabledByDefautPresent)
	vsfsConnected = True
	onBackendChanged()
	
def uuidRead(data,**context):
	global configProviderUuid
	configProviderUuid = data.decode('utf-8')
	
def uuidNotPresent(**context):
	global configProviderUuid
	configProviderUuid = str(uuid.uuid4())
	vsfs.write("config-uuid",configProviderUuid.encode('utf-8'))
	
def nameRead(data,**context):
	global configProviderName
	configProviderName = data.decode('utf-8')
	
def disabledByDefautPresent(data,**context):
	global configProviderDisabledByDefault
	configProviderDisabledByDefault = True
	
def onVsfsDisconnect(**context):
	global configProviderUuid
	configProviderUuid = None
	global configProviderName
	configProviderName = ""
	vsfsConnected = False
	onBackendChanged()

def onBackendChanged():
	provider.backendChanged()
		
class GetSectionInfoHandler:
	def __init__(self,modclient,context,cookie):
		self.modclient = modclient
		self.context = context
		self.cookie = cookie
		self.counter = 4
		self.rawType = None
		self.size = None
		self.enabled = None
		self.applicationWritable = None
		self.readonly = False
		
	def success():
		return not (rawType is None or size is None or enabled is None or applicationWritable is None)
		
	def rawTypeRead(self,data,**context):
		self.rawType = data.decode('utf-8')	
		self.onCalled()
		
	def rawTypeFailed(self,**context):
		self.onCalled()
		
	def sizeReturned(self,size,**context):
		self.size = size
		self.onCalled()
		
	def sizeFailed(self,size,**context):
		self.onCalled()
		
	def disabledRead(self,data,**context):
		self.enabled = False
		self.onCalled()
		
	def disabledNotPresent(self,**context):
		self.enabled = True
		self.onCalled()
		
	def disabledFailed(self,**context):
		self.onCalled()
	
	def applicationWritableRead(self,data,**context):
		self.applicationWritable = True
		self.onCalled()
		
	def applicationWritableNotPresent(self,**context):
		self.applicationWritable = False
		self.onCalled()
		
	def applicationWritableFailed(self,**context):
		self.onCalled()

	def onCalled(self):
		self.counter = self.counter-1
		if self.counter > 0:
			return
		if self.success():
			self.modclient.callback(0,self.cookie,
				self.rawType,
				self.size,
				self.enabled,
				self.readonly,
				self.appWriteable,**context)
		else:
			self.modclient.callback(1,self.cookie,**self.context)

class MakeSectionHandler:
	def __init__(self,modclient,context,cookie):
		self.modclient = modclient
		self.context = context
		self.cookie = cookie
		self.counter = 4
		self.disableWritten = None
		self.appWriteableWritten = None
		self.typeWritten = None
		self.dataWritten = None
		self.hasInvalidName = False
		
	def success(self):
		return not (self.disableWritten is None or self.appWriteableWritten is None or self.disableWritten is None or self.dataWritten is None)
		
	def notDisabled(self,**context):
		self.disableWritten = False
		self.onCalled()
		
	def notAppWritable(self,**context):
		self.appWriteableWritten = False
		self.onCalled()
		
	def disableFileWritten(self,**context):
		self.disableWritten = True
		self.onCalled()
		
	def disableFileFailed(self,**context):
		self.onCalled()
	
	def appWriteableFileWritten(self,**context):
		self.appWriteableWritten = True
		self.onCalled()
		
	def appWriteableFileFailed(self,**context):
		self.onCalled()
		
	def typeFileWritten(self,**context):
		self.typeWritten = True
		self.onCalled()
		
	def typeFileFailed(self,**context):
		self.onCalled()
		
	def dataFileWritten(self,**context):
		self.dataWritten = True
		self.onCalled()
		
	def dataFileFailed(self,**context):
		self.onCalled()
	
	def invalidName(self,**context):
		self.hasInvalidName = True
		self.onCalled()
		
	def onCalled(self):
		self.counter=self.counter-1
		if self.counter > 0:
			return
		if self.hasInvalidName:
			self.modclient.callback(3,self.cookie,**self.context) #Invalid name!
		elif self.success():
			self.modclient.callback(0,self.cookie,**self.context) #success!
		else:
			self.modclient.callback(2,self.cookie,**self.context) #Read only!
	
class DeleteSectionHandler:
	def __init__(self,modclient,context,cookie):
		self.modclient = modclient
		self.context = context
		self.cookie = cookie
		self.counter = 4
		self.deleted = False
		self.notDeleted = False
		self.readOnly = False
	
	def returnedDontCare(self,**context):
		self.onCalled()
		
	def deleted(self,**context):
		self.deleted = True
		self.onCalled()
		
	def notDeleted(self,**context):
		self.notDeleted = True
		self.onCalled()
		
	def readOnly(self,**context):
		self.readOnly = True
		self.onCalled()
		
	def onCalled(self):
		self.counter=self.counter-1
		if self.counter > 0:
			return
		if self.notDeleted:
			self.modclient.callback(2,self.cookie,**self.context) #noSuchSection
		elif self.deleted:
			self.modclient.callback(0,self.cookie,**self.context) #deleted!
		else:
			self.modclient.callback(1,self.cookie,**self.context) #Read Only!

class SetSectionEnabledHandler:
	def __init__(self,modclient,context,cookie):
		self.modclient = modclient
		self.context = context
		self.cookie = cookie
		self.counter = 2
		self.readOnly = False

	def returnedDontCare(self,**context):
		self.onCalled()
		
	def readOnly(self,**context):
		self.readOnly = True
		self.onCalled()
		
	def onCalled(self,**context):
		self.counter=self.counter-1
		if self.counter > 0:
			return
		#All information collected
		if self.readOnly:
			self.modclient.callback(2,self.cookie,**self.context) #flag is read only
		else:
			self.modclient.callback(0,self.cookie,**self.context) #success
			
class TestIfSectionExistsHandler:
	def __init__(self,name,onExists=None,onNotExists=None,**context):
		self.context = context
		self.name = name
		self.onExists = onExists
		self.onNotExists = onNotExists
		
	def onFailed(self,**context):
		if callable(self.onNotExists):
			self.onNotExists(**self.context)
	
	def onListReturn(self,names,**context):
		if ("section."+self.name+".data" in names) and ("section."+self.name+".type" in names):
			if callable(self.onExists):
				self.onExists(**self.context)
		else:
			if callable(self.onNotExists):
				self.onNotExists(**self.context)
			
class ConfigProvider:	
	def getProviderInfo(self,cookie,proxy=None,**context):
		if configProviderUuid is None:
			proxy.modclient.callback(1,cookie,**context) #not connected
		else:
			proxy.modclient.callback(0,cookie,configProviderUuid,configProviderName,False,False,0,0,**context)
			
	def listSections(self,cookie,proxy=None,**context):
		if configProviderUuid is None:
			proxy.modclient.callback(1,cookie,**context) #not connected
		else:
			self.vsfs.list("section.",
				onSuccess=Context(self.listSections_vsfsListReturn,cookie=cookie,proxy=proxy,**context),
				onNotConnected=Context(self.listSections_vsfsDisconnected,cookie=cookie,proxy=proxy,**context))
			
	def listSections_vsfsListReturn(self,names,cookie=None,proxy=None,**context):
		sections = []
		for name in names:
			sections.append(name[8:])
		proxy.modclient.callback(0,sections,**context)
		
	def listSections_vsfsDisconnected(self,cookie=None,proxy=None,**context):
		proxy.modclient.callback(1,cookie,**context)
			
	def getSectionInfo(self,cookie,sectionName,proxy=None,**context):
		infoHandler = GetSectionInfoHandler(proxy.modclient,context,cookie)
		vsfs.read("section."+sectionName+".type",
			onSuccess=infoHandler.rawTypeRead,
			onDoesNotExist=infoHandler.rawTypeFailed,
			onNotConnected=infoHandler.rawTypeFailed)
		vsfs.read("section."+sectionName+".disabled",
			onSuccess=infoHandler.disabledRead,
			onDoesNotExist=infoHandler.disabledNotPresent,
			onNotConnected=infoHandler.disabledFailed)
		vsfs.read("section."+sectionName+".appWriteable",
			onSuccess=infoHandler.applicationWritableRead,
			onDoesNotExist=infoHandler.applicationWritableNotPresent,
			onNotConnected=infoHandler.applicationWritableFailed)
		vsfs.getSize("section."+sectionName+".data",
			onSuccess=infoHandler.sizeReturned,
			onDoesNotExist=infoHandler.sizeFailed,
			onNotConnected=infoHandler.sizeFailed)
		
	def readSectionData(self,cookie,sectionName,proxy=None,**context):
		vsfs.read("section."+sectionName+".data",
			onSuccess=self.readSectionData_success,
			onDoesNotExist=self.readSectionData_failure,
			onNotConnected=self.readSectionData_failure,
			cookie=cookie,proxy=proxy,**context)
		
	def readSectionData_success(self,data,cookie=None,proxy=None,**context):
		proxy.modclient.callback(0,cookie,data,**context)
		
	def readSectionData_failure(self,cookie=None,proxy=None,**context):
		proxy.modclient.callback(1,cookie,**context)
		
	def writeSectionData(self,cookie,sectionName,data,proxy=None,**context):
		name = "section."+sectionName+".data"
		vsfs.list(name,
			onSuccess=Context(self.writeSectionData_listReturn,cookie=cookie,sectionPath=name,data=data,proxy=proxy,**context),
			onNotConnected=Context(self.writeSectionData_failureNotPresent,cookie=cookie,proxy=proxy,**context))
		
	def writeSectionData_listReturn(self,names,cookie=None,sectionPath=None,data=None,proxy=None,**context):
		if not sectionName in names:
			self.writeSectionData_failureNotPresent(cookie)
		else:
			self.vsfs.write(sectionPath,data,
				onSuccess=self.writeSectionData_success,
				onReadOnly=self.writeSectionData_failureReadOnly,
				onInvalidName=self.writeSectionData_failureNotPresent,
				onNotEnoughSpace=self.writeSectionData_failureReadOnly,
				onNotConnected=self.writeSectionData_failureNotPresent,
				cookie=cookie,**context)
		
	def writeSectionData_success(self,cookie,proxy=None,**context):
		proxy.modclient.callback(0,cookie,**context)
		
	def writeSectionData_failureNotPresent(self,cookie=None,proxy=None,**context):
		proxy.modclient.callback(1,cookie,**context)
	
	def writeSectionData_failureReadOnly(self,cookie,proxy=None,**context):
		proxy.modclient.callback(2,cookie,**context)
		
	def makeSection(self,cookie,sectionName,rawType,enabled,applicationWritable,proxy=None,**context):
		if not isNameOk(sectionName):
			proxy.modclient.callback(3,cookie,**context) #invalid Name
			return
		if not isNameOk(rawType):
			proxy.modclient.callback(4,cookie,**context) #invalid Type
			return
		vsfs.list("section."+sectionName,
			onSuccess=self.makeSection_listReturn,
			onNotConnected=self.makeSection_providerIsReadOnly,
			cookie=cookie,proxy=proxy,sectionName=sectionName,rawType=rawType,enabled=enabled,appWriteable=applicationWritable,**context)
	
	def makeSection_listReturn(self,names,cookie=None,sectionName=None,rawType=None,enabled=None,appWriteable=None,proxy=None,**context):
		if "section."+sectionName+".data" in names and "section."+sectionName+".type" in names:
			proxy.modclient.callback(1,cookie,**context) #already exists
		else:
			handler = MakeSectionHandler(proxy.modclient,context,cookie)
			vsfs.write("section."+sectionName+".data",b'',
				onSuccess=handler.dataFileWritten,
				onInvalidName=handler.invalidName,
				onReadOnly=handler.dataFileFailed,
				onNotEnoughSpace=handler.dataFileFailed,
				onNotConnected=handler.dataFileFailed)
			vsfs.write("section."+sectionName+".type",rawType.encode('utf-8'),
				onSuccess=handler.dataFileWritten,
				onInvalidName=handler.invalidName,
				onReadOnly=handler.dataFileFailed,
				onNotEnoughSpace=handler.dataFileFailed,
				onNotConnected=handler.dataFileFailed)
			if not enabled:
				vsfs.write("section."+sectionName+".disabled",b'',
					onSuccess=handler.dataFileWritten,
					onInvalidName=handler.invalidName,
					onReadOnly=handler.dataFileFailed,
					onNotEnoughSpace=handler.dataFileFailed,
					onNotConnected=handler.dataFileFailed)
			else:
				handler.notDisabled()
			if appWriteable:
				vsfs.write("section."+sectionName+".appWriteable",b'',
					onSuccess=handler.dataFileWritten,
					onInvalidName=handler.invalidName,
					onReadOnly=handler.dataFileFailed,
					onNotEnoughSpace=handler.dataFileFailed,
					onNotConnected=handler.dataFileFailed)
			else:
				handler.notAppWritable()
				
	def makeSection_providerIsReadOnly(self,cookie=None,proxy=None,**context):
		print("context:",context)
		proxy.modclient.callback(2,cookie,**context)
		
	def deleteSection(self,cookie,name,proxy=None,**context):
		handler = DeleteSectionHandler(proxy.modclient,context,cookie)
		vsfs.delete("section."+name+".data",
			onSuccess=handler.deleted,
			onDoesNotExist=handler.notDeleted,
			onReadOnly=handler.readOnly,
			onNotConnected=handler.notDeleted)
		vsfs.delete("section."+name+".type",
			onSuccess=handler.deleted,
			onDoesNotExist=handler.notDeleted,
			onReadOnly=handler.readOnly,
			onNotConnected=handler.notDeleted)
		vsfs.delete("section."+name+".disabled",
			onSuccess=handler.deleted,
			onDoesNotExist=handler.returnedDontCare,
			onReadOnly=handler.readOnly,
			onNotConnected=handler.notDeleted)
		vsfs.delete("section."+name+".appWriteable",
			onSuccess=handler.deleted,
			onDoesNotExist=handler.returnedDontCare,
			onReadOnly=handler.readOnly,
			onNotConnected=handler.notDeleted)
		
	def setSectionEnabled(self,cookie,name,enabled,proxy=None,**context):
		handler = TestIfSectionExistsHandler(name,
			onExists=self.setSectionEnabled_exists,
			onNotExists=self.setSectionEnabled_notExists,
			proxy=proxy,enabled=enabled,name=name,cookie=cookie,**context)
		vsfs.list("section.",
			onSuccess=handler.onListReturn,
			onNotConnected=handler.onFailed)

	def setSectionEnabled_exists(self,proxy=None,enabled=None,name=None,cookie=None,**context):
		handler = SetSectionEnabledHandler(proxy.modclient,context,cookie)
		if enabled:
			vsfs.delete("section."+name+".disabled",
				onSuccess=handler.returnedDontCare,
				onDoesNotExist=handler.returnedDontCare,
				onReadOnly=handler.readOnly,
				onNotConnected=handler.readOnly)
		else:
			vsfs.write("section."+name+".disabled",b'',
				onSuccess=handler.returnedDontCare,
				onInvalidName=handler.readOnly,
				onReadOnly=handler.readOnly,
				onNotEnoughSpace=handler.readOnly,
				onNotConnected=handler.readOnly)
		
	def setSectionEnabled_notExists(self,proxy=None,enabled=None,name=None,cookie=None,**context):
		proxy.modclient.callback(1,cookie,**context) #does not exist
		
	def listIndices(self,cookie,proxy=None,**context):
		if configProviderUuid is None:
			raise Return(1,cookie) #not connected
		else:
			self.vsfs.list("index.",
				onSuccess=Context(self.listIndices_vsfsListReturn,cookie=cookie,proxy=proxy,**context),
				onNotConnected=Context(self.listIndices_vsfsDisconnected,cookie=cookie,proxy=proxy,**context))
			
	def listIndices_vsfsListReturn(self,names,cookie=None,proxy=None,**context):
		sections = []
		for name in names:
			sections.append(name[6:]) #remove the "index." thingy
		proxy.modclient.callback(0,sections,**context)
		
	def listIndices_vsfsDisconnected(self,cookie=None,proxy=None,**context):
		proxy.modclient.callback(1,cookie,**context) #backend not connected
		
	def getIndexInfo(self,cookie,indexName,proxy=None,**context):
		if not isNameOk(indexName):
			proxy.modclient.callback(1,cookie,**context) #No such index
			return
		vsfs.read("index."+indexName,
			onSuccess=self.getIndexInfo_read,
			onDoesNotExist=self.getIndexInfo_notRead,
			onNotConnected=self.getIndexInfo_notRead,
			indexName=indexName,cookie=cookie,proxy=proxy,**context)
	
	def getIndexInfo_read(self,data,indexName=None,cookie=None,proxy=None,**context):
		vsfs.append("index."+indexName,b'',
			onSuccess=Context(self.getIndexInfo_writable,writable=True),
			onReadOnly=self.getIndexInfo_writable,
			onInvalidName=self.getIndexInfo_notRead,
			onNotConnected=self.getIndexInfo_notRead,
			data=data,cookie=cookie,proxy=proxy,**context)
			
	def getIndexInfo_writable(self,writable=False,data=None,cookie=None,proxy=None,**context):
		data = data.decode('utf-8')
		lines = data.split("\n")
		defaultSupersection = ""
		enabled = True
		readOnly = not writable
		if len(data) > 0:
			defaultSupersection = data[0]
		if len(data) > 1:
			flags = data[1].split(" ")
			for flag in flags:
				if flag == "disabled":
					enabled = False
				if flag == "readonly":
					readOnly = True
		proxy.modclient.callback(0,cookie,defaultSupersection,enabled,readOnly)
		
	def getIndexInfo_notRead(self,cookie=None,proxy=None,**context):
		proxy.modclient.callback(1,cookie,**context) #no such index
		
	def resolveSectionName(self,cookie,indexName,superSectionName,sectionName,proxy=None,**context):
		if not isNameOk(indexName):
			proxy.modclient.callback(2,cookie,**context) #No such index
			return
		if not isNameOk(superSectionName):
			proxy.modclient.callback(3,cookie,**context) #No such supersection
			return
		if not isNameOk(sectionName):
			proxy.modclient.callback(4,cookie,**context) #No such section
			return
		vsfs.read("index."+indexName,
			onSuccess=Context(self.resolveSectionName_read,superSectionName=superSectionName,sectionName=sectionName),
			onDoesNotExist=self.resolveSectionName_noSuchIndex,
			onNotConnected=self.resolveSectionName_noSuchIndex,
			proxy=proxy,cookie=cookie,**context)
			
	def resolveSectionName_read(self,data,superSectionName=None,sectionName=None,cookie=None,proxy=None,**context):
		defaultSupersection,flags,supersections = parseIndex(data.decode("utf-8"))
		if superSectionName in supersections:
			supersection = supersections[superSectionName]
			if sectionName in supersection:
				section = supersection[sectionName]
				if type(section) is list:
					#internal
					proxy.modclient.callback(0,cookie,section[0],section[1],**context)
				elif type(section) is tuple:
					#external
					proxy.modclient.callback(1,cookie,section[0],section[1],**context)
				else:
					#should nerver happen with this parser but ...
					proxy.modclient.callback(4,cookie,**context) #No such section
			else:
				proxy.modclient.callback(4,cookie,**context) #No such section
		else:
			proxy.modclient.callback(3,cookie,**context) #No such supersection
			
	def resolveSectionName_noSuchIndex(cookie=None,proxy=None,**context):
		proxy.modclient.callback(2,cookie,**context)
		
	def listIndexSupersections(self,cookie,indexName,proxy=None,**context):
		if not isNameOk(indexName):
			proxy.modclient.callback(1,cookie,**context) #No such index
			return
		vsfs.read("index."+indexName,
			onSuccess=self.listIndexSupersections_read,
			onDoesNotExist=self.listIndexSupersections_noSuchIndex,
			onNotConnected=self.listIndexSupersections_noSuchIndex,
			proxy=proxy,cookie=cookie,**context)
	
	def listIndexSupersections_read(self,data,cookie=None,proxy=None,**context):
		defaultSupersection,flags,supersections = parseIndex(data.decode("utf-8"))
		ret = []
		for supersection,sections in supersections.items():
			ret.append(supersection)
		proxy.modclient.callback(0,cookie,ret,**context)
	
	def listIndexSupersections_noSuchIndex(self,cookie=None,proxy=None,**context):
		proxy.modclient.callback(1,cookie,**context)
		
	def listIndexSections(self,cookie,indexName,superSectionName,proxy=None,**context):
		if not isNameOk(indexName):
			proxy.modclient.callback(1,cookie,**context) #No such index
			return
		if not isNameOk(superSectionName):
			proxy.modclient.callback(3,cookie,**context) #No such supersection
			return
		vsfs.read("index."+indexName,
			onSuccess=Context(self.listIndexSupersections_read,superSectionName=superSectionName),
			onDoesNotExist=self.listIndexSupersections_noSuchIndex,
			onNotConnected=self.listIndexSupersections_noSuchIndex,
			proxy=proxy,cookie=cookie,**context)
	
	def listIndexSections_read(self,data,superSectionName=None,cookie=None,proxy=None,**context):
		defaultSupersection,flags,supersections = parseIndex(data.decode("utf-8"))
		if superSectionName in supersections:
			sections = supersections[superSectionName]
			ret = []
			for section,entrys in sections.items():
				ret.append(section)
			proxy.modclient.callback(0,cookie,ret,**context)
		else:
			proxy.modclient.callback(2,cookie,**context) #no such supersection
	
	def listIndexSections_noSuchIndex(self,cookie=None,proxy=None,**context):
		proxy.modclient.callback(1,cookie,**context)
	
	def makeIndex(self,cookie,indexName,proxy=None,**context):
		if not isNameOk(indexName):
			proxy.modclient.callback(3,cookie,**context) #Invalid index name
			return
		vsfs.read("index."+indexName,
			onSuccess=self.makeIndex_exists,
			onDoesNotExist=self.makeIndex_create,
			onNotConnected=self.makeIndex_readOnly,
			proxy=proxy,cookie=cookie,indexName=indexName,**context)
	
	def makeIndex_exists(self,data,cookie=None,proxy=None,**context):
		proxy.modclient.callback(1,cookie,**context)
		
	def makeIndex_readOnly(self,cookie=None,proxy=None,**context):
		#also when disconnected
		proxy.modclient.callback(2,cookie,**context)
	
	def makeIndex_create(self,indexName=None,cookie=None,proxy=None,**context):
		vsfs.append("index."+indexName,b'', #append beacause it is safer if two operations on the same file collide
			onSuccess=self.makeIndex_created,
			onInvalidName=self.makeIndex_invalidName,
			onReadOnly=self.makeIndex_readOnly,
			onNotEnoughSpace=self.makeIndex_readOnly,
			onNotConnected=self.makeIndex_readOnly,
			proxy=proxy,cookie=cookie,**context)
		
	def makeIndex_created(self,cookie=None,proxy=None,**context):
		proxy.modclient.callback(0,cookie,**context)
	
	def makeIndex_invalidName(self,cookie=None,proxy=None,**context):
		proxy.modclient.callback(3,cookie,**context)
	
	def makeIndexSupersection(self,cookie,indexName,superSectionName,proxy=None,**context):
		if not isNameOk(indexName):
			proxy.modclient.callback(3,cookie,**context) #No such index
			return
		if not isNameOk(superSectionName):
			proxy.modclient.callback(4,cookie,**context) #Invalid supersection name
			return
		if indexWriteLock.acquire(timeout=3):
			vsfs.read("index."+indexName,
				onSuccess=self.makeIndexSupersection_read,
				onDoesNotExist=self.makeIndexSupersection_notRead,
				onNotConnected=self.makeIndexSupersection_notRead,
				proxy=proxy,cookie=cookie,superSectionName=superSectionName,indexName=indexName,**context)
		else:
			proxy.modclient.callback(2,cookie,**context) #Read only
	
	def makeIndexSupersection_read(self,data,indexName=None,superSectionName=None,cookie=None,proxy=None,**context):
		defaultSupersection,flags,supersections = parseIndex(data.decode("utf-8"))
		if superSectionName in supersections:
			indexWriteLock.release()
			proxy.modclient.callback(1,cookie,**context) #Already exists
		else:
			supersections[superSectionName] = {}
			newdata = makeIndex(defaultSupersection,flags,supersections).encode('utf-8')
			vsfs.write("index."+indexName,
				onSuccess=self.makeIndexSupersection_written,
				#beacause the read worked the vsfs is a little bit wired if this happens, also I'm lazy ...
				onInvalidName=self.makeIndexSupersection_readOnly,
				onNotEnoughSpace=self.makeIndexSupersection_readOnly,
				onReadOnly=self.makeIndexSupersection_readOnly,
				onNotConnected=self.makeIndexSupersection_readOnly,
				proxy=proxy,cookie=cookie,data=newdata,**context)
		
	def makeIndexSupersection_notRead(self,cookie=None,proxy=None,**context):
		indexWriteLock.release()
		proxy.modclient.callback(3,cookie,**context) #No such index
	
	def makeIndexSupersection_readOnly(self,cookie=None,proxy=None,**context):
		indexWriteLock.release()
		proxy.modclient.callback(2,cookie,**context) #readOnly
	
	def makeIndexSupersection_written(self,cookie=None,proxy=None,**context):
		indexWriteLock.release()
		proxy.modclient.callback(0,cookie,**context)
		
	def removeIndex(self,cookie,indexName,proxy=None,**context):
		if not isNameOk(indexName):
			proxy.modclient.callback(1,cookie,**context) #No such index
			return
		vsfs.delete("index."+indexName,
			onSuccess=self.removeIndex_deleted,
			onReadOnly=self.removeIndex_readOnly,
			onDoesNotExist=self.removeIndex_doesNotExist,
			onNotConnected=self.removeIndex_doesNotExist,
			cookie=cookie,proxy=proxy,**context)
	
	def removeIndex_deleted(self,cookie=None,proxy=None,**context):
		proxy.modclient.callback(0,cookie,**context)
		
	def removeIndex_readOnly(self,cookie=None,proxy=None,**context):
		proxy.modclient.callback(2,cookie,**context)
		
	def removeIndex_doesNotExist(self,cookie=None,proxy=None,**context):
		proxy.modclient.callback(1,cookie,**context)
		
	def removeIndexSupersection(self,cookie,indexName,superSectionName,proxy=None,**context):
		if not isNameOk(indexName):
			proxy.modclient.callback(3,cookie,**context) #No such index
			return
		if not isNameOk(superSectionName):
			proxy.modclient.callback(1,cookie,**context) #Does not exist
			return
		if indexWriteLock.acquire(timeout=3):
			vsfs.read("index."+indexName,
				onSuccess=self.removeIndexSupersection_read,
				onDoesNotExist=self.removeIndexSupersection_notRead,
				onNotConnected=self.removeIndexSupersection_notRead,
				proxy=proxy,cookie=cookie,superSectionName=superSectionName,**context)
		else:
			proxy.modclient.callback(2,cookie,**context) #Read only
	
	def removeIndexSupersection_read(self,data,superSectionName=None,cookie=None,proxy=None,**context):
		defaultSupersection,flags,supersections = parseIndex(data.decode("utf-8"))
		if superSectionName in supersections:
			del supersections[superSectionName]
			newdata = makeIndex(defaultSupersection,flags,supersections).encode('utf-8')
			vsfs.write("index."+indexName,
				onSuccess=self.removeIndexSupersection_written,
				#beacause the read worked the vsfs is a little bit wired if this happens, also I'm lazy ...
				onInvalidName=self.removeIndexSupersection_readOnly,
				onNotEnoughSpace=self.removeIndexSupersection_readOnly,
				onReadOnly=self.removeIndexSupersection_readOnly,
				onNotConnected=self.removeIndexSupersection_readOnly,
				proxy=proxy,cookie=cookie,data=newdata,**context)
		else:
			indexWriteLock.release()
			proxy.modclient.callback(1,cookie,**context) #Does not exist
			
	def removeIndexSupersection_notRead(self,cookie=None,proxy=None,**context):
		indexWriteLock.release()
		proxy.modclient.callback(3,cookie,**context) #No such index
	
	def removeIndexSupersection_readOnly(self,cookie=None,proxy=None,**context):
		indexWriteLock.release()
		proxy.modclient.callback(2,cookie,**context) #readOnly
	
	def removeIndexSupersection_written(self,cookie=None,proxy=None,**context):
		indexWriteLock.release()
		proxy.modclient.callback(0,cookie,**context)
		
	def mapIndexSection(self,cookie,indexName,superSectionName,sectionName,destinationSectionName,applicationWritable,proxy=None,**context):
		if not isNameOk(indexName):
			proxy.modclient.callback(2,cookie,**context) #No such index
			return
		if not isNameOk(superSectionName):
			proxy.modclient.callback(3,cookie,**context) #No such supersection
			return
		if not isNameOk(sectionName):
			proxy.modclient.callback(4,cookie,**context) #Invalid section name
			return
		if not isNameOk(destinationSectionName):
			proxy.modclient.callback(5,cookie,**context) #Invalid destination Name
			return
		if indexWriteLock.acquire(timeout=3):
			vsfs.read("index."+indexName,
				onSuccess=self.mapIndexSection_read,
				onDoesNotExist=self.mapIndexSection_notRead,
				onNotConnected=self.mapIndexSection_notRead,
				proxy=proxy,cookie=cookie,indexName=indexName,superSectionName=superSectionName,sectionName=sectionName,
				destinationSectionName=destinationSectionName,applicationWritable=applicationWritable,**context)
		else:
			proxy.modclient.callback(1,cookie,**context) #Read only
	
	def mapIndexSection_read(self,data,indexName=None,superSectionName=None,sectionName=None,destinationSectionName=None,applicationWritable=None,cookie=None,proxy=None,**context):
		defaultSupersection,flags,supersections = parseIndex(data.decode("utf-8"))
		if superSectionName in supersections:
			sections = supersections[superSectionName]
			sections[sectionName] = [destinationSectionName,applicationWritable]
		else:
			indexWriteLock.release()
			proxy.modclient.callback(3,cookie,**context) #No such supersection
			return
		newdata = makeIndex(defaultSupersection,flags,supersections).encode('utf-8')
		vsfs.write("index."+indexName,
			onSuccess=self.mapIndexSection_written,
			#beacause the read worked the vsfs is a little bit wired if this happens, also I'm lazy ...
			onInvalidName=self.mapIndexSection_readOnly,
			onNotEnoughSpace=self.mapIndexSection_readOnly,
			onReadOnly=self.mapIndexSection_readOnly,
			onNotConnected=self.mapIndexSection_readOnly,
			proxy=proxy,cookie=cookie,data=newdata,**context)
		
	def mapIndexSection_notRead(self,cookie=None,proxy=None,**context):
		indexWriteLock.release()
		proxy.modclient.callback(2,cookie,**context) #No such index
	
	def mapIndexSection_readOnly(self,cookie=None,proxy=None,**context):
		indexWriteLock.release()
		proxy.modclient.callback(1,cookie,**context) #readOnly
	
	def mapIndexSection_written(self,cookie=None,proxy=None,**context):
		indexWriteLock.release()
		proxy.modclient.callback(0,cookie,**context)
		
	def mapIndexExternalSection(self,cookie,indexName,superSectionName,sectionName,provider,resource,proxy=None,**context):
		if not isNameOk(indexName):
			proxy.modclient.callback(2,cookie,**context) #No such index
			return
		if not isNameOk(superSectionName):
			proxy.modclient.callback(3,cookie,**context) #No such supersection
			return
		if not isNameOk(sectionName):
			proxy.modclient.callback(4,cookie,**context) #Invalid section name
			return
		if not isNameOk(provider):
			proxy.modclient.callback(5,cookie,**context) #Invalid provider Name
			return
		if not isNameOk(resource):
			proxy.modclient.callback(6,cookie,**context) #Invalid resource Name
			return
		if indexWriteLock.acquire(timeout=3):
			vsfs.read("index."+indexName,
				onSuccess=self.mapIndexExternalSection_read,
				onDoesNotExist=self.mapIndexExternalSection_notRead,
				onNotConnected=self.mapIndexExternalSection_notRead,
				proxy=proxy,cookie=cookie,indexName=indexName,superSectionName=superSectionName,sectionName=sectionName,provider=provider,resource=resource,**context)
		else:
			proxy.modclient.callback(1,cookie,**context) #Read only
	
	def mapIndexExternalSection_read(self,data,indexName=None,superSectionName=None,sectionName=None,provider=None,resource=None,cookie=None,proxy=None,**context):
		defaultSupersection,flags,supersections = parseIndex(data.decode("utf-8"))
		if superSectionName in supersections:
			sections = supersections[superSectionName]
			sections[sectionName] = (provider,resource)
		else:
			indexWriteLock.release()
			proxy.modclient.callback(3,cookie,**context) #No such supersection
			return
		newdata = makeIndex(defaultSupersection,flags,supersections).encode('utf-8')
		vsfs.write("index."+indexName,
			onSuccess=self.mapIndexExternalSection_written,
			#beacause the read worked the vsfs is a "little bit" wired/broken if this happens, also I'm lazy ...
			onInvalidName=self.mapIndexExternalSection_readOnly,
			onNotEnoughSpace=self.mapIndexExternalSectionOnly,
			onReadOnly=self.mapIndexExternalSection_readOnly,
			onNotConnected=self.mapIndexExternalSection_readOnly,
			proxy=proxy,cookie=cookie,data=newdata,**context)
		
	def mapIndexExternalSection_notRead(self,cookie=None,proxy=None,**context):
		indexWriteLock.release()
		proxy.modclient.callback(2,cookie,**context) #No such index
	
	def mapIndexExternalSection_readOnly(self,cookie=None,proxy=None,**context):
		indexWriteLock.release()
		proxy.modclient.callback(1,cookie,**context) #readOnly
	
	def mapIndexExternalSection_written(self,cookie=None,proxy=None,**context):
		indexWriteLock.release()
		proxy.modclient.callback(0,cookie,**context)
		
	def unmapIndexSection(self,cookie,indexName,superSectionName,sectionName,proxy=None,**context):
		if not isNameOk(indexName):
			proxy.modclient.callback(2,cookie,**context) #No such index
			return
		if not isNameOk(superSectionName):
			proxy.modclient.callback(3,cookie,**context) #No such supersection
			return
		if not isNameOk(sectionName):
			proxy.modclient.callback(4,cookie,**context) #No such section name
			return
		if indexWriteLock.acquire(timeout=3):
			vsfs.read("index."+indexName,
				onSuccess=self.unmapIndexSection_read,
				onDoesNotExist=self.unmapIndexSection_notRead,
				onNotConnected=self.unmapIndexSection_notRead,
				proxy=proxy,cookie=cookie,indexName=indexName,superSectionName=superSectionName,sectionName=sectionName,**context)
		else:
			proxy.modclient.callback(1,cookie,**context) #Read only
	
	def unmapIndexSection_read(self,data,indexName=None,superSectionName=None,sectionName=None,cookie=None,proxy=None,**context):
		defaultSupersection,flags,supersections = parseIndex(data.decode("utf-8"))
		if superSectionName in supersections:
			sections = supersections[superSectionName]
			del sections[sectionName]
		else:
			indexWriteLock.release()
			proxy.modclient.callback(3,cookie,**context) #No such supersection
			return
		newdata = makeIndex(defaultSupersection,flags,supersections).encode('utf-8')
		vsfs.write("index."+indexName,
			onSuccess=self.unmapIndexSection_written,
			#beacause the read worked the vsfs is a "little bit" wired/broken if this happens, also I'm lazy ...
			onInvalidName=self.unmapIndexSection_readOnly,
			onNotEnoughSpace=self.unmapIndexSectionOnly,
			onReadOnly=self.unmapIndexSection_readOnly,
			onNotConnected=self.unmapIndexSection_readOnly,
			proxy=proxy,cookie=cookie,data=newdata,**context)
		
	def unmapIndexSection_notRead(self,cookie=None,proxy=None,**context):
		indexWriteLock.release()
		proxy.modclient.callback(2,cookie,**context) #No such index
	
	def unmapIndexSection_readOnly(self,cookie=None,proxy=None,**context):
		indexWriteLock.release()
		proxy.modclient.callback(1,cookie,**context) #readOnly
	
	def unmapIndexSection_written(self,cookie=None,proxy=None,**context):
		indexWriteLock.release()
		proxy.modclient.callback(0,cookie,**context)
		
	def setIndexEnabled(self,cookie,indexName,enabled,proxy=None,**context):
		if not isNameOk(indexName):
			proxy.modclient.callback(1,cookie,**context) #No such index
			return
		if indexWriteLock.acquire(timeout=3):
			vsfs.read("index."+indexName,
				onSuccess=self.setIndexEnabled_read,
				onDoesNotExist=self.setIndexEnabled_notRead,
				onNotConnected=self.setIndexEnabled_notRead,
				proxy=proxy,indexName=indexName,cookie=cookie,enabled=enabled,**context)
		else:
			proxy.modclient.callback(2,cookie,**context) #Read only
	
	def setIndexEnabled_read(self,data,indexName,enabled=None,cookie=None,proxy=None,**context):
		defaultSupersection,flags,supersections = parseIndex(data.decode("utf-8"))
		if enabled:
			flags.remove("disabled")
		else:
			if not ("disabled" in flags):
				flags.append("disabled")
		newdata = makeIndex(defaultSupersection,flags,supersections).encode('utf-8')
		vsfs.write("index."+indexName,
			onSuccess=self.setIndexEnabled_written,
			#beacause the read worked the vsfs is a little bit wired if this happens, also I'm lazy ...
			onInvalidName=self.setIndexEnabled_readOnly,
			onNotEnoughSpace=self.setIndexEnabled_readOnly,
			onReadOnly=self.setIndexEnabled_readOnly,
			onNotConnected=self.setIndexEnabled_readOnly,
			proxy=proxy,cookie=cookie,data=newdata,**context)
		
	def setIndexEnabled_notRead(self,cookie=None,proxy=None,**context):
		indexWriteLock.release()
		proxy.modclient.callback(1,cookie,**context) #No such index
	
	def setIndexEnabled_readOnly(self,cookie=None,proxy=None,**context):
		indexWriteLock.release()
		proxy.modclient.callback(2,cookie,**context) #readOnly
	
	def setIndexEnabled_written(self,cookie=None,proxy=None,**context):
		indexWriteLock.release()
		proxy.modclient.callback(0,cookie,**context)
		
	def renameProvider(self,cookie,newName,proxy=None,**context):
		print("context:",context)
		if isNameOk(newName):
			vsfs.write("config-name",newName.encode('utf-8'),
				onSuccess=self.renameProvider_written,
				onInvalidName=self.renameProvider_noRename,
				onReadOnly=self.renameProvider_noRename,
				onNotEnoughSpace=self.renameProvider_noRename,
				onNotConnected=self.renameProvider_noRename,
				newName=newName,cookie=cookie,proxy=proxy,**context)
		else:
			proxy.modclient.callback(2,cookie,**context) #invalid name

	def renameProvider_written(self,newName=None,cookie=None,proxy=None,**context):
		global configProviderName
		configProviderName = newName
		proxy.modclient.callback(0,cookie,**context)
		provider.nameChanged(newName)
	
	def renameProvider_noRename(self,cookie=None,proxy=None,**context):
		proxy.modclient.callback(1,cookie,**context)
	
	def setSectionApplicationWritable(self,cookie,sectionName,applicationWritable,proxy=None,**context):
		if not isNameOk(sectionName):
			proxy.modclient.callback(2,cookie,**context) #no such section
			return
		handler = TestIfSectionExistsHandler(name,
			onExists=self.setSectionApplicationWritable_exists,
			onNotExists=self.setSectionApplicationWritable_doesNotExist,
			proxy=proxy,sectionName=sectionName,applicationWritable=applicationWritable,cookie=cookie,**context)
		vsfs.list("section.",
			onSuccess=handler.onListReturn,
			onNotConnected=handler.onFailed)
		
	def setSectionApplicationWritable_exists(self,data,sectionName=None,applicationWritable=None,cookie=None,proxy=None,**context):
		if appWriteable:
			vsfs.delete("section."+sectionName+".appWriteable",
				onSuccess=self.setSectionApplicationWritable_success,
				#beacause we want the file gone, if it already is, thats ok
				onDoesNotExist=self.setSectionApplicationWritable_success, 
				onReadOnly=self.setSectionApplicationWritable_readOnly,
				onNotConnected=self.setSectionApplicationWritable_doesNotExist,
				proxy=proxy,sectionName=sectionName,applicationWritable=applicationWritable,**context)
		else:
			vsfs.write("section."+sectionName+".appWriteable",b'',
				onSuccess=self.setSectionApplicationWritable_success,
				onInvalidName=self.setSectionApplicationWritable_readOnly,
				onNotEnoughSpace=self.setSectionApplicationWritable_readOnly,
				onReadOnly=self.setSectionApplicationWritable_readOnly,
				onNotConnected=self.setSectionApplicationWritable_doesNotExist,
				proxy=proxy,sectionName=sectionName,applicationWritable=applicationWritable,**context)
				
	def setSectionApplicationWritable_doesNotExist(self,cookie=None,proxy=None,**context):
		proxy.modclient.callback(2,cookie,**context)
			
	def setSectionApplicationWritable_readOnly(self,cookie=None,proxy=None,**context):
		proxy.modclient.callback(1,cookie,**context)		
	
	def setSectionApplicationWritable_success(self,cookie=None,proxy=None,**context):
		proxy.modclient.callback(0,cookie,**context)
		
	def setDefaultSupersection(self,cookie,indexName,supersectionName,proxy=None,**context):
		if not isNameOk(indexName):
			proxy.modclient.callback(1,cookie,**context) #No such index
			return
		if not isNameOk(supersectionName):
			proxy.modclient.callback(3,cookie,**context) #Invalid supersection name
			return
		if indexWriteLock.acquire(timeout=3):
			vsfs.read("index."+indexName,
				onSuccess=self.setDefaultSupersection_read,
				onDoesNotExist=self.setDefaultSupersection_notRead,
				onNotConnected=self.setDefaultSupersection_notRead,
				proxy=proxy,cookie=cookie,supersectionName=supersectionName,indexName=indexName,**context)
		else:
			proxy.modclient.callback(2,cookie,**context) #Read only
	
	def setDefaultSupersection_read(self,data,indexName=None,supersectionName=None,cookie=None,proxy=None,**context):
		defaultSupersection,flags,supersections = parseIndex(data.decode("utf-8"))
		newdata = makeIndex(supersectionName,flags,supersections).encode('utf-8')
		vsfs.write("index."+indexName,
			onSuccess=self.setDefaultSupersection_written,
			#beacause the read worked the vsfs is a little bit wired if this happens, also I'm lazy ...
			onInvalidName=self.setDefaultSupersection_readOnly,
			onNotEnoughSpace=self.setDefaultSupersection_readOnly,
			onReadOnly=self.setDefaultSupersection_readOnly,
			onNotConnected=self.setDefaultSupersection_readOnly,
			proxy=proxy,cookie=cookie,data=newdata,**context)
		
	def setDefaultSupersection_notRead(self,cookie=None,proxy=None,**context):
		indexWriteLock.release()
		proxy.modclient.callback(1,cookie,**context) #No such index
	
	def setDefaultSupersection_readOnly(self,cookie=None,proxy=None,**context):
		indexWriteLock.release()
		proxy.modclient.callback(2,cookie,**context) #readOnly
	
	def setDefaultSupersection_written(self,cookie=None,proxy=None,**context):
		indexWriteLock.release()
		proxy.modclient.callback(0,cookie,**context)
		
def onStart(**context):
	global vsfs
	vsfs = vsfs_client.VsfsClientInterfaceProxy(client,vsfsInterfaceId)
	vsfs.on("connected",onVsfsConnect)
	vsfs.on("disconnected",onVsfsDisconnect)
	
	global provider
	provider = config_provider.ConfigProviderInterfaceProxy(client,ConfigProvider(),configInterfaceId)
	
	#client.openInterface(vsfsInterfaceId,"vsfs-client",1)
	client.openInterface(configInterfaceId,"config-provider",-1)
	print("started")

client.on("start",onStart)
