import modserv.easyclient as easyclient
import modserv.easydriver as easydriver

client = easyclient.getEasyClient()

def portsetupToString(id,portsetup):
	return str(id)+": "+str(portsetup.protocol)+"\n < "+str(portsetup.inSide)+" v"+str(portsetup.inMinVersion)+"\n > "+str(portsetup.outSide)+" v"+str(portsetup.outMinVersion)

def getKeyReturnHandler(id,key):
	print("Module crated id: "+str(id)+" key: "+str(key))
	
def requestDirectCildrenReturnHandler(moduleId,children):
	print("Direct children of Module "+str(moduleId)+":\n"+str(children))
	
def requestDirectCildrenErrorHandler():
	print("No such Module")

def requestModuleInfoReturnHandler(moduleId,parentId,listOfOpenInterfaces):
	print("Information for Module "+str(moduleId)+"\nparent: "+str(parentId)+"\nopen Interfaces:"+str(listOfOpenInterfaces))

def requestModuleInfoErrorHandler():
	print("No such Module")
	
def requestInterfaceInfoReturnHandler(moduleId,interfaceId,maxconnections,listOfOpenConnections,mapOfPortsetups):
	porttext = "Open Ports:\n"
	for id,portsetup in mapOfPortsetups.items():
		porttext = porttext+portsetupToString(id,portsetup)
	print("Information for Interface "+str(moduleId)+"."+str(interfaceId)+"\nmaxconnections: "+str(maxconnections)+"\nopen Connections:"+str(listOfOpenConnections)+"\n"+porttext)
	
def requestInterfaceInfoErrorHandler():
	print("No such Module or Interface")

def connectSuccessHandler(midA,ifidA,cidA,midB,ifidB,cidB):
	print("Connected "+str(midA)+"."+str(ifidA)+"."+str(cidA)+" and "+str(midB)+"."+str(ifidB)+"."+str(cidB)+" !")

def connectErrorHandler(midA,ifidA,midB,ifidB,reason):
	print("Error connecting "+str(midA)+"."+str(ifidA)+" and "+str(midB)+"."+str(ifidB)+" ["+reason+"]")
	
def requestConnectionInfoSuccessHandler(moduleId,interfaceId,connectionId,peerModuleId,peerInterfaceId,peerConnectionId):
	print("Information for Connection "+str(moduleId)+"."+str(interfaceId)+"."+str(connectionId)+"\npeer: "+str(peerModuleId)+"."+str(peerInterfaceId)+"."+str(peerConnectionId))

def requestConnectionInfoErrorHandler():
	print("No such Module, Interface or Connection")

def disconnectSuccessHandler():
	print("Connection closed!")
	
def disconnectErrorHandler():
	print("No such Module, Interface or Connection")

def shutdownSuccessHandler(data=-1):
	print("Module "+str(data)+" shutdown!")
	
def shutdownErrorHandler(data=-1):
	print("No Such Module!")

#main command input loop
def mainloop():
	i = input(">")
	ins = i.split(" ")
	cmd = ins[0]
	#print(cmd,ins)
	if cmd == "exit":
		print("Shuttig down ...")
		client.shutdown()
		return False
	elif cmd == "getkey" or cmd == "gk":
		print("getting Key ...")
		client.makeCild(onSuccess=getKeyReturnHandler)
	elif cmd == "lsm":
		#list modules
		try:
			mid = int(ins[1])
			client.requestDirectCildren(mid,onSuccess=requestDirectCildrenReturnHandler,onFailure=requestDirectCildrenErrorHandler)
		except:
			print("Usage: lsm <moduleId>")
	#get module info
	elif cmd == "modinfo":
		#list modules
		try:
			mid = int(ins[1])
			client.requestModuleInfo(mid,onSuccess=requestModuleInfoReturnHandler,onFailure=requestModuleInfoErrorHandler)
		except:
			print("Usage: modinfo <moduleId>")
	elif cmd == "intfinfo":
		try:
			mid = int(ins[1])
			ifid = int(ins[2])
			client.requestInterfaceInfo(mid,ifid,onSuccess=requestInterfaceInfoReturnHandler,onFailure=requestInterfaceInfoErrorHandler)
		except:
			print("Usage: intfinfo <moduleId> <interfaceId>")
			raise
	elif cmd == "connect":
		try:
			midA = int(ins[1])
			ifidA = int(ins[2])
			midB = int(ins[3])
			ifidB = int(ins[4])
			client.connect(midA,ifidA,midB,ifidB,onSuccess=connectSuccessHandler,onFailure=connectErrorHandler)
		except:
			print("Usage: connect <moduleIdA> <interfaceIdA> <moduleIdB> <interfaceIdB>")
	elif cmd == "conninfo":
		try:
			mid = int(ins[1])
			ifid = int(ins[2])
			cid = int(ins[3])
			client.requestConnectionInfo(mid,ifid,cid,onSuccess=requestConnectionInfoSuccessHandler,onFailure=requestConnectionInfoErrorHandler)
		except:
			print("Usage: conninfo <moduleId> <interfaceId> <connectionId>")
	elif cmd == "disconnect":
		try:
			mid = int(ins[1])
			ifid = int(ins[2])
			cid = int(ins[3])
			client.closeConnection(mid,ifid,cid,onSuccess=disconnectSuccessHandler,onFailure=disconnectErrorHandler)
		except:
			print("Usage: disconnect <moduleId> <interfaceId> <connectionId>")
	elif cmd == "shutdown":
		#list modules
		try:
			mid = int(ins[1])
			client.shutdown(mid,onSuccess=shutdownSuccessHandler,onFailure=shutdownErrorHandler,data=mid)
		except:
			print("Usage: shutdown <moduleId>")
	elif cmd == "help":
		print(
			"getkey\n"+
			"shutdown <moduleId>\n"+
			"lsm <moduleId>\n"+
			"modinfo <moduleId>\n"+
			"intfinfo <moduleId> <interfaceId>\n"+
			"connect <moduleIdA> <interfaceIdA> <moduleIdB> <interfaceIdB>\n"+
			"conninfo <moduleId> <interfaceId> <connectionId>\n"+
			"disconnect <moduleId> <interfaceId> <connectionId>"
		)
	else:
		print("Unknown command ... (Use 'help' for a list of commands)")
	return True
	
def selfShutdown():
	print("Shut down!")
	
# Put initalization code here
client.on("selfShutdown",selfShutdown)


easydriver.EasyDriverLooper(mainloop,client,daemon=True)
client.initalized()

#Put cleanup code here if you wrote a driver
