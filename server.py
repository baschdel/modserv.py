import socket
import modserv.server.module as module
import struct
import sys
from modserv.packetsocket import PacketSocket
from modserv.cronjob import Cronjob
from modserv.server.netadapterServer import ServerModuleCallbackHandler,ServerPacketHandler
from threading import Thread

module_registry = module.ModuleRegistry()
root_module = module.Module(module_registry)
root_module.key = "0123"

running = True

########################################################
import modserv.typeenc as typeenc

class serverHandshakeHandler():
	def __init__(self,module_registry):
		self.module_registry = module_registry
		
	def onPacket(self,binary,packetsocket):
		inputlist = typeenc.DSList().fromBinary(binary).getList()
		if len(inputlist) == 2 and len(inputlist[0]) == 8:
			mid = struct.unpack(">Q",inputlist[0])[0]
			key = inputlist[1].decode('utf-8')
			module = module_registry.getModule(mid)
			if not (module is None):
				if module.key == key and (not module.connected):
					#attach handlers to module and packetsocket
					scbh = ServerModuleCallbackHandler(packetsocket)
					sph = ServerPacketHandler(module,packetsocket)
					packetsocket.setPackethandler(sph)
					module.setModuleCallbackHandler(scbh)
					packetsocket.send(b"OK")
					module.onModuleConnect()
					print("[HANDSHAKE HANDLER] logged module "+str(mid)+" with key "+key+" in!")
				else:
					packetsocket.close()
			else:
				packetsocket.close()
			
	def onClose(self,packetsocket):
		pass

########################################################

#clients = [] let's see if it works without

serverSocket = None

def autoShutdown():
	#print("autoshutdown?",module_registry.modules)
	if len(module_registry.modules) == 0:
		print("No modules registred, goodbye!")
		global running
		running = False
		serverSocket.close()
		sys.exit(0)

def setupSocket(localhostOnly=True,port=8383):
	if localhostOnly:
		s = socket.socket()
		s.bind(("127.0.0.1",port))
		s.listen()
		return s
	else:
		s = socket.socket()
		s.bind(("0.0.0.0/32",port))
		s.listen()
		return s


def serverLoop(socket,handler):
	global running
	while running:
		client,addr = socket.accept()
		cps = PacketSocket(client,handler)
		cps.start()
		
serverSocket = setupSocket()
Cronjob(autoShutdown,5,daemon=False)
Thread(target=serverLoop,args=(serverSocket,serverHandshakeHandler(module_registry)),daemon=True).start()
