from modserv.modclient2 import Port
from modserv.modclient2 import ModeCallback

CALL_LIST_SECTIONS = 1
CALL_GET_SECTION_INFO = 2
CALL_READ_SECTION_DATA = 3
CALL_WRITE_SECTION_DATA = 4
CALL_MAKE_SECTION = 5
CALL_DELETE_SECTION = 6
CALL_SET_SECTION_ENABLED = 7
CALL_SET_SECTION_FORMAT = 8
CALL_RENAME_SECTION = 9
CALL_COPY_SECTION = 10
CALL_SET_SECTION_APPLICATION_WRITABLE = 11

CALL_LIST_INDICES = 100
CALL_GET_INDEX_INFO = 101
CALL_RESOLVE_SECTION_NAME = 102
CALL_LIST_INDEX_SUPERSECTIONS = 103
CALL_LIST_INDEX_SECTIONS = 104
CALL_MAKE_INDEX = 105
CALL_MAKE_INDEX_SUPERSECTION = 106
CALL_REMOVE_INDEX = 107
CALL_REMOVE_INDEX_SUPERSECTION = 108
CALL_MAP_INDEX_SECTION = 109
CALL_MAP_INDEX_EXTERNAL_SECTION = 110
CALL_UNMAP_INDEX_SECTION = 111
CALL_SET_INDEX_ENABLED = 112
CALL_SET_DEFAULT_SUPERSECTION = 113

CALL_BACKEND_CHANGED = 200
CALL_NAME_UPDATED = 201

CALL_GET_PROVIDER_INFO = 300
CALL_RENAME_PROVIDER = 301

class ConfigProvider(Port):
	def __init__(self,provider):
		self.calls = {
			CALL_LIST_SECTIONS:self._listSections,
			CALL_GET_SECTION_INFO:self._getSectionInfo,
			CALL_READ_SECTION_DATA:self._readSectionData,
			CALL_WRITE_SECTION_DATA:self._writeSectionData,
			CALL_MAKE_SECTION:self._makeSection,
			CALL_DELETE_SECTION:self._deleteSection,
			CALL_SET_SECTION_ENABLED:self._setSectionEnabled,
			CALL_SET_SECTION_FORMAT:self._setSectionFormat,
			CALL_RENAME_SECTION:self._renameSection,
			CALL_COPY_SECTION:self._copySection,
			CALL_SET_SECTION_APPLICATION_WRITABLE:self._setSectionApplicationWritable,

			CALL_LIST_INDICES:self._listIndices,
			CALL_GET_INDEX_INFO:self._getIndexInfo,
			CALL_RESOLVE_SECTION_NAME:self._resolveSectionName,
			CALL_LIST_INDEX_SUPERSECTIONS:self._listIndexSupersections,
			CALL_LIST_INDEX_SECTIONS:self._listIndexSections,
			CALL_MAKE_INDEX:self._makeIndex,
			CALL_MAKE_INDEX_SUPERSECTION:self._makeIndexSupersection,
			CALL_REMOVE_INDEX:self._removeIndex,
			CALL_REMOVE_INDEX_SUPERSECTION:self._removeIndexSupersection,
			CALL_MAP_INDEX_SECTION:self._mapIndexSection,
			CALL_MAP_INDEX_EXTERNAL_SECTION:self._mapIndexExternalSection,
			CALL_UNMAP_INDEX_SECTION:self._unmapIndexSection,
			CALL_SET_INDEX_ENABLED:self._setIndexEnabled,
			CALL_SET_DEFAULT_SUPERSECTION:self._setDefaultSupersection,
			
			CALL_GET_PROVIDER_INFO:self._getProviderInfo,
			CALL_RENAME_PROVIDER:self._renameProvider,
			}
		self.maxconnections = -1
		self.protocol = "config-provider"
		self.inSide = "p" #provider
		self.outSide = "m" #manager
		self.provider = provider
	
	#event callbacks
	def backendChanged(self):
		self._broadcast(CALL_BACKEND_CHANGED)
		
	def nameUpdated(self,name):
		self._broadcast(CALL_NAME_UPDATED,name)
		
	#section calls
	
	def _listSections(self,callcontext,cookie):
		callcontext.returnSectionList = ModeCallback(callcontext,0)
		callcontext.backendNotConnected = ModeCallback(callcontext,1)
		self.provider.listSections(callcontext)
		
	def _getSectionInfo(self,callcontext,cookie,name):
		callcontext.returnSectionInfo = ModeCallback(callcontext,0)
		callcontext.noSuchSection = ModeCallback(callcontext,1)
		self.provider.getSectionInfo(callcontext,name)
		
	def _readSectionData(self,callcontext,cookie,name):
		callcontext.returnSectionData = ModeCallback(callcontext,0)
		callcontext.noSuchSection = ModeCallback(callcontext,1)
		self.provider.readSectionData(callcontext,name)
		
	def _writeSectionData(self,callcontext,cookie,name,data):
		callcontext.returnSectionDataWritten = ModeCallback(callcontext,0)
		callcontext.noSuchSection = ModeCallback(callcontext,1)
		callcontext.sectionIsReadOnly = ModeCallback(callcontext,2)
		self.provider.writeSectionData(callcontext,name,data)
		
	def _makeSection(self,callcontext,cookie,name,rawType,dataFormat,size,enabled,readonly,applicationWritable):
		callcontext.returnSectionCreated = ModeCallback(callcontext,0)
		callcontext.sectionAlreadyExists = ModeCallback(callcontext,1)
		callcontext.providerIsReadOnly = ModeCallback(callcontext,2)
		callcontext.invalidName = ModeCallback(callcontext,3)
		callcontext.invalidType = ModeCallback(callcontext,4)
		self.provider.makeSection(callcontext,name,rawType,dataFormat,size,enabled,readonly,applicationWritable)
		
	def _deleteSection(self,callcontext,cookie,name):
		callcontext.returnSectionDeleted = ModeCallback(callcontext,0)
		callcontext.sectionIsReadOnly = ModeCallback(callcontext,1)
		callcontext.sectionDoesNotExist = ModeCallback(callcontext,2)
		self.provider.deleteSection(callcontext,name)
	
	def _setSectionEnabled(self,callcontext,cookie,name,enabled):
		callcontext.returnSectionEnableSet = ModeCallback(callcontext,0)
		callcontext.noSuchSection = ModeCallback(callcontext,1)
		callcontext.flagIsReadOnly = ModeCallback(callcontext,2)
		self.provider.setSectionEnabled(callcontext,name,enabled)
		
	def _setSectionFormat(self,callcontext,cookie,name,dataFormat):
		callcontext.returnFormatSet = ModeCallback(callcontext,0)
		callcontext.noSuchSection = ModeCallback(callcontext,1)
		callcontext.sectionIsReadOnly = ModeCallback(callcontext,2)
		self.provider.setSectionFormat(callcontext,name,dataFormat)
		
	def _renameSection(self,callcontext,cookie,name,newName):
		callcontext.returnSectionRenamed = ModeCallback(callcontext,0)
		callcontext.sectionAlreadyExists = ModeCallback(callcontext,1)
		callcontext.providerIsReadOnly = ModeCallback(callcontext,2)
		callcontext.invalidName = ModeCallback(callcontext,3)
		callcontext.noSuchSection = ModeCallback(callcontext,4)
		self.provider.renameSection(callcontext,name,newName)
		
	def _copySection(self,callcontext,cookie,name,toName):
		callcontext.returnSectionCreated = ModeCallback(callcontext,0)
		callcontext.sectionAlreadyExists = ModeCallback(callcontext,1)
		callcontext.providerIsReadOnly = ModeCallback(callcontext,2)
		callcontext.invalidName = ModeCallback(callcontext,3)
		callcontext.noSuchSection = ModeCallback(callcontext,4)
		self.provider.copySection(callcontext,name,toName)
		
	def _setSectionApplicationWritable(self,callcontext,cookie,name,applicationWritable):
		callcontext.returnApplicationWritableSet = ModeCallback(callcontext,0)
		callcontext.sectionIsReadOnly = ModeCallback(callcontext,1)
		callcontext.noSuchSection = ModeCallback(callcontext,2)
		self.provider.setSectionApplicationWritable(callcontext,name,applicationWritable)
		
	#index calls
	
	def _listIndices(self,callcontext,cookie):
		callcontext.returnIndexList = ModeCallback(callcontext,0)
		callcontext.backendNotConnected = ModeCallback(callcontext,1)
		self.provider.listIndices(callcontext)
		
	def _getIndexInfo(self,callcontext,cookie,name):
		callcontext.returnIndexInfo = ModeCallback(callcontext,0)
		callcontext.noSuchIndex = ModeCallback(callcontext,1)
		self.provider.getIndexInfo(callcontext,name)
		
	def _resolveSectionName(self,callcontext,cookie,indexName,supersectionName,sectionName):
		callcontext.section = ModeCallback(callcontext,0)
		callcontext.external = ModeCallback(callcontext,1)
		callcontext.noSuchIndex = ModeCallback(callcontext,2)
		callcontext.noSuchSupersection = ModeCallback(callcontext,3)
		callcontext.noSuchSection = ModeCallback(callcontext,4)
		self.provider.resolveSectionName(callcontext,indexName,supersectionName,sectionName)
		
	def _listIndexSupersections(self,callcontext,cookie,indexName):
		callcontext.returnSupersectionList = ModeCallback(callcontext,0)
		callcontext.noSuchIndex = ModeCallback(callcontext,1)
		self.provider.listIndexSupersections(callcontext,indexName)
		
	def _listIndexSections(self,callcontext,cookie,indexName,supersectionName):
		callcontext.returnSupersectionList = ModeCallback(callcontext,0)
		callcontext.noSuchIndex = ModeCallback(callcontext,1)
		callcontext.noSuchSupersection = ModeCallback(callcontext,2)
		self.provider.listIndexSections(callcontext,indexName,supersectionName)
		
	def _makeIndex(self,callcontext,cookie,name):
		callcontext.returnIndexCreated = ModeCallback(callcontext,0)
		callcontext.alreadyExists = ModeCallback(callcontext,1)
		callcontext.providerIsReadOnly = ModeCallback(callcontext,2)
		callcontext.invalidName = ModeCallback(callcontext,3)
		self.provider.makeIndex(callcontext,name)
		
	def _makeIndexSupersection(self,callcontext,cookie,indexName,supersectionName):
		callcontext.returnSupersectionCreated = ModeCallback(callcontext,0)
		callcontext.alreadyExists = ModeCallback(callcontext,1)
		callcontext.indexIsReadOnly = ModeCallback(callcontext,2)
		callcontext.noSuchIndex = ModeCallback(callcontext,3)
		callcontext.invalidSupersectionName = ModeCallback(callcontext,4)
		self.provider.makeIndexSupersection(callcontext,cookie,indexName,supersectionName)
		
	def _removeIndex(self,callcontext,cookie,name):
		callcontext.returnIndexRemoved = ModeCallback(callcontext,0)
		callcontext.indexDoesNotExist = ModeCallback(callcontext,1)
		callcontext.indexIsReadOnly = ModeCallback(callcontext,2)
		self.provider.removeIndex(callcontext,name)
	
	def _removeIndexSupersection(self,callcontext,cookie,indexName,supersectionName):
		callcontext.returnSupersectionRemoved = ModeCallback(callcontext,0)
		callcontext.supersectionDoesNotExist = ModeCallback(callcontext,1)
		callcontext.indexIsReadOnly = ModeCallback(callcontext,2)
		callcontext.noSuchIndex = ModeCallback(callcontext,3)
		self.provider.removeIndexSupersection(callcontext,indexName,supersectionName)
		
	def _mapIndexSection(self,callcontext,cookie,indexName,supersectionName,sectionName,destinationSectionName,applicationWritable):
		callcontext.returnSectionMapped = ModeCallback(callcontext,0)
		callcontext.indexIsReadOnly = ModeCallback(callcontext,1)
		callcontext.noSuchIndex = ModeCallback(callcontext,2)
		callcontext.noSuchSupersection = ModeCallback(callcontext,3)
		callcontext.invalidSectionName = ModeCallback(callcontext,4)
		callcontext.invalidDestinationName = ModeCallback(callcontext,5)
			self.provider.mapIndexSection(callcontext,indexName,supersectionName,sectionName,destinationSectionName,applicationWritable)
		
	def _mapIndexExternalSection(self,callcontext,cookie,indexName,supersectionName,sectionName,provider,resource):
		callcontext.returnExternalSectionMapped = ModeCallback(callcontext,0)
		callcontext.indexIsReadOnly = ModeCallback(callcontext,1)
		callcontext.noSuchIndex = ModeCallback(callcontext,2)
		callcontext.noSuchSupersection = ModeCallback(callcontext,3)
		callcontext.invalidSectionName = ModeCallback(callcontext,4)
		callcontext.invalidProviderName = ModeCallback(callcontext,5)
		callcontext.invalidResourceName = ModeCallback(callcontext,6)
		self.provider.mapIndexExternalSection(callcontext,indexName,supersectionName,sectionName,provider,resource)
		
	def _unmapIndexSection(self,callcontext,cookie,indexName,supersectionName,sectionName):
		callcontext.returnSectionUnmapped = ModeCallback(callcontext,0)
		callcontext.indexIsReadOnly = ModeCallback(callcontext,1)
		callcontext.noSuchIndex = ModeCallback(callcontext,2)
		callcontext.noSuchSupersection = ModeCallback(callcontext,3)
		callcontext.noSuchSection = ModeCallback(callcontext,4)
		self.provider.unmapIndexSection(callcontext,indexName,supersectionName,sectionName)
			
	def _setIndexEnabled(self,callcontext,cookie,name,enabled):
		callcontext.returnIndexEnableSet = ModeCallback(callcontext,0)
		callcontext.noSuchIndex = ModeCallback(callcontext,1)
		callcontext.indexIsReadOnly = ModeCallback(callcontext,2)
		self.provider.setIndexEnabled(callcontext,name,enabled)
		
	def _setDefaultSupersection(self,callcontext,cookie,indexName,supersectionName):
		callcontext.returnDefaultSupersectionSet = ModeCallback(callcontext,0)
		callcontext.noSuchIndex = ModeCallback(callcontext,1)
		callcontext.indexIsReadOnly = ModeCallback(callcontext,2)
		callcontext.invalidSupersectionName = ModeCallback(callcontext,3)
		self.provider.setDefaultSupersection(callcontext,indexName,supersectionName)
		
	#provider calls
	
	def _getProviderInfo(self,callcontext,cookie):
		callcontext.reuturnProviderInfo = ModeCallback(callcontext,0)
		callcontext.backendNotConnected = ModeCallback(callcontext,1)
		self.provider.getProviderInfo
		
	def _renameProvider(self,callcontext,cookie,newName):
		callcontext.returnProviderRenamed = ModeCallback(callcontext,0)
		callcontext.providerCanNotBeRenamed = ModeCallback(callcontext,1)
		callcontext.invalidName = ModeCallback(callcontext,2)
		self.provider.renameProvider
		
class Provider:

	#section calls
	
	def listSections(self,callcontext):
		callcontext.backendNotConnected()
		
	def getSectionInfo(self,callcontext,name):
		callcontext.noSuchSection()
		
	def readSectionData(self,callcontext,name):
		callcontext.noSuchSection()
		
	def writeSectionData(self,callcontext,name,data):
		callcontext.noSuchSection()
		
	def makeSection(self,callcontext,name,rawType,dataFormat,size,enabled,readonly,applicationWritable):
		callcontext.providerIsReadOnly()
		
	def deleteSection(self,callcontext,name):
		callcontext.sectionDoesNotExist()
	
	def setSectionEnabled(self,callcontext,name,enabled):
		callcontext.noSuchSection()
	
	def setSectionFormat(self,callcontext,name,dataFormat):
		callcontext.noSuchSection()
		
	def renameSection(self,callcontext,name,newName):
		callcontext.providerIsReadOnly()
		
	def copySection(self,callcontext,name,toName):
		callcontext.providerIsReadOnly()
		
	def setSectionApplicationWritable(self,callcontext,name,applicationWritable):
		callcontext.noSuchSection()
		
	#index calls
	
	def listIndices(self,callcontext):
		callcontext.backendNotConnected()
		
	def getIndexInfo(self,callcontext,name):
		callcontext.noSuchIndex()
		
	def resolveSectionName(self,callcontext,indexName,supersectionName,sectionName):
		callcontext.noSuchIndex()
		
	def listIndexSupersections(self,callcontext,indexName):
		callcontext.noSuchIndex()
		
	def listIndexSections(self,callcontext,indexName,supersectionName):
		callcontext.noSuchIndex()
		
	def makeIndex(self,callcontext,name):
		callcontext.providerIsReadOnly()
		
	def makeIndexSupersection(self,callcontext,indexName,supersectionName):
		callcontext.noSuchIndex()
		
	def removeIndex(self,callcontext,name):
		callcontext.indexDoesNotExist()
	
	def removeIndexSupersection(self,callcontext,indexName,supersectionName):
		callcontext.supersectionDoesNotExist()
		
	def mapIndexSection(self,callcontext,indexName,supersectionName,sectionName,destinationSectionName,applicationWritable):
		callcontext.noSuchIndex()
		
	def mapIndexExternalSection(self,callcontext,indexName,supersectionName,sectionName,provider,resource):
		callcontext.noSuchIndex()
		
	def unmapIndexSection(self,callcontext,indexName,supersectionName,sectionName):
		callcontext.noSuchIndex()
			
	def setIndexEnabled(self,callcontext,name,enabled):
		callcontext.noSuchIndex()
		
	def setDefaultSupersection(self,callcontext,indexName,supersectionName):
		callcontext.noSuchIndex()
		
	#provider calls
	
	def getProviderInfo(self,callcontext):
		callcontext.backendNotConnected()
	
	def renameProvider(self,callcontext,newName):
		callcontext.providerCanNotBeRenamed()
