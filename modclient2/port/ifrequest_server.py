from modserv.modclient2 import Port

CALL_LIST = 0
CALL_REQUEST = 1
CALL_NEW_TYPES = 2

class IfRequestServer(Port):
	def __init__(self):
		self.calls = {
			CALL_LIST:self._list_,
			CALL_REQUEST:self._request_}
		self.maxconnections = -1
		self.protocol = "ifrequest"
		self.inSide = "p" #provider
		self.outSide = "r" #reqester
		self.requestHandlers = {}
		
	def _list_(self,callcontext,cookie):
		types = []
		for iftype,_ in self.requestHandlers.items():
			types.append(iftype)
		callcontext.callback(types)
		
	def _request_(self,callcontext,cookie,iftype,description):
		if iftype in self.requestHandlers:
			self.requestHandlers[iftype](CallbackProxy(callcontext),description)
		else:
			callcontext.callback(1) #type not supported
		
	def setInterfaceFactory(self,iftype,callback):
		if callback is None:
			del self.requestHandlers[iftype]
		else:
			self.requestHandlers[iftype] = callback
		self._broadcast(CALL_NEW_TYPES)
			
class CallbackProxy:
	def __init__(self,callcontext):
		self.callcontext = callcontext
	
	def invalidArgument(self,key,value):
		self.callcontext.callback(2,key,value)
		
	def cannotSupply(self):
		self.callcontext.callback(3)
		
	def useInterface(self,moduleId,interfaceId):
		self.callcontext.callback(0,moduleId,interfaceId)
