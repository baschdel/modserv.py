from modserv.modclient2 import Port

class PrimitiveCli(Port):
	def __init__(self,onMessage=None,onConnectionOpened=None,onConnectionClosed=None):
		if not (onMessage is None):
			self.onMessage = onMessage
		if not (onConnectionOpened is None):
			self._onConnectionOpened = onConnectionOpened
		if not (onConnectionClosed is None):
			self._onConnectionClosed = onConnectionClosed
		self.calls = [self.onMessage]
		self.maxconnections = -1
		self.protocol = "primitive-cli"
		self.inSide = "x"
		self.outSide = "x"
	
	def onMessage(self,context,message):
		pass
		
	def send(self,connectionId,message):
		self._call(connectionId,0,message)
		
	def broadcast(self,message):
		self._broadcast(0,message)
