from modserv.modclient2 import Port
from modserv.cookieGenerator import generateCookie
from modserv.eventbus import Eventbus

CALL_LIST = 0
CALL_REQUEST = 1
CALL_NEW_TYPES = 2

class IfRequestClient(Port):
	def __init__(self):
		self.calls = {
			CALL_NEW_TYPES:self._typesUpdated_}
		self.maxconnections = 1
		self.protocol = "ifrequest"
		self.inSide = "r" #reqester
		self.outSide = "p" #provider
		self.connectionId = None
		self.eventbus = Eventbus()
		
		self.on = self.eventbus.on
		self.ignore = self.eventbus.ignore
		self.don = self.eventbus.don
		self.dignore = self.eventbus.dignore
	
	def isConnected(self):
		return not (self.connectionId is None)
		
	def _onConnectionOpened(self,connectionId):
		self.connectionId = connectionId
		
	def _onConnectionClosed(self,connectionId):
		self.connectionId = None
		
	def _typesUpdated_(self):
		self.eventbus.fire("typesUpdated")
		
	def list(self,onSuccess=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(self.connectionId,CALL_LIST,generateCookie(),callback=onSuccess)
	
	#description should be a lit of tuples
	def request(self,iftype,description,onSuccess=None,onTypeNotSupported=None,onInvalidArgument=None,onCannotSupply=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		if type(description) is dict:
			ndesc = []
			for key,value in description.items():
				ndesc.append((key,value))
			description = ndesc
		self._call(self.connectionId,CALL_REQUEST,generateCookie(),iftype,description,callback={
			0:onSuccess,
			1:onTypeNotSupported,
			2:onInvalidArgument,
			3:onCannotSupply,
			})
