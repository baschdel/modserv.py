from modserv.modclient2 import Port
from modserv.cookieGenerator import generateCookie

CALL_READ = 0
CALL_WRITE = 1
CALL_APPEND = 2
CALL_DELETE = 3
CALL_LIST = 4
CALL_GETSIZE = 5

class VsfsServer(Port):
	def __init__(self):
		self.calls = [
			self.read,
			self.write,
			self.append,
			self.delete,
			self.list,
			self.getSize]
		self.maxconnections = -1
		self.protocol = "vsfs"
		self.inSide = "s"
		self.outSide = "c"

	def read(self,callcontext,cookie,name):
		callcontext.callback(1)
		
	def write(self,callcontext,cookie,name,data):
		callcontext.callback(2)
		
	def append(self,callcontext,cookie,name,data):
		callcontext.callback(2)
		
	def delete(self,callcontext,cookie,name):
		callcontext.callback(1)
		
	def list(self,callcontext,cookie,name):
		callcontext.callback([])
		
	def getSize(self,callcontext,cookie,name):
		callcontext.callback(1)
