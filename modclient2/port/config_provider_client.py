from modserv.modclient2 import Port
from modserv.cookieGenerator import generateCookie
from modserv.eventbus import Eventbus

CALL_LIST_SECTIONS = 1
CALL_GET_SECTION_INFO = 2
CALL_READ_SECTION_DATA = 3
CALL_WRITE_SECTION_DATA = 4
CALL_MAKE_SECTION = 5
CALL_DELETE_SECTION = 6
CALL_SET_SECTION_ENABLED = 7
CALL_SET_SECTION_FORMAT = 8
CALL_RENAME_SECTION = 9
CALL_COPY_SECTION = 10
CALL_SET_SECTION_APPLICATION_WRITABLE = 11

CALL_LIST_INDICES = 100
CALL_GET_INDEX_INFO = 101
CALL_RESOLVE_SECTION_NAME = 102
CALL_LIST_INDEX_SUPERSECTIONS = 103
CALL_LIST_INDEX_SECTIONS = 104
CALL_MAKE_INDEX = 105
CALL_MAKE_INDEX_SUPERSECTION = 106
CALL_REMOVE_INDEX = 107
CALL_REMOVE_INDEX_SUPERSECTION = 108
CALL_MAP_INDEX_SECTION = 109
CALL_MAP_INDEX_EXTERNAL_SECTION = 110
CALL_UNMAP_INDEX_SECTION = 111
CALL_SET_INDEX_ENABLED = 112
CALL_SET_DEFAULT_SUPERSECTION = 113

CALL_BACKEND_CHANGED = 200
CALL_NAME_UPDATED = 201

CALL_GET_PROVIDER_INFO = 300
CALL_RENAME_PROVIDER = 301

class ConfigProviderClient(Port):
	def __init__():
		self.calls = {
			CALL_BACKEND_CHANGED:self._onBackendChanged,
			CALL_NAME_UPDATED:self._onNameUpdated,
		}
		self.maxconnections = 1
		self.protocol = "config-provider"
		self.inSide = "m" #manager
		self.outSide = "p" #provider
		self.connectionId = None
		self.eventbus = Eventbus()
		
		self.on = self.eventbus.on
		self.ignore = self.eventbus.ignore
		self.don = self.eventbus.don
		self.dignore = self.eventbus.dignore
	
	def isConnected(self):
		return not (self.connectionId is None)
		
	def _onConnectionOpened(self,connectionId):
		self.connectionId = connectionId
		
	def _onConnectionClosed(self,connectionId):
		self.connectionId = None
	
	def _onBackendChanged(self):
		self.eventbus.fire("backendChanged")
		
	def _onNameUpdated(self,name):
		self.eventbus.fire("nameUpdated")
		
	#section calls
	
	def listSections(self,onSuccess=None,onBackendNotConnected=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(CALL_LIST_SECTIONS,generateCookie(),callback={
			0:onSuccess,
			1:onBackendNotConnected,
		})
		
	def getSectionInfo(self,name,onSuccess=None,onNoSuchSection=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(CALL_LIST_SECTIONS,generateCookie(),callback={
			0:onSuccess,
			1:onNoSuchSection,
		})
		
	def readSectionData(self,name,onSuccess=None,onNoSuchSection=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(CALL_LIST_SECTIONS,generateCookie(),callback={
			0:onSuccess,
			1:onNoSuchSection,
		})
		
	def writeSectionData(self,name,data,onSuccess=None,onNoSuchSection=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(CALL_LIST_SECTIONS,generateCookie(),callback={
			0:onSuccess,
			1:onNoSuchSection,
		})
		
	def makeSection(self,name,rawType,dataFormat,size,enabled,readonly,applicationWritable
		,onSuccess=None,onSectionAlreadyExists=None,onProviderIsReadOnly=None,onInvalidName=None
		,onInvalidType=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(CALL_LIST_SECTIONS,generateCookie(),callback={
			0:onSuccess,
			1:onSectionAlreadyExists,
			2:onProviderIsReadOnly,
			3:onInvalidName,
			4:onInvalidType,
		})
		
	def deleteSection(self,name,onSuccess=None,onSectionIsReadOnly=None,onSectionDoesNotExist=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(CALL_LIST_SECTIONS,generateCookie(),callback={
			0:onSuccess,
			1:onSectionIsReadOnly,
			2:onSectionDoesNotExist,
		})
		
	def setSectionEnabled(self,name,enabled,onSuccess=None,onNoSuchSection=None,onFlagIsReadOnly=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(CALL_LIST_SECTIONS,generateCookie(),callback={
			0:onSuccess,
			1:onNoSuchSection,
			2:onFlagIsReadOnly,
		})
		
	def setSectionFormat(self,name,dataFormat,onSuccess=None,onNoSuchSection=None,onSectionIsReadOnly=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(CALL_LIST_SECTIONS,generateCookie(),callback={
			0:onSuccess,
			1:onNoSuchSection,
			2:onSectionIsReadOnly,
		})
		
	def renameSection(self,name,newName
		,onSuccess=None,onSectionAlreadyExists=None,onProviderIsReadOnly=None,onInvalidName=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(CALL_LIST_SECTIONS,generateCookie(),callback={
			0:onSuccess,
			1:onSectionAlreadyExists,
			2:onProviderIsReadOnly,
			3:onInvalidName,
		})
		
	def copySection(self,name,toName
		,onSuccess=None,onSectionAlreadyExists=None,onProviderIsReadOnly=None,onInvalidName=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(CALL_LIST_SECTIONS,generateCookie(),callback={
			0:onSuccess,
			1:onSectionAlreadyExists,
			2:onProviderIsReadOnly,
			3:onInvalidName,
		})
		
	def setSectionApplicationWritable(self,name,applicationWritable
		,onSuccess=None,onSectionIsReadOnly=None,onNoSuchSection=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(CALL_LIST_SECTIONS,generateCookie(),callback={
			0:onSuccess,
			1:sectionIsReadOnly,
			2:onNoSuchSection,
		})
		
	#index calls
	
	def listIndices(self,onSuccess=None,onBackendNotConnected=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(CALL_LIST_SECTIONS,generateCookie(),callback={
			0:onSuccess,
			1:onBackendNotConnected,
		})
		
	def getIndexInfo(self,name,onSuccess=None,onNoSuchIndex=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(CALL_LIST_SECTIONS,generateCookie(),callback={
			0:onSuccess,
			1:onNoSuchIndex,
		})
		
	def resolveSectionName(self,indexName,supersectionName,sectionName
		,onSection=None,onExternalSection=None,onNoSuchIndex=None,onNoSuchSupersection=None
		,onNoSuchSection=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(CALL_LIST_SECTIONS,generateCookie(),callback={
			0:onSection,
			1:onExternalSection,
			2:onNoSuchIndex,
			3:onNoSuchSupersection,
			4:onNoSuchSection,
		})
		
	def listIndexSupersections(self,indexName,onSuccess=None,onNoSuchIndex=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(CALL_LIST_SECTIONS,generateCookie(),callback={
			0:onSuccess,
			1:onNoSuchIndex,
		})
		
	def listIndexSections(self,indexName,supersectionName
		,onSuccess=None,onNoSuchIndex=None,onNoSuchSupersection=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(CALL_LIST_SECTIONS,generateCookie(),callback={
			0:onSuccess,
			1:onNoSuchIndex,
			2:onNoSuchSupersection,
		})
		
	def makeIndex(self,name
		,onSuccess=None,onAlreadyExists=None,onProviderIsReadOnly=None,onInvalidName=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(CALL_LIST_SECTIONS,generateCookie(),callback={
			0:onSuccess,
			1:onAlreadyExists,
			2:onProviderIsReadOnly,
			3:onInvalidName,
		})
		
	def makeIndexSupersection(self,indexName,supersectionName
		,onSuccess=None,onAlreadyExists=None,onIndexIsReadOnly=None,onNoSuchIndex=None
		,onInvalidSupersectionName=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(CALL_LIST_SECTIONS,generateCookie(),callback={
			0:onSuccess,
			1:onAlreadyExists,
			2:onIndexIsReadOnly,
			3:onNoSuchIndex,
			4:onInvalidSupersectionName,
		})
		
	def removeIndex(self,name,onSuccess=None,onIndexDoesNotExist=None,onIndexIsReadOnly=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(CALL_LIST_SECTIONS,generateCookie(),callback={
			0:onSuccess,
			1:onIndexDoesNotExist,
			2:onIndexIsReadOnly,
		})
		
	def removeIndexSupersection(self,indexName,supersectionName
		,onSuccess=None,onSupersectionDoesNotExist=None,onIndexIsReadOnly=None,onNoSuchIndex=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(CALL_LIST_SECTIONS,generateCookie(),callback={
			0:onSuccess,
			1:onSupersectionDoesNotExist,
			2:onIndexIsReadOnly,
			3:onNoSuchIndex,
		})
		
	def mapIndexSection(self,indexName,supersectionName,sectionName,destinationSectionName,applicationWritable
		,onSuccess=None,onIndexIsReadOnly=None,onNoSuchIndex=None,onNoSuchSupersection=None
		,onInvalidSectionName=None,onInvalidDestinationName=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(CALL_LIST_SECTIONS,generateCookie(),callback={
			0:onSuccess,
			1:onIndexIsReadOnly,
			2:onNoSuchIndex,
			3:onNoSuchSupersection,
			4:onInvalidSectionName,
			5:onInvalidDestinationName,
		})
		
	def mapIndexExternalSection(self,indexName,supersectionName,sectionName,provider,resource
		,onSuccess=None,onIndexIsReadOnly=None,onNoSuchIndex=None,onNoSuchSupersection=None
		,onInvalidSectionName=None,onInvalidProviderName=None,onInvalidResourceName=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(CALL_LIST_SECTIONS,generateCookie(),callback={
			0:onSuccess,
			1:onIndexIsReadOnly,
			2:onNoSuchIndex,
			3:onNoSuchSupersection,
			4:onInvalidSectionName,
			5:onInvalidProviderName,
			6:onInvalidResourceName,
		})
		
	def unmapIndexSection(self,indexName,supersectionName,sectionName
		,onSuccess=None,onIndexIsReadOnly=None,onNoSuchIndex=None,onNoSuchSupersection=None
		,onNoSuchSection=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(CALL_LIST_SECTIONS,generateCookie(),callback={
			0:onSuccess,
			1:onIndexIsReadOnly,
			2:onNoSuchIndex,
			3:onNoSuchSupersection,
			4:onNoSuchSection,
		})
		
	def setIndexEnabled(self,name,enabled,onSuccess=None,onNoSuchIndex=None,onIndexIsReadOnly=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(CALL_LIST_SECTIONS,generateCookie(),callback={
			0:onSuccess,
			1:onNoSuchIndex,
			2:onIndexIsReadOnly,
		})
		
	def setDefaultSupersection(self,indexName,supersectionName
		,onSuccess=None,onNoSuchIndex=None,onIndexIsReadOnly=None,onInvalidSupersectionName=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(CALL_LIST_SECTIONS,generateCookie(),callback={
			0:onSuccess,
			1:onNoSuchIndex,
			2:onIndexIsReadOnly,
			3:onInvalidSupersectionName,
		})
		
	#provider calls
	
	def getProviderInfo(self,onSuccess=None,onBackendNotConnected=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(CALL_LIST_SECTIONS,generateCookie(),callback={
			0:onSuccess,
			1:onBackendNotConnected,
		})
		
	def renameProvider(self,newName,onSuccess=None,onProviderCanotBeRenamed=None,onInvalidName=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(CALL_LIST_SECTIONS,generateCookie(),callback={
			0:onSuccess,
			1:onProviderCanotBeRenamed,
			2:onInvalidName,
		})
