from modserv.modclient2 import Port
from modserv.cookieGenerator import generateCookie

CALL_READ = 0
CALL_WRITE = 1
CALL_APPEND = 2
CALL_DELETE = 3
CALL_LIST = 4
CALL_GETSIZE = 5

class VsfsClient(Port):
	def __init__(self):
		self.calls = []
		self.maxconnections = 1
		self.protocol = "vsfs"
		self.inSide = "c"
		self.outSide = "s"
		self.connectionId = None
		
	def isConnected(self):
		return not (self.connectionId is None)
		
	def _onConnectionOpened(self,connectionId):
		self.connectionId = connectionId
		
	def _onConnectionClosed(self,connectionId):
		self.connectionId = None
		
	def read(self,name,onSuccess=None,onDoesNotExist=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(self.connectionId,CALL_READ,generateCookie(),name,callback={
			0:onSuccess,
			1:onDoesNotExist
			})
	
	def write(self,name,data,onSuccess=None,onInvalidName=None,onReadOnly=None,onNotEnoughSpace=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(self.connectionId,CALL_WRITE,generateCookie(),name,data,callback={
			0:onSuccess,
			1:onInvalidName,
			2:onReadOnly,
			3:onNotEnoughSpace
			})
				
	def append(self,name,data,onSuccess=None,onInvalidName=None,onReadOnly=None,onNotEnoughSpace=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(self.connectionId,CALL_APPEND,generateCookie(),name,data,callback={
			0:onSuccess,
			1:onInvalidName,
			2:onReadOnly,
			3:onNotEnoughSpace
			})
			
	def delete(self,name,onSuccess=None,onDoesNotExist=None,onReadOnly=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(self.connectionId,CALL_DELETE,generateCookie(),name,callback={
			0:onSuccess,
			1:onDoesNotExist,
			2:onReadOnly,
			})
			
	def list(self,name,onSuccess=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(self.connectionId,CALL_LIST,generateCookie(),name,callback=onSuccess)
				
	def getSize(self,name,onSuccess=None,onDoesNotExist=None,onNotConnected=None):
		if not self.isConnected():
			if callable(onNotConnected):
				onNotConnected()
			return
		self._call(self.connectionId,CALL_GETSIZE,generateCookie(),name,callback={
			0:onSuccess,
			1:onDoesNotExist,
			})
