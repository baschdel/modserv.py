from modserv.modclient2 import Interface
from modclient2.port.ifrequest_client import IfRequestClient

class IfRequestClientInterface(Interface):
	def __init__(self):
		Interface.__init__(Interface)
		
		self.ifrequestPort = IfRequestClient()
		
		self.setPort(9,self.ifrequestPort)
		
		self.isConnected = self.ifrequestPort.isConnected
		self.list = self.ifrequestPort.list
		self.request = self.ifrequestPort.request
		
	def onTypesUpdated(self,callback):
		self.ifrequestPort.on("typesUpdated",callback)
		
	def ignoreTypesUpdated(self,callback):
		self.ifrequestPort.ignore("typesUpdated",callback)
