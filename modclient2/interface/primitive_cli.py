from modserv.modclient2 import Interface
from modclient2.port.primitive_cli import PrimitiveCli

class PrimitiveCliInterface(Interface):
	def __init__(self,onMessage=None,onConnectionOpened=None,onConnectionClosed=None):
		if not (onMessage is None):
			self.onMessage = onMessage
		if not (onConnectionOpened is None):
			self._onConnectionOpened = onConnectionOpened
		if not (onConnectionClosed is None):
			self._onConnectionClosed = onConnectionClosed
		self.opened = False
		self.cliPort = PrimitiveCli(onMessage=self.onMessage)
		self.setPort(0,self.cliPort)
		
		self.send = self.cliPort.send
		self.broadcast = self.cliPort.broadcast
		
	def onMessage(self,context,message):
		pass
		
