from modserv.modclient2 import Interface
from modclient2.port.ifrequest_server import IfRequestServer

class IfRequestServerInterface(Interface):
	def __init__(self):
		Interface.__init__(self)
		self.ifrequestPort = IfRequestServer()
		self.setPort(9,self.ifrequestPort)
		
		self.setInterfaceFactory = self.ifrequestPort.setInterfaceFactory
