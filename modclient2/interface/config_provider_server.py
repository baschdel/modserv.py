from modserv.modclient2 import Interface
from modclient2.port.config_provider_server import ConfigProvider

class ConfigProviderServerInterface(Interface):
	def __init__(self,provider):
		Interface.__init__(self)
		self.configProviderPort = ConfigProvider(provider)
		self.setPort(7,self.configProviderPort)
		
		self.backendChanged = self.configProviderPort.backendChanged
		self.nameUpdated = self.configProviderPort.nameUpdated
