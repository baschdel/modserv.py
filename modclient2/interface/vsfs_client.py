from modserv.modclient2 import Interface
from modclient2.port.vsfs_client import VsfsClient

class VsfsClientInterface(Interface):
	def __init__(self):
		Interface.__init__(self)
		self.vsfsPort = VsfsClient()
		self.setPort(9,self.vsfsPort)
		
		#make the vsfs functions easier to use
		self.read = self.vsfsPort.read
		self.write = self.vsfsPort.write
		self.append = self.vsfsPort.append
		self.delete = self.vsfsPort.delete
		self.list = self.vsfsPort.list
		self.getSize = self.vsfsPort.getSize
