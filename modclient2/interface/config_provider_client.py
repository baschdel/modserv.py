from modserv.modclient2 import Interface
from modclient2.port.config_provider_client import ConfigProviderClient

class ConfigProviderClientInterface(Interface):
	def __init__(self):
		Interface.__init__(Interface)
		self.configProviderPort = ConfigProvider(provider)
		self.setPort(7,self.configProviderPort)
		
		self.isConnected = self.configProviderPort.isConnected
		#section stuffs
		self.listSections = self.configProviderPort.listSections
		self.getSectionInfo = self.configProviderPort.getSectionInfo
		self.readSectionData = self.configProviderPort.readSectionData
		self.writeSectionData = self.configProviderPort.writeSectionData
		self.makeSection = self.configProviderPort.makeSection
		self.deleteSection = self.configProviderPort.deleteSection
		self.setSectionEnabled = self.configProviderPort.setSectionEnabled
		self.setSectionFormat = self.configProviderPort.setSectionFormat
		self.renameSection = self.configProviderPort.renameSection
		self.copySection = self.configProviderPort.copySection
		self.setSectionApplicationWritable = self.configProviderPort.setSectionApplicationWritable
		#index stuffs
		self.listIndices = self.configProviderPort.listIndices
		self.getIndexInfo = self.configProviderPort.getIndexInfo
		self.resolveSectionName = self.configProviderPort.resolveSectionName
		self.listIndexSupersections = self.configProviderPort.listIndexSupersections
		self.listIndexSections = self.configProviderPort.listIndexSections
		self.makeIndex = self.configProviderPort.makeIndex
		self.makeIndexSupersection = self.configProviderPort.makeIndexSupersection
		self.removeIndex = self.configProviderPort.removeIndex
		self.removeIndexSupersection = self.configProviderPort.removeIndexSupersection
		self.mapIndexSection = self.configProviderPort.mapIndexSection
		self.mapIndexExternalSection = self.configProviderPort.mapIndexExternalSection
		self.unmapIndexSection = self.configProviderPort.unmapIndexSection
		self.setIndexEnabled = self.configProviderPort.setIndexEnabled
		self.setDefaultSupersection = self.configProviderPort.setDefaultSupersection
		#provider stuffs
		self.getProviderInfo = self.configProviderPort.getProviderInfo
		self.renameProvider = self.configProviderPort.renameProvider
		
	def onBackendChanged(self,callback):
		self.configProviderPort.on("backendChanged",callback)
		
	def ignoreBackendChanged(self,callback):
		self.configProviderPort.ignore("backendChanged",callback)
		
	def onNameUpdated(self,callback):
		self.configProviderPort.on("nameUpdated",callback)
		
	def ignoreNameUpdated(self,callback):
		self.configProviderPort.ignore("nameUpdated",callback)
