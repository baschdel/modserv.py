from modserv.modclient2 import Interface
from modclient2.port.vsfs_server import VsfsServer

class VsfsServerInterface(Interface):
	def __init__(self,vsfsServerPort):
		Interface.__init__(self)
		self.vsfsPort = vsfsServerPort
		self.setPort(9,self.vsfsPort)
