from modserv.transcoders import Decoder,ModeDecoder
import modserv.decoders as dec

protocol = "vsfs"
side = "s"
version = 1

#call names
CALL_READ = 0
CALL_APPEND = 2
CALL_DELETE = 3
CALL_LIST = 4
CALL_GET_SIZE = 5
CALL_WRITE = 1

COOKIE_TEXT_DECODER = Decoder([dec.bin,dec.utf8])
COOKIE_TEXT_DATA_DECODER = Decoder([dec.bin,dec.utf8,dec.bin])

decoders = {
	CALL_DELETE: COOKIE_TEXT_DECODER,
	CALL_APPEND: COOKIE_TEXT_DATA_DECODER,
	CALL_LIST: COOKIE_TEXT_DECODER,
	CALL_READ: COOKIE_TEXT_DECODER,
	CALL_WRITE: COOKIE_TEXT_DATA_DECODER,
	CALL_GET_SIZE: COOKIE_TEXT_DECODER,
}

def hasMode(functionId):
	if functionId in decoders:
		return type(decoders[functionId]) is ModeDecoder
	else:
		return False
