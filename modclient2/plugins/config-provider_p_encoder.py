from modserv.transcoders import Encoder,ModeEncoder
import modserv.encoders as enc

protocol = "config-provider"
side = "p"
version = 1

#call names
CALL_LIST_SECTIONS = 1
CALL_GET_SECTION_INFO = 2
CALL_READ_SECTION_DATA = 3
CALL_WRITE_SECTION_DATA = 4
CALL_MAKE_SECTION = 5
CALL_DELETE_SECTION = 6
CALL_SET_SECTION_ENABLED = 7
CALL_SET_SECTION_FORMAT = 8
CALL_RENAME_SECTION = 9
CALL_COPY_SECTION = 10
CALL_SET_SECTION_APPLICATION_WRITABLE = 11
CALL_REMOVE_INDEX_SUPERSECTION = 108
CALL_MAP_INDEX_EXTERNAL_SECTION = 110
CALL_UNMAP_INDEX_SECTION = 111
CALL_SET_INDEX_ENABLED = 112
CALL_SET_DEFAULT_SUPERSECTION = 113
CALL_RENAME_PROVIDER = 301
CALL_GET_PROVIDER_INFO = 300
CALL_MAP_INDEX_SECTION = 109
CALL_REMOVE_INDEX = 107
CALL_MAKE_INDEX_SUPERSECTION = 106
CALL_MAKE_INDEX = 105
CALL_LIST_INDEX_SECTIONS = 104
CALL_LIST_INDEX_SUPERSECTIONS = 103
CALL_RESOLVE_SECTION_NAME = 102
CALL_GET_INDEX_INFO = 101
CALL_LIST_INDICES = 100

COOKIE_TEXT_DATA_ENCODER = Encoder([enc.bin,enc.utf8,enc.bin])
COOKIE_TEXT_TEXT_TEXT_TEXT_BOOL_ENCODER = Encoder([enc.bin,enc.utf8,enc.utf8,enc.utf8,enc.utf8,enc.bool])
COOKIE_TEXT_BOOL_ENCODER = Encoder([enc.bin,enc.utf8,enc.bool])
COOKIE_TEXT_ENCODER = Encoder([enc.bin,enc.utf8])
COOKIE_TEXT_TEXT_ENCODER = Encoder([enc.bin,enc.utf8,enc.utf8])
COOKIE_TEXT_TEXT_TEXT_TEXT_TEXT_ENCODER = Encoder([enc.bin,enc.utf8,enc.utf8,enc.utf8,enc.utf8,enc.utf8])
COOKIE_TEXT_TEXT_TEXT_ENCODER = Encoder([enc.bin,enc.utf8,enc.utf8,enc.utf8])
COOKIE_TEXT_TEXT_TEXT_BOOL_BOOL_ENCODER = Encoder([enc.bin,enc.utf8,enc.utf8,enc.utf8,enc.bool,enc.bool])
COOKIE_ENCODER = Encoder([enc.bin])

encoders = {
	CALL_SET_SECTION_FORMAT: COOKIE_TEXT_TEXT_ENCODER,
	CALL_LIST_INDICES: COOKIE_ENCODER,
	CALL_GET_INDEX_INFO: COOKIE_TEXT_ENCODER,
	CALL_UNMAP_INDEX_SECTION: COOKIE_TEXT_TEXT_TEXT_ENCODER,
	CALL_COPY_SECTION: COOKIE_TEXT_TEXT_ENCODER,
	CALL_GET_SECTION_INFO: COOKIE_TEXT_ENCODER,
	CALL_LIST_INDEX_SUPERSECTIONS: COOKIE_TEXT_ENCODER,
	CALL_MAKE_SECTION: COOKIE_TEXT_TEXT_TEXT_BOOL_BOOL_ENCODER,
	CALL_LIST_INDEX_SECTIONS: COOKIE_TEXT_TEXT_ENCODER,
	CALL_MAP_INDEX_EXTERNAL_SECTION: COOKIE_TEXT_TEXT_TEXT_TEXT_TEXT_ENCODER,
	CALL_MAKE_INDEX: COOKIE_TEXT_ENCODER,
	CALL_REMOVE_INDEX: COOKIE_TEXT_ENCODER,
	CALL_MAKE_INDEX_SUPERSECTION: COOKIE_TEXT_TEXT_ENCODER,
	CALL_SET_SECTION_ENABLED: COOKIE_TEXT_BOOL_ENCODER,
	CALL_LIST_SECTIONS: COOKIE_ENCODER,
	CALL_RENAME_PROVIDER: COOKIE_TEXT_ENCODER,
	CALL_WRITE_SECTION_DATA: COOKIE_TEXT_DATA_ENCODER,
	CALL_SET_SECTION_APPLICATION_WRITABLE: COOKIE_TEXT_BOOL_ENCODER,
	CALL_RENAME_SECTION: COOKIE_TEXT_TEXT_ENCODER,
	CALL_REMOVE_INDEX_SUPERSECTION: COOKIE_TEXT_TEXT_ENCODER,
	CALL_GET_PROVIDER_INFO: COOKIE_ENCODER,
	CALL_DELETE_SECTION: COOKIE_TEXT_ENCODER,
	CALL_MAP_INDEX_SECTION: COOKIE_TEXT_TEXT_TEXT_TEXT_BOOL_ENCODER,
	CALL_READ_SECTION_DATA: COOKIE_TEXT_ENCODER,
	CALL_SET_INDEX_ENABLED: COOKIE_TEXT_BOOL_ENCODER,
	CALL_RESOLVE_SECTION_NAME: COOKIE_TEXT_TEXT_TEXT_ENCODER,
	CALL_SET_DEFAULT_SUPERSECTION: COOKIE_TEXT_TEXT_ENCODER,
}

def hasMode(functionId):
	if functionId in encoders:
		return type(encoders[functionId]) is ModeEncoder
	else:
		return False
