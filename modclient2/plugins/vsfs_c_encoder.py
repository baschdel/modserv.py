from modserv.transcoders import Encoder,ModeEncoder
import modserv.encoders as enc

protocol = "vsfs"
side = "c"
version = 1

#call names
CALL_READ = 0
CALL_APPEND = 2
CALL_DELETE = 3
CALL_LIST = 4
CALL_GET_SIZE = 5
CALL_WRITE = 1

MODE_COOKIE_DATA_ENCODER = ModeEncoder([enc.u8,enc.bin,[enc.bin]])
COOKIE_TEXTDSLIST_ENCODER = Encoder([enc.bin,enc.dslist(enc.utf8)])
MODE_COOKIE_SIZE_ENCODER = ModeEncoder([enc.u8,enc.bin,enc.u32])
MODE_COOKIE_ENCODER = ModeEncoder([enc.u8,enc.bin])

encoders = {
	CALL_DELETE: MODE_COOKIE_ENCODER,
	CALL_APPEND: MODE_COOKIE_ENCODER,
	CALL_LIST: COOKIE_TEXTDSLIST_ENCODER,
	CALL_READ: MODE_COOKIE_DATA_ENCODER,
	CALL_WRITE: MODE_COOKIE_ENCODER,
	CALL_GET_SIZE: MODE_COOKIE_SIZE_ENCODER,
}

def hasMode(functionId):
	if functionId in encoders:
		return type(encoders[functionId]) is ModeEncoder
	else:
		return False
