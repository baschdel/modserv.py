from modserv.transcoders import Encoder,ModeEncoder
import modserv.encoders as enc

protocol = "vsfs"
side = "s"
version = 1

#call names
CALL_READ = 0
CALL_APPEND = 2
CALL_DELETE = 3
CALL_LIST = 4
CALL_GET_SIZE = 5
CALL_WRITE = 1

COOKIE_TEXT_ENCODER = Encoder([enc.bin,enc.utf8])
COOKIE_TEXT_DATA_ENCODER = Encoder([enc.bin,enc.utf8,enc.bin])

encoders = {
	CALL_DELETE: COOKIE_TEXT_ENCODER,
	CALL_APPEND: COOKIE_TEXT_DATA_ENCODER,
	CALL_LIST: COOKIE_TEXT_ENCODER,
	CALL_READ: COOKIE_TEXT_ENCODER,
	CALL_WRITE: COOKIE_TEXT_DATA_ENCODER,
	CALL_GET_SIZE: COOKIE_TEXT_ENCODER,
}

def hasMode(functionId):
	if functionId in encoders:
		return type(encoders[functionId]) is ModeEncoder
	else:
		return False
