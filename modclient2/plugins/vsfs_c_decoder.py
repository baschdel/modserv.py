from modserv.transcoders import Decoder,ModeDecoder
import modserv.decoders as dec

protocol = "vsfs"
side = "c"
version = 1

#call names
CALL_READ = 0
CALL_APPEND = 2
CALL_DELETE = 3
CALL_LIST = 4
CALL_GET_SIZE = 5
CALL_WRITE = 1

MODE_COOKIE_DATA_DECODER = ModeDecoder([dec.u8,dec.bin,[dec.bin]])
COOKIE_TEXTDSLIST_DECODER = Decoder([dec.bin,dec.dslist(dec.utf8)])
MODE_COOKIE_DECODER = ModeDecoder([dec.u8,dec.bin])
MODE_COOKIE_SIZE_DECODER = ModeDecoder([dec.u8,dec.bin,dec.u32])

decoders = {
	CALL_DELETE: MODE_COOKIE_DECODER,
	CALL_APPEND: MODE_COOKIE_DECODER,
	CALL_LIST: COOKIE_TEXTDSLIST_DECODER,
	CALL_READ: MODE_COOKIE_DATA_DECODER,
	CALL_WRITE: MODE_COOKIE_DECODER,
	CALL_GET_SIZE: MODE_COOKIE_SIZE_DECODER,
}

def hasMode(functionId):
	if functionId in decoders:
		return type(decoders[functionId]) is ModeDecoder
	else:
		return False
