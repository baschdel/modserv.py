from modserv.transcoders import Decoder,ModeDecoder
import modserv.decoders as dec

protocol = "primitive-cli"
side = "x"
version = 1

#call names
CALL_CALL = 0

CALL_DECODER = Decoder([dec.utf8])

decoders = {
	CALL_CALL: CALL_DECODER,
}

def hasMode(functionId):
	if functionId in decoders:
		return type(decoders[functionId]) is ModeDecoder
	else:
		return False
