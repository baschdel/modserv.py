from modserv.transcoders import Decoder,ModeDecoder
import modserv.decoders as dec

protocol = "ifrequest"
side = "p"
version = 1

#call names
CALL_REQUEST = 1
CALL_LIST = 0

COOKIE_TEXT_TEXTKVLIST_DECODER = Decoder([dec.bin,dec.utf8,dec.dslist(dec.dsentry(dec.utf8,dec.utf8))])
COOKIE_DECODER = Decoder([dec.bin])

decoders = {
	CALL_LIST: COOKIE_DECODER,
	CALL_REQUEST: COOKIE_TEXT_TEXTKVLIST_DECODER,
}

def hasMode(functionId):
	if functionId in decoders:
		return type(decoders[functionId]) is ModeDecoder
	else:
		return False
