from modserv.transcoders import Encoder,ModeEncoder
import modserv.encoders as enc

protocol = "primitive-cli"
side = "x"
version = 1

#call names
CALL_CALL = 0

CALL_ENCODER = Encoder([enc.utf8])

encoders = {
	CALL_CALL: CALL_ENCODER,
}

def hasMode(functionId):
	if functionId in encoders:
		return type(encoders[functionId]) is ModeEncoder
	else:
		return False
