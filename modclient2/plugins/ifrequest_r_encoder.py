from modserv.transcoders import Encoder,ModeEncoder
import modserv.encoders as enc

protocol = "ifrequest"
side = "r"
version = 2

#call names
CALL_LIST = 0
CALL_REQUEST = 1
CALL_NEW_TYPES = 2

NO_ARGS_ENCODER = Encoder([])
COOKIE_TEXTLIST_ENCODER = Encoder([enc.bin,enc.dslist(enc.utf8)])
MODE_COOKIE_MID_IFID_ENCODER = ModeEncoder([enc.u8,enc.bin,{0:enc.u64,2:enc.utf8},{0:enc.u32,2:enc.utf8}])

encoders = {
	CALL_REQUEST: MODE_COOKIE_MID_IFID_ENCODER,
	CALL_LIST: COOKIE_TEXTLIST_ENCODER,
	CALL_NEW_TYPES: NO_ARGS_ENCODER,
}

def hasMode(functionId):
	if functionId in encoders:
		return type(encoders[functionId]) is ModeEncoder
	else:
		return False
