from modserv.transcoders import Decoder,ModeDecoder
import modserv.decoders as dec

protocol = "ifrequest"
side = "r"
version = 2

#call names
CALL_LIST = 0
CALL_REQUEST = 1
CALL_NEW_TYPES = 2

MODE_COOKIE_MID_IFID_DECODER = ModeDecoder([dec.u8,dec.bin,{0:dec.u64,2:dec.utf8},{0:dec.u32,2:dec.utf8}])
NO_ARGS_DECODER = Decoder([])
COOKIE_TEXTLIST_DECODER = Decoder([dec.bin,dec.dslist(dec.utf8)])

decoders = {
	CALL_REQUEST: MODE_COOKIE_MID_IFID_DECODER,
	CALL_LIST: COOKIE_TEXTLIST_DECODER,
	CALL_NEW_TYPES: NO_ARGS_DECODER,
}

def hasMode(functionId):
	if functionId in decoders:
		return type(decoders[functionId]) is ModeDecoder
	else:
		return False
