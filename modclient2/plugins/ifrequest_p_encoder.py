from modserv.transcoders import Encoder,ModeEncoder
import modserv.encoders as enc

protocol = "ifrequest"
side = "p"
version = 1

#call names
CALL_REQUEST = 1
CALL_LIST = 0

COOKIE_ENCODER = Encoder([enc.bin])
COOKIE_TEXT_TEXTKVLIST_ENCODER = Encoder([enc.bin,enc.utf8,enc.dslist(enc.dsentry(enc.utf8,enc.utf8))])

encoders = {
	CALL_LIST: COOKIE_ENCODER,
	CALL_REQUEST: COOKIE_TEXT_TEXTKVLIST_ENCODER,
}

def hasMode(functionId):
	if functionId in encoders:
		return type(encoders[functionId]) is ModeEncoder
	else:
		return False
