import modserv.easyclient as easyclient
import modserv.easydriver as easydriver
from modserv.portsetup import Portsetup
from modserv.cronjob import Cronjob

client = easyclient.getEasyClient()
prompt = ">"
onInput = None
	
def printintf():
	print(client.interfaces,id(client.interfaces))
	
#Cronjob(printintf,0.1)
	
def out(message):
	print(message)

def mainloop():
	global onInput
	i = input(prompt)
	#print(onInput)
	if not onInput is None:
		onInput(i)
	else:
		print("--DISCONNECTED--")
	return True
		
		
class CliConnection(easyclient.AbstractConection):
	def onInit(self):
		global onInput
		onInput = self
		print("Ready!")
		#self("Hello World!")
		
	def __call__(self,input):
		self.call(0,0,[input.encode('utf-8')])
		
	def closed(self):
		global onInput
		onInput = None
		print("--DISCONNECTED--")
		
	def called(self,portId,functionId,arguments): 
		if portId==0 and functionId==0 and len(arguments)==1:
			out(arguments[0].decode('utf-8'))
			
cliInterface = easyclient.EasyInterface({0:Portsetup("primitive-cli","x",1,"x",1)},1,None,CliConnection)

client.openInterface(3,cliInterface)

#TODO implement without global variables using an event listener on the interface

easydriver.EasyDriverLooper(mainloop,client,daemon=True)			
client.initalized()
