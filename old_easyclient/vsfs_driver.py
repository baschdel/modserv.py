import modserv.easyclient as easyclient
from modserv.portsetup import Portsetup
import modserv.easycoder as easycoder
import modserv.encoders as enc
import modserv.decoders as dec
import os

directory = "./vsfs_files"

directory = os.path.normpath(directory)

class Return(Exception):
	def __init__(self,*arguments):
		Exception.__init__(self)
		self.arguments = arguments

def isSubpathOf(root,path):
	path = os.path.normpath(path)
	return path.startswith(root)

def sanitizePath(name):
	out = ""
	i = 0
	map = {'\\':"\\\\",'\0':"\\0",'/':"\\-"}
	while i < len(name):
		if name[i] in map:
			out=out+map[name[i]]
		else:
			out=out+name[i]
		i=i+1
	return out
	
def reverseSanitizePath(name):
	out = ""
	i = 0
	while i<len(name):
		if name[i] == "\\":
			i = i+1
			if len(name)<=i:
				return out
			map = {'0':"\0",'-':"/",'\\':"\\"}
			if name[i] in map:
				out = out+map[name[i]]
		else:
			out = out+name[i]
		i=i+1
	return out
	
client = easyclient.getEasyClient()
vsfsDecoder = None
vsfsEncoder = None

class VSFSConnection(easyclient.AbstractConection):
	#root is the path to the folder this module has access to without a / at the end
	def onInit(self,root):
		self.root = root
		
	def closed(self):
		pass
		
	def vsfsCallback(self,functionId,*arguments):
		self.call(9,functionId,arguments)	
	
	def called(self,portId,functionId,arguments):
		if portId == 9:
			try:
				vsfsDecoder(functionId,*arguments,obj=self)
			except Return as ret:
				vsfsEncoder(functionId,*(ret.arguments),obj=self)
		 		
	def read(self,cookie,name):
		name=self.root+"/"+sanitizePath(name)
		if not isSubpathOf(self.root,name):
			raise Return(1,cookie) #invalid Name
		if not os.path.exists(name):
			raise Return(1,cookie) #invalid Name
		data = b''
		try:
			f = open(name,"rb")
			data = f.read()
			f.close()
		except:
			raise Return(1,cookie) #invalid Name
		raise Return(0,cookie,data)
		
	def write(self,cookie,name,data):
		name=self.root+"/"+sanitizePath(name)
		if not isSubpathOf(self.root,name):
			raise Return(1,cookie) #invalid Name
		try:
			f = open(name,"wb")
			f.write(data)
			f.close()
		except:
			raise Return(2,cookie) #read Only
		raise Return(0,cookie) #success
		
	def append(self,cookie,name,data):
		name=self.root+"/"+sanitizePath(name)
		if not isSubpathOf(self.root,name):
			raise Return(1,cookie) #invalid Name
		try:
			f = open(name,"ab")
			f.write(data)
			f.close()
		except:
			raise Return(2,cookie) #read Only
		raise Return(0,cookie) #success
	
	def delete(self,cookie,name):
		name=self.root+"/"+sanitizePath(name)
		if not isSubpathOf(self.root,name):
			raise Return(1,cookie) #does not exist
		if not os.path.exists(name):
			raise Return(1,cookie) #does not exist
		try:
			os.remove(name)
		except OSError:
			raise Return(2,cookie) #read Only
		raise Return(0,cookie)
		
	def list(self,cookie,name):
		paths = os.listdir(self.root)
		ret = []
		for path in paths:
			path = reverseSanitizePath(path)
			if path.startswith(name):
				ret.append(path)
		raise Return(cookie,ret)
		
		
	def getSize(self,cookie,name):
		name=self.root+"/"+sanitizePath(name)
		if not isSubpathOf(self.root,name):
			raise Return(1,cookie) #does not exist
		try:
			raise Return(0,cookie,os.path.getsize(name))
		except OSError:
			raise Return(1,cookie) #does not exist

vsfsDecoder = easycoder.EasyDecoder([
	#decoders
	[dec.bin,dec.utf8],
	[dec.bin,dec.utf8,dec.bin],
	[dec.bin,dec.utf8,dec.bin],
	[dec.bin,dec.utf8],
	[dec.bin,dec.utf8],
	[dec.bin,dec.utf8]
	],[
	#callbacks
	VSFSConnection.read,
	VSFSConnection.write,
	VSFSConnection.append,
	VSFSConnection.delete,
	VSFSConnection.list,
	VSFSConnection.getSize
	])
	
vsfsEncoder = easycoder.EasyEncoder([
	[enc.u8,enc.bin,[enc.bin]],
	[enc.u8,enc.bin],
	[enc.u8,enc.bin],
	[enc.u8,enc.bin],
	[enc.bin,enc.dslist(enc.utf8)],
	[enc.u8,enc.bin,[enc.u32]]
	],VSFSConnection.vsfsCallback)

vsfsInterface = easyclient.EasyInterface({9:Portsetup("vsfs","s",1,"c",1)},-1,None,VSFSConnection,directory)

client.openInterface(9,vsfsInterface)			
client.initalized()
		
	
