import modserv.easyclient as easyclient
from modserv.portsetup import Portsetup
import modserv.encoders as enc
import modserv.decoders as dec
import modserv.easycoder as easycoder
from modserv.cookiejar import CookieJar

class VSFSClientConnection(easyclient.AbstractConection):
	def onInit(self,clientInterface,decoder):
		clientInterface.onConnect(self)
		self.clientInterface = clientInterface
		self.decoder = decoder
		
	def closed(self):
		self.clientInterface.onDisconnect()
		
	def called(self,portId,functionId,arguments):
		if portId == 9:
			self.decoder(functionId,*arguments)
			
class VSFSClientInterface:
	def __init__(self,client,interfaceId,onConnected=None,onDisconnected=None):
		self.client = client
		self.connection = None
		self.cookieJar = CookieJar()
		self.onConnected = onConnected
		self.onDisconnected = onDisconnected
		self.encoder = easycoder.EasyEncoder([
			[enc.bin,enc.utf8],
			[enc.bin,enc.utf8,enc.bin],
			[enc.bin,enc.utf8,enc.bin],
			[enc.bin,enc.utf8],
			[enc.bin,enc.utf8],
			[enc.bin,enc.utf8]
		],
		self.call)
		
		self.decoder = easycoder.EasyDecoder([
			[dec.u8,dec.bin,[dec.bin]],
			[dec.u8,dec.bin],
			[dec.u8,dec.bin],
			[dec.u8,dec.bin],
			[dec.bin,dec.dslist(dec.utf8)],
			[dec.u8,dec.bin,[dec.u32]],
		],[
			self.fireModeCookie,
			self.fireModeCookie,
			self.fireModeCookie,
			self.fireModeCookie,
			self.fireNormalCookie,
			self.fireModeCookie,
		])
		self.interface = easyclient.EasyInterface({9:Portsetup("vsfs","c",1,"s",1)},1,None,VSFSClientConnection,self,self.decoder)
		self.client.openInterface(interfaceId,self.interface)
	
	def fireNormalCookie(self,cookie,*arguments):
		self.cookieJar.fire(cookie,*arguments)
		
	def fireModeCookie(self,mode,cookie,*arguments):
		self.cookieJar.fire(cookie,mode,*arguments)
		
	def onConnect(self,connection):
		self.connection = connection
		if callable(self.onConnected):
				self.onConnected(self)
		
	def onDisconnect(self):
		self.connection = None
		if callable(self.onDisconnected):
			self.onDisconnected(self)
			
	def call(self,functionId,*arguments):
		print("calling",functionId,arguments)
		self.connection.call(9,functionId,arguments)
		
	def isConnected(self):
		return not (self.connection is None)
		
	def read(self,name,onSuccess=None,onDoesNotExist=None,onNotConnected=None,**context):
		if self.connection is None:
			if callable(onNotConnected):
				onNotConnected()
			return
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onDoesNotExist],**context)
		#print("read("+str(cookie)+","+name+")")
		self.encoder(0,cookie,name)
		
	def write(self,name,data,onSuccess=None,onInvalidName=None,onReadOnly=None,onNotEnoughSpace=None,onNotConnected=None,**context):
		if self.connection is None:
			if callable(onNotConnected):
				onNotConnected()
			return
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onInvalidName,onReadOnly,onNotEnoughSpace],**context)
		self.encoder(1,cookie,name,data)
		
	def append(self,name,data,onSuccess=None,onInvalidName=None,onReadOnly=None,onNotEnoughSpace=None,onNotConnected=None,**context):
		if self.connection is None:
			if callable(onNotConnected):
				onNotConnected()
			return
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onInvalidName,onReadOnly,onNotEnoughSpace],**context)
		self.encoder(2,cookie,name,data)
		
	def delete(self,name,onSuccess=None,onDoesNotExist=None,onReadOnly=None,onNotConnected=None,**context):
		if self.connection is None:
			if callable(onNotConnected):
				onNotConnected()
			return
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onDoesNotExist,onReadOnly],**context)
		self.encoder(3,cookie,name)
		
	def list(self,name,onSuccess=None,onNotConnected=None,**context):
		if self.connection is None:
			if callable(onNotConnected):
				onNotConnected()
			return
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),onSuccess,**context)
		self.encoder(4,cookie,name)
		
	def getSize(self,name,onSuccess=None,onDoesNotExist=None,onNotConnected=None,**context):
		if self.connection is None:
			if callable(onNotConnected):
				onNotConnected()
			return
		cookie = self.cookieJar.hook(self.cookieJar.getCookie(),[onSuccess,onDoesNotExist],**context)
		self.encoder(5,cookie,name)
