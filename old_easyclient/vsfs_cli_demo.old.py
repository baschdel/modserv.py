import modserv.easyclient as easyclient
from modserv.portsetup import Portsetup
from time import sleep
from threading import Semaphore

from componentlib.vsfsClient import VSFSClientInterface

class NotConnectedException(Exception):
	pass
	
#human readable size
def hrsize(size):
	unit = "B"
	if size >= 1024*1024:
		size = size/(1024*1024)
		unit = "MB"
	if size >= 1024:
		size = size/1024
		unit = "KB"
	return str(size)+unit
	
#exception is the exception class
class RaiseExceptionCallback:
	def __init__(self,exception,*arguments):
		self.arguments = arguments
		self.exception = exception
	
	def __call__(self,*x):
		raise self.exception(*arguments)

class InvokeCallback:
	def __init__(self,function,*arguments):
		self.arguments = arguments
		self.function = function
	
	def __call__(self,*x):
		self.function(*self.arguments)

class ListCallback:
	def __init__(self,cli):
		self.cli = cli
		
	def __call__(self,plist):
		out = ""
		for path in plist:
			out = out+"\n"+path
		self.cli.send(out[1:])

class SListCallback:
	def __init__(self,cli,easyclient,vsfs):
		self.cli = cli
		self.easyclient = easyclient
		self.vsfs = vsfs
		
	def __call__(self,plist):
		easyclient.AsyncCallback(self.asyncf)(plist)
		
	def asyncf(self,plist):
		self.sizes = {} #maps path to size
		self.tlen = len(plist) #target length
		self.semaphore = Semaphore(value=0)
		#collect sizes
		for path in plist:
			vsfs.getSize(path,onSuccess=self.gotSize, onDoesNotExist=self.failed, onNotConnected=self.failed,path=path)
		self.semaphore.acquire(timeout=3)
		#render and send output
		maxsw = 3
		for path in plist:
			if not path in self.sizes:
				self.sizes[path] = "TIMEOUT"
			sz = self.sizes[path]
			if len(sz)>maxsw:
				maxsw = len(sz)
		out = ""
		for path in plist:
			sz = self.sizes[path]
			out = out+"\n"+(" "*(maxsw-len(sz)))+sz+"|"+path
		self.cli.send(out[1:])
		
	def failed(self,path=None):
		self.sizes[path] = "N/A"
		if len(self.sizes) == self.tlen:
			self.semaphore.release()
		
	def gotSize(self,size,path=None):
		self.sizes[path] = hrsize(size)
		if len(self.sizes) == self.tlen:
			self.semaphore.release()
		
class SizeCallback:
	def __init__(self,cli,filename):
		self.cli = cli
		self.filename = filename
		
	def __call__(self,size):
		self.cli.send(self.filename+" : "+hrsize(size))

client = easyclient.getEasyClient()

vsfs = VSFSClientInterface(client,9)

class CliConnection(easyclient.AbstractConection):
	def onInit(self,vsfs):
		self.readMode = False
		self.readBuffer = b""
		self.command = None
		self.name = None
		self.vsfs = vsfs
		
	def send(self,msg):
		if type(msg) is str:
			msg = msg.encode('utf-8')
		self.call(0,0,[msg])
		
	def called(self,portId,functionId,arguments): 
		if portId==0 and functionId==0 and len(arguments)==1:
			if self.readMode:
				if arguments[0]==b'<EOF>':
					if self.command == "write": #write
				 		self.vsfs.write(self.name,self.readBuffer,
					  	onSuccess=InvokeCallback(self.send,"Written to file "+str(self.name)+" !"),
					  	onInvalidName=InvokeCallback(self.send,str(self.name)+"is an invalid name!"),
					  	onReadOnly=InvokeCallback(self.send,"File is read only!"),
					  	onNotEnoughSpace=InvokeCallback(self.send,"Not enough space!"),
					  	onNotConnected=InvokeCallback(self.send,"Not Connected!"))
					if self.command == "append": #append
						self.vsfs.append(self.name,self.readBuffer,
							onSuccess=InvokeCallback(self.send,"Appended to file "+str(self.name)+" !"),
							onInvalidName=InvokeCallback(self.send,str(self.name)+"is an invalid name!"),
							onReadOnly=InvokeCallback(self.send,"File is read only!"),
							onNotEnoughSpace=InvokeCallback(self.send,"Not enough space!"),
							onNotConnected=InvokeCallback(self.send,"Not Connected!"))
					self.readBuffer = b''
					self.readMode = False
				else:
					self.readBuffer = self.readBuffer+arguments[0]+b'\n'
			else:
				text = arguments[0].decode('utf-8')
				i = text.split(" ",2)
				if len(i)	== 2:
					self.command = i[0]
					self.name = i[1]
					if not self.command in ["read","write","append","list","size","delete"]:
						self.command = "help"
				elif len(i) == 1:
					self.command = i[0]
					self.name = ""
					if not self.command == "list":
						self.command == "help"
				else:
					self.command = "help"
					self.name = ""
				if self.command == "write" or self.command == "append":
					self.readMode = True
					self.send("Please send the new content and write <EOF> into the last line.")
				if self.command == "read":
					print("Reading file "+self.name)
					self.vsfs.read(self.name,
						onSuccess=easyclient.EasyCallback(self.send),
						onDoesNotExist=InvokeCallback(self.send,"File does not Exist!"),
						onNotConnected=InvokeCallback(self.send,"Not Connected!"))
				if self.command == "list":
					self.vsfs.list(self.name,
						#onSuccess=ListCallback(self),
						onSuccess=SListCallback(self,self.interface.easyclient,self.vsfs),
						onNotConnected=InvokeCallback(self.send,"Not Connected!"))
				if self.command == "size":
					self.vsfs.getSize(self.name,
						onSuccess=SizeCallback(self,self.name),
						onDoesNotExist=InvokeCallback(self.send,"File does not Exist!"),
						onNotConnected=InvokeCallback(self.send,"Not Connected!"))
				if self.command == "delete":
					self.vsfs.delete(self.name,
						onSuccess=InvokeCallback(self.send,"Deleted file "+str(self.name)+" !"),
						onDoesNotExist=InvokeCallback(self.send,"File does not Exist!"),
					 	onReadOnly=InvokeCallback(self.send,"File is read only!"),
				  	onNotConnected=InvokeCallback(self.send,"Not Connected!"))
				if self.command=="help":
					self.send("{read|write|append|delete|list|size} <name>")
cliInterface = easyclient.EasyInterface({0:Portsetup("primitive-cli","x",1,"x",1)},-1,None,CliConnection,vsfs)

client.openInterface(3,cliInterface)
