import modserv.modclient2 as modclient
import uuid
from modserv.asyncronus import TimeoutOrEarlier,Callbackcollector
from modserv.modclient2 import Context,ChainStarter
from modclient2.interface.config_provider_server import ConfigProviderServerInterface
from modclient2.interface.vsfs_client import VsfsClient

indexWriteLock = Lock() #acquire this lock to write to any index (acquire before reading)

vsfsInterfaceId = 9
configInterfaceId = 12
defautTimeout = 3

client = modclient.getModclient(debug=True)

providerConnecions = []
configProviderUuid = None
configProviderName = ""
configProviderDisabledByDefault = False
vsfsConnected = False

def parseIndex(index):
	defaultSupersection = ""
	flags=[]
	supersections={}
	currentSupersection = None
	linecounter = 0
	lines = index.split("\n")
	for line in lines:
		linecounter = linecounter+1
		#section start
		if line == "":
			pass
		elif (line[0] == "[") and (line[-1] == "]"):
			currentSupersection = line[1:-1]
			supersections[currentSupersection] = {}
		elif currentSupersection is None:
			if linecounter == 1:
				defaultSupersection = line
			if linecounter == 2:
				flags = line.split(" ")
		else :
			tokens = line.split("\t")
			if len(tokens) >= 3 and tokens[0]=="int":
				applicationWritable = False
				if len(tokens) >= 4 and tokens[3]=="[aw]":
					applicationWritable = True
				supersections[currentSupersection][tokens[1]] = [tokens[2],applicationWritable]
			if len(tokens) >= 4 and tokens[0]=="ext":
				supersections[currentSupersection][tokens[1]] = (tokens[2],tokens[3])
	return (defaultSupersection,flags,supersections)

def makeIndex(defaultSupersection,flags,supersections):
	out = defaultSupersection+"\n"
	firstflag = True
	for flag in flags:
		if firstflag:
			firstflag = False
			out=out+flag
		else:
			out = out+" "+flag
	out = out+"\n"
	for supersection,sections in supersections.items():
		out = out+"["+supersection+"]\n"
		for section,content in sections.items():
			if type(content) is tuple:
				out=out+"int\t"+section+"\t"+content[0]+"\t"+content[1]+"\n"
			if type(content) is list:
				out=out+"int\t"+section+"\t"+content[0]
				if content[1]:
					out=out+"\t[aw]"
				out=out+"\n"
	return out
	
def isNameOk(name):
	return not (("\n" in name) or ("\0" in name) or ("\t" in name))


def doesSectionExist(section,names):
	exists =           "section."+section+".data" in names
	exists = exists or "section."+section+".type" in names
	exists = exists or "section."+section+".format" in names
	return exists

def onVsfsConnect(connectionId,interfaceId=None):
	vsfs.read("config-uuid",onSuccess=uuidRead,onDoesNotExist=uuidNotPresent)
	vsfs.read("config-name",onSuccess=nameRead)
	configProviderDisabledByDefault = False
	vsfs.read("config-disabled",onSuccess=disabledByDefautPresent)
	vsfsConnected = True
	onBackendChanged()
	
def uuidRead(callcontext,data):
	global configProviderUuid
	configProviderUuid = data.decode('utf-8')
	
def uuidNotPresent(callcontext):
	global configProviderUuid
	configProviderUuid = str(uuid.uuid4())
	vsfs.write("config-uuid",configProviderUuid.encode('utf-8'))
	
def nameRead(callcontext,data):
	global configProviderName
	configProviderName = data.decode('utf-8')
	
def disabledByDefautPresent(callcontext,data):
	global configProviderDisabledByDefault
	configProviderDisabledByDefault = True
	
def onVsfsDisconnect(connectionId,interfaceId=None):
	global configProviderUuid
	configProviderUuid = None
	global configProviderName
	configProviderName = ""
	vsfsConnected = False
	onBackendChanged()

def onBackendChanged():
	provider.backendChanged()

class ListSectionsChain:
	def __init__(self,vsfs):
		self.vsfs = vsfs
	
	def __call__(self,callcontext):
			self.callcontext = callcontext
			self.vsfs.list("section.",
				onSuccess=self.listReturn,
				onNotConnected=self.notConnected)
			
	def listReturn(self,callcontext,names):
		sections = []
		for name in names:
			sections.append(name[8:]) #remove the section. from the names
		self.callcontext.returnSectionList(sections)
	
	def notConnected(self,*args)
		self.callcontext.backendNotConnected()

class GetSectionInfoChain:
	def __init__(self,vsfs):
		self.vsfs = vsfs
		self.readonly = False
		
	def __call__(self,callcontext,sectionName):
		self.callcontext = callcontext
		self.collector = Callbackcollector("type","disabled","appWriteable","dataSize").timeout(defautTimeout)
		self.callcollector.on("success",self.onSuccess)
		self.callcollector.on("timeout",self.callcontext.noSuchSection)
		self.callcollector.on("failure",self.callcontext.noSuchSection)
		self.vsfs.read("section."+sectionName+".type",
			onSuccess=self.collector.collector({1:"type"}),
			onDoesNotExist=self.collector.putOnCall({'type':None},fail=True),
			onNotConnected=self.collector.putOnCall({'type':None},fail=True))
		self.vsfs.read("section."+sectionName+".disabled",
			onSuccess=self.collector.putOnCall({'disabled':True}),
			onDoesNotExist=self.collector.putOnCall({'disabled':False}),
			onNotConnected=self.collector.putOnCall({'disabled':None},fail=True))
		self.vsfs.read("section."+sectionName+".appWriteable",
			onSuccess=self.collector.putOnCall({'appWriteable':True}),
			onDoesNotExist=self.collector.putOnCall({'appWriteable':False}),
			onNotConnected=self.collector.putOnCall({'appWriteable':None},fail=True))
		self.vsfs.getSize("section."+sectionName+".data",
			onSuccess=self.self.collector.collector({1:"dataSize"}),
			onDoesNotExist=self.collector.putOnCall({'dataSize':None},fail=True),
			onNotConnected=self.collector.putOnCall({'dataSize':None},fail=True))
	
	def onSuccess():
		self.callcontext.returnSectionInfo(
				self.collector.get("type").decode("utf-8"),
				self.collector.get("dataSize"),
				not self.collector.get("disabled"),
				self.readonly,
				self.collector.get("appWriteable"))

class ReadSectionDataChain:
	def __init__(self,vsfs):
		self.vsfs = vsfs
		
	def __call__(self,callcontext,sectionName):
		self.callcontext = callcontext
		vsfs.read("section."+sectionName+".data",
			onSuccess=self.dataRead,
			onDoesNotExist=self.failed,
			onNotConnected=self.failed)
	
	def dataRead(self,callcontext,data):
		self.callcontext.returnSectionData(data)
		
	def failed(self,*args):
		self.callcontext.noSuchSection()

class WriteSectionDataChain:
	def __init__(self,vsfs):
		self.vsfs = vsfs
		
	def __call__(self,callcontext,sectionName,data):
		self.callcontext = callcontext
		self.name = "section."+sectionName+".data"
		self.data = data
		self.vsfs.list(name,
			onSuccess=self.listReturn,
			onNotConnected=self.notPresent)
		
	def listReturn(self,callcontext,names):
		if not sectionName in names:
			self.notPresent()
		else:
			self.vsfs.write(self.name,self.data,
				onSuccess=self.success,
				onReadOnly=self.readOnly,
				onInvalidName=self.notPresent,
				onNotEnoughSpace=self.readOnly,
				onNotConnected=self.notPresent)
		
	def success(self,*args):
		self.callcontext.returnSectionDataWritten()
		
	def notPresent(self,*args):
		self.callcontext.noSuchSection()
		
	def readOnly(self,*args):
		self.callcontext.sectionIsReadOnly()
	
class MakeSectionChain:
	def __init__(self,vsfs):
		self.vsfs = vsfs
	
	def __call__(self,callcontext,sectionName,rawType,dataFormat,enabled,applicationWritable):
		self.callcontext = callcontext
		self.sectionName = sectionName
		self.rawType = rawType
		self.dataFormat = dataFormat
		self.enabled = enabled
		self.applicationWritable = applicationWritable
		if not isNameOk(sectionName):
			self.callcontext.invalidName()
			return
		if not isNameOk(rawType):
			self.callcontext.invalidType()
			return
		self.vsfs.list("section."+sectionName,
			onSuccess=self.listReturn,
			onNotConnected=self.readOnly)
			
	def listReturn(self,callcontext,names):
		if "section."+self.sectionName+".data" in names and "section."+self.sectionName+".type" in names and "section."+self.sectionName+".format" in names:
			self.callcontext.sectionAlreadyExists()
		else:
			self.collector = Callbackcollector("data","type","format","disabled","appWriteable").timeout(defautTimeout)
			self.collector.on("timeout",self.callcontext.providerIsReadOnly)
			self.collector.on("success",self.callcontext.returnSectionCreated)
			self.collector.on("failure",self.onFailure)
			self.vsfs.write("section."+sectionName+".data",b'',
				onSuccess=self.collector.putOnCall({'data':True}),
				onInvalidName=self.collector.putOnCall({'data':False,'invalidName':True},fail=True),
				onReadOnly=self.collector.putOnCall({'data':False},fail=True),
				onNotEnoughSpace=self.collector.putOnCall({'data':False},fail=True),
				onNotConnected=self.collector.putOnCall({'data':False},fail=True))
			self.vsfs.write("section."self.+sectionName+".type",self.rawType.encode('utf-8'),
				onSuccess=self.collector.putOnCall({'type':True}),
				onInvalidName=self.collector.putOnCall({'type':False,'invalidName':True},fail=True),
				onReadOnly=self.collector.putOnCall({'type':False},fail=True),
				onNotEnoughSpace=self.collector.putOnCall({'type':False},fail=True),
				onNotConnected=self.collector.putOnCall({'type':False},fail=True))
			self.vsfs.write("section."self.+sectionName+".format",self.dataFormat.encode('utf-8'),
				onSuccess=self.collector.putOnCall({'format':True}),
				onInvalidName=self.collector.putOnCall({'format':False,'invalidName':True},fail=True),
				onReadOnly=self.collector.putOnCall({'format':False},fail=True),
				onNotEnoughSpace=self.collector.putOnCall({'format':False},fail=True),
				onNotConnected=self.collector.putOnCall({'format':False},fail=True))
			if not self.enabled:
				self.vsfs.write("section."+self.sectionName+".disabled",b'',
					onSuccess=self.collector.putOnCall({'disabled':True}),
					onInvalidName=self.collector.putOnCall({'disabled':False,'invalidName':True},fail=True),
					onReadOnly=self.collector.putOnCall({'disabled':False},fail=True),
					onNotEnoughSpace=self.collector.putOnCall({'disabled':False},fail=True),
					onNotConnected=self.collector.putOnCall({'disabled':False},fail=True))
			else:
				self.collector.put("disabled",True)
			if self.appWriteable:
				self.vsfs.write("section."+self.sectionName+".appWriteable",b'',
					onSuccess=self.collector.putOnCall({'appWriteable':True}),
					onInvalidName=self.collector.putOnCall({'appWriteable':False,'invalidName':True},fail=True),
					onReadOnly=self.collector.putOnCall({'appWriteable':False},fail=True),
					onNotEnoughSpace=self.collector.putOnCall({'appWriteable':False},fail=True),
					onNotConnected=self.collector.putOnCall({'appWriteable':False},fail=True))
			else:
				self.collector.put("appWriteable",True)
			
	def onFailure(self):
		if collector.get("invalidName"):
			self.callcontext.invalidName()
		else:
			self.callcontext.providerIsReadOnly()
	
class DeleteSectionChain:
	def __init__(self,vsfs):
		self.vsfs = vsfs
		
	def __call__(self,callcontext,sectionName):
		self.callcontext = callcontext
		self.collector = Callbackcollector("data","type","disabled","appWriteable").timeout(defautTimeout)
		collector.on("success",self.callcontext.returnSectionDeleted)
		collector.on("timeout",self.callcontext.sectionDoesNotExist)
		collector.on("failure",self.onFailure)
		vsfs.delete("section."+sectionName+".data",
			onSuccess=self.collector.putOnCall({'data':True}),
			onDoesNotExist=self.collector.putOnCall({'notDeleted':True,'data':False},fail=True),
			onReadOnly=self.collector.putOnCall({'readOnly':True,'data':False},fail=True),
			onNotConnected=self.collector.putOnCall({'notDeleted':True,'data':False},fail=True))
		vsfs.delete("section."+sectionName+".type",
			onSuccess=self.collector.putOnCall({'type':True}),
			onDoesNotExist=self.collector.putOnCall({'notDeleted':True,'type':False},fail=True),
			onReadOnly=self.collector.putOnCall({'readOnly':True,'type':False},fail=True),
			onNotConnected=self.collector.putOnCall({'notDeleted':True,'type':False},fail=True))
		vsfs.delete("section."+sectionName+".disabled",
			onSuccess=self.collector.putOnCall({'disabled':True}),
			onDoesNotExist=self.collector.putOnCall({'disabled':True}),
			onReadOnly=self.collector.putOnCall({'readOnly':True,'disabled':False},fail=True),
			onNotConnected=self.collector.putOnCall({'notDeleted':True,'disabled':False},fail=True))
		vsfs.delete("section."+sectionName+".appWriteable",
			onSuccess=self.collector.putOnCall({'appWriteable':True}),
			onDoesNotExist=self.collector.putOnCall({'appWriteable':True}),
			onReadOnly=self.collector.putOnCall({'readOnly':True,'appWriteable':False},fail=True),
			onNotConnected=self.collector.putOnCall({'notDeleted':True,'appWriteable':False},fail=True))
	
	def onFailure(self):
		if collector.get("notDeleted"):
			self.callcontext.sectionDoesNotExist()
		elif collector.get("readOnly"):
			self.callcontext.sectionIsReadOnly()
		else:
			self.callcontext.sectionDoesNotExist()

class SetSectionEnabledChain:
	def __init__(self,vsfs):
		self.vsfs = vsfs
		
	def __call__(self,callcontext,sectionName,enabled):
		self.callcontext = callcontext
		self.sectionName = sectionName
		self.enabled = enabled
		vsfs.list("section.",
			onSuccess=self.onListReturn,
			onNotConnected=self.doesNotExist)
			
	def onListReturn(self,callcontext,names):
		if doesSectionExist(self.sectionName,names):
			self.exists()
		else:
			self.doesNotExist()
		
	def exists():
		if self.enabled:
			self.vsfs.delete("section."+self.sectionName+".disabled",
				onSuccess=self.returned,
				onDoesNotExist=self.returned,
				onReadOnly=self.readOnly,
				onNotConnected=self.readOnly)
		else:
			self.vsfs.delete("section."+self.sectionName+".disabled",b'',
				onSuccess=self.returned,
				onInvalidName=self.readOnly,
				onReadOnly=self.readOnly,
				onNotEnoughSpace=self.readOnly,
				onNotConnected=self.readOnly)
			
	def returned(self,callcontext):
		self.callcontext.returnSectionEnableSet()
		
	def readOnly(self,*args):
		self.callcontext.flagIsReadOnly()
		
	def doesNotExist(self):
		self.callcontext.noSuchSection()

class SetSectionFormatChain:
	def __init__(self,vsfs):
		self.vsfs = vsfs
	
	def __call__(self,callcontext,sectionName,dataFormat):
		self.callcontext = callcontext
		self.sectionName = sectionName
		self.dataFormat = dataFormat
		vsfs.list("section.",
			onSuccess=self.onListReturn,
			onNotConnected=self.doesNotExist)
			
	def onListReturn(self,callcontext,names):
		if doesSectionExist(self.sectionName,names):
			self.exists()
		else:
			self.doesNotExist()
		
	def exists(self):
		self.vsfs.write("section"+self.sectionName+".format",self.dataFormat.encode('utf-8'),
			onSuccess=self.success,
			onInvalidName=self.readOnly,
			onReadOnly=self.readOnly,
			onNotEnoughSpace=self.readOnly,
			onNotConnected=self.doesNotExist)
		
	def success(self,callcontext):	
		self.callcontext.returnFormatSet()
		
	def doesNotExist(self,*args):
		self.callcontext.noSuchSection()
		
	def readOnly(self,callcontext):
		self.callcontext.sectionIsReadOnly()

class CopySectionChain:
	def __init__(self,vsfs,rename=False):
		self.vsfs = vsfs
		self.rename = rename #turns this into a move/rename Chain
	
	def __call__(self,callcontext,sectionName,newName):
		self.callcontext = callcontext
		self.sectionName = sectionName
		self.newName = newName
		if not isNameOk(newName):
			callcontext.invalidName()
			return
		self.vsfs.list("section.",onSuccess=self.listReturn,onNotConnected=self.readOnly)
		
	def listReturn(self,callcontext,names):
		if doesSectionExist(self.sectionName,names):
			if doesSectionExist(self.newName,names):
				self.callcontext.sectionAlreadyExists()
			else:
				#everything ok, read data, test write capability and reserve space
				self.collector = Callbackcollector("data","type","format","disabled","appWriteable").timeout(defautTimeout)
				self.collector.on("success",self.writeAll)
				self.collector.on("failure",self.cancel)
				self.collector.on("failure",self.onStage1Failure)
				self.collector.on("timeout",self.cancel)
				self.collector.on("timeout",self.callcontext.noSuchSection)
				self.vsfs.read("section."+self.sectionName+".data",
					onSuccess=self.collector.collector({1:"data"}),
					onDoesNotExist=self.collector.putOnCall({'data':None,'notRead':True},fail=True),
					onNotConnected=self.collector.putOnCall({'data':None,'notRead':True},fail=True))
				self.vsfs.read("section."+self.sectionName+".type",
					onSuccess=self.collector.collector({1:"type"}),
					onDoesNotExist=self.collector.putOnCall({'type':None,'notRead':True},fail=True),
					onNotConnected=self.collector.putOnCall({'type':None,'notRead':True},fail=True))
				self.vsfs.read("section."+self.sectionName+".format",
					onSuccess=self.collector.collector({1:"format"}),
					onDoesNotExist=self.collector.putOnCall({'format':None,'notRead':True},fail=True),
					onNotConnected=self.collector.putOnCall({'format':None,'notRead':True},fail=True))
				self.vsfs.read("section."+self.sectionName+".disabled",
					onSuccess=self.collector.putOnCall({'disabled':True}),
					onDoesNotExist=self.collector.putOnCall({'disabled':False}),
					onNotConnected=self.collector.putOnCall({'disabled':None,'notRead':True},fail=True))
				self.vsfs.read("section."+self.sectionName+".appWriteable",
					onSuccess=self.collector.putOnCall({'appWriteable':True}),
					onDoesNotExist=self.collector.putOnCall({'appWriteable':False}),
					onNotConnected=self.collector.putOnCall({'appWriteable':None,'notRead':True},fail=True))
				self.vsfs.write("section."+self.newName+".data",b'',
					onInvalidName=self.collector.putOnCall({'invalidName':True},fail=True),
					onReadOnly=self.collector.putOnCall({'readOnly':True},fail=True),
					onNotEnoughSpace=self.collector.putOnCall({'notEnoughSpace':True},fail=True),
					onNotConnected=self.collector.putOnCall({'notConnected':True},fail=True))
		else:
			self.callcontext.noSuchSection()
			
	def cancel(self):
		self.vsfs.delete("section."+self.newName+".data")
		self.vsfs.delete("section."+self.newName+".type")
		self.vsfs.delete("section."+self.newName+".format")
		self.vsfs.delete("section."+self.newName+".disabled")
		self.vsfs.delete("section."+self.newName+".appWriteable")
			
	def onStage1Failure(self):
		if self.collector.get("notRead"):
			self.callcontext.noSuchSection()
		elif self.collector.get("invalidName"):
			self.callcontext.invalidName()
		else:
			self.callcontext.providerIsReadOnly()
			
	def writeAll(self):
		self.writeCollector("data","type","format","disabled","appWriteable").timeout(defautTimeout)
		if self.rename:
			self.writeCollector.on("success",self.deleteAllOld)
			self.writeCollector.on("success",self.callcontext.returnSectionRenamed)
		else:
			self.writeCollector.on("success",self.callcontext.returnSectionCreated)
		self.writeCollector.on("failure",self.cancel)
		self.writeCollector.on("failure",self.onStage2Failure)
		self.writeCollector.on("timeout",self.cancel)
		self.writeCollector.on("timeout",self.callcontext.providerIsReadOnly)
		self.vsfs.write("section."+self.newName+".data",self.collector.get("data"),
			onSuccess=self.writeCollector.putOnCall({'data':True}),
			onInvalidName=self.writeCollector.putOnCall({'data':False,'invalidName':True},fail=True),
			onReadOnly=self.writeCollector.putOnCall({'data':False},fail=True),
			onNotEnoughSpace=self.writeCollector.putOnCall({'data':False},fail=True),
			onNotConnected=self.writeCollector.putOnCall({'data':False},fail=True))
		self.vsfs.write("section."+self.newName+".type",self.collector.get("type"),
			onSuccess=self.writeCollector.putOnCall({'type':True}),
			onInvalidName=self.writeCollector.putOnCall({'type':False,'invalidName':True},fail=True),
			onReadOnly=self.writeCollector.putOnCall({'type':False},fail=True),
			onNotEnoughSpace=self.writeCollector.putOnCall({'type':False},fail=True),
			onNotConnected=self.writeCollector.putOnCall({'type':False},fail=True))
		self.vsfs.write("section."+self.newName+".format",self.collector.get("format"),
			onSuccess=self.writeCollector.putOnCall({'format':True}),
			onInvalidName=self.writeCollector.putOnCall({'format':False,'invalidName':True},fail=True),
			onReadOnly=self.writeCollector.putOnCall({'format':False},fail=True),
			onNotEnoughSpace=self.writeCollector.putOnCall({'format':False},fail=True),
			onNotConnected=self.writeCollector.putOnCall({'format':False},fail=True))
	
	def onStage2Failure(self):
		if self.writeCollector.get("invalidName"):
			self.callcontext.invalidName
		else:
			self.callcontext.providerIsReadOnly
			
	def deleteAllOld(self):
		self.vsfs.delete("section."+self.sectionName+".data")
		self.vsfs.delete("section."+self.sectionName+".type")
		self.vsfs.delete("section."+self.sectionName+".format")
		self.vsfs.delete("section."+self.sectionName+".disabled")
		self.vsfs.delete("section."+self.sectionName+".appWriteable")
	
class SetSectionApplicationWritableChain:
	def __init__(self,vsfs):
		self.vsfs = vsfs
		
	def __call__(self,callcontext,sectionName,appWriteable):
		self.callcontext = callcontext
		self.sectionName = sectionName
		self.appWriteable = appWriteable
		vsfs.list("section.",
			onSuccess=self.onListReturn,
			onNotConnected=self.doesNotExist)
			
	def onListReturn(self,callcontext,names):
		if doesSectionExist(self.sectionName,names):
			self.exists()
		else:
			self.doesNotExist()
		
	def exists(self):
		self.vsfs.append("section."+self.sectionName+".data",b'',
			onSuccess=self.appendable,
			onInvalidName=self.doesNotExist,
			onReadOnly=self.readOnly,
			onNotEnoughSpace=self.readOnly,
			onNotConnected=self.doesNotExist)
			
	def appendable(self,callcontext):
		if self.appWriteable:
			self.vsfs.write("section."+self.sectionName+".appWriteable",b'',
				onSuccess=self.returned,
				onInvalidName=self.readOnly,
				onReadOnly=self.readOnly,
				onNotEnoughSpace=self.readOnly,
				onNotConnected=self.readOnly)
		else:
			self.vsfs.delete("section."+self.sectionName+".appWriteable",
				onSuccess=self.returned,
				onDoesNotExist=self.returned,
				onReadOnly=self.readOnly,
				onNotConnected=self.readOnly)
	
	def returned(self,callcontext):
		self.callcontext.returnApplicationWritableSet()
		
	def readOnly(self,*args):
		self.callcontext.sectionIsReadOnly()
		
	def doesNotExist(self):
		self.callcontext.noSuchSection()

class ListIndicesChain:
	def __init__(self,vsfs):
		self.vsfs = vsfs
		
	def __call__(self,callcontext):
		self.callcontext = callcontext
		self.vsfs.list("index.",
			onSuccess=self.onSuccess,
			onNotConnected=self.callcontext.backendNotConnected)
	
	def onSuccess(self,callcontext,names):
		sections = []
		for name in names:
			sections.append(name[6:]) #remove the "index." thingy
		self.callcontext.returnIndexList(sections)

class GetIndexInfoChain:
	def __init__(self,vsfs):
		self.vsfs = vsfs
		
	def __call__(self,callcontext,indexName):
		self.callcontext = callcontext
		self.collector = Callbackcollector("index","writable").timeout(defautTimeout)
		self.collector.on("success",self.onSuccess)
		self.collector.on("failure",self.onFailure)
		self.collector.on("timeout",self.onFailure)
		self.indexName = indexName
		if not isNameOk(indexName):
			self.callcontext.noSuchIndex()
			return
		vsfs.read("index."+indexName,
			onSuccess=self.collector.collector({1:"index"}),
			onDoesNotExist=self.collector.putOnCall({'index':None},fail=True),
			onNotConnected=self.collector.putOnCall({'index':None},fail=True))
		vsfs.append("index."+indexName,b'',
			onSuccess=self.collector.putOnCall({'writable':True}),
			onReadOnly=self.collector.putOnCall({'writable':False}),
			onInvalidName=self.collector.putOnCall({'writable':None},fail=True),
			onNotEnoughSpace=self.collector.putOnCall({'writable':None},fail=True),
			onNotConnected=self.collector.putOnCall({'writable':None},fail=True))
	
	def onSuccess(self):
		lines = self.collector.get("index").decode('utf-8').split("\n")
		defaultSupersection = ""
		enabled = True
		readOnly = not self.collector.get("writable")
		if len(lines) > 0:
			defaultSupersection = lines[0]
		if len(lines) > 1:
			flags = lines[1].split(" ")
			for flag in flags:
				if flag == "disabled":
					enabled = False
				if flag == "readonly":
					readOnly = True
		self.callcontext.returnIndexInfo(defaultSupersection,enabled,readOnly)
	
	def onFailure(self):
		self.callcontext.noSuchIndex()
		
class ResolveSectionNameChain:
	def __init__(self,vsfs):
		self.vsfs = vsfs
		
	def __call__(self,callcontext,indexName,superSectionName,sectionName):
		self.callcontext = callcontext
		self.superSectionName = superSectionName
		self.sectionName = sectionName
		if not isNameOk(indexName):
			self.callcontext.noSuchIndex()
			return
		if not isNameOk(superSectionName):
			self.callcontext.noSuchSupersection()
			return
		if not isNameOk(sectionName):
			self.callcontext.noSuchSection()
			return
		vsfs.read("index."+indexName,
			onSuccess=self.onRead,
			onDoesNotExist=self.callcontext.noSuchIndex,
			onNotConnected=self.callcontext.noSuchIndex)
			
	def onRead(self,callcontext,data):
		defaultSupersection,flags,supersections = parseIndex(data.decode("utf-8"))
		if self.superSectionName in supersections:
			supersection = supersections[self.superSectionName]
			if self.sectionName in supersection:
				section = supersection[self.sectionName]
				if type(section) is list:
					#internal
					self.callcontext.section(section[0],section[1])
				elif type(section) is tuple:
					#external
					self.callcontext.external(section[0],section[1])
				else:
					#should nerver happen with this parser but ...
					self.callcontext.noSuchSection()
			else:
				self.callcontext.noSuchSection()
		else:
			self.callcontext.noSuchSupersection()

class ListIndexSupersectionsChain:
	def __init__(self,vsfs):
		self.vsfs = vsfs
		
	def __call__(self,callcontext,indexName):
		self.callcontext = callcontext
		if not isNameOk(indexName):
			self.callcontext.noSuchIndex()
			return
		vsfs.read("index."+indexName,
			onSuccess=self.onRead,
			onDoesNotExist=self.callcontext.noSuchIndex,
			onNotConnected=self.callcontext.noSuchIndex)
			
	def onRead(self,callcontext,data):
		defaultSupersection,flags,supersections = parseIndex(data.decode("utf-8"))
		ret = []
		for supersection,sections in supersections.items():
			ret.append(supersection)
		self.callcontext.returnSupersectionList(ret)
		
class ListIndexSectionsChain:
	def __init__(self,vsfs):
		self.vsfs = vsfs
		
	def __call__(self,callcontext,indexName,superSectionName):
		self.callcontext = callcontext
		self.superSectionName = superSectionName
		if not isNameOk(indexName):
			self.callcontext.noSuchIndex()
			return
		vsfs.read("index."+indexName,
			onSuccess=self.onRead,
			onDoesNotExist=self.callcontext.noSuchIndex,
			onNotConnected=self.callcontext.noSuchIndex)
			
	def onRead(self,callcontext,data):
		defaultSupersection,flags,supersections = parseIndex(data.decode("utf-8"))
		if self.superSectionName in supersections:
			sections = supersections[self.superSectionName]
			ret = []
			for section,entrys in sections.items():
				ret.append(section)
			self.callcontext.returnSectionList(ret)
		else:
			self.callcontext.noSuchSupersection()
		

class ConfigProvider:
	def __init__(self,vsfs):
		self.vsfs = vsfs
		
		#section call Chains
		self.listSections = ChainStarter(ListSectionsChain,vsfs)
		self.getSectionInfo = ChainStarter(GetSectionInfoChain,vsfs)
		self.readSectionData = ChainStarter(ReadSectionDataChain,vsfs)
		self.writeSectionData = ChainStarter(WriteSectionDataChain,vsfs)
		self.makeSection = ChainStarter(MakeSectionChain,vsfs)
		self.deleteSection = ChainStarter(DeleteSectionChain,vsfs)
		self.setSectionEnabled = ChainStarter(SetSectionEnabledChain,vsfs)
		self.setSectionFormat = ChainStarter(SetSectionFormatChain,vsfs)
		self.renameSection = ChainStarter(CopySectionChain,vsfs,rename=True)
		self.copySection = ChainStarter(CopySectionChain,vsfs)
		self.setSectionApplicationWritable = ChainStarter(SetSectionApplicationWritableChain,vsfs)
		#index calls
		self.listIndices = ChainStarter(ListIndicesChain,vsfs)
		self.getIndexInfo = ChainStarter(GetIndexInfoChain,vsfs)
		self.resolveSectionName = ChainStarter(ResolveSectionNameChain,vsfs)
		self.listIndexSupersections = ChainStarter(ListIndexSupersectionsChain,vsfs)
		self.listIndexSections = ChainStarter(ListIndexSectionsChain,vsfs)
		
	#section calls
	
	#def listSections(self,callcontext): CHAIN
	#def getSectionInfo(self,callcontext,name): CHAIN
	#def readSectionData(self,callcontext,name): CAHIN
	#def writeSectionData(self,callcontext,name,data): CHAIN
	#def makeSection(self,callcontext,name,rawType,dataFormat,size,enabled,readonly,applicationWritable): CHAIN
	#def deleteSection(self,callcontext,name): CHAIN
	#def setSectionEnabled(self,callcontext,name,enabled): CHAIN
	#def setSectionFormat(self,callcontext,name,dataFormat): CHAIN
	#def renameSection(self,callcontext,name,newName): CHAIN
	#def copySection(self,callcontext,name,toName): CHAIN
	#def setSectionApplicationWritable(self,callcontext,name,applicationWritable): CHAIN
		
	#index calls
	
	#def listIndices(self,callcontext): CHAIN
	#def getIndexInfo(self,callcontext,name): CHAIN
	#def resolveSectionName(self,callcontext,indexName,supersectionName,sectionName): CHAIN
	#def listIndexSupersections(self,callcontext,indexName): CHAIN
		
	def listIndexSections(self,callcontext,indexName,supersectionName):
		callcontext.noSuchIndex()
		
	def makeIndex(self,callcontext,name):
		callcontext.providerIsReadOnly()
		
	def makeIndexSupersection(self,callcontext,indexName,supersectionName):
		callcontext.noSuchIndex()
		
	def removeIndex(self,callcontext,name):
		callcontext.indexDoesNotExist()
	
	def removeIndexSupersection(self,callcontext,indexName,supersectionName):
		callcontext.supersectionDoesNotExist()
		
	def mapIndexSection(self,callcontext,indexName,supersectionName,sectionName,destinationSectionName,applicationWritable):
		callcontext.noSuchIndex()
		
	def mapIndexExternalSection(self,callcontext,indexName,supersectionName,sectionName,provider,resource):
		callcontext.noSuchIndex()
		
	def unmapIndexSection(self,callcontext,indexName,supersectionName,sectionName):
		callcontext.noSuchIndex()
			
	def setIndexEnabled(self,callcontext,name,enabled):
		callcontext.noSuchIndex()
		
	def setDefaultSupersection(self,callcontext,indexName,supersectionName):
		callcontext.noSuchIndex()
		
	#provider calls
	
	def getProviderInfo(self,callcontext):
		if configProviderUuid is None:
			callcontext.backendNotConnected()
		else
			callcontext.reuturnProviderInfo(configProviderUuid,configProviderName,False,False,0,0)
	
	def renameProvider(self,callcontext,newName):
		callcontext.providerCanNotBeRenamed()

@client.start	
def onStart(**context):
	global vsfs
	client.on("interface."+str(vsfsInterfaceId)+".newConnection",onVsfsConnect)
	client.on("interface."+str(vsfsInterfaceId)+".connectionClosed",onVsfsDisconnect)
	vsfs = VsfsClient().open(client,vsfsInterfaceId)
	
	global provider
	provider = ConfigProviderServerInterface(ConfigProvider(vsfs)).open(client,configInterfaceId)
