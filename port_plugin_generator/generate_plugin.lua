protocol = ...

if not protocol then
	print("Usage: generate <protocol>")
	return
end

descriptions = require(protocol..".generate")
transcoders = require(protocol..".transcoders")

for k,v in pairs(descriptions) do
	descriptions[k] = require(protocol.."."..v)
end

encoders = {
	bin="enc.bin",
	utf8="enc.utf8",
	["utf-8"]="enc.utf8",
	u8="enc.u8",
	u16="enc.u16",
	u32="enc.u32",
	u64="enc.u64",
	s8="enc.s8",
	s16="enc.s16",
	s32="enc.s32",
	s64="enc.s64",
	bool="enc.bool",
	dslist="enc.dslist",
	dsmap="enc.dsmap",
	dsentry="enc.dsentry",
	flvlist="enc.flvlist"
}

decoders = {
	bin="dec.bin",
	utf8="dec.utf8",
	["utf-8"]="dec.utf8",
	u8="dec.u8",
	u16="dec.u16",
	u32="dec.u32",
	u64="dec.u64",
	s8="dec.s8",
	s16="dec.s16",
	s32="dec.s32",
	s64="dec.s64",
	bool="dec.bool",
	dslist="dec.dslist",
	dsmap="dec.dsmap",
	dsentry="dec.dsentry",
	flvlist="dec.flvlist"
}

function encodeArgument(argdesc,fnlookup)
	--print("encodeArgument",fnlookup,argdesc)
	if argdesc == nil then return "None" end
	if type(argdesc) == "string" then
		local fnname = fnlookup[argdesc]
		if fnname == nil then
			fnname = argdesc
		end
		return fnname
	elseif type(argdesc) == "table" then
		local fnname = fnlookup[argdesc["type"]]
		if fnname == nil then
			fnname = argdesc["function"]
		end
		if fnname == nil then
			error("No function name specified")
		end
		return fnname..argsToString(encodeArguments(argdesc,fnlookup))
	elseif type(argdesc) == "number" then
		return tostring(argdesc)
	elseif type(argdesc) == "bool" then
		if argdesc then
			return "True"
		else
			return "False"
		end
	end
end

function encodeArguments(arguments,fnlookup)
--print("encodeArguments",fnlookup)
	local args = {}
	local i=1
	while true do
		argdesc = arguments[i]
		if argdesc == nil then break end
		args[i] = encodeArgument(argdesc,fnlookup)
		i=i+1
	end
	return args
end

function argsToString(args)
	local out = ""
	local firsttime = true
	local i=1
	while true do
		if not args[i] then break end
		if not firsttime then out=out.."," end
		firsttime = false
		out=out..tostring(args[i])
		i=i+1
	end
	return "("..out..")"
end

function argsToList(args)
	local out = ""
	local firsttime = true
	local i=1
	while true do
		if not args[i] then break end
		if not firsttime then out=out.."," end
		firsttime = false
		out=out..tostring(args[i])
		i=i+1
	end
	return "["..out.."]"
end

--WARNING this function is not safe
--please don't use for full automatic generators
function dictToString(args)
	local out = ""
	local firsttime = true
	for k,v in pairs(args) do
		if not firsttime then out=out.."," end
		firsttime = false
		if type(k) == "string" then
			out=out.."'"..k.."':"..tostring(v)
		elseif type(k) == "number" then
			out=out..k..":"..tostring(v)
		end
	end
	return "{"..out.."}"
end

function getEncoder(arguments)
	local encoder = "Encoder"
	if arguments == nil then return encoder.."()" end
	local args = {}
	local i=1
	if arguments.hasMode then
		encoder = "ModeEncoder"
	end
	while true do
		local argdesc = arguments[i]
		if type(argdesc) == "string" then
			args[i] = encodeArgument(argdesc,encoders)
		elseif type(argdesc) == "table" then
			--either a mode table or a complex encoder
			if argdesc.type then
				--complex encoder
				args[i] = encodeArgument(argdesc,encoders)
			else
				--modetable
				encoder = "ModeEncoder"
				local j=0
				local entries = {}
				local listCompatible = true
				for k,v in pairs(argdesc) do
					entries[k] = encodeArgument(v,encoders)
					if not (type(k) == "number") then
						listCompatible = false
					elseif (k > #argdesc) or (k <= 0) then
						listCompatible = false
					end
					j=j+1
				end
				if j<#argdesc then --this is the case when the table contains a nil value
					listCompatible = false
				end
				if listCompatible then
					args[i] = argsToList(entries)
				else
					--lua tables start at 1 but we need to start at 0 for the dictionary
					dictentries = {}
					for k,v in pairs(entries) do
						if type(k) == "number" then
							k=k-1
						end
						dictentries[k] = v
					end
					args[i] = dictToString(dictentries)
				end
			end
		else
			break
		end
		i=i+1
	end
	return encoder.."("..argsToList(args)..")"
end

function getDecoder(arguments)
	local decoder = "Decoder"
	if arguments == nil then return decoder.."()" end
	local args = {}
	local i=1
	if arguments.hasMode then
		decoder = "ModeDecoder"
	end
	while true do
		local argdesc = arguments[i]
		if type(argdesc) == "string" then
			args[i] = encodeArgument(argdesc,decoders)
		elseif type(argdesc) == "table" then
			--either a mode table or a complex decoder
			if argdesc.type then
				--complex decoder
				args[i] = encodeArgument(argdesc,decoders)
			else
				--modetable
				decoder = "ModeDecoder"
				local j=0
				local entries = {}
				local listCompatible = true
				for k,v in pairs(argdesc) do
					entries[k] = encodeArgument(v,decoders)
					if not (type(k) == "number") then
						listCompatible = false
					elseif (k > #argdesc) or (k <= 0) then
						listCompatible = false
					end
					j=j+1
				end
				if j<#argdesc then --this is the case when the table contains a nil value
					listCompatible = false
				end
				if listCompatible then
					args[i] = argsToList(entries)
				else
					--lua tables start at 1 but we need to start at 0 for the dictionary
					dictentries = {}
					for k,v in pairs(entries) do
						if type(k) == "number" then
							k=k-1
						end
						dictentries[k] = v
					end
					args[i] = dictToString(dictentries)
				end
			end
		else
			break
		end
		i=i+1
	end
	return decoder.."("..argsToList(args)..")"
end

function getConstant(name)
	constant = ""
	prevupper = true
	prevuscore = false
	for i=1,#name do
		c = name:sub(i,i)
		if c:match("[%d%l]") then
			constant = constant..c:upper()
			prevupper = false
			prevuscore = false
		elseif c:match("%u") then
			if prevupper or prevuscore then
				constant = constant..c
				prevuscore = false
			else
				constant = constant.."_"..c
			end
			prevupper = true
		elseif (c=="-" or c=="_" or c==".") and ((not prevuscore) or (prevuscore==c and prevuscore=="_")) then
			constant = constant.."_"
			prevuscore = c
		end
	end
	return constant
end

function generateEncoder(protocol,description,transcoders)
	
	sections = {}
	
	function add(section,stuff)
		sections[section]=(sections[section] or "")..stuff.."\n"
	end

	--add imports
	add("import","from modserv.transcoders import Encoder,ModeEncoder")
	add("import","import modserv.encoders as enc")

	--define constants
	add("constants","protocol = \""..protocol.."\"")
	add("constants","side = \""..description.side.."\"")
	add("constants","version = "..description.version)

	--define CALL_NAMES
	add("constants","")
	add("constants","#call names")
	for k,v in pairs(description) do
		if type(k) == "number" then
			if not v.constant and v.name then
				v.constant = getConstant(v.name)
			end
			if v.constant then
				add("constants","CALL_"..v.constant.." = "..k)
				--encoderconstants[k] = "CALL_"..v.constant
			end
		end
	end

	--generate protocol object class
	encodertable = {}
	encoderdict = {}

	--generate encoder list
	for k,v in pairs(description) do
		if type(k) == "number" then
			constant = v.constant
			if not constant then
				constant = tostring(k)
			end
			encoder = nil
			if v.transcoder then
				encoder = v.transcoder.."_ENCODER"
				if not encodertable[encoder] then
					if transcoders[v.transcoder] then
						encodertable[encoder] = getEncoder(transcoders[v.transcoder])
					else
						error("Unknown transcoder: "..tostring(v.transcoder))
					end
				end
			end
			if v.arguments then
				encoder = getEncoder(v.arguments)
			end
			encoderdict[constant] = encoder
		end
	end

	for k,v in pairs(encodertable) do
		add("encoders",k.." = "..v)
	end
	add("encoder-table","encoders = {")
	for k,v in pairs(encoderdict) do
		add("encoder-table","	CALL_"..k..": "..v..",")
	end
	add("encoder-table","}")
	
	add("hasMode","def hasMode(functionId):")
	add("hasMode","	if functionId in encoders:")
	add("hasMode","		return type(encoders[functionId]) is ModeEncoder")
	add("hasMode","	else:")
	add("hasMode","		return False")
	
	order = {"import","constants","encoders","encoder-table","hasMode"}
	output = ""
	
	for i=1,#order do
		key = order[i]
		output = output.."\n"..sections[key]
	end
	
	return output:sub(2)
end
	
	
function generateDecoder(protocol,description,transcoders)
	
	sections = {}
	
	function add(section,stuff)
		sections[section]=(sections[section] or "")..stuff.."\n"
	end

	--add imports
	add("import","from modserv.transcoders import Decoder,ModeDecoder")
	add("import","import modserv.decoders as dec")


	--define constants
	add("constants","protocol = \""..protocol.."\"")
	add("constants","side = \""..description.side.."\"")
	add("constants","version = "..description.version)

	--define CALL_NAMES
	add("constants","")
	add("constants","#call names")
	for k,v in pairs(description) do
		if type(k) == "number" then
			if not v.constant and v.name then
				v.constant = getConstant(v.name)
			end
			if v.constant then
				add("constants","CALL_"..v.constant.." = "..k)
				--encoderconstants[k] = "CALL_"..v.constant
			end
		end
	end
	
	decodertable = {}
	decoderdict = {}

	--generate decoder list
	for k,v in pairs(description) do
		if type(k) == "number" then
			constant = v.constant
			if not constant then
				constant = tostring(k)
			end
			decoder = nil
			if v.transcoder then
				decoder = v.transcoder.."_DECODER"
				if not decodertable[decoder] then
					if transcoders[v.transcoder] then
						decodertable[decoder] = getDecoder(transcoders[v.transcoder])
					else
						error("Unknown transcoder: "..tostring(v.transcoder))
					end
				end
			end
			if v.arguments then
				decoder = getDecoder(v.arguments)
			end
			decoderdict[constant] = decoder
		end
	end

	for k,v in pairs(decodertable) do
		add("decoders",k.." = "..v)
	end
	add("decoder-table","decoders = {")
	for k,v in pairs(decoderdict) do
		add("decoder-table","	CALL_"..k..": "..v..",")
	end
	add("decoder-table","}")
	
	add("hasMode","def hasMode(functionId):")
	add("hasMode","	if functionId in decoders:")
	add("hasMode","		return type(decoders[functionId]) is ModeDecoder")
	add("hasMode","	else:")
	add("hasMode","		return False")

	order = {"import","constants","decoders","decoder-table","hasMode"}
	output = ""
	
	for i=1,#order do
		key = order[i]
		output = output.."\n"..sections[key]
	end
	
	return output:sub(2)
end

outfiles = {}

-- call all the functions
for _,description in pairs(descriptions) do
	--generate encoder
	filename = protocol.."_"..description.side.."_encoder.py"
	print("generateing encoder for side "..description.side.." > "..filename)
	outfiles[filename] = generateEncoder(protocol,description,transcoders)
	--generate decoder
	filename = protocol.."_"..description.side.."_decoder.py"
	print("generateing decoder for side "..description.side.." > "..filename)
	outfiles[filename] = generateDecoder(protocol,description,transcoders)
end

for name,data in pairs(outfiles) do
	file = io.open(name,"w")
	file:write(data)
	file:close()
end

