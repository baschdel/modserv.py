Interface request Interface

0:listInterfaceTypes(bin:cookie)
->returnAvaiableInterfaceTypes(bin:cookie,dslist<utf-8:type>)

1:request(bin:cookie,utf-8:type,dslist<dsentry<utf-8:key,utf-8:value>>:description)
->useInterface(mode=0,bin:cookie,u64:moduleId,u32:interfaceId)
->typeNotSupportet(mode=1,bin:cookie)
->invalidArgument(mode=2,bin:cookie,utf-8:key,utf-8:value)
->cannotSupplyInterface(mode=3,bin:cookie)

2>newTypes()

