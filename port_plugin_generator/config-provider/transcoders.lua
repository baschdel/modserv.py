return {
	--call
	COOKIE = {"bin"},
	COOKIE_TEXT = {"bin","utf-8"},
	COOKIE_TEXT_DATA = {"bin","utf-8","bin"},
	COOKIE_TEXT_BOOL = {"bin","utf-8","bool"},
	COOKIE_TEXT_TEXT = {"bin","utf-8","utf-8"},
	COOKIE_TEXT_TEXT_BOOL = {"bin","utf-8","utf-8","bool"},
	COOKIE_TEXT_TEXT_TEXT = {"bin","utf-8","utf-8","utf-8"},
	COOKIE_TEXT_TEXT_TEXT_BOOL_BOOL = {"bin","utf-8","utf-8","utf-8","bool","bool"},
	COOKIE_TEXT_TEXT_TEXT_TEXT_BOOL = {"bin","utf-8","utf-8","utf-8","utf-8","bool"},
	COOKIE_TEXT_TEXT_TEXT_TEXT_TEXT = {"bin","utf-8","utf-8","utf-8","utf-8","utf-8"},
	--callback
	MODE_COOKIE = {"u8","bin"},
	MODE_COOKIE_0_DATA = {"u8","bin",{"bin"}},
	MODE_COOKIE_0_TEXTLIST = {"u8","bin",{{"utf-8",type="dslist"}}},
	MODE_COOKIE_0_TEXT_BOOL_BOOL = {"u8","bin",{"utf-8"},{"bool"},{"bool"}},
	MODE_COOKIE_0_TEXT_TEXT_U32_BOOL_BOOL_BOOL = {"u8","bin",{"utf-8"},{"utf-8"},{"u32"},{"bool"},{"bool"},{"bool"}},
	MODE_COOKIE_0_TEXT_TEXT_BOOL_BOOL_U64_U64 = {"u8","bin",{"utf-8"},{"utf-8"},{"bool"},{"bool"},{"u64"},{"u64"}},
	RETURN_RESOLVED_SECTION = {"u8","bin",{"utf-8","utf-8"},{"bool","utf-8"}},
	--event
	BLANK={},
	TEXT={"utf-8"},
}
