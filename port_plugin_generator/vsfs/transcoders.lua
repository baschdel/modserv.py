return {
	COOKIE_TEXT = {"bin","utf8"},
	COOKIE_TEXT_DATA = {"bin","utf8","bin"},
	MODE_COOKIE = {"u8","bin",hasMode=true},
	MODE_COOKIE_DATA = {"u8","bin",{"bin"}},
	COOKIE_TEXTDSLIST = {"bin",{"utf8",type="dslist"}},
	MODE_COOKIE_SIZE = {"u8","bin","u32",hasMode=true}
}
