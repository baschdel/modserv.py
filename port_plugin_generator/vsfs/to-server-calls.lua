return {
	version=1,
	side="s",
	[0]={
		name = "read",
		constant = "READ",
		transcoder="COOKIE_TEXT"
	},
	[1]={
		name = "write",
		constant = "WRITE",
		transcoder="COOKIE_TEXT_DATA"
	},
	[2]={
		name = "append",
		constant = "APPEND",
		transcoder="COOKIE_TEXT_DATA"
	},
	[3]={
		name = "delete",
		constant = "DELETE",
		transcoder="COOKIE_TEXT"
	},
	[4]={
		name = "list",
		constant = "LIST",
		transcoder="COOKIE_TEXT"
	},
	[5]={
		name = "getSize",
		constant = "GET_SIZE",
		transcoder="COOKIE_TEXT"
	},
}
