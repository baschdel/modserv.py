import modserv.modclient2 as modclient
from modclient2.interface.primitive_cli import PrimitiveCliInterface
import modserv.easydriver as easydriver

client = modclient.getModclient()

cliInterface = None
isConnected = False

def mainloop():
	msg = input()
	if cliInterface is None:
		print("--NOT READY--")
		return True
	if not isConnected:
		print("--NOT CONNECTED--")
		return True
	cliInterface.broadcast(msg)
	return True

def onConnect(connectionId):
	print("--CONNECTED--")
	global isConnected
	isConnected = True
	
def onDisconnect(connectionId):
	print("--DISCONNECTED--")
	global isConnected
	isConnected = False
	
@client.start
def start():
	global cliInterface
	cliInterface = PrimitiveCliInterface(
		onMessage=onMessage,
		onConnectionClosed=onDisconnect,
		onConnectionOpened=onConnect)
	cliInterface.open(client,3,maxconnections=1)
	easydriver.EasyDriverLooper(mainloop,client,daemon=True)

def onMessage(context,message):
	print(message)
