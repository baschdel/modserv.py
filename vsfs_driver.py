import modserv.modclient2 as modclient
import os
import modserv.interfaceAutocloser as Autocloser
import modserv.interfaceOpener as Opener
from modclient2.port.vsfs_server import VsfsServer
from modclient2.interface.vsfs_server import VsfsServerInterface
from modclient2.interface.ifrequest_server import IfRequestServerInterface

directory = "./vsfs_files"

client = modclient.getModclient(debug=True)
directory = os.path.normpath(directory)

def isSubpathOf(root,path):
	path = os.path.normpath(path)
	return path.startswith(root)

def sanitizePath(name):
	out = ""
	i = 0
	map = {'\\':"\\\\",'\0':"\\0",'/':"\\-"}
	while i < len(name):
		if name[i] in map:
			out=out+map[name[i]]
		else:
			out=out+name[i]
		i=i+1
	return out
	
def reverseSanitizePath(name):
	out = ""
	i = 0
	while i<len(name):
		if name[i] == "\\":
			i = i+1
			if len(name)<=i:
				return out
			map = {'0':"\0",'-':"/",'\\':"\\"}
			if name[i] in map:
				out = out+map[name[i]]
		else:
			out = out+name[i]
		i=i+1
	return out

class NixVsfsServer(VsfsServer):
	def __init__(self,path,base=""):
		VsfsServer.__init__(self)
		self.root=path
		self.base=base

	def read(self,callcontext,cookie,name):
		name=self.root+"/"+sanitizePath(self.base+name)
		if not isSubpathOf(self.root,name):
			callcontext.callback(1) #invalid Name
		if not os.path.exists(name):
			callcontext.callback(1) #invalid Name
		data = b''
		try:
			f = open(name,"rb")
			data = f.read()
			f.close()
		except:
			callcontext.callback(1) #invalid Name
		callcontext.callback(0,data)
		
	def write(self,callcontext,cookie,name,data):
		name=self.root+"/"+sanitizePath(self.base+name)
		if not isSubpathOf(self.root,name):
			callcontext.callback(1) #invalid Name
		try:
			f = open(name,"wb")
			f.write(data)
			f.close()
		except:
			callcontext.callback(2) #read Only
		callcontext.callback(0) #success
		
	def append(self,callcontext,cookie,name,data):
		name=self.root+"/"+sanitizePath(self.base+name)
		if not isSubpathOf(self.root,name):
			callcontext.callback(1) #invalid Name
		try:
			f = open(name,"ab")
			f.write(data)
			f.close()
		except:
			callcontext.callback(2) #read Only
		callcontext.callback(0) #success
	
	def delete(self,callcontext,cookie,name):
		name=self.root+"/"+sanitizePath(self.base+name)
		if not isSubpathOf(self.root,name):
			callcontext.callback(1) #does not exist
		if not os.path.exists(name):
			callcontext.callback(1) #does not exist
		try:
			os.remove(name)
		except OSError:
			callcontext.callback(2) #read Only
		callcontext.callback(0)
		
	def list(self,callcontext,cookie,name):
		paths = os.listdir(self.root)
		ret = []
		for path in paths:
			path = reverseSanitizePath(path)
			if path.startswith(self.base+name):
				ret.append(path[len(self.base):])
		callcontext.callback(ret)
		
	def getSize(self,callcontext,cookie,name):
		name=self.root+"/"+sanitizePath(self.base+name)
		if not isSubpathOf(self.root,name):
			callcontext.callback(1) #does not exist
		try:
			callcontext.callback(0,os.path.getsize(name))
		except OSError:
			callcontext.callback(1) #does not exist
			
class RequestedVsfsInterface(VsfsServerInterface):
	def __init__(self,root,base):
		self.base = base
		vsfsPort = NixVsfsServer(root,base)
		VsfsServerInterface.__init__(self,vsfsPort)
			
requestedInterfaces = {}

def reqestInterface(base):
	if base in requestedInterfaces:
		return requestedInterfaces[base]
	else:
		interface = RequestedVsfsInterface(directory,base)
		interfaceId = Opener.open(client,interface,1000,4000)
		if interfaceId is None:
			return None
		else:
			Autocloser.CloseOnLastDisconnect(client,interfaceId)
			return interfaceId
			
def vsfsInterfaceRequestHandler(callbackproxy,description):
	base = ""
	for k,v in description:
		if (k == "base") and not (base is None):
			base = v
		else:
			callbackproxy.invalidArgument(k,v)
			return
	ifid = reqestInterface(base)
	if ifid is None:
		callbackproxy.cannotSupply()
	else:
		callbackproxy.useInterface(client.me(),ifid)
		
@client.start
def start():
	vsfsMainInterface = VsfsServerInterface(NixVsfsServer(directory)).open(client,9)
	requestedInterfaces[""] = 9
	ifrequestInterface = IfRequestServerInterface().open(client,12)
	ifrequestInterface.setInterfaceFactory("vsfs-provider",vsfsInterfaceRequestHandler)
