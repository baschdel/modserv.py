import modserv.modclient2 as modclient
from modserv.context import Context
from modserv.asyncronus import TimeoutOrEarlier
from modclient2.interface.primitive_cli import PrimitiveCliInterface
from modclient2.interface.vsfs_client import VsfsClientInterface

client = modclient.getModclient()

#human readable size
def hrsize(size):
	unit = "B"
	if size >= 1024*1024:
		size = size/(1024*1024)
		unit = "MB"
	if size >= 1024:
		size = size/1024
		unit = "KB"
	return str(size)+unit

class Invokecall:
	def __init__(self,function,*arguments):
		self.arguments = arguments
		self.function = function
	
	def __call__(self,*x):
		self.function(*self.arguments)

class CommandContext:
	def __init__(self,command,name):
		self.command = command
		self.name = name
		self.buffer = ""

class FileSender:
	def __init__(self,function):
		self.function = function
		
	def __call__(self,callcontext,data):
		try:
			self.function(data.decode('utf-8'))
		except:
			self.function("Sorry, this is not a utf-8 textfile ...\nAnd i have no hexviewer")
			pass

class SListcall:
	def __init__(self,send,vsfs):
		self.send = send
		self.vsfs = vsfs	
		
	def __call__(self,callcontext,plist):
		if len(plist) == 0:
			self.send("")
			return
		self.sizes = {} #maps path to size
		self.tlen = len(plist) #target length
		self.timeout = TimeoutOrEarlier(self.done,3)
		self.plist = plist
		#collect sizes
		for path in plist:
			self.vsfs.getSize(path,
				onSuccess=Context(self.gotSize,path=path),
				onDoesNotExist=Context(self.failed,path=path),
				onNotConnected=Context(self.failed,path=path))
		self.timeout.start()
	
	def done(self):
		#render and send output
		maxsw = 5
		for path in self.plist:
			if not path in self.sizes:
				self.sizes[path] = "TIMEOUT"
			sz = self.sizes[path]
			if len(sz)>maxsw:
				maxsw = len(sz)
		out = ""
		for path in self.plist:
			sz = self.sizes[path]
			out = out+"\n"+(" "*(maxsw-len(sz)))+sz+"|"+path
		self.send(out[1:])
		
	def failed(self,callcontext,path=None):
		self.sizes[path] = "N/A"
		if len(self.sizes) == self.tlen:
			self.timeout.early()
		
	def gotSize(self,callcontext,size,path=None):
		self.sizes[path] = hrsize(size)
		if len(self.sizes) == self.tlen:
			self.timeout.early()
	
class Sizecall:
	def __init__(self,send,filename):
		self.send = send
		self.filename = filename
		
	def __call__(self,callcontext,size):
		self.send(self.filename+" : "+hrsize(size))	
		
#maps a connectionId to a CommandContext if it is currently waiting for context commands
connectionCommands = {}

@client.start
def start():
	
	def onMessage(context,message):
		connectionId = context.connectionId
		if connectionId in connectionCommands:
			cc = connectionCommands[connectionId]
			#currently reading some data for a command
			if message == "<EOF>":
				#end of file > execute command
				if cc.command == "write": #write
			 		vsfsInterface.write(cc.name,cc.buffer.encode('utf-8'),
				  	onSuccess =        Invokecall(context.call,"Written to file "+cc.name+" !"),
				  	onInvalidName =    Invokecall(context.call,cc.name+"is an invalid name!"),
				  	onReadOnly =       Invokecall(context.call,"File is read only!"),
				  	onNotEnoughSpace = Invokecall(context.call,"Not enough space!"),
				  	onNotConnected =   Invokecall(context.call,"Not Connected!"))
				if cc.command == "append": #append
					vsfsInterface.append(cc.name,cc.buffer.encode('utf-8'),
						onSuccess =        Invokecall(context.call,"Appended to file "+cc.name+" !"),
						onInvalidName =    Invokecall(context.call,cc.name+"is an invalid name!"),
						onReadOnly =       Invokecall(context.call,"File is read only!"),
						onNotEnoughSpace = Invokecall(context.call,"Not enough space!"),
						onNotConnected =   Invokecall(context.call,"Not Connected!"))
				#delete callcontext, beacause it is no longer needed
				del connectionCommands[connectionId]
			else:
				#add data to buffer
				cc.buffer = cc.buffer+message+"\n"
		else:
			#just a normal command
			#decode command
			i = message.split(" ",2)
			command = "help"
			name = ""
			if len(i)	== 2:
				command = i[0]
				name = i[1]
				if not command in ["read","write","append","list","size","delete"]:
					command = "help"
			elif len(i) == 1:
				command = i[0]
				if not command == "list":
					command == "help"
			#execute command		
			if command == "write" or command == "append":
				connectionCommands[connectionId] = CommandContext(command,name)
				context.call("Please send the new content and write <EOF> into the last line.")
			if command == "read":
				vsfsInterface.read(name,
					onSuccess      = FileSender(context.call),
					onDoesNotExist = Invokecall(context.call,"File does not Exist!"),
					onNotConnected = Invokecall(context.call,"Not Connected!"))
			if command == "list":
				vsfsInterface.list(name,
						onSuccess      = SListcall(context.call,vsfsInterface),
						onNotConnected = Invokecall(context.call,"Not Connected!"))
			if command == "size":
				vsfsInterface.getSize(name,
					onSuccess =      Sizecall(context.call,name),
					onDoesNotExist = Invokecall(context.call,"File does not Exist!"),
					onNotConnected = Invokecall(context.call,"Not Connected!"))
			if command == "delete":
				vsfsInterface.delete(name,
					onSuccess =      Invokecall(context.call,"Deleted file "+name+" !"),
					onDoesNotExist = Invokecall(context.call,"File does not Exist!"),
				 	onReadOnly =     Invokecall(context.call,"File is read only!"),
			  	onNotConnected = Invokecall(context.call,"Not Connected!"))
			if command=="help":
				context.call("{read|write|append|delete|list|size} <name>")
			
	def onCliDisconect(connectionId):
		if connectionId in connectionCommands:
			del connectionCommands[connectionId]
	
	cliInterface = PrimitiveCliInterface(onMessage=onMessage,onConnectionClosed=onCliDisconect).open(client,3)
	vsfsInterface = VsfsClientInterface().open(client,9)
